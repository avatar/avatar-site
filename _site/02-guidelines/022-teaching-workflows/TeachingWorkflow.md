# Teaching workflows

# Navigation Structure
{: .no_toc }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>

```mermaid
graph LR

subgraph two ["Discovering XR"]
A0["Discovering XR"]
A0 --> A1["A1 <BR> Basic concepts"] 
A1 --> A11["A11 <BR> Milgram's continuum"]
A1 --> A12["A12 <BR> VR vs MR vs AR"]
A1 --> A13["A13 <BR> Sensors"]
A1 --> A14["A14 <BR> Human senses"]

A0 --> A2[A2 <BR> Experience devices]

A2 --> A21["A21 <BR> Rendering devices"] 
A21 --> A211["A211 <BR> HMD"] 
A21 --> A212["A212 <BR> Stereoscopic display"]
A21 --> A212["A212 <BR> CAVE"] 
A21 --> A213["A213 <BR> Holography"] 
A21 --> A214["A214 <BR> See Through glasses"] 
A21 --> A215["A215 <BR> Video Mapping"] 
A21 --> A216["A216 <BR> Powerwall"] 
A21 --> A216["A217 <BR> HUD : Head Up Display"] 
A2 --> A22["A22 <BR> Control devices"]
A22 --> A220["A220 <BR> Mouse"]
A22 --> A221["A221 <BR> Wand"]
A22 --> A222["A222 <BR> Motion Capture"]
A22 --> A223["A223 <BR> Tactile surfaces"]
A22 --> A224["A224 <BR> Haptics"]
A22 --> A226["A226 <BR> ..."]
A2 --> A23["A22 <BR> Audio rendering and capture"]
A2 --> A24["A24 <BR> Smell and taste devices"]
A2 --> A25["A25 <BR> Integrated devices"]
A0 --> A3["A3 <BR> Testing XR applications"]
end
```

```mermaid
graph LR

subgraph one [Create VRAR environment]
0A[XR Basics] --> 0B(Modelling Virtual Scene)
0B & 0D[Trcking] & 0E[User tracking]--> 0C(Scene behavior)
0B & 0C --> 0F[VR device] & 0G[AR device]
0D --> 0F
end
```

```mermaid
graph LR

subgraph two [Digital Twin]
1A[Mechanism] 
1B[Machine Sensors] 
1C[Machine Control]
1A --> 1D[Machine Simulation]
1B --> 1E[Digital Shadow]
1D & 1E --> 1F[Digital Twin] --> 1C
end
```

```mermaid
graph LR
subgraph three [Training through XR]
2A[Create Machine DT]
2A --> 2B[Include Activity Sequence]
2B --> 2C[Produce VR Environment] & 2D[Produce AR Environment]
2E[Calibrate] --> 2D
end
```
