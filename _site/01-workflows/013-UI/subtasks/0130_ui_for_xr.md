<!-- ---
layout: default
title: UI for xr
grand_parent: Workflows
parent: UI Development
nav_order: 0
--- -->
# UI for XR

Description of the activities to create user interfaces (UI) for extended reality (XR) environments. Beginning with the creation of a storyboard to visualize the UI, the process continues with the establishment of a use case model that provides the foundation for system-user interaction. Following this is a detailed description phase of the metaphor that will be used to define how the different states of the system are represented. The next phase is the implementation phase, which involves the virtual creation of the interface and the assignment of the interactions. The final stage involves an evaluation of the interface through user experience

---

## A1 Story Board Creation

The storyboard is a sequence of image illustrating the vision of the user interface. It illustrates specific major context and shows how the user interface should look like but indeed it prepare the specification of visual metaphors.

These sketches can be used collaboratively to converge on a common vision. They make sense also to share ideas with either end user or the potential client.

|![00](assets/storyboard1.png){:height="270px" width="414px"}|![01](assets/storyboard2.png){:height="270px" width="377px"}|![02](assets/storyboard3.png){:height="270px" width="463px"}|

Sketches to illustrate the Story Board


- Input

The user interface goal may be described in an informal,nonn-structured, document to hightlight the main expected functions and concepts. It may be also the direct transcription of what the operator has in mind.

- Output

The main ouput is a set of illustrations where we can either describe the overall environment (sketch 1 here), then the specific content of the display in various contexts (sketch 2 and 3 here). Some text may be associated to explain the ideas but we already specify the type of metaphors that will be used since it draws how object should be represented, how we interact with objects and potentially their specific behavior.

- Control

To make a correct storyboard a good understanding of final device and display capacities are expected. On a tactile display selection will be done by a finger touch directly on the metaphor projection. In a gave gaze direction picking or laser ray cast may be used for the same type of operation.

---

## A2 Use Case Model

In VR and AR application the lowest level actions are selection, navigation and manipulation :

- Selection allows to specify which objects in the scene is currently undertaken. It may lead to change the behavior of an action or propose some context actions often presented as context menus.

- Navigation allows to change the point of view on the model. In 3D navigation is understood as a physical motion of the observation camera but it can be also some buttons or actions to switch from one panel to another one in 2D contexts.

- Manipulation allows to change the position and orientation of objects in the scene.

The metaphors for selection, navigation or manipulation depends on the technologies capacities and choices made by the developer team. For example navigation may be operated as a flyer mode or through teleportation. Some ideas about the expected metaphor are suggested by the storyboard but the use case model does not describe this level of detail.

The use case mode explains how the main end-user functions request sub-functions. The functions are recursively described until the low level that should be selection, navigation and manipulation.

In the above example the use case explains a single use case ("Associate virtual and physical models). To enable this function, he must open the camera, load a model, and create pairs of points. All these operations does not need nor navigation or manipulation but selection. But we highligth 4 types of selections which will be developed separately. If the user interface is limited to this single use case then there is no much added value. Whenever the basic use cases are shared by several upper use-cases then this work highlights the basic functions to ne developed once and reused in various contexts.

<img src="assets/2023-01-08-09-59-15-Capture%20d’écran%20du%202023-01-08%2009-58-41.png" title="" alt="" width="910">

- Input

The scene actors are the main input to draft the use case. They are the external entry point of the use case and the message between single use case (function and actors) describes the information flows

- Output

The output is mainly represented is a use case graph (In the picture, here a UML use case diagram is used). But indeed the real output is a hierarchy of functions from high level ones to very basic ones. By converging on selection, navigation and manipulation functions we ensure that a VR/AR environment can be developed based on this specification.

- Control

It is never obvious to fix the level of detail and to stop expanding such a model with new functions. The initial specification (the storyboard indeed) is a reference to decide when we have enough details and when the use case is complete enough.

- Resource

There are plenty solutions to describe the model but we promote to use the well known and practiced UML use case diagram. A use cas diagram mainly contains actors :

| **Symbol**                                                                      | **Function**                                                                                       |
| <img src="assets/2023-01-08-10-14-31-image.png" title="" alt="" width="50"> | Representation of humans or system acting on the user interface                                |
| <img src="assets/2023-01-08-10-17-14-image.png" title="" alt="" width="82"> | The use case diagram is made of many use cases. A use case is a function of the user interface |
| <img src="assets/2023-01-08-10-18-46-image.png" title="" alt="" width="89"> | The extend link which defines that a use case generalizes another use case.                    |
| <img src="assets/2023-01-08-10-20-14-image.png" title="" alt="" width="79"> | The include link which identifies that a use case need sub use cases to be performed.          |

Many tools are dedicated to create UML Graph. Here the software yEld was used.

---

## A3 Metaphors Description

The storyboard defined a first specification of rendered artifact/actor and its behavior. Here every artifact may be fully specified.

- its shape and import format ; it may be a basic shape (cylinder, sphere, cube, etc) or a more complex polyhedron which will be imported from various type of files (wavefront (obj), gltf, etc)

- its size 

- its rendering characteristics, material, colors, effects (diffuse, specular, etc)

- its behaviors : is it a fixed shape or is it articulated, or can it be deformed. What are the triggers handling its external of internal motion or deformation ?

- Input

The definition of actors from both the use case and storyboard are used to make this definition.

- Output

A complete description of every expected metaphor

- Control

Obviously the selection of metaphors depends on the capacity of the rendering environment to render them and to interact with it.

- Resources
The list of available metaphors but also of the import libraries is the main resource to decide what may be specified.

---

## A4 Implementation

We enter if the encoding phase where the basic function should be developped

- Import model

- Render metaphors

- Select a metaphor or its inner characteristic

- Manipulate a metaphor or its inner characteristic : manipulation will move artifacts or sub-components but may also expect more or less complex simulators to define the behavior of the artifacts.

- Navigate in the scene

Then every upper use case may be developed by integration of lower level use case

- Input


The definition of metaphors but also the use case definition.

- Output

A user interface, once compiled which support the initial specification

- Control

The selection, Manipulation and navigation functions must be mapped to the final device choice. Depending on the type of device controllers this mapping identifies which buttons or event triggers a selection a manipulation or a navigation effect.

- Resources


In most Virtual or augmented reality development environment, this task is based on script developments (in C#, python, C++, whatever.)

---

## A5 assessment

A user interface is a system and any designed system a development process needs to converge from specification to a product then to progressively check its behavior. We can reference a V-Cycle or any model and we will face :

- unit tests : at a very low level checking that every basic function is working. It is especially necessary here to check
  
  - the importers functions
  
  - the metaphor rendering
  
  - the metaphor behaviors
  
  - the low level selection functions
  
  - the low level navigation functions
  
  - the low level manipulation functions

- once unit tests are validated some technical integration tests will support to check that every upper functions are properly accessible. The use case diagram here allow to plan integration test starting from low level function and following backwards the extend and include links. When integration tests are validated we may say that the product is functional but it is not yet acceptable.

- the end user perception tests are a complementary assessment to ensure that the system is :
  
  - efficient : based on basic scenarios we can measure the performance and quality of task performed :
    
    - performance may be measured as a task duration or a quantity of sub-tasks performed in a given delay
    
    - the quality will measure the quality of the task result which depends on the task itself: Artifacts properly positioned, completeness of the activity, etc
  
  - the use perception may be assessed through 
    
    - bio-sensors : to measure strengths, brain load, stress etc
    
    - when bio-sensors are not available or when no competencies about these sensors, some questionnaires provides good views
      
      - SUS system

-  Input


The developped user interface

- Output


A validation of the user interface or a set of expected corrections

- Control


The use case is a good tool to plan the unit and integratiion test

- Resources

Sensors and questionnaires. Hereafter a set of available and referenced questionnaires

1. **System Usability Scale (SUS)**  
   ![SUS Image](assets/2023-01-08-12-44-10-image.png)

2. **Usefulness questionnaire (USE)**  
   ![USE Image](assets/2023-01-08-12-45-07-image.png)

3. **Attractivity (AttrackDif2)**  
   ![AttrackDif2 Image](assets/AttrackDIf2.png)

4. **Task load index (Nasa TLX)**  
   ![Nasa TLX Image](assets/NASATLX.png)

