<!-- ---
layout: default
title: Basic Interactions AR
grand_parent: Workflows
parent: Interaction
nav_order: 1
--- -->
# Basic Interactions AR

Augmented reality (AR) is a technology that allows users to see and interact with digital content in the real world. This content can be overlaid on top of the user's view of the physical environment, creating an immersive, hybrid experience that blends the virtual and the real.

Using Unity, developers can create AR experiences that are triggered by various inputs such as voice commands, gestures, or the user's gaze. They can also add features such as physics, lighting, and audio to enhance the realism and immersion of the experience.

With HoloLens2 and Unity, developers can create a wide range of AR applications for education, training, entertainment, and productivity. These applications allow users to interact with digital content in a natural and intuitive way, using their own body and surroundings as a canvas

This page describes how to create AR using Unity 3D and Mixed Reality Toolkit (MTRK) for the HoloLens 2.

---
## A1 Interactions Metaphors Definition

1. Navigation: How the user will move through the virtual content? <br>
Using the HoloLens 2, normally, the user needs to move physically in the real space to reach the virtual content, but this can also be done the other way around, so the virtual content always moves to the user's location.
2. Selection: How will the user select in augmente reality?
- Fingertip <br> On HoloLens 2, the user's hands are recognized and interpreted creating a skeletal model of both hands right/left. By default there is a collider at the tip of the index fingers. With it, near interactions can be performed to directly manipulate holomagrams that are close to the user, such as a butom that can be activated by pressing it or an object that can be picked up by grabbing it, and the 2D content behaves like a virtual touchscreen. 
- Hand Rays <br>
Hand ray is a "laser" that shoots out from the center of the user's palm. This ray is treated as an extension of the hand. A donut-shaped cursor is attached to the end of the ray to indicate the location where the ray intersects with a target object. The object that the cursor lands on can then receive gestural commands from the hand. The command is triggered by using the thumb and index finger to do the air-tap action. <br> By using the hand ray to point and air tap to commit, users can target, select, and manipulate out of reach 2D and 3D content.
- Voice commands <br>  Enable to command and control an action that gives direct orders to a hologram without having to use hand gestures and can be a natural way to communicate an intention. Due to the complexity of human language, it is recommended: <br>
Use concise commands - choose key words of two or more syllables. Example: "Play video" is better than "Play the currently selected video". <br>
Use simple vocabulary - Example: "Show note" is better than "Show placard" <br> Avoid similar sounding commands - Avoid registering multiple speech commands that sound similar. Example: "Show more" and "Show store" can be similar sounding.
- Eye and Gaze <br> Mixed reality headsets can use the position and orientation of the user's head to determine their head direction vector. The gaze is a laser pointing straight ahead from directly between the user's eyes. This is a fairly coarse approximation of where the user is looking.<br> Whereas, eye tracking, track where the user is looking in real time, allowing him to interact with the holograms across their view.

3. Manipulation: How to manipulate objects in augmnted reality? <br>
Direct hand manipulation is the most natural and intuitive way to manipulate objects in augmented reality, allows the user to move, rotate and scale an object with their own hands. <br>
Direct manipulation is affordance-based, meaning it's user-friendly. There are no symbolic gestures to teach users. It can be extended by using the Ray to interact with objects that are out of reach.

---

## A2 Interactions Modes Selection

Based on the interaction metaphors described above, select the inputs/outputs that will allow to achieve the interaction.
1. Inputs:
- Hand Tracking: <br> It is a popular input method on HoloLens 2, because it is intuitive and easy to use. Using hand gestures and touch to interact with digital content is a natural and familiar way for most people, as it is similar to how we interact with physical objects in the real world. <br> In addition, hand tracking allows users to perform a wide range of actions and gestures, including pinching, swiping, and tapping, which can be used to control various features and functions of AR applications.

2. Outputs: <br>

c


---

## A3 XR Interaction Device

Although the HoloLens 2 has been chosen at the beginning, here are some considerations that should be taken into account when developing an augmented reality application for this device: 

- The field of view (FOV): 

The FOV of a device refers to the extent of the observable world that is visible to the user at any given time. The FOV of HoloLens 2 is 52 degrees diagonally, which is relatively narrow compared to other AR and virtual reality (VR) headsets. This means that the user can only see a small portion of the real world and digital content at any given time, and must move their head to see different parts of the view.

- Holograms Accuracy:

HoloLens 2 uses a combination of cameras, sensors, and algorithms to scan and map the user's surroundings in real-time, allowing digital content to be anchored to specific locations in the physical environment. It can also track the user's movements, allowing the digital content to remain in place as the user moves around.

The accuracy of hologram positioning on HoloLens 2 is generally high, allowing digital content to be placed with a high degree of precision within the user's view. However, the accuracy of hologram positioning can be affected by various factors, such as the lighting conditions, the surface reflectivity of the environment, and the distance of the digital content from the user.

- Learning curve: 

In general, HoloLens 2 is designed to be intuitive and easy to use, with a range of input methods such as voice commands, hand gestures, and gaze tracking that allow users to interact with digital content in a natural and familiar way.

However, as with any new technology, there may be a learning curve involved in getting used to the capabilities and limitations of the device, as well as the specific applications and experiences that are available.

Some users may find it easy to pick up and use HoloLens 2 with minimal instruction, while others may require more time and practice to become proficient with the device.

---

## A4 Interactivity-Based Assets Classification

Knowing that in augmented reality the user will not only see the virtual content, but also the real world, it is very important not to pollute or saturate the user's view. Too much content can be overwhelming and distract from the overall experience, while too little content can make the experience feel empty or incomplete. 

There are several ways to manage the amount of content in an AR application, including:

- Limiting the number of elements: By limiting the number of elements (such as 3D models, images, and text) that are displayed at any given time, developers can avoid overwhelming the user with too much information.

- Using transitions and animations: Smooth transitions and animations can help to pace the flow of content and prevent the user from being overwhelmed.

- Providing clear navigation and organization: Using clear navigation and organization can help the user to understand the content and move through it at their own pace.

- Using 3D models with optimized geometry: When creating 3D models for AR applications, it is important to keep the geometry simple and efficient. Complex models with a high number of vertices and triangles can impact performance and reduce the frame rate of the application.

In short, keep everything simple and functional. 

---

## A5 Building Behaviors

Let's start by creating a first interaction in augmented reality. <br>

It requires a 3D representation of the object to be manipulated, to make everything simple let's use the cube that comes by default in Unity.

Before setting up the interactions, add a collider on the parts of the object that will be manipulated. Once this is done, click on Add Component and add the ```Object Manipulatedt``` script.

The ObjectManipulator script makes an object movable, scalable, and rotatable using one or two hands. The object manipulator can be configured to control how the object will respond to various inputs.

To make the object respond to near articulated hand input, add the ```NearInteractionGrabbable``` script as well.

By just doing this, the object is now manipulable in Augmented Reality. And by adding the ```Rigidbody``` component, it can have physical properties, such as gravity. 

![Assets](assets/HoloLens2 Events.PNG){:height="520px" width="520px"}


- Manipulation events

The term "manipulation events" refers to events that are triggered when a user manipulates an object. 

Manipulation handler provides the following events:

OnManipulationStarted: Fired when manipulation starts. <br>
OnManipulationEnded: Fires when the manipulation ends. <br>
OnHoverStarted: Fires when a hand / controller hovers the manipulatable, near or far. <br>
OnHoverEnded: Fires when a hand / controller un-hovers the manipulatable, near or far. <br>


The event fire order for manipulation is:

OnHoverStarted → OnManipulationStarted → OnManipulationEnded → OnHoverEnded

If there is no manipulation, it will still get hover events with the following fire order:

OnHoverStarted → OnHoverEnded



---

<table style="width:100%;">
  <tr>
    <td style="text-align:right;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0120-vr-Interactions.html" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Basic Interactions VR</div>
      </a>
    </td>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/013-ui-creation.html" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">UI Development Workflow</div>
      </a>
    </td>
  </tr>
</table>



