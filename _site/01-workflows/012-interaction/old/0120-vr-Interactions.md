<!-- ---
layout: default
title: Basic Interactions VR
grand_parent: Workflows
parent: Interaction
nav_order: 0
--- -->
# Basic Interactions VR
This page describes how to create interactions in virtual reality starting from a virtual environment using Unity 3D and the XR Interaction Toolkit.

![Assets](assets/img00.JPG) <br>

---
## A1 Interactions Metaphors Definition

To define the interaction metaphors, we must first define the activities the user will perform within the virtual environment. In this case, the user will be able to perform two main activities.
- Explore the virtual environment
- Interaction with objects

*To better understand the interactions that can exist in an activity, each activity must be decomposed into its most elementary action.*

Now take the three main interactions (navigation/selection/manipulation) and define the metaphors with respect to the activities that have been defined.

1. Navigation: How will the user move in the virtual environment?
- Teleportation: it is a technique in which the user selects a point within a predefined area and is immediately transported there. 
- Physical movement: The user will move physically in a real area and this movement will be reflected in the virtual environment.
2. Selection, How will the user select in the XR environment?
- Raycasting: it is an interaction technique that uses a "laser beam" to extend the range of user interaction. The beam is sent from a specific location and in a specific direction (usually the user's hand), and if it hits something, it gives information about what and where it hit.
- Direct hand interaction: just like in real life the user can use his hands to reach objects.

3. Manipulation: How to manipulate objects in the virtual environment? <br>
As before, Raycasting and hands will be used for manipulation. 

The interaction systems mentioned above are just some of the many that exist, do not forget to select the one that best fits your case study.

---

## A2 Interactions Modes Selection

Based on the interaction metaphors described above, select the inputs/outputs that will allow to achieve the interaction.
1. Inputs:
- Head tracking: To track the position and rotation of the user.
- Controllers: To represent the position and rotation of the hands. Controls are also used to obtain other types of inputs, such as buttons, triggers and tactile.
2. Outputs:
- Visual rendering, show the user the visual composition of the virtual environment (colors, textures...), as well as a virtual representation of his hands.
- Haptic, knowing that the user will interact with his hands, the ideal is to have some feedback each time he picks up an object.

---

## A3 XR Interaction Device

Considering the two previous activities, create a list of devices that satiate both metaphors and modes of interaction.

- CAVE
- HMD - HTC Vive
- HMD - Varjo
- MHD - Valve Index

To choose the device consider the expected perceptions and the two previous stagesa and other criteria such as price, ergonomics, batery life... etc, are also important. 
The HMD Valve Index was selected because of its display fidelity and because the controls have individual finger tracking and a much more natural and immersive feel than other VR controls.

---

## A4 Interactivity-Based Assets Classification

Based on the activities that the user will perform in the virtual environment, classify the assets between interactive and non-interactive.

- Explore the virtual environment:
The user will be able to teleport and move only a zone that has been previously defined. ![teleport](assets/img0.JPG)

- Interaction with objects:
The objects with which the user will be able to interact will be only those that are on the table, such as: safety hat, safety goggles and jigsaw.![objects](assets/img1.JPG)

---

## A5 Building Behaviors

At this point we already have all the components and features of the virtual environment. Now we must build the behaviors associated to each interaction. To achieve this, it is possible to use plugins, APIs or create its own system. 
In this example we will use Unity and XR Interaction Toolkit.

1. Create a player: The player is the virtual representation of the user named "XRRig". To create it you can follow the following hierarchy. <br> Where the camera represents the position and orientation of the user's head and the hands represent the position and rotation of the user's controls.
     - XRRig
         - Camera
         - LeftHand
         - RightHand <br>


2. Teleportation Area: ![Teleport](assets/img2.JPG)
- Define the user input to perform the teleportation, in this case, thumbstick up to activate teleportation and release thumbstick to execute teleportation.
- Create a teleportation area where the user can move freely. You can also create specific teleportation points. This will be the area or points that the user can reach in the virtual environment, note also the zones where the user cannot reach.
- Deliver some output to the user, how the user will know that the teleportation has been activated, and how he will know where he will be teleported to. In this case it is a curve and a reticle.
3. Object selection: 
- Add collisions and rigidbodies that will allow raycasting and the hand to detect the presence of an object. For collisions it is suggested to use a primitive shape (cube, sphere, cylinder) that will represent the object.  is possible to create behavior when the user is selecting an object, using different states: <br> - On Hover Entered / On Hover Exited <br> - Select Entered / Select Exited <br> ![Assets](assets/img3.JPG) <br>
- The above states can be used to give information to the user when an object has been hit and can be selected. In this case, the raycasting will change color when it hits the object and the object will increase in size. <br> ![Assets](assets/img4.png) <br>
4. Object manipulation:
- Create a socket or define a place where the selected object will be manipulated. In this case the selected object will go directly to the user's hand. It is also possible to create a gripping pose to give the user a more realistic feel. It is possible to create behavior when the user is manipulating an object, using different states: <br> - On Manipulation Started / On Manipulation ended <br> ![Assets](assets/img5.JPG) <br>

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/012-Interaction.html" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Interaction Workflow</div>
      </a>
    </td>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0121-ar-Interactions.html" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Basic Interactions AR</div>
      </a>
    </td>
  </tr>
</table>
         



