<!-- ---
layout: default
title: Robot Digital Shadow
grand_parent: Workflows
parent: Simulation
nav_order: 0
--- -->

```
Book note: 
- Check text writing
```

#### Navigation Structure
{: .no_toc : .fs-5}

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>

# Robot Digital Shadow

The development and simulation of robotic behavior can be performed using third-party software, focusing on a specific task, such as "Pick and Place". The first step involves defining the capabilities of the robot and the task to be simulated. The development of robotic behaviors is performed through simulation software, such as ROS or Robo DK.

Then, the robot model is recreated in the simulation software, taking into account its physical, dynamic, electrical and thermal properties. Once the simulation is obtained, it is integrated into a 3D rendering software for Extended Reality (XR), such as Unity 3D or Unreal Engine.

---
## A1 -  System Behavior Definition

The first step is to know the specific robot's capabilities, such as its range of motion, its degrees of freedom, its load capacity, the number of axes of its manipulator and the capabilities of its end effector.

Next, define the task to be simulated, in this case Pick and Place. This would include details such as the location of the object to be picked, the target location where the object is to be placed and any specific orientation or alignment in which the object is to be during pick or place, as well as objects to be avoided during the trajectory.

And finally for a pick and place operation, key variables might include the position and orientation of the object and gripper, the grip force, the velocity and acceleration of the motion and for the robot it is important to consider the position of the joints, their speed, their acceleration as well as the torque.

---
## A2 - Behavior Development

The development of robotic behaviors is implemented using third-party software called Robot Operation System (ROS), a well-known framework for creating robotic software that uses a collection of tools, libraries, and conventions intended to simplify the process of creating complex and robust robotic behaviors on a wide variety of robots.

To simulate any robot in ROS it is necessary to have the Unified Robot Description Format (URDF) file of the robot. The URDF files consists of an XML format for representing and describing a robot model while taking into account the mating information between joints and physical properties (inertia, mass, etc).

Depending on the robot there may be two scenarios, one in which the manufacturer delivers those files, and the other in which they do not deliver them, so they must be built on their own: 

### Building a URDF file from Solidworks

The example of a Universal Robot UR5e is reported. A digital model, in neutral, .stp format, is available for download in the documentation area of the [Universal Robots website](https://www.universal-robots.com/download/?query=). 

Once ready, the model can be imported in Solidworks, where it should appear as completely fixed without possibility of relative movement between its subcomponents. A series of operations is then needed to realistically represent the relative movement between the links of each arm according to the degrees of freedom of the actual cobot of reference: 

* Select the first component on the feature manager (the father of all the other components), then right-click and select the “Dissolve Feature” command.
* Save the model as an assembly (.SLDASM format). This allows to make changes and manipulate the geomentric properties of the model.
* On the feature manager, it is convenient to re-arrange the hierarchy and to re-name the components according to the robotics standards, as shown in the related image. In order to establish parent/child relationships between subparts, the “Form New Subassembly” function must be used.

![Geometric_Model](Images/Geometric_Model.png)

* At this point, all the links must be set as floating ("Float" setting), except for the base one (UR5_LINK_00), that should be kept fixed ("Fix" setting) to the ground.
* It is then necessary to define the mates (tangent surfaces/coaxial surfaces) to allow the relative movements between each link.
* Creating an exploded view and, possibly, an animation is recommended to highlight the different parts that compose the cobot and how they're joint.
* Finally, an adequate end-effector has to be chosen in relation to the task that the cobot will virtually perform. A gripper model, called 2FG7 by OnRobot is presented hereby.

![Gripper_Model](Images/Gripper_Model.png){:height="300px" width="300px"}

* After downloading the .stp file, this was opened on SolidWorks, repeating the steps performed for the general cobot model to enable the relative movement of the gripping components (i.e., horizontal translation). It was then integrated to the cobot assembly.

![Gripper_Model_on_Cobot](Images/Gripper_Model_on_Cobot.png)

A direct kinematic model is the starting point to correctly set up the behaviour of each component of the cobot. It consists in determining the position and orientation - namely, the posture - of the end effector of the cobot in terms of joint variables. 
In the following procedure, a methodology to connect the Kinematic model to Solidworks is documented in order to test if the various parameters are correctly set up.

The documentation provided on the UR website about the Denavit-Hartenberg convention can be used in order to study the direct kinematic model of the cobot, as shown at [this link](https://www.universal-robots.com/articles/ur/application-installation/dh-parameters-for-calculations-of-kinematics-and-dynamics/). The Denavit-Hartenberg model is one of the best known and widely adopted models for the description of the kinematics of automated manipulators in a standardized way. It requires the attachment of a coordinate reference frame to each joint to define the four DH parameters -link length, link twist, link offset and joint angle-, as shown in the following figure with reference to a generic UR cobot.

![Denavit_Hartenberg_1](Images/Denavit_Hartenberg_1.png)

With reference to the image above, two of the four DH parameters are visible:
* parameter ai, which is the distance between zi axis and zi-1 axis, thus defining the link length;
* parameter di, which is the link offset of the xi axis from the xi-1 axis, measured along the zi-1 axis.
The other two parameters are angular:
* parameter ϑ𝑖, which is the angle about the zi-1 axis between xi axis and xi-1 axis;
* parameter α𝑖, that is the angle about common normal between zi-1 axis and zi axis.
While the other parameters are constant, parameter ϑ𝑖 represents the real variable of the kinematic study.

By following the scheme of the DH parameters provided in the UR documentation, the local reference systems of each joint have been set in SolidWorks as shown in the figure below.

![Denavit_Hartenberg_2](Images/Denavit_Hartenberg_2.png)

A recommended step to verify the correct application of mates between links is the simulation of the direct kinematic model by means of an Excel tool provided by Universal Robots.

According to the DH model, the generic transformation matrix between two consecutive links (𝑖−1,𝑖) can be defined as follows:

![Transformation_Matrix](Images/Transformation_Matrix.png)

The multiplication between all the matrices, defined for each couple of consecutive links, allows to compute the coordinates of each joint, thus defining the final configuration of the cobot in the space.

### Exporting a URDF file from Solidworks

The URDF exporter allows to export, in a few steps, Solidworks Parts and Assemblies into a URDF package. For doing so, after having installed the [plug-in](http://wiki.ROS.org/sw_urdf_exporter), a new tree hierarchy must be created, where the base link is set as the parent of link1, link1 as the parent of link2 and so forth.

For each link, it is possible to specify the type of joint, that for the cobot are all revolute type, and the related reference systems. Instead, for the body of the gripper a fixed joint is applied, whereas the two fingers are specified as prismatic joints.

![URDF_Setup](Images/URDF_Setup.png)

Once the setup procedure for the generation of the URDF package is complete, it is possible to move on by exporting the files to a new folder. By using the plug-in mentioned in these guidelines, the process in broadly automatic, and a new directory is created to a specified position in order to include all the necessary data. In this way the exporter creates a ROS-like package that contains a directory for meshes, textures and additional information including the URDF file itself.

![URDF_Export](Images/URDF_Export.png)

### Importing the package in ROS

A powerful ROS tool is Moveit, which enables motion planning, manipulation, 3D perception, kinematics, control and navigation of the robot. 

Start by creating the ROS workspace, once created, import the URDF file into the `src` folder. Use ROS launch to launch `moveit_setup_assistant`. It is a graphical user interface for configuring any robot for use with MoveIt.<br>
When the assistant finishes the setup, the robot can be visualized via RViz on a virtual environment (scene) and is now ready to create start and goal states for the robot interactively, test various motion planners, and visualize the output.

![MoveIt](Images/Moveit.png)

### Creating a pick and place task

To perform a Pick and Place scenario, it is necessary to create and run a script, ROS supports scripting in both Python and C++. 
Here are the main functions to be implemented, one examples is available [here](https://github.com/ros-planning/moveit_tutorials/blob/master/doc/pick_place/src/pick_place_tutorial.cpp).

1. AddCollisionObjects<br>
Add and place the objects which are part of the scene as well as the target, the object to be picked up. So when the trajecotory is computed it will avoid collisions with the objects that have been added. 

2. Pick<br>
The pick task is divided into the following subtasks, grasp pose, pre-grasp approach, post-grasp retreat, for each of them define the pose (position and rotation) of the hand, in relation to the base frame and the position of the target.

3. Open/Close Gripper <br>
Assign the robot fingers and set the opening and closing positions.

4. Place <br>
The pick task is divided into the following subtasks, place location pose, pre-place approach, post-grasp retreat, or each of them define the pose (position and rotation) of the hand, in relation to the base frame and the place position.

![MoveIt](Images/PickPlace.png)

---
## A3 - 3D Behavior Embedding

In order to bring our simulation to a 3D rendering tool compatible with XR technologies, Unity 3D is an option. Unity 3D is a versatile game engine that facilitates compatibility with more than 25 platforms, including augmented reality (AR), virtual reality (VR) and mobile devices. This engine allows the use of C# scripting, thus enabling communication with external software. In addition, it has its own in-house robotics package called [Unity Robotics Hub](https://github.com/Unity-Technologies/Unity-Robotics-Hub), which offers a high-level library for communicating with ROS as well as a URDF Importer.

### Importing a robot from a URDF file in Unity 3D

Using the URDF importer enables the reconstruction of the robot in Unity, represented by its visual meshes, collisions, and physical properties.

Visual meshes serve as a representation of the robot's physical structure. Collisions come into play for calculating interactions between the Link's rigid bodies. Physical properties, including inertia, contact coefficients, and joint dynamics, are indispensable for a precise physical simulation.

The importer reads the URDF file and deposits the data into C# classes that depict the joints and links of the robot. This data then serves as the basis for recreating a GameObjects hierarchy. Each GameObject in this hierarchy possesses an ArticulationBody component that represents a specific link. The properties derived from the URDF are assigned to the corresponding fields in the ArticulationBody.

The image below displays the robot Franka Emika imported into Unity from a URDF package.

![Panda Unity URDF](Images/URDFUnity.png)

Once the robot has been imported, a script named "controller" is available that enables control of the robot through forward kinematics via the keyboard.

### Comunication between Unity 3D and ROS

To establish a comunication between Unity 3D it is necessary to set up the following packages

On the Unity side: 

- [URDF Importer](https://github.com/Unity-Technologies/URDF-Importer.git?path=/com.unity.robotics.urdf-importer):	Unity package for handle URDF files
- [ROS TCP Connector](https://github.com/Unity-Technologies/ROS-TCP-Connector?path=/com.unity.robotics.ros-tcp-connector): Unity package for sending, receiving, and visualizing messages from ROS

On the ROS side:  

- [ROS TCP Endpoint](https://github.com/Unity-Technologies/ROS-TCP-Endpoint):	ROS node for sending/receiving messages from Unity

The communication process between Unity and ROS follows the same structure as ROS nodes, encompassing Publishers/Subscribers, Services/Clients, and Topics/Messages. On the ROS end, the ros_tcp_endpoint script functions as the communication node with Unity. The ROSConnection script operates on the Unity end with the ROS IP set. After the communication link has been established, any C# script can interact with the ROS nodes.

In this context, it's essential to develop a Unity script that subscribes to the /joint_states topic, which carries a message of the sensor_msgs/JointState.msg type. This message contains data detailing the state of a set of torque-controlled joints. The state of each joint, whether revolute or prismatic, is defined by:

- The joint's position (rad or m),
- The joint's velocity (rad/s or m/s) and
- The effort applied to the joint (Nm or N).

Upon receiving this information, it gets integrated into the Unity model, generating a Digital Shadow that mirrors the robot's movements in ROS. It's crucial to note that this process remains the same when data is extracted from the actual robot.

Presented here is a script enabling subscription to the /joint_states topic and setting the positions of the joints within the Unity robot's joints.

<details open markdown="block">
  <summary>
    Unity ROSJointStateSubscriber script
  </summary>

```cs
using System.Collections;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using SensorUnity = RosMessageTypes.Sensor.JointStateMsg;
public class ROSJointStateSubscriber  : MonoBehaviour
{
    [SerializeField] private string rosTopic = "joint_states";
    [SerializeField] private ROSConnection ROS;
    [SerializeField] private ArticulationBody[] robotJoints = new ArticulationBody[9];
    void Start()
    {
        SubscribeToTopic(rosTopic);
    }
    private void SubscribeToTopic(string rosTopic)
    {
        ROS = ROSConnection.GetOrCreateInstance();
        ROS.Subscribe<SensorUnity>(rosTopic, GetJointPositions);
    }
    private void GetJointPositions(SensorUnity sensorMsg)
    {
        StartCoroutine(SetJointValues(sensorMsg));
    }
    IEnumerator SetJointValues(SensorUnity message)
    {
        for (int i = 0; i < message.name.Length; i++)
        {
            var joint1XDrive = robotJoints[i].xDrive;
            joint1XDrive.target = (float)(message.position[i]) * Mathf.Rad2Deg;
            robotJoints[i].xDrive = joint1XDrive;
        }

        yield return new WaitForSeconds(0.5f);
    }
    public void UnSubscribeToTopic(string rosTopic)
    {
        ROS.Unsubscribe(rosTopic);
    }
  ```
</details>

Simulation running in ROS side and displayed in Unity 3D Side

|Unity 3D            | ROS |

![MoveIt](Images/pickplaceUnityRos.png)

---
## A4 - Calibration and Adjustment

To calibrate and adjust the simulation, a practical approach is to set identical tasks in the real system and in the simulation. Considering robots as an example, it is possible to trace a common trajectory from point A to point B that both the real and the simulated robot should calculate and follow.

Once this trajectory is determined, record the same values to be studied and compared, such as the elapsed time, the current position, and the velocity of the robot's joints during the execution of the trajectory.


Then, it is crucial to compare these data between simulation and reality to ensure that there are no significant discrepancies. If any discrepancies are discovered, adjustments must be implemented to align the simulation more closely with the actual behavior of the robot. Parameters such as stiffness, friction, servo motor control, or even the creation of a multi-system model of the robot could be taken into consideration. 

It is important to note that this process is performed by external software, which introduces an additional component of communication between the two systems. In this communication, data must be transferred, which involves a certain transit time.

This time must be taken into account, as it may introduce a delay or latency in the communication. It is necessary to determine if this time is negligible, or if it is affecting the accuracy and effectiveness of the simulation.

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/011-simulation/011-simulation.html" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Simulation Workflow</div>
      </a>
    </td>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/012-Interaction.html" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Interaction Workflow</div>
      </a>
    </td>
  </tr>
</table>