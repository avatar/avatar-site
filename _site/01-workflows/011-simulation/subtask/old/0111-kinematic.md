<!-- ---
# layout: default
# title: Kinematic Integration Unity
# grand_parent: Workflows
# parent: Simulation
# nav_order: 2
--- -->
## How to integrate a robotic simulator in unity
{: .fs-5}

Simulation is a key aspect of digital twin creation, particularly when it comes to analyzing and predicting the behavior of a system. In order to effectively simulate the behavior of a real-world system, it is necessary to accurately model the system and define its properties. <br>

For this reason, this section aims to describe the method to connect Unity with  Robot Operating System (ROS) in order to simulate the robot's behavior in the XR environment. It should be noted that ROS is just one of many tools that can be used to accomplish this purpose. However, the starting point will always be the same: the correct modeling of the cobot and the definition of its properties (joints, inertia, limits,...).<br>

[Unity Robotics Hub](https://blog.unity.com/technology/robotics-simulation-in-unity-is-as-easy-as-1-2-3) is a package developed by Unity that allows to create a communication and interaction between a Unity project and a ROS package. Unity's robotics tools are able to support importing URDF files and sending and receiving messages between ROS and Unity.

---
#### A1 Defining the task to be simulated 
{: .fs-5}

First of all, we must define the task to be simulated in the XR environment. In this case it is a pick and place, a classic task for a Cobot. 
Once the activity has been defined, we need to integrate the necessary packages to perform the simulation, in both Unity and ROS. <br>

On the Unity side: 

- URDF Importer:	Unity package for loading URDF files
- ROS TCP Connector: Unity package for sending, receiving, and visualizing messages from ROS

On the ROS side:  

- ROS TCP Endpoint:	ROS node for sending/receiving messages from Unity


---
#### A2 Bringing your robot into XR environment
{: .fs-5 }

Using the URDF importer you are able to rebuild your robot in Unity presented by its visual meshes, collisions and physical properties.  

The visual meshes represent the physical structure of the robot. The collisions are used to calculate the collisions between the Link's rigid bodies. Physical properties, such as inertia, contact coefficients, and joint dynamics, are necessary for an accurate physical simulation.  

The importer parses the URDF file and stores the data in C# classes that represent the joints and links of the robot. With this data, it recreates a GameObjects hierarchy. Where each GameObject has an ArticulationBody component representing a particular link. It assigns properties from the URDF to the corresponding fields in ArticulationBody.

Below you can see our robot Franka Emika imported into Unity from a URDF package. 

![Panda Unity URDF](Images/URDFUnity.png)

Also, once the robot is imported, it has a script named it "controller", that allows to control the robot by forward kinematics using the keyboard. 

---
#### A3 ROS Package
{: .fs-5 }

A powerful ROS tool is Moveit, which enables motion planning, manipulation, 3D perception, kinematics, control and navigation of the robot. 

Start by creating the ROS workspace, once created, import the URDF file into the `src` folder. Use ros launch to launch `moveit_setup_assistant`. It is a graphical user interface for configuring any robot for use with MoveIt.<br>
When the assistant finishes the setup, the robot can be visualized via RViz on a virtual environment (scene) and is now ready to create start and goal states for the robot interactively, test various motion planners, and visualize the output.

![MoveIt](Images/Moveit.png)

#### - Creating a pick and place
{: .fs-4}

To perform a Pick and Place scenario, it is necessary to create and run a script, ROS supports scripting in both Python and C++. Here are the main functions to be implemented.

- AddCollisionObjects

Add and place the objects which are part of the scene as well as the target, the object to be picked up. So when the trajecotory is computed it will avoid collisions with the objects that have been added. 

- Pick

The pick task is divided into the following subtasks, grasp pose, pre-grasp approach, post-grasp retreat, for each of them define the pose (position and rotation) of the hand, in relation to the base frame and the position of the target.

- Open/Close Gripper

Assign the robot fingers and set the opening and closing positions.

- Place

The pick task is divided into the following subtasks, place location pose, pre-place approach, post-grasp retreat, or each of them define the pose (position and rotation) of the hand, in relation to the base frame and the place position.

![MoveIt](Images/PickPlace.png)


---
#### A4 Conexion
{: .fs-5 }

The communication between Unity and ROS follows the same structure as ROS nodes, which includes Publishers/Subscribers, Services/Clients, and Topics/Messages. On the ROS side, the ```ros_tcp_endpoint``` script runs and serves as the communication node with Unity. On the Unity side, the ```ROSConnection``` script runs with the ROS IP set.
When the communication has been established, any C# script could interact with the ROS nodes. 

In this case, it is necessary to create a unity script that subscribes to the /joint_states topic, which contains a message of type sensor_msgs/JointState.msg. This message holds data that describes the state of a set of torque-controlled joints. The state of each joint (revolute or prismatic) is defined by:

- The position of the joint (rad or m),
- The velocity of the joint (rad/s or m/s) and 
- The effort that is applied in the joint (Nm or N).

Once this information is received, it will be injected into the Unity model,creating a Digital Shadow that will replicate the movements of the robot in ROS. It is worth noting that this process is the same when data is captured from the real robot.

Here is a script that allows for subscribing to the /joint_states topic and setting the positions of the joints in the robot's joints in Unity.


```cs
using System.Collections;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using SensorUnity = RosMessageTypes.Sensor.JointStateMsg;
public class ROSJointStateSubscriber  : MonoBehaviour
{
    [SerializeField] private string rosTopic = "joint_states";
    [SerializeField] private ROSConnection ROS;
    [SerializeField] private ArticulationBody[] robotJoints = new ArticulationBody[9];
    void Start()
    {
        SubscribeToTopic(rosTopic);
    }
    private void SubscribeToTopic(string rosTopic)
    {
        ROS = ROSConnection.GetOrCreateInstance();
        ROS.Subscribe<SensorUnity>(rosTopic, GetJointPositions);
    }
    private void GetJointPositions(SensorUnity sensorMsg)
    {
        StartCoroutine(SetJointValues(sensorMsg));
    }
    IEnumerator SetJointValues(SensorUnity message)
    {
        for (int i = 0; i < message.name.Length; i++)
        {
            var joint1XDrive = robotJoints[i].xDrive;
            joint1XDrive.target = (float)(message.position[i]) * Mathf.Rad2Deg;
            robotJoints[i].xDrive = joint1XDrive;
        }

        yield return new WaitForSeconds(0.5f);
    }
    public void UnSubscribeToTopic(string rosTopic)
    {
        ROS.Unsubscribe(rosTopic);
    }
}
```
![MoveIt](Images/pickplaceUnityRos.png)