<!-- ---
layout: default
title: Controling and vizualizing a CNC Machine using Virtual Reality
parent: 1st Year Student Work
nav_order: 10
--- -->

# Controling and vizualizing a CNC machine using Virtual Reality
## Hybrid Manufacturing Environment for a Machining Center
---
## Introduction

The objective of this document is to present the process of creating a digital twin controlled by Virtual Reality. Also to give the main lines to realize reproduce such a process easily. At the end of this document, a general architecture that can be adapted to other case studies is presented.
![Use_Case_Objective](assets/Introduction_01.JPG)

## Workflow Description

In the following workflow, we will present the key steps to create a digital twin of a CNC machine. We summurized all the steps followed along this process on workflow that.

![Use_Case_Workflow](assets/WorkFlow_01.JPG)

1. The first step, is to define the objectives of this digital twin, and what it can be used for, and know if it brings us an added value or not. For that before starting this project, discussions were held to define the objectives and the final result and what it should have in terms of functionality.  Two objectives were defined for the digital twin, the first one being to allow the control of the machine from anywhere. The second is to visualize and control the physical twin from a VR environment. 

2. Once the objectives are defined, the thing to do is to analyze the physical machine and understand how it works and the interface that allows it to work normaly. this approach will help on the modeling and other functionality that goes with it. Regarding our cnc machine (which was built in our lab), we have detailed the components of the machine. It is composed of a PLC and 3 drivers that control the axes, we also have one motor for each axis and 2 limit Switches. In all we have 5 axes, 5 motors, 3 Drivers and 1 PLC that control all of them. All these components come from a single brand which is beckhoff. Also the machine is managed by a program called TwinCat3. Its role is to program the PLC and control it. 
![Workflow_Step2](assets/WorkFlow_Step2.JPG)

3. The third step is to model and try to make the best possible 3D copy of the physical twin.The 3D modeling allows us to build the digital twin and give it a shape. It was an assembly of several hundred parts. This made it unusable on Unity, because processing so many parts at the same time is one of the limitations of Unity. So I had to simplify and optimize this model. To do this, I did the following:

   3.1 First thing using Solidworks is to separate the 3D model by complementary parts by separating the moving parts from the fixed ones.

   3.2 Once the parts are obtained, and with solidworks we merge the parts into a single part and export it in STEP format. 

   3.3 To convert the STEP into OBJ, 3DS MAX was used because it gave the possibility to arrange the origins of the parts and not to lose much data. (Like dimensions, positioning).

In the end, we obtained models in OBJ format usable on unity. 

![Workflow_Step3](assets/WorkFlow_Step3.JPG)

So we imported all these parts and it gives this model, ready to be programmed so that it can imitate the physical machine.

![Workflow_Step4](assets/WorkFlow_Step4.JPG)

4. After having the model, it is necessary to add a layer of kinematics, which allows to imitate the movements of the physical machine.In order to implement the kinematics, it is necessary to pass the programming, and this using C# codes.

   The axes to make in move are the X Y Z axes and the rotations axes also.

   For this for each Axis a function has been created that allows to create translations and rotations.

   [Kinematic result](https://youtu.be/K29wHRbqodk)

5. After having the 3D model and kinematic, we have to communicate with the 3D model and be able to control it, and/or visualize it. The machine in this case is controlled by a software named twincat3 which allows to program the plc and to inject inputs in order to do actions like move axes. In order to control the machine, the idea is to communicate with twincat. This communication allows to exchange data with twincat that the latter compiles, so that the machine understands what the instruction means. For that a program coded in C# must exist on the unity model so that it can send information and understand the return. This communication is based on the TCP IP protocol 

   ![Workflow_Step5](assets/WorkFlow_Step5_01.JPG)

   For giving an idea, this is part of code who shows some logic about how communication work is this case. The first one allows us to create a unity client and connect it to the twinCat ip address. Once this is done, the data can be exchanged with the condition that the same variable name is on unity and twincat. In This way twincat automatically understands which variable is changing and the same for unity.

   ![Workflow_Step5](assets/WorkFlow_Step5_02.JPG)

   Communication between twincat and unity is established. But the problem is in this way unity client can control only one machine and twincat is a localhost. So to be able to control more than one machine from the same place and remotely, it is necessary to add a server, from which we can control, visualize them in VR .

   ![Workflow_Step5](assets/WorkFlow_Step5_03.JPG)

   Mirror is a Unity library that allows you to create servers and clients on Unity with the possibility of making them communicate in real time without losing data.

   ![Workflow_Step5](assets/WorkFlow_Step5_04.JPG)


6. final step, is to be able to visualize and control a machine with VR. This part requires VR equipment, as well as steamVr which manages this equipment and some libraries and some code to control and make actions.

   ![Workflow_Step5](assets/WorkFlow_Step6.JPG){: height="450px" width="450px"}

The architecture of this case study is as follows:

![Architecture_1](assets/Architecture_01.JPG){: height="450px" width="450px"}

But the architecture used in this case study is as follows:

![Architecture_2](assets/Architecture_02.JPG)

## Results

Following the proposed workflow, we were able to start this case study, with only one physical machine and here we are with the beginning of a digital twin ready to be used.

The following video justifies the result.

[Final result](https://youtu.be/Tkh6j74IM8o)


## Conclusion 
Creating a digital twin of a machine requires going through several steps as we have proposed above. Integrating Virtual Reality has a very important added value, as it should allow to go further by having a more realistic perspective and user experience.

---
#### Lamine AMIR
#### Grenoble-INP génie industriel
#### amir.lamine@yahoo.com // lamine.amir@grenoble-inp.org
---
### Avatar for me 
AVATAR, represents for me an unprecedented experience in the field of industry 4.0. An experience full of added value, collaboration and learning. During this project, I had the opportunity to work with people with exceptional professionalism. Their knowledge in the field of XR technologies allowed me to learn a lot in a short time. Since the beginning of the avatar project, I was autonomous, which gave me the possibility to go and find the best solutions among others on each step. I was involved in the creation of a virtual reality scene from existing 3D CAD models, understanding then how to animate them and how to build virtual environment. My activities for this case study focused on experimenting with different tools and file formats. I discovered many tools as well as programming languages and software. After all this work, I think that we have reached the goals we have set with a lot of effort and commitment.
---



import re
import os

def markdown_to_latex(md_text):
    lines = md_text.split('<br>')
    latex_text = []

    for line in lines:
        line = line.strip()

        # Check for bullet points
        if line.startswith('•'):
            # Check for bolded content
            if '**' in line:
                line = re.sub(r'\*\*([^*]+)\*\*', r'\\textbf{\1}', line)
                latex_text.append(f"    \\item {line[1:].strip()}")
            else:
                # Nested bullet point handling
                latex_text.append(f"            \\item {line[1:].strip()}")
        # Check for numbered lists
        elif re.match(r'\d+\)', line):
            latex_text.append(f"        \\item {line.split(')')[1].strip()}")
        else:
            latex_text.append(line)

    # Adding environment wrappers for itemize and enumerate
    final_latex = []
    in_itemize = False
    in_enumerate = False

    for line in latex_text:
        if '\\item' in line and not in_itemize and not in_enumerate:
            final_latex.append('\\begin{itemize}')
            in_itemize = True
        elif '\\item' in line and in_itemize and not in_enumerate:
            if '            ' in line:
                final_latex.append('        \\begin{itemize}')
                in_enumerate = True
        elif '\\item' in line and in_itemize and in_enumerate:
            if not '            ' in line:
                final_latex.append('        \\end{itemize}')
                in_enumerate = False
        final_latex.append(line)

    if in_enumerate:
        final_latex.append('        \\end{itemize}')
    if in_itemize:
        final_latex.append('\\end{itemize}')

    return '\n'.join(final_latex)

def extract_content_from_table(text):
    # Split content by the "|" character and filter out segments containing the "Description" keyword
    segments = [segment.strip() for segment in text.split('|')]
    return [segment for segment in segments if "**Description:**" in segment]

main_directory = "/home/medina/mkbook/mdfiles"
input_path = os.path.join(main_directory, "input.md")

# Read the document content from the specified file
with open(input_path, 'r') as file:
    document_content = file.read()

structured_contents = extract_content_from_table(document_content)

all_converted_segments = []  # list to hold all the converted segments

for idx, content in enumerate(structured_contents):
    latex_conversion = markdown_to_latex(content)
    all_converted_segments.append(latex_conversion)
    print(f"Segment {idx + 1}:\n{latex_conversion}\n\n")

# Join all converted segments with '\n\n' separator
final_latex_content = '\n\n'.join(all_converted_segments)

# Save the final LaTeX conversion to a single file:
output_filename = "all_segments_converted.tex"
with open(os.path.join(main_directory, output_filename), 'w') as file:
    file.write(final_latex_content)
