---
layout: default
title: A4.3 - Metaphors Description
grand_parent: XR Development Workflows
parent: A4 - UI Creation
nav_order: 0
---

# A4.3 - Metaphors Description

The storyboard defined a first specification of rendered artifact/actor and its behavior. Here every artifact may be fully specified.

- Its shape and import format ; it may be a basic shape (cylinder, sphere, cube, etc) or a more complex polyhedron which will be imported from various type of files (wavefront (obj), gltf, etc)

- Its size 

- Its rendering characteristics, material, colors, effects (diffuse, specular, etc)

- Its behaviors : is it a fixed shape or is it articulated, or can it be deformed. What are the triggers handling its external of internal motion or deformation ?

### Input

The definition of actors from both the use case and storyboard are used to make this definition.

### Output

A complete description of every expected metaphor

### Control

Obviously the selection of metaphors depends on the capacity of the rendering environment to render them and to interact with it.

### Resources

The list of available metaphors but also of the import libraries is the main resource to decide what may be specified.

---
[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0131_use_case_model.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A4.2 - Use Case Model</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0133_implementation.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A4.4 - Implementation</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt