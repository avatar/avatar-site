---
layout: default
title: A4.1 - Story Board Creation
grand_parent: XR Development Workflows
parent: A4 - UI Creation
nav_order: 0
---

# A4.1 - Story Board Creation

The storyboard is a sequence of image illustrating the vision of the user interface. It illustrates specific major context and shows how the user interface should look like but indeed it prepare the specification of visual metaphors.

These sketches can be used collaboratively to converge on a common vision. They make sense also to share ideas with either end user or the potential client.

|![Sketches to illustrate the Story Board](assets/storyboard1.png){:height="270px" width="414px"}|![Sketches to illustrate the Story Board](assets/storyboard2.png){:height="270px" width="377px"}|![Sketches to illustrate the Story Board](assets/storyboard3.png){:height="270px" width="463px"}|

[comment_text]: # snt

*Sketches to illustrate the Story Board*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

### Input

The user interface goal may be described in an informal,nonn-structured, document to hightlight the main expected functions and concepts. It may be also the direct transcription of what the operator has in mind.

### Output

The main ouput is a set of illustrations where we can either describe the overall environment (sketch 1 here), then the specific content of the display in various contexts (sketch 2 and 3 here). Some text may be associated to explain the ideas but we already specify the type of metaphors that will be used since it draws how object should be represented, how we interact with objects and potentially their specific behavior.

### Control

To make a correct storyboard a good understanding of final device and display capacities are expected. On a tactile display selection will be done by a finger touch directly on the metaphor projection. In a gave gaze direction picking or laser ray cast may be used for the same type of operation.

---
[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/013-ui-creation.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A4 - UI Creation Workflow</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0131_use_case_model.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A4.2 - Use case model</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt