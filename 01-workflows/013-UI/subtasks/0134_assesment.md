---
layout: default
title: A4.5 - Assessment
grand_parent: XR Development Workflows
parent: A4 - UI Creation
nav_order: 0
---

# A4.5 - Assessment

A user interface is a system and any designed system a development process needs to converge from specification to a product then to progressively check its behavior. We can reference a V-Cycle or any model and we will face :

- unit tests : at a very low level checking that every basic function is working. It is especially necessary here to check
  
  - the importers functions
  
  - the metaphor rendering
  
  - the metaphor behaviors
  
  - the low level selection functions
  
  - the low level navigation functions
  
  - the low level manipulation functions

- once unit tests are validated some technical integration tests will support to check that every upper functions are properly accessible. The use case diagram here allow to plan integration test starting from low level function and following backwards the extend and include links. When integration tests are validated we may say that the product is functional but it is not yet acceptable.

- the end user perception tests are a complementary assessment to ensure that the system is :
  
  - efficient : based on basic scenarios we can measure the performance and quality of task performed :
    
    - performance may be measured as a task duration or a quantity of sub-tasks performed in a given delay
    
    - the quality will measure the quality of the task result which depends on the task itself: Artifacts properly positioned, completeness of the activity, etc
  
  - the use perception may be assessed through 
    
    - bio-sensors : to measure strengths, brain load, stress etc
    
    - when bio-sensors are not available or when no competencies about these sensors, some questionnaires provides good views
      
      - SUS system

### Input


The developped user interface

### Output


A validation of the user interface or a set of expected corrections

### Control


The use case is a good tool to plan the unit and integratiion test

### Resources

Sensors and questionnaires. Hereafter a set of available and referenced questionnaires

1. **System Usability Scale (SUS)**  
   ![System Usability Scale Image](assets/sus.png){:height="300px" width="300px"}
   
   [comment_text]: # snt

   *System Usability Scale Image*{: .fs-3 .text-grey-dk-000}
   
   [comment_text]: # fnt

2. **Usefulness questionnaire (USE)**  
   ![USE Image](assets/img6.png){:height="300px" width="300px"}
      
   [comment_text]: # snt

   *Usefulness questionnaire Image*{: .fs-3 .text-grey-dk-000}
   
   [comment_text]: # fnt

3. **Attractivity (AttrackDif2)**  
   ![AttrackDif2 Image](assets/AttrackDIf2.png){:height="300px" width="300px"}

    [comment_text]: # snt

   *AttrackDif2 Image*{: .fs-3 .text-grey-dk-000}
   
   [comment_text]: # fnt

4. **Task load index (Nasa TLX)**  
   ![Nasa TLX Image](assets/NASATLX.png){:height="300px" width="300px"}
   
  [comment_text]: # snt

   *Nasa TLX Image*{: .fs-3 .text-grey-dk-000}
   
   [comment_text]: # fnt

---
[comment_text]: # snt

<table style="width:100%; width: 50%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0133_implementation.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A4.5 - Implementation</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/014-operation-integration/014-operation-integration.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;"> A5 - System Integration & Operation Workflow</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt
