---
layout: default
title: A4.4 - Implementation
grand_parent: XR Development Workflows
parent: A4 - UI Creation
nav_order: 0
---
# A4.4 - Implementation

We enter if the encoding phase where the basic function should be developped

- Import model

- Render metaphors

- Select a metaphor or its inner characteristic

- Manipulate a metaphor or its inner characteristic : manipulation will move artifacts or sub-components but may also expect more or less complex simulators to define the behavior of the artifacts.

- Navigate in the scene

Then every upper use case may be developed by integration of lower level use case

### Input

The definition of metaphors but also the use case definition.

### Output

A user interface, once compiled which support the initial specification

### Control

The selection, Manipulation and navigation functions must be mapped to the final device choice. Depending on the type of device controllers this mapping identifies which buttons or event triggers a selection a manipulation or a navigation effect.

### Resources

In most Virtual or augmented reality development environment, this task is based on script developments (in C#, python, C++, whatever.)

---

[comment_text]: # snt

<table style="width:100%; ">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0132_metaphors_description.html" class="hover-move-left"  style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A4.3 - Metaphors Description</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0134_assesment.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A4.5 - Assessment</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt