---
layout: default
title: A4 - UI Creation
parent: XR Development Workflows
nav_order: 3
has_children: true
---
# A4 - UI Creation

It is recommanded to sketch the user interface behavior like a story board to specify the initial vision. Then, Human-machine interaction design requires that:

- The scene is properly constructed, with clear definition of actors, ie: potential handled objects
- Actions on actors are clearly identified (A use case model may be drawn)
- Actors behaviors is specified. It can be a very basic reflex action, or a more complex behavior, developed inside a complex script. It may involve more or less complex simulations 

Three main basic actions are used :

- Selection: how to select an actor ?
- Navigation: how to move inside the scene (change my point of view) ?
- Manipulation: how do I act on a actor ?

These actions are not independent: we need to select an actor to manipulate it. We can select an actor for navigation (for example for teleportation). But this decomposition means that every human-machine specification abut interaction can be described by these three operations.

Then the technical procedure to select, navigate or manipulate depends on a device selection plus  mapping of device sensors/actuators with selection, navigation, manipulation  metaphors.

---

## Workflow Structure

[comment_text]: # stable

| |  **<center>CONTROLS:</center>**<center> UI specification - Rendering system capacity - Device capacity - Expected mapping - Intial performance specification </center>|  |
| **INPUTS:** <br> Scene Actors |![avatar](assets/013_workflow0.png)|   **OUTPUTS:** <br> UI for XR environment |
| | **<center>RESOURCES:</center>** <center>  UML Framework - Available assets - Available device - Script libraries - Questionnaries, sensors, etc  </center>|   |

[comment_text]: # ftable

[comment_text]: # snt

*UI Creation workflow*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

---

## Workflow Building-Blocks


| Activities | Overview |
|:--- |:--- |
| [**A4.1** <br> Story Board<br>Creation](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0130_story_board.html) | • **Description:** The storyboard will present the main expected views and highlight what is expected. <br> • **Input:** User interface goals. <br> • **Output:** Sketches and metaphors. <br> • **Control:** Knowledge about the device and visualization capacities. <br> • **Resource:** Drawing tools, information sources. |
| [**A4.2**<br>Create a Use Case model<br>(UML-like description)](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0131_use_case_model.html) | • **Description:** The use case will create a link between the actors of the virtual scene and human operators. Decompose use case functions into Selection/Navigation/Manipulation keywords (see Bowman or Poupyrev!). Pay attention to the number of degrees of freedom expected for each action. The use case usually highlights actors and it is good practice to define information sources as specific actors. Then the use case model enables checking that end user functions are completely defined and that they can access the expected information. <br> • **Input:** Scene Actors. <br> • **Output:** Functions (extend, inherits). <br> • **Control:** User interaction specification. <br> • **Resource:** UML Framework. |
| [**A4.3**<br>Metaphor<br>Description](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0132_metaphors_description.html) | • **Description:** List the various states that can be reached by an actor. An actor is a metaphor for rendering an object in the scene. The actor is governed by parameters and states. Define the rendering rules which link a (state, parameters) into visual, auditive, haptic (feedbacks). Among actor parameters, we can have position, orientation, colors, textures, or special effects for visual rendering, but we can also add haptic (force, torque parameters), and also audio effects. <br> • **Input:** Scene Actors, Rendering system. <br> • **Output:** Parameterized scene with controls (we call it the set of metaphors).<br> • **Control:** Rendering system capacity. <br> • **Resource:** List of objects/assets available. |
| [**A4.4** <br> Implementation](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0133_implementation.html) | • **Description:** Depending on the rendering development environment (Unity3D, Unreal, Babylon.js), there are plugins to connect VR/AR/Rendering/Interaction devices. Implement mapping: in a given state, acting on a device button in a given condition changes actor parameters. This mapping is usually scripted in a specific computer science language (C# in Unity 3D, C++, Python or BluePrint in Unreal, Python in Godo, Python in Blender, etc.). To complete this activity, check the [Interaction](http://127.0.0.1:4000/01-workflows/012-interaction/012-Interaction.html) section. <br> • **Input:** Metaphors, Rendering system. <br> • **Output:** New scripts and compiled system. <br> • **Control:** Expected mapping. <br>• **Resource:** Script libraries. |
| [**A4.5** <br> UI Assessment](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0134_assesment.html) | • **Description:** Several tests should be conducted - Function tests, to check that scripts and mapping are properly implemented. - Integration tests, to check that the overall interface is working fine. Here, some basic scenarios can be tested, but also some kind of monkey tests with quite random actions. <br>- Cognitive and ergonomic tests. Even if we always specify the user interface trying to minimize ergonomic difficulties and cognitive load, this may be checked through experiences with final users and adequate measures and questionnaires: <br> 1. Bio-sensor measures. <br> 2. Task performance (time to realize a given task, number of errors.) <br> 3. Questionnaires: - NASA-TLX: workload - SUS: System Usability Scale. <br> • **Input:** Executable framework. <br> • **Output:** Evaluation grid, re-design and correction specifications. <br> • **Control:** Initial performance specification. <br>• **Resource:** Sensors, questionnaires, etc. |

---

## Application With Students

[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0314/0314_Desing_HMI_for_AR.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Design a Human Machine Interface for AR</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0316/0316_HMI_Robot.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Realization of a human-machine interface in a collaborative human-robot workplace</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0317/0317_xr_training.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Creation of an application in an XR environment for operator training</div>
      </a>
    </td>
  </tr>
</table>

---
<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0124_building_behaviors.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A3.5 - Building Behaviors</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0130_story_board.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A4.1 - Story Board Creation</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt