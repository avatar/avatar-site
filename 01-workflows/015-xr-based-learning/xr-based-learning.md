---
layout: default
title: XR-Based Learning
parent: XR Development Workflows
nav_order: 5
has_children: true
---

# XR-Based Learning
Higher education has to cope with the current trends in digital technologies, in particular in the field of industrial engineering where the required com- petences are quickly evolving. Digital technologies and, specifical XR, offer new opportunities to innovate teaching approaches in higher education, in particular in the field of engineering,  offering students a direct experience with advanced digital technologies in a realistic environment. 

The workflows that have been designed and developed through the AVATAR project provides building blocks for the design of innovative learning approaches leveraging XR technologies.

Hence, within the AVATAR project, the following XR-based learning approaces and experiences has been carried out:


### L1 - Assembly of a Mechanical Component

In this application, a VR environment is used to implement an innovative learning approach for the assembly of a complex mechanical component. Specifically, the students are requested to operate the assembly of a gearbox. The taks has to be carried out on a virtual workbench where all thje components are available, and targeted instructions made available,.
Furthermore, the VR environemnt provides the poèssibility to handle (virtually) the components to be assemnbled and receive a positive feedback if the tasks are being executed correctly.

[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/015-xr-based-learning/L1-assembly-mechanical-component.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore&rarr;</div>
        <div style="color: blue;">L1 - Assembly of a Mechanical Component</div>
      </a>
    </td>
  </tr>
</table>

[comment_text]: # fnt

### L2 - Human-Robot Collaborative Assembling Processes

In this application, a VR environment is used to learn how to design and validate a collaborative manufacturing operation (e.g. an assembly) when machines (e.g. a cobot) and humans operates together in an hybrid setting.
A humanoid avatar in the 3D virtual scene is exploited, togehter with the 3D modelling of the robot and the parts to be processed. The VR scene can be experienced by a trainee by means of a head mounted display so that he or she can acquire the skills required to perform the same set of operations.

[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/015-xr-based-learning/L2-workflow_assembly.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore&rarr;</div>
        <div style="color: blue;">L2 - Human-Robot Collaborative Assembling Processes</div>
      </a>
    </td>
  </tr>
</table>

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/014-operation-integration/subtask/0144_dt_demonstration_operation.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A5.5 - DT Demonstration and Operation</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/015-xr-based-learning/L1-assembly-mechanical-component.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">L1 - Assembling a Mechanical Component</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

