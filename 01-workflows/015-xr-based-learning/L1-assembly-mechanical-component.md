---
layout: default
title: L1 - Assembly of a Mechanical Component
grand_parent: XR Development Workflows
parent: XR-Based Learning
nav_order: 0
---
# L1 - Assembling a Mechanical Component #

The goal of this workflow is to show how to build an application for teaching students how a mechanical assembly works. Tho workflow makes heavy reuse of previous workflow defined in the project, combining them to obtain a full learning experience in an immersive XR environment.

|![Assembly of a Mechanical Component Workflow](assets/img_2.png)|



[comment_text]: # snt

*Assembly of a Mechanical Component Workflow*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

## Workflow Building-Blocks ##



|  Acitivities |   Overview      |
|:---|:------------------|
| [**A1.1** <br> Experience Setup<br>]()| • **Description:** he goal is to create a proper 3D environment to deliver the learning experience. The environment is built according to the workflow  [A1 - Scene Creation.]("https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/010-scene-creation.html") <br> • **Input:** 3D models of parts and assembly of the mechanical component and environment. <br> • **Output:**   XR Environment  <br>  • **Control:** Model specifications - Tracking rate - Technical datasheets <br> • **Resource:** XR hardware and software, technical specifications, docs and CAD modelers <br> |
| [**A1.2** <br> UI Creation <br>]() | • **Description:** the goal is to enrich the XR environment with UI elements that support the task the VR environment is built for, i.e. explain how a mechanical assembly works. The workflow [A4 - UI Creation](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/013-ui-creation.html) has been used as reference for this activity. For this specific application, the bill of material and the technical drawing of the parts and assemblies are needed as visual element in the scene.  <CENTER>	<figure>	<img src="assets/screen_1.png" align="center" style="width:80%" />	</figure> </CENTER>	  <br> • **Input:** XR Environment, Bill of Material, technical drawings, visual elements (e.g. buttons). <br> • **Output:** XR with UI. <br>  • **Control:** UI specifications and device capacity. <br> • **Resource:** XR hardware and software, technical specifications and documentation, available assets and devices. 	<CENTER>	<figure> 	<img src="assets/screen_2.png" align="center" style="width:80%" /></figure> 	</CENTER> <br> |
| [**A1.3** <br>  Interaction <br> and Behavior <br> Definition]() | • **Description:** the goal is to provide the XR environment and UI the capability to react to user input properly, following the prescription provided by the workflow [A3- Interation](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/012-Interaction.html). In this activity, the interaction modalities will be defined in terms of (1) which objects in the scene the user can interact with and (2) for each of the objects, which interaction modality is more suited. For example, which parts of the gearbox should be grabbable, how the user can interact with the BOM elements, etc. <br> • **Input:**  XR Environment with UI, assembly sequence and precedence diagram. <br> • **Output:** XR Environment with interactions and behavior <br>  • **Control:** Interaction requirements, device capacity, expected behavior. <br> • **Resource:** Expected behavior, APIs, Rendering system, Scripts. <br> |
| [**A1.4** <br>Learning <br> Experience in VR]() | • **Description:** By using the XR environment with interaction and behavior, the students can actively use the VR application. The gearbox parts are grabbable and selectable and the student can disassemble the different elements of the gearbox with and without precendence constraints. The exercise aims at mimicking the real process of disassembly of the gearbox. While disassembling, the students can select the parts and check their name and technical representation on the blackboard behind the desk. The students can also explore the Bill of Material (BOM) related to the gearbox and the assembly drawing. <br> • **Input:** XR Environment with interactions and behavior. While performing the procedure, if some step is wrong (e.g. violating a precedence) the application warns the user who cannot proceed until the step is done properly. <br> • **Output:** text file logging the experience. <br>  • **Control:** trainers/professors supervising the learning process.  <br> • **Resource:** XR hardware and software <br> |


---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/015-xr-based-learning/xr-based-learning.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">XR-Based Learning</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/015-xr-based-learning/L2-workflow_assembly.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">L2 - Human-Robot Collaborative Assembling Processes</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

