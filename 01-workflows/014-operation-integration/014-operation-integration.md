---
layout: default
title: A5 - System Integration & Operation
parent: XR Development Workflows
nav_order: 5
has_children: true
---
# A5 - System Integration & Operation

The SYSTEM INTEGRATION & OPERATION workflow defines a set of activities that students need to carry out to make developed Manufacturing DT System fully integrated, ready to be deployed and operated. Therefore, the goal of this activity is threefold:

1.	integration of HW and SW components of all Building Bricks (A1 to A4), with a physical manufacturing system (such as a CNC machine tool, robot or similar) and thus creating a ready to deploy Manufacturing DT System with built-in HMI function based on XR
2.	deployment of the integrated system - bringing the integrated physical and virtual system to a fully operational state and fine tuning, and 
3.	demonstration of its function and performance, which includes: 
    - basic operations of the deployed system and testing its functional performances, 
    - execution of assigned operational tasks (like robot programming by XR demonstration, for instance) and, through all of the above,
    - gaining basic practical experience and skills in using manufacturing DT technology with built-in XR HMI function.

These goals are intertwined, because each step of system-integration is always accompanied by performing partial operational activities to check the correctness of installed HW and SW components and to check the performance of the achieved functions. Accordingly, each step requires the following sequence to be performed:

1.	installation of mechanical and software modules, i.e. their integration into the system created in the preceding steps, and
2.	revival of the achieved stage of the system integration process (bringing it to a functional state, partially or in whole), that requires the following iterative activities to be executed:

    - activation of integrated modules (HW and SW),
    - quality assessment - testing of added sub-functionalities, and then,
    - if quality is NOT GOOD, performing needed adjustments and eventual correction of the physical hardware and/or related software, until the desired performances of the added sub-functionalities are reached,
    - if quality is GOOD, then go to the next step of system integration.

Obviously, this is an iterative and optimization procedure, typical of any engineering work. This means that only after the successful completion of the above-mentioned sequence, the next step can be carried out and thus gradually progress towards the full physical integration of the designed Manufacturing DT System. In some cases, since system integration is not necessarily a linear (uncoupled) process, students should take a few steps back to perform necessary corrective actions that were omitted because they were not previously recognized as potentially important.

When all the integration activities are completed and all subsystems are brought to the desired operational state, the integrated manufacturing system will be deployed, and after that, ready for the final demonstration and experimentation.


---
## Workflow Structure

[comment_text]: # stable
<table>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>CONTROLS:</b><br> Building Bricks specs - Manufacturing Equipment specs - Required performances - Safety rules and measures.  </td>
    <td></td>
  </tr>
  <tr>
    <td><b>INPUTS:</b><br> Manufacturing system HW and SW - Building Bricks A1 to A4 - technical specs.</td>
    <td style="width: 65%; text-align: center;"><img src="assets/014_workflow0.png" alt="scenecreation" style="display: block; margin: 0 auto; max-width: 100%; height: auto;"></td>
    <td><b>OUTPUTS:</b><br> XR based manufacturing DT System tested </td>
  </tr>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>RESOURCES:</b><br> XR development SW - Manufacturing system specs - Technical docs - SW development tools</td>
    <td></td>
  </tr>
</table>

[comment_text]: # ftable

[comment_text]: # snt

*System Integration & Operation workflow*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

---
## Workflow Building-Blocks

|  Activities |   Overview      |
|:---|:------------------|
| [**A5.1** <br> XR Platform <br> Integration]()| • **Description:** Integration of the XR Platform hardware and software. XR system configuration, anticipating integration of basic Building Bricks for developed Manufacturing DT System.  <br> • **Input:** Computer hardware and software, XR hardware and software, technical specifications and docs <br> • **Output:**  XR Platform ready for basic Building Bricks integration (A1 to A4, with a content that depends of the selected manufacturing use case). <br>  • **Control:** Integrated system assessment - checking performances, settings and adjustments. <br> • **Resource :** Computer hardware and software, XR hardware and software, technical specifications and docs, Guidance provided by Lab technical staff <br> |
|[**A5.2** <br> Building Bricks <br> Integration]()| • **Description:** Integration of previously created Building Bricks A1 to A4 into the Basic XR Platform of the designed Manufacturing DT System, Building Bricks interlinking and integration, setting up and preliminary testing of the integrated XR system behaviour and performances, adjustments and fixing errors, tuning up and similar. <br> • **Input:** Executable Building Bricks A1 to A4. <br> • **Output:** Fully functional XR System with integrated Building Bricks A1 to A4 <br> • **Control:** Compatibility of bricks, requested specifications / performances of XR environment <br> • **Resource:** Building Bricks specific resources, SW development tools for Bricks installation, integration and interlinking|
|[**A5.3** <br> Digital Twin <br> Integration]()| • **Description:** The activity which should provide real-time interactivity of the XR System with  a counterpart physical system of the chosen manufacturing use-case. This includes integration of the HW and SW components for data acquisition and exchange (comm interfaces installation and configuration) – Digital Shadow creation, installation of simulation applications and digital control equipment programming. <br> • **Input:** Fully operational XR System, specifications for manufacturing DT building, tech docs, and similar for manufacturing HW and SW <br> • **Output:** Manufacturing DT system (including Digital Shadow) fully operational and ready for deployment, including dedicated SW for real-time data acquisition for system/operator-behaviour recording.  <br> • **Control:** Initial specifications of requested performances, compatibility of XR system and manufacturing system control HW / SW <br> • **Resource :** Open architecture control system of manufacturing hardware, network and multiprocess communication tools, dedicated tools for programming and setting up of manufacturing equipment control systems and cell controller, dedicated simulation SW.|
|[**A5.4** <br> DT System <br> Deployment]()| • **Description:** First run of the integrated manufacturing DT system, performing basic functions, checking performances, adjustments and corrective activities at the system level, fine tuning-up. <br> • **Input:** Fully integrated and deployment ready Manufacturing DT System for the selected manufacturing use-case with built-in XR HMI function <br> • **Output:** Deployed Manufacturing DT System with built-in XR HMI function, ready for use. <br> • **Control:** Testing primitive functions related to the Building Bricks functions and for the bidirectional interaction of the XR-based DT and the physical manufacturing system counterpart, according to the selected use case and its usage scenarios <br> • **Resource:** The same as for A5.3|
|[**A5** <br> DT Demonstration <br> and Operation]()| • **Description:** Training students to perform basic operations on the developed Manufacturing DT System and conducting planned experiments. Recording the behaviour of students using the developed Manufacturing DT System for future technical and pedagogical quantitative evaluation. <br> • **Input:** Deployed manufacturing DT system (cnc machine tool, robotic system, assembly line or the like) ready for use  <br> • **Output:** Recorded data for future evaluation  <br> • **Control:** Safety measures <br> • **Resource:** Technical support of laboratory staff.|


---

## Application With Students

[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/034/034_PT_DT.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Technological Cell - PT-DT Interaction</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/037/037_physical_digital_twin.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Physical and Digital Twin</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/039/039_controling_vizualizing%20_CNC_machine_using_VR.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Controling and vizualizing a CNC machine using Virtual Reality</div>
      </a>
    </td>
  </tr>
</table>


<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0315/0315.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">COBOZ - Cobot Training from VR</div>
      </a>
    </td>
  </tr>
</table>

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0134_assesment.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A4.5 - Assessment</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/014-operation-integration/subtask/0140_xr_platform_Integration%20copy%202.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A5.1 XR Platform Integration</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt
