```mermaid
graph LR

A1["A1 <BR> Specification"] --> A2[A2 <BR> Bricks]

A2 --> A3["A3 <BR> Integration"] --> A4["A4<BR>Assesment"] --> A5["A5<BR>Deployment"]
```
