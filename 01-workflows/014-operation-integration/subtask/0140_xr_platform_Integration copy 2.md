---
layout: default
title: A5.1 XR Platform Integration
grand_parent: XR Development Workflows
parent: A5 - System Integration & Operation
nav_order: 0
---
# A5.1 - XR Platform Integration

Integration of the XR Platform is a complex process, which strongly depends on the development platform used. 
In the case of the Unity Game Engine (a cross-platform game engine developed by Unity Technologies), the XR Platform Integration begins with the integration of the Unity Hub and the Unity development platform. Subsequently, the Unity Project should be created as the next step in the process. 

In general, Unity Hub serves as a project and installation manager, allowing user to organize and access Unity projects and multiple Unity Editor versions in one location. The Unity development platform, also known as the Unity Editor, is the primary application where developer (a) design, (b) develop, and (c) create its interactive content using various tools and features. Together, Unity Hub and the Unity development platform offer a comprehensive ecosystem for developer to efficiently manage its projects and create engaging real-time experiences.

The Unity project is a collection of files, assets, and settings organized within the Unity development environment for the purpose of creating interactive applications, simulations, virtual reality (VR), and augmented reality (AR) interfaces / experiences, and other real-time content. It serves as the foundation for the development process, providing a structured workspace where developer design, build, and test its projects.

Considering above given introductory notes, the XR Platform Integration as a formal procedure requires execution of the following high-level workflow, described in a form that allow understanding of the basic process: 

### A5.1.1		Set up the Development Environment:

- a.	or A5.1.1.a - Install Unity: Download and install the latest version of Unity from the Unity website.
- b.	Install VR SDK:  Depending on the VR platform you want to develop for (e.g., Oculus Rift, HTC Vive, Windows Mixed Reality), install the corresponding VR software development kit (SDK) or plugin provided by the platform.

### A5.1.2	Open Unity Hub: 

Launch Unity Hub, the central management tool for Unity projects. If not installed, download and install it from the Unity website. Unity Hub is a standalone application developed by Unity Technologies, designed to streamline the management of Unity projects and installations. It serves as a central hub that provides various features and functionalities for developers working with Unity.
### A5.1.3	Create New Project:

In Unity Hub, click on the "New" button to create a new Unity project.
### 5.1.4		Project Setup:
- a.	Name and Location: 
Choose a name and location for your project, where you want to save the project files on your computer.
- b.	Template Selection: 
Select a template that suits your project's needs, such as 2D, 3D, 2D with Physics, HDRP, URP, or start from scratch by choosing "Empty."
- c.	Target Platform: 
Choose the target platform(s) for your project, like PC, Mac, Android, iOS, consoles, etc. You can select multiple platforms if needed.
- d.	Enable Package Manager: 
Enable the Package Manager to manage packages and dependencies in the project if not already enabled.

### A5.1.5	Create Project:
Click the "Create" button to initiate the creation of the Unity project. Unity will set up the project, create the necessary folder structure, and configuration files.

### A5.1.6	Explore Unity Interface:
After the project is created, Unity will open the Unity Editor, providing an interface for you to develop and design your application. Familiarize yourself with the Unity interface, which includes various windows like Scene, Hierarchy, Project, Inspector, Console, and Game views. Each window serves a specific purpose to help you manage and develop your project efficiently.

---

### Input	

Computer hardware and software, XR hardware and software (in case of Unity development environment: Unity Hub, Unity Editor, XR SDK)
### Output

XR Platform Integrated and Unity project created, ready for you to develop and design your application, i.e., integration of Basic Building Bricks (A1 to A4), created for the content that is defined by the selected manufacturing use case. Experience in use of the Unity development environment and creating Unity project.
### Control

Initial assessment of the integrated XR Platform by checking its performances and settings, and providing necessary adjustments. 

### Resource

Computer hardware and software, XR hardware and software, technical specifications and docs, Guidance provided by Lab technical staff as well as by the online support of used XR software provider. Important: Unity Hub provides links to Unity Learn, an educational platform offering tutorials, courses, and learning resources to help developers enhance their Unity skills and knowledge.
 

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/014-operation-integration/014-operation-integration.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A5 - System Integration & Operation</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/014-operation-integration/subtask/0141_building_bricks_integration.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A5.2 - Building Bricks Integration</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>
