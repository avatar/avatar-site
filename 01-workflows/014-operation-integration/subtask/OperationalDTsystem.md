---
layout: default
title: Operational DT system
grand_parent: Workflows
parent: Operation Integration
nav_order: 0
---
 
# Operational DT system

The goal of this activity is twofold: 

1. To integrate all physical (mechanical and electrical hardware) and software (control and similar) modules created within activities A1 to A4 and thus create an operational DT system with included XR functions, and 

2. Demonstrate the function of the integrated system by operate it in laboratory conditions, verify its overall functionality and gain basic operational experience and skills. 

Both  of  these  goals  are  functionally  intertwined,  because  system-integration  is  always  a  hierarchical  and sequentially organized process, which must take place step by step. Each step requires the following sequence of activities to be performed:  

1. Installation of physical and software modules, i.e. their integration into the system created in the preceding steps, and  
2. revival of a partially or completely integrated system (bringing it to a functional state), that requires the following iterative activities to be executed: 

    1. activation of integrated modules, 
    2. testing of added sub-functionalities to a RWC system, and then 
    3. adjustments and eventual correction of the physical hardware and/or related software, until the desired performances of the added sub-functionalities are reached. 

Only after the completion of such a sequence it is possible to perform the next step and thus gradually progress towards the full integration of the designed/developed system. When all the integration activities are completed and all the functions of its subsystems are brought to the desired operational state, the developed system will be ready for the final operational demonstration and experimentation. 

- Input: 

Hardware and software subsystems developed or used as ready-to-install modules in activities A1 to A4; 

- Output:  

Tangible: Integrated physical system composed of physical hardware (mechanical frames, robots, grippers, 

conveyors, controllers, XR hardware, computers, HRI interfaces, ...) and associated software modules (robot control software, interfacing software, visualisation software, XR software, at all hierarchical levels), fully functional, adjusted, tested and ready for operation; 

Pedagogical  (learning  outcomes):  (a)  hands  on  in  system  integration,  (b)  experience  in  using  XR technology and DT technology in operation of the modern manufacturing system (i.e., Robotic Welding Cell) and (c) basic skills in application of modern XR and DT technology;  

- Tools:  

Tools for installing mechanical and electrical hardware, measuring tools for function adjustment, software development platforms: MS Visual Studio, YASKAWA MotoPlus SDK, MikroElektronika MicroC, MatLab, Oculus Rift SDK  + SolidWorks CAD system, SolidWorks API, UNITY system.   

- Note:  

This activity is technically very complex and requires the constant presence and supervision of a well-trained instructor. 

In the case of Robotic Work-Cell (RWC) system integration and demonstration, activity A.5 contains the following major sub-activities: 

- Subactivity A5.1 –  Installation of RWC basic physical hardware 

    Subactivity 5.1.1 – Installation of the mechanical frame, i.e., physical platform for supporting the welding and assisting robot arms; 

    Subactivity 5.1.2 – Installation and integration of the welding robot, its control system and the arc-welding equipment, wiring the whole robotic subsystem and testing its operational readiness; 

    Subactivity 5.1.3 – Installation of the robot assistant, its control system and the additional hardware which enable fun the Robot Open Architecture Control system (PC + a dedicated application development, interfacing and monitoring functions),  wiring  the  whole  robotic  subsystem  and  testing  its  operational readiness; 

- Subactivity A5.2 –   

Installation of the kinesthetic HRI interface to the robot assistant arm wrist and associated microcontroller-based control hardware, wiring the interface system to the robot controller and testing its operational readiness; 

- Subactivity A5.3 –   

Installation of the teleoperation HRI interface to the robot assistant arm wrist and associated microcontroller-based control hardware, wiring the interface system to the robot controller and testing its operational readiness; 

- Subactivity A5.4 –   

Installation of the RWC cell controller system, wiring the cell controller PC the robot controllers and HMI interfaces and testing its operational readiness; 

- Subactivity A5.5 –   Installation of the XR system 

    - Subactivity 5.5.1 – Integration of the XRHMD system into the UNITY project 

    - Subactivity 5.5.2 – Installation of the XRHMD PACKAGE of drivers in the UNITY project + setting up the OVRplayer controller; 

    - Subactivity 5.5.2 – Configuration of a safe workspace – OCULUS guardian setup – the system is ready for operation; 

- Subactivity A5.6 –   Installation of the DIGITAL SHADOW System 

    - Subactivity 5.6.1 – Installation of the software routine for real-time reading robot joint angles in the Robot Open Architecture Control system (Robot OAC); 

    - Subactivity 5.6.2 – Networking of the Robot OAC and UNITY project for robot joint angles transfer via UDP communication protocol; 

    - Subactivity 5.6.3 – Installation of the C# script class in the UNITY platform for robot joint angles acquisition and robot posture updating in real-time; 

    - Subactivity 5.6.4 – Installation of the C# script class for robot joint angles RECORDING and C# function for robot recorded motion PLAYBACK (reading an ASCII file), RWC Digital Shadow function ready to use; 

- Subactivity A5.7 –   Installation of the DIGITAL TWIN System 

    - Subactivity  5.7.1  –  Installation  of  the  software  routine,  created  in  MatLab environment, for off-line mirroring the robot virtual motions based on creation of the robot job task (programmed in robot programming language, generated by selected robot motion planner/simulation software, or by preprocessing the recorded data (ACSCII file) generated by the Digital Shadow function, or by importing data from any other source) and uploading the code via UDP to the robot controller; 

    - Subactivity 5.7.2 – Installation of the robot motion planner and/or simulation / optimization software RoboDK in the RWC Cell Controller, RWC Digital Twin function ready to use; 

- Subactivity A5.8 –   RWC DT operation 

