---
layout: default
title: A5.4 DT System Deployment
grand_parent:  XR Development Workflows
parent: A5 - System Integration & Operation
nav_order: 3
---

# A5.4 - DT System Deployment

Once the Manufacturing DT system is fully integrated and brought to full operational status - with implemented functional mechanisms for bidirectional data and information exchange between physical and virtual spaces - such a system can be considered ready for deployment. By definition, deployment represents the activity of bringing integrated system resources into effective action. In the specific case of developing the AVATAR Manufacturing Digital Twin for educational and laboratory research purposes, deployment is limited only to the activities that follow the ones covered in A5.3 Digital Twin Software and Hardware Integration. These activities, in fact, constitute the system rollout, which includes the following:

### A5.4.1	

First run of the integrated AVATAR Manufacturing Digital Twin system 
### A5.4.2	

Conducting testing and validation of the integrated AVATAR Manufacturing Digital Twin to ensure required functionality, accuracy, and reliability. 
### A5.4.3	

Complementing the integrated AVATAR Manufacturing Digital Twin system with the missing components required for its integration into the specific laboratory environment, and facilitating the planned educational activities concerning the operational aspects. 
### A5.4.4

Developing technical documentation and supporting training materials for educational purposes, providing guidance on effectively utilizing the integrated AVATAR Manufacturing Digital Twin (Learning by Doing approach, for instance).


Deployment of the AVATAR Manufacturing Digital Twin system strongly depends on the specific manufacturing use case, particularly for activity A5.4.3. In the case of robotic processes, activity A5.4.3 involves complementing the Digital Twin with necessary equipment and SW tools to create robot applications, in particular interfaces for robot Programming by Demonstration (PbD). 
There are three general concepts for robot programming to execute specific manufacturing tasks:

1.	Programming by Explicit Code Writing: This is the conventional approach, but it can be very tedious, involving technical complexity and consuming a lot of time. Due to direct work with the physical robot, it carries inherent risks to the robot, surrounding equipment, and particularly, the human programmer.
2.	Programming by Simulation: This is an advanced approach that involves using sophisticated algorithms for robot motion planning and dedicated simulation tools to model manufacturing processes and robotic systems. Comprehensive training for the robot programmer is essential to effectively implement this approach.
3.	Programming by Demonstration (PbD): Unlike explicit programming, PbD does not require manual code writing. Instead, the focus is on demonstrating the application rather than coding it. The robot effectively memorizes and replicates human behavior communicated through demonstrations.
In the AVATAR course, students are guided to acquire hands-on experience and explore Programming by Demonstration (PbD) technology through Human-Robot Interaction, employing a variety of five different human-robot interfaces.
1.	KINESTHETIC Haptic Controller: 
    - a.	KINESTHETIC Haptic Controller #1: This controller enables direct robot guidance by the programmer's hands, utilizing tactile sensing integrated into the robot wrist (3DOF force sensor) and providing real-time visual feedback. The visual feedback is based on the programmer's direct natural perception of the physical robot arm position in its physical task space.
    - b.	KINESTHETIC Haptic Controller #2: This controller enables direct robot guidance by the programmer's hands, utilizing tactile sensing integrated into the robot wrist (3DOF force sensor) and providing real-time visual feedback. The visual feedback is based on the programmer's visual perception of the virtual robot arm's position in its virtual task space, using Virtual Reality (VR) with the aid of Digital Shadow technology.  
2.	TELEOPERATION Haptic Controller
    - a.	TELEOPERATION Haptic Controller #1: This controller enables direct robot guidance remotely by the programmer's hands, but with no physical tactile contact with the robot arm. The visual feedback is based on the programmer's direct natural perception of the physical robot arm position in its physical task space.
    - b.	TELEOPERATION Haptic Controller #2: This controller enables direct robot guidance remotely by the programmer's hands, but with no physical tactile contact with the robot arm. The visual feedback is based on the programmer's visual perception of the virtual robot arm's position in its virtual task space, using Virtual Reality (VR) with the aid of Digital Shadow technology. VR with the aid of Digital Shadow technology provides full remote access, including very long distances, where Internet communication resources can be used for information exchange.
3.	VIRTUAL Haptic Controller: The VIRTUAL haptic controller enables complete interaction within a virtual space. Robot commands are generated by hand-held controller units. Visual feedback is achieved through Virtual Reality visual perception and associated Digital Shadow.

The physical appearance of the aforementioned three classes of interfaces is shown in the next figure.


![Digital twin robotics process](assets/img_01.png)

[comment_text]: # snt

*Digital twin robotics process*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

 
Through a diverse range of human-robot interactions while performing the fundamental task of robot programming, students can experience the value, potential, and capacity of virtual reality and digital twin technologies.

--- 

### Input

Fully integrated and deployment ready Manufacturing DT System for the selected manufacturing use-case with built-in XR HMI function and a set of three distinct types of human-robot interaction interfaces (HW and SW)
### Output

Deployed Manufacturing DT System with built-in XR HMI functions, ready for use.
### Control

Testing the primitive functions associated with the Building Bricks and examining the bidirectional interaction between the XR-based Digital Twin and its physical manufacturing system counterpart based on the selected use case and its usage scenarios.
### Resource

The same as in A5.3, SW development tools for integration of human-robot interaction interfaces.

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/014-operation-integration/subtask/0142_digital_twin_integration.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A5.3 - Digital Twin Integration</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/014-operation-integration/subtask/0144_dt_demonstration_operation.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A5.5 - DT Demonstration and Operation</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>