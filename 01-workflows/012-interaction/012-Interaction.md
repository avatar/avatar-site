---
layout: default
title: A3 - Interaction
parent: XR Development Workflows
nav_order: 2
has_children: true
---
# A3 - Interaction

Workflow INTERACTION defines a set of main activities that must be completed to integrate interactions in an XR environment. 

Interactions will determine the appearance and behavior of the XR environment, as well as the way the user interacts with and feels the XR environment. A good interaction will allow the user to interact comfortably and effectively with the XR environment to perform the defined tasks. A bad interaction will obstruct the user's effectiveness, either by presenting a confusing and disorienting XR environment or by making interactions in the environment difficult or impossible to perform.

---
## Workflow Structure

[comment_text]: # stable
<table>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>CONTROLS:</b><br> Expected perception - Interaction requirements - Device capacity - Device capacity - Expected behavior </td>
    <td></td>
  </tr>
  <tr>
    <td><b>INPUTS:</b><br> XR environment</td>
    <td style="width: 65%; text-align: center;"><img src="assets/012_workflow0.png" alt="scenecreation" style="display: block; margin: 0 auto; max-width: 100%; height: auto;"></td>
    <td><b>OUTPUTS:</b><br> XR Environment with Interactions and behaviors</td>
  </tr>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>RESOURCES:</b><br> XR environment - Activity description - Available device - Expected behavior - APIs, Rendering system, Scripts </td>
    <td></td>
  </tr>
</table>

[comment_text]: # ftable

[comment_text]: # snt

*Interaction workflow*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

---
## Workflow Building-Blocks

|  Acitivities |   Overview      |
|:---|:------------------|
| [**A3.1** <br> Interactions Metaphors<br>Definition](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0120_Interactions_metaphors_definition.html)| • **Description:** Metaphor refers to the way in which the user is supposed to relate to the XR environment. Based on the use case and expected perception define the metaphors of interaction (navigation/selection/Manipulation). To create a metaphor interacion;: <br >1) Define the activities that the user can and cannot do in the XR environment <br> 2) Decompose each activity into elementary tasks <br> 3) Create the interaction that corresponds to each task <br> • **Input:** XR environment <br> • **Output:**  Interactions metaphors based on a particular use case  <br>  • **Control:** Expected perception <br> • **Resource :** XR environment <br> |
| [**A3.2** <br> Interactions Modes<br>Selection](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0121_interactions_modes_selection.html) | • **Description:** Select the inputs and outputs according to each interaction. Inputs come from the user (hand tracking, eye tracking, controllers) and outputs (sounds, haptics, colors) come from the virtual environment in response to an interaction. Use inputs/Outputs to consolidate the feeling of presence and improve the performance of a task.   <br> • **Input:** Interactions metaphors <br> • **Output:** Interactions modes  <br>  • **Control:** Interaction requirements <br> • **Resource :** Interactions and description of activities <br> |
| [**A3.3** <br>  XR Device<br>Selection](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0122_xr_device_selection.html) | • **Description:** Select the XR device that matches both metaphors and interaction modes by list its degrees of freedoms and describe which dof will be mapped into selection, navigation or manipulation.  <br> • **Input:** XR Devices Output A1 and A2 <br> • **Output:** Mapping between device and metaphors <br>  • **Control:** Device capacity respect to metaphor parameters <br> • **Resource :** Available device <br> |
| [**A3.4** <br>Interactivity-Based<br>Assets Classification](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0123_Interactivity_based_assets_classification.html) | • **Description:** A large number of interactive assets can result in a poor frame rate leading to an undesired experience or even cyber sickness. This is why it is important to identify and limit the number of interactive assets for the user considering the added value of the interaction to the expected perception. <br> • **Input:** XR Environment and XR Device  <br> • **Output:** List of interactive and non-interactive asets <br>  • **Control:**  Frame rate  <br> • **Resource :** Available device and XR Environment <br> |
| [**A3.5** <br> Building<br>Behaviors](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0124_building_behaviors.html)  | • **Description:** Build the behaviors between the user interactions and the XR environment. These behaviors are based on a trigger event that calls an action that has been assigned and parameterized. Behaviors can be generated directly by the rendering system, by external APIs or by scripts. <br> • **Input:** XR Environment and XR Interactions <br> • **Output:** XR Environment with Interactions behaviors <br>  • **Control:** Frame rate and Expected behavior <br> • **Resource :** APIs, Rendering system, Scripts <br> |

---


## Application With Students

[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/039/039_controling_vizualizing%20_CNC_machine_using_VR.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Controling and vizualizing a CNC machine using Virtual Reality</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0310/0310_3d_cad_web-based_vr.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">From CAD to Web-Based Virtual Reality</div>
      </a>
    </td>
  </tr>
</table>


<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0313/0313_3d_modelling_vr.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">3D Modelling in Virtual Reality</div>
      </a>
    </td>
  </tr>
</table>


<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0315/0315.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">COBOZ - Cobot Training from VR</div>
      </a>
    </td>
  </tr>
</table>

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/011-simulation/subtask/0113_calibration_adjustment.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A2.4 - Calibration and Adjustment</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0120_Interactions_metaphors_definition.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A3.1 - Interaction Metaphors Definition</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt