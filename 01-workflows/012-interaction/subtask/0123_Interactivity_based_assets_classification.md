---
layout: default
title:  A3.4 - Interactivity-Based Assets Classification
grand_parent: XR Development Workflows
parent: A3 - Interaction
nav_order: 3
---
# A3.4 - Interactivity-Based Assets Classification
Regarding the activities that the user will perform in immersive experiences, it is essential to classify assets into interactive and non-interactive. Interactive are those with which the user intervenes directly, such as a cube to pick up, and non-interactive are those with which there is no intervention, such as the sky or a tree in the distance. 
In VR:
The basic activities that the user can perform are:

- Exploration: The user can teleport or move within a predefined area, which allows limiting the user's field of movement. 
- Interaction with objects: Defined objects with which the user can interact. These interactive objects can be any asset of the virtual environment (3D models, videos, images, etc). 

For AR, Knowing that in augmented reality the user will not only see the virtual content, but also the real world, it is very important not to pollute or saturate the user's view. Too much content can be overwhelming and distract from the overall experience, while too little content can make the experience feel empty or incomplete. 

There are several ways to manage the amount of content in an AR application, including:

- Limiting the number of elements: By limiting the number of elements (such as 3D models, images, and text) that are displayed at any given time, developers can avoid overwhelming the user with too much information.

- Using transitions and animations: Smooth transitions and animations can help to pace the flow of content and prevent the user from being overwhelmed.

- Providing clear navigation and organization: Using clear navigation and organization can help the user to understand the content and move through it at their own pace.

- Using 3D models with optimized geometry: When creating 3D models for AR applications, it is important to keep the geometry simple and efficient. Complex models with a high number of vertices and triangles can impact performance and reduce the frame rate of the application.

In short, keep everything simple and functional. 

### Input

Criteria for asset classification based on their levels of interactivity

### Output

Division of assets into interactive and non-interactive categories, optimized for VR and AR environments. 

### Control

- Categorization of interactivity: Distinction between assets based on direct user intervention.
- Activity mapping: Identification of user activities in XR (e.g., exploration, interaction with objects) and matching these activities with the appropriate assets.
- Content overload control: Ensuring that virtual content does not overload users with excessive information, using a variety of content management techniques.
- Performance optimization: Monitoring application performance to ensure fluid operation, especially when displaying complex 3D models.

### Resources

- Asset libraries: Asset collections for XR available for integration into immersive environments.
- User testing facilities: Spaces equipped to test XR experiences with real users to evaluate interactivity.
Performance monitoring tools: Software solutions that monitor the performance of XR apps in real time.
- 3D modeling software: Platforms such as Blender, Maya or Unity3D that can assist in the creation and optimization of 3D models for XR applications.

---

[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0122_xr_device_selection.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A3.3 - XR Device Selection</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/subtask/0124_building_behaviors.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A3.5 - Building Behaviors</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt