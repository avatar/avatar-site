---
layout: default
title: A1 - Scene Creation
parent: XR Development Workflows
nav_order: 0
has_children: true
---

# A1 - Scene Creation

The SCENE CREATION workflow defines a set of activities that students need to carry out to create a 3D geometric model of an existing manufacturing system that is suited for the implementation as an Extended Reality (XR) environment.

The focus of the activities is not on the design of the manufacturing system itself, nor its subsystems or components, but on creating a 3D geometric model that must have:
- High geometric fidelity.
- High visual fidelity in terms of materials representation and shading (possibly avoiding artistic details that produce a large computational load).

Methodologically, SCENE CREATION workflow is based on a hybrid approach, i.e., a symbiosis of Computer-aided design (CAD) tools, for accurate parametric geometry modeling, and VR artistic tools, for creating a photo-realistic XR environment. 
The geometric model of the manufacturing system must be designed to enable further activities of students to create Digital Twin (DT) that integrate Virtual and Augmented Reality user interfaces (XR HMIs).

---

## Workflow Structure 
[comment_text]: # stable

<table>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>CONTROL:</b><br> Model specifications - Tracking rate and accuracy - Technical datasheets </td>
    <td></td>
  </tr>
  <tr>
    <td><b>INPUT:</b><br> 2D part drawings - models library</td>
    <td style="width: 65%; text-align: center;"><img src="assets/010_workflow0.png" alt="Scene Creation workflow" width="500" height="400" style="display: block; margin: 0 auto;"></td>
    <td><b>OUTPUT:</b><br> XR environment including user avatar</td>
  </tr>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>RESOURCE:</b><br> CAD editor - 3D computer graphics - Rendering engine - XR Platform - Tracking system</td>
    <td></td>
  </tr>
</table>

[comment_text]: # ftable

[comment_text]: # snt

*Scene Creation workflow*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

---

## Workflow Building-Blocks

|  Acitivities |   Overview      |
|:---|:------------------|
| [**A1.1** <br> Parts Modeling](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0100_parts_modeling.html) <br> | • **Description:** The goal is to create a collection of 3D models of all parts for each technological entity of the manufacturing system and understand their working principles. The steps to carry out are the following: <br> 1) Generating the 3D models inside a CAD environment; two approaches are possible: <br> • Download ready-made 3D models for standard elements and commercially available products (robots, machine tools, etc.). In this case, carefully check the rights of use from the provider. <br> • Create a geometric model from scratch by implementing standard CAD features (e.g., extrude, revolve, hole definition, etc). <br> 2) Specifying a consistent method for the reference point definition (i.e., the origin) of each atomic component with reference to each own bounding boxes (i.e., bottom, center, etc).<br> 3) Adding basic colors, materials and textures. Since the created CAD model is parameterized, modifications are possible using the design tree.  <br> • **Input:** Bill of materials and processes. <br> • **Output:**  3D parametric models of parts for all technological entities in a neutral interchange format (i.e., step, dxf, 3ds). <br>  • **Control:** Supervision and guidance by instructor. <br> • **Resource :** 3D CAD modeling software (i.e., Solidworks), technical datasheets. <br> |
| [**A1.2** <br> Assembly <br> Generation](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0101_assembly_generation.html) | • **Description:**  Using CAD models of previously created parts, 3D models of assemblies of all technological units of the production system should be created by applying the following steps: <br> 1) Create the assembly file in CAD modeler by importing and mating with geometric constraints the atomic components modeled in A1 according to a bottom-up approach. <br> 2) The assembly creation sequence is recurrent, so previously created assemblies can be inserted as subassemblies into higher-level assemblies: <br> This implies the organization of 3D assets according to a hierarchy of nodes based on the decomposition into elementary parts based on specific criteria (i.e., by distinguishing static and floating components to manage animations). This method also applies to 3D assets obtained from online resources: in this case, assemblies must be manually subdivided into elementary parts according to a top-down approach.  <br> • **Input:** Technical data of standard or commercially available equipment; 3D models of parts, access to the CAD models library. <br> • **Output:** 3D parametric models of assemblies and subassemblies for all technological entities in a neutral interchange format (i.e., step). <br>  • **Control:** Supervision and guidance by instructor. <br> • **Resource :** 3D CAD modeling software (i.e., Solidworks), technical datasheets. <br> |
| [**A1.3** <br> Digital Human -<br> Avatar](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0102-human-avatar.html) | • **Description:** Avatars in a virtual environment embody the user by providing a sense of self-localization, agency and bodily ownership. They also allow the user to interact with the environment and its elements. To track the user's body and bring its movements to life within an avatar, the steps to carry out are the following:<br>  1) Begin by setting up the body tracking and capture equipment, ideally in a large space where the user has freedom of movement.<br> 2) Equip the user with all necessary gear to accurately track their body movements.<br> 3) Stream the body tracking data to the XR environment for processing. <br> 4) Import a 3D model of the avatar inside of the XR environment and integrates the user's body tracking data. <br> • **Input:** Tracking hardware (kinect, cameras...), 3D model representing the user and the user's movements. <br> • **Output:** Complete body tracking and full embodiment of the avatar in the XR environment. <br>  • **Control:** Tracking points, latency of streaming, avatar rig and behavior.<br> • **Resource :** Capture Software and Hardware (e.g., Optitracking/Motive, Azure Kinect, Realsense), modeling software (e.g., Blender, Maya, C4D), XR development platform (e.g., Unity, Unreal), code editor (e.g., MS Visual Studio) <br> |
| [**A1.4** <br> Scene Integration](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0103_scene_Integration.html)| • **Description:** 3D models of parts and assemblies created in A1 and A2 must be defined according to a data structure that complies with the generation of an exploitable VR environment. This implies: <br> 1) The choice of a compatible format for 3D models, since standard CAD interchange formats (e.g., STEP) cannot be imported in most VR development platforms, which only support mesh-based representations (e.g., glTF, obj). <br> 2) A specific method for the scene configuration, that can be based on: <br> a) An interchange file (e.g., JSON, XML, etc...) that embeds all the assets and scene properties, so that parts and assemblies can be configured as modular blocks (example [here](https://virtualfactory.gitbook.io/vlft/kb/instantiation/assets/json)). <br> b) The direct import of 3D models in the VR environment from a dedicated library. Further refinements can be carried out by fine tuning the position and orientation of the imported assets.  <br> c) The direct import in the VR environment of a super-assembly that already includes all the assets that will populate the scene in their default configuration, if that's been previously exported in a single step from the CAD environment. <br> In addition, geometric model of a human operator (AVATAR) can be inserted into the scene. The resulting scene assembly is static, yet fully parameterized, which allows its offline editing and modification, as well as real-time interaction between CAD modeler and VR engine if appropriate API-based Add-Ins and stand alone utilities are provided. <br> • **Input:**  3D assembly models of technological entities, AVATAR geometric model <br> • **Output:** XR scene populated by 3D assets. <br>  • **Control:** Supervision and guidance by instructor. <br> • **Resource :** 3D CAD modeler and glTF/.obj exporter (i.e., Blender, add-In SolidWorks XR Exporter utility for .sldasm to .glTF conversion ), VR development platform (i.e., Unity), code editor (i.e., MS Visual Studio). <br> |
| [**A1.5** <br> XR  Environment <br> Definition](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0104_xr_environment_definition.html) | • **Description:** The scene file defined in A4 is ready to be processed and rendered with VR specific development tools. For this purpose, it is possible to further improve the visual quality of the 3D assets in glTF format by applying advanced materials based on the Physically-Based Rendering pipeline (example [here](https://virtualfactory.gitbook.io/vlft/kb/instantiation/3d-models/vr)), and by setting up the scene lighting. This operation can be carried out inside the XR visualization platform, or by editing materials in a separate environment that imports and exports mesh based 3D models (i.e., Blender). More details to make the XR scene exploitable by the user, (import drivers, libraries and classes for the selected HMD and associated control devices and set their parameters) are presented in the [Interaction](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/012-Interaction.html) workflows and use cases. <br> • **Input:** XR scene 3D model file. <br> • **Output:** XR scene with fully operational HMD devices and improved visual appearance. <br> • **Control:** Supervision and guidance by instructor; b) Verification and evaluation of produced outcomes; c) Written short report containing students' experiences and indication of possible difficulties in their work, communication, and in understanding the task and the overall topic <br> • **Resource :** XR development platform (i.e., Unity), PBR materials editing tool (i.e., Blender). |

---

## Application With Students

[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/030/030_digital_human_modeling.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Digital Human Modeling</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/031/031_human_operator_modeling.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Human Operator Modeling</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/032/032_geometric_modeling.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Technological Cell - Geometric Modeling</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/035/035_DT_cobot.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Digital Twin of a Cobot</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/036/036_from_cad_to_simulation.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Exporting from CAD an URDF package usable for simulation and VR</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0310/0310_3d_cad_web-based_vr.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Manufacturing Environment for a Machining Center and Pallet Assembly</div>
      </a>
    </td>
  </tr>
</table>

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/00-main/01-workflows.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Workflows</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0100_parts_modeling.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A1.1 - Parts Modeling</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt