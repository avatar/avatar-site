---
layout: default
title: A1.4 - Scene Integration
parent: A1 - Scene Creation
grand_parent: XR Development Workflows
nav_order: 3
---
# A1.4 - Scene Integration

Different types of interchange files can be implemented to collect all the information of 3D assets like node hierarchies, directory paths to repositories (local or remote), transform data of elementary components (position, orientation, scale), etc.. Herein, three, equally valid approaches are proposed; refer to the associated links to obtain detailed information on how to implement each:

* [Spreadsheets](https://virtualfactory.gitbook.io/vlft/kb/instantiation/assets/spreadsheet): A virtual factory model and the related 3D scene can be defined inside a spreadsheet where "Context" an "Assets" are defined in 2 dedicated pages.
* [JSON](https://virtualfactory.gitbook.io/vlft/kb/instantiation/assets/json): The assets composing a factory model, including the 3D scene, can be defined according to a JSON schema. JSON (JavaScript Object Notation) is a lightweight text-based data-interchange format that is easy to read/parse and write/generate both for humans and machines. It is supported by XR development platforms like [Unity](https://docs.unity3d.com/Manual/JSONSerialization.html) and [Babylon.js](https://doc.babylonjs.com/advanced_topics/.babylonFileFormat).
* [Ontology](https://virtualfactory.gitbook.io/vlft/kb/instantiation/assets/ontology): Assets can be defined by instantiating the Factory Data Model as an ontology Abox through a dedicated software tool like [OntoGui](http://www.terkaj.com/tools.html).

Regarding the specific file format for the 3D assets themselves, .glTF is a convenient choice due to the possibility to store in a single file different types of data, from the 3D geometry to materials and textures, that will be further described in A5. There are two possibilities to obtain a .glTF file starting from CAD:

* Export the part/assembly from a B-REP format (i.e., .STEP, .IGES) to .STL and import it as a mesh in a mesh editing tool (i.e., Blender), and export it again as .glTF.
* Export the part/assembly from a B-REP format to .glTF from a dedicated rendering tool (i.e., Solidworks Visualize, Keyshot).

In both cases, make sure that relevant information (i.e., the positioning of origins for elementary parts) are preserved across these import/export steps. Further resources can be found [here](https://virtualfactory.gitbook.io/vlft/kb/instantiation/3d-models/vr).

### Input

CAD (Computer Aided Design) files in a B-REP format like .STEP or .IGES, which need to be converted to .glTF.

Interchange files containing all necessary information of 3D assets, such as node hierarchies, directory paths, and transform data of elementary components.

### Output

A virtual factory model and related 3D scene defined within the chosen interchange file format (spreadsheet, JSON, or ontology).

### Control

- Consistency: Verify if the collected 3D asset information in the interchange files accurately represents the necessary components, transformations, and hierarchies.
- Data Integrity: When converting CAD files to .glTF, check whether the relevant information (e.g., positioning of origins for elementary parts) is preserved across the import/export steps.
- Visual Inspection: Inspect the virtual factory model and the associated 3D scene for correctness and completeness.

### Resources

- XR development platforms like Unity and Babylon.js
- Mesh editing tool like Blender for converting formats
- Dedicated rendering tools like Solidworks Visualize and Keyshot 


---
[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:right;  width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0102-human-avatar.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A1.3 - Digital Human - Avatar</div>
      </a>
    </td>
    <td style="text-align:left;  width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0104_xr_environment_definition.html" class="hover-move-right"  style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A1.5 - XR Environment Definition</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt