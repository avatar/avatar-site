---
layout: default
title: A1.5 - XR Environment Definition
parent: A1 - Scene Creation
grand_parent: XR Development Workflows
nav_order: 3
---

# A1.5 - XR Environment Definition

Once 3D assets have been properly organized in hierarchies and made available as .glTF files, it is possible to apply realistic looking materials that exploit the PBR (physically-based rendering) pipeline of XR development platforms such as Unity or Babylon.js..
Materials can be downloaded from online resources (preferably with CC licenses, like [ambientCG](https://ambientcg.com/) and come as texture sets that include several types of image files, each for a specific type of map:

* [Diffuse](https://en.wikipedia.org/wiki/Texture_mapping): manages the base color.
* [Roughness](https://en.wikipedia.org/wiki/Specularity): describes the surface irregularities that cause light diffusion.
* [Normal](https://en.wikipedia.org/wiki/Normal_mapping): simulates the lighting of bumps/dents.
* [Displacement](https://en.wikipedia.org/wiki/Displacement_mapping): displaces the actual geometry of the mesh based on image data.

It is also possible to create your own custom maps from existing 2D images with a software tool called [Materialize](http://www.boundingboxsoftware.com/materialize/).

XR development platforms such as Unity allow to import blank .glTF files and apply PBR materials while accurately managinng their setup. An official guide can be found [here](https://learn.unity.com/tutorial/creating-physically-based-materials-unity-2019-3#5de792caedbc2a00211d245d). For this purpose, it is highly recommended to launch the project by choosing the [High Definition Rendering Pipeline](https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@13.1/manual/index.html) template.

Alternatively, mesh editing tool such as Blender allow to manage PBR parameters from the [node editor](https://docs.blender.org/manual/en/2.79/editors/node_editor/index.html) and export as a single file the .glTF package. A use case implementing this approach can be found [here](https://virtualfactory.gitbook.io/vlft/use-cases/factory-assets/workstation-vr).

![Nodes structure of a PBR material in Blender](assets/img3.jpg)

[comment_text]: # snt

*Nodes structure of a PBR material in Blender*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

A .glTF model with PBR materials of the pin insertion station presented in A2 is provided below.

![Pin insertion station rendered with PBR materials](assets/img4.jpg)

[comment_text]: # snt

*Pin insertion station rendered with PBR materials*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

|[🔗 Pin_Insertion_Station_PBR.glb](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Vibrating_Bowl_PBR.glb?inline=false)|

### Input

3D assets hierarchically structured and available as .glTF files or a platform-compatible file. 

Texture sets for specific map types (Diffuse, Roughness, Normal, Displacement) downloaded from online resources or created with software tools such as Materialize.

### Output

A 3D model rendered with realistic-looking materials that exploit the PBR pipeline in XR development platforms such as Unity or Babylon.js.

### Control

- Rendering Verification: Verify if the PBR pipeline has been exploited properly to provide realistic materials and textures in the 3D scene.

### Resources

- XR development platforms like Unity or Babylon.js 
- Appropriate computing hardware and software to handle 3D modelling, texture mapping, and rendering tasks.

---

[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0103_scene_Integration.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A1.4 - Scene Integration</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/011-simulation/011-simulation.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A2 - Simulation Workflow</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt