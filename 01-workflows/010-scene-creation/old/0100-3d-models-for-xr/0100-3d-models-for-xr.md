<!-- ---
layout: default
title: 3D Models for XR
parent: Scene Creation
grand_parent: Workflows
nav_order: 0
--- -->

```
Book note: 
- Check text writing
```

#### Navigation Structure
{: .no_toc : .fs-5}

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>

# 3D Models for XR

The generation of 3D assets for XR implementation must take into account a series of preparatory steps that set apart this kind of modeling activity from what is normally required to design products and fixtures for real world implementation. This implies that while some aspects can be disregarded (for example, the accurate modeling of tiny details like fixing components, as well as reproducing complex, usually hidden geometries like motors assemblies), the organization of assemblies in consistent hierarchies is critical, as well as the application of advanced materials and textures to enhance the sense of realism when the models are visualized in the virtual environment.


---

## A1 - Parts Modeling

Starting from existing references (i.e., technical datasheet) it is possible to reproduce 3D assets representing industrial equipment and products in a traditional CAD environment. This route is recommended in those cases when ready-made 3D models cannot be found in the OEMs resources or through online libraries (i.e., [GrabCAD](https://grabcad.com/)). Another potential issue can be related to restrictions in terms of rights of use and privacy policies, since manufacturers that make their 3D models available for download may not allow to publish any result even for non commercial use.

Herein, the example of a vibrating bowl is available both as a .STEP file originally exported from a traditional CAD envirnoment (e.g., Solidworks) and as a .glTF. It is presented as one multibody part (beside the bowl itself, it includes the feets, plus a lateral funnel for parts feeding, etc.). Moreover, it is recommended to prioritize the remodeling of visible components only, skipping the hidden ones (e.g, the vibrating motors) that would not be visible anyway once imported in the VR scene, as well as the tiny ones (bolts, nuts, etc.).

![Vibrating_Bowl_Model](assets/img0.jpg)
<figcaption align = "center">Vibrating bowl model with basic materials</figcaption>

|[🔗 Vibrating_Bowl.STEP](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Vibrating_Bowl.STEP?inline=false)|

|[🔗 Vibrating_Bowl.glb](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Vibrating_Bowl.glb?inline=false)|

<br> Another aspect to take into account is the consistent definition of reference points (i.e., the origin) for each elementary part: specifically, when considering the bounding box of parts, a univocal position for its origin should be considered. For the case of the vibrating bowl, the center of the bottom face of its bounding box was chosen, as shown in the image below. The same logic must be applied to all elementary parts that will make up more complex assemblies that will populate the VR scene. Notice that this operation can be carried out either in the context of this A1 activity from the CAD sofware by displacing bodies relative to the origin (e.g., [Move/Copy Bodies](https://help.solidworks.com/2021/english/SolidWorks/sldworks/t_Move_Copy_Bodies.htm) feature in Solidworks) as well as in a the A4 step when dealing with the exported asset in .glTF format inside a mesh editing tool like Blender. An automatic bounding box generator for Blender to be installed from the Edit/Preferences/Add-ons section is provided below as a Phyton code.

![Vibrating_Bowl_BoundingBox](assets/img1.jpg)
<figcaption align = "center">Vibrating bowl with origin at the center of the bottom face of its bounding box</figcaption>


|[🔗 Bounding_Box_Generator.py](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Bounding_Box_Generator.py?inline=false)|

---

## A2 - Assembly Generation

For the purpose of this workflow, when approaching the re-modeling of industrial equipment and products in terms of parts and assemblies, a crucial aspect to consider for the instatiation of factory assets is the identification of elementary  components in the objects' hierarchies; For this task, it is not relevant to refer to the actual, real-world bill of materials of a given object and the related assembly structure; considering the requirements of the target application for these assets, that is the implementation inside a virtual environment, a simpler identification method can be based on the distinction between static and floating components, to be appropriately grouped as multibody parts if they are constituted by subcomponents that are jointly connected as in the case of the vibrating bowl presented in A1 (all static). Furthermore, the positions of the origins as defined in A1 must be used to characterize more complex assemblies by specifying the relative position of subcomponents according to the following steps: 

* Multiparts assets will have a parent empty object to which its static components will be referenced and positioned.
* Subcomponents have to be grouped matching the hierarchy of the assembly, i.e., assets forming a sub-assembly must be grouped together.
* Parenthood relationships between the 3D models have to be defined by specifying the associate relative positioning. CAD environments use this approach to manage multipart assemblies and dependencies.

The example below shows a pin insertion station in which static and floating components are treted as hierarchy nodes. For example, the tip of the insertion machine is indicated with the "Floating_X" suffix ("X" stands for the direction of the movement relative to the workstation's system of reference; "Y" and "Z" can also be found in other configurations or for other machines) so that it can be displaced (i.e., its positional data are set to vary across a series of timestamps) relative to its parent, which does not change its position when animations are launched and is indicated by the "Static" suffix. Other parts that do not have any related child and don't move are indicated only by their name and do not have the "Static/Floating" suffix (e.g, "Conveyor Main", "Vibrating Bowl Pins").
Additional details on this case can be found [here](https://virtualfactory.gitbook.io/vlft/use-cases/factory-assets/workstation).

![Pin_Insertion_Station](assets/img2.JPG)

<figcaption align = "center">Pin insertion station with ballons</figcaption>

| Component ID          | Label  | Parent                 | Relative Position [mm]  |
|---------------------- |------- |----------------------- |------------------------ |
| Vibrating_Bowl        | 1      | Pin Insertion_Station  | [-633.24, 0,-335.51]    |
| Conveyor              | 2      | Pin Insertion_Station  | 0, 0, 0                 |
| PI_Machine_Static     | 3      | Pin Insertion_Station  | [-417.5, 0, 0]          |
| PI_Machine_FloatingX  | 4      | PI_Machine_Static      | [21, 146.59, -218.71]   |

A similar approach has been applied to another case previously developed in the context of the AVATAR project, available [here](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/studentsfirstyear/11_GiovanniLaRosa/3DModelingtowardsVRWorkflow.html).

At this point, assemblies can be created according to two - equally valid - approaches:

* Bottom-up: single parts of elementary components are joint in a higher level assembly.
* Top-down: a complex part or assembly model is split into elementary objects according to a given instatiation method (i.e., static/floating components). This is usually the case of assets downloaded from the web (i.e., models of collaborative robots), whose hierarchies should be manually recreated.

---

## A3 - Digital Human - Avatar

The main activities to create and integrate a digital human in an XR environment are described in the following section. 

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/0101-human-avatar/0101-human-avatar.html" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore&rarr;</div>
        <div style="color: blue;">Digital Human - Avatar for XR</div>
      </a>
    </td>
  </tr>
</table>

---

## A4 - Scene Creation

Different types of interchange files can be implemented to collect all the information of 3D assets like node hierarchies, directory paths to repositories (local or remote), transform data of elementary components (position, orientation, scale), etc.. Herein, three, equally valid approaches are proposed; refer to the associated links to obtain detailed information on how to implement each:

* [Spreadsheets](https://virtualfactory.gitbook.io/vlft/kb/instantiation/assets/spreadsheet): A virtual factory model and the related 3D scene can be defined inside a spreadsheet where "Context" an "Assets" are defined in 2 dedicated pages.
* [JSON](https://virtualfactory.gitbook.io/vlft/kb/instantiation/assets/json): The assets composing a factory model, including the 3D scene, can be defined according to a JSON schema. JSON (JavaScript Object Notation) is a lightweight text-based data-interchange format that is easy to read/parse and write/generate both for humans and machines. It is supported by XR development platforms like [Unity](https://docs.unity3d.com/Manual/JSONSerialization.html) and [Babylon.js](https://doc.babylonjs.com/advanced_topics/.babylonFileFormat).
* [Ontology](https://virtualfactory.gitbook.io/vlft/kb/instantiation/assets/ontology): Assets can be defined by instantiating the Factory Data Model as an ontology Abox through a dedicated software tool like [OntoGui](http://www.terkaj.com/tools.html).

Regarding the specific file format for the 3D assets themselves, .glTF is a convenient choice due to the possibility to store in a single file different types of data, from the 3D geometry to materials and textures, that will be further described in A5. There are two possibilities to obtain a .glTF file starting from CAD:

* Export the part/assembly from a B-REP format (i.e., .STEP, .IGES) to .STL and import it as a mesh in a mesh editing tool (i.e., Blender), and export it again as .glTF.
* Export the part/assembly from a B-REP format to .glTF from a dedicated rendering tool (i.e., Solidworks Visualize, Keyshot).

In both cases, make sure that relevant information (i.e., the positioning of origins for elementary parts) are preserved across these import/export steps. Further resources can be found [here](https://virtualfactory.gitbook.io/vlft/kb/instantiation/3d-models/vr).

---

## A5 - XR Environment Definition

Once 3D assets have been properly organized in hierarchies and made available as .glTF files, it is possible to apply realistic looking materials that exploit the PBR (physically-based rendering) pipeline of XR development platforms such as Unity or Babylon.js..
Materials can be downloaded from online resources (preferably with CC licenses, like [ambientCG](https://ambientcg.com/) and come as texture sets that include several types of image files, each for a specific type of map:

* [Diffuse](https://en.wikipedia.org/wiki/Texture_mapping): manages the base color.
* [Roughness](https://en.wikipedia.org/wiki/Specularity): describes the surface irregularities that cause light diffusion.
* [Normal](https://en.wikipedia.org/wiki/Normal_mapping): simulates the lighting of bumps/dents.
* [Displacement](https://en.wikipedia.org/wiki/Displacement_mapping): displaces the actual geometry of the mesh based on image data.

It is also possible to create your own custom maps from existing 2D images with a software tool called [Materialize](http://www.boundingboxsoftware.com/materialize/).

XR development platforms such as Unity allow to import blank .glTF files and apply PBR materials while accurately managinng their setup. An official guide can be found [here](https://learn.unity.com/tutorial/creating-physically-based-materials-unity-2019-3#5de792caedbc2a00211d245d). For this purpose, it is highly recommended to launch the project by choosing the [High Definition Rendering Pipeline](https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@13.1/manual/index.html) template.

Alternatively, mesh editing tool such as Blender allow to manage PBR parameters from the [node editor](https://docs.blender.org/manual/en/2.79/editors/node_editor/index.html) and export as a single file the .glTF package. A use case implementing this approach can be found [here](https://virtualfactory.gitbook.io/vlft/use-cases/factory-assets/workstation-vr).

![PBR_NodeEditor](assets/img3.jpg)
<figcaption align = "center">Nodes structure of a PBR material in Blender</figcaption>

A .glTF model with PBR materials of the pin insertion station presented in A2 is provided below.

![Pin_Insertion_Station_PBR_Materials](assets/img4.jpg)
<figcaption align = "center">Pin insertion station rendered with PBR materials</figcaption>

|[🔗 Pin_Insertion_Station_PBR.glb](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Vibrating_Bowl_PBR.glb?inline=false)|

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/010-scene-creation.html" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Scene Creation</div>
      </a>
    </td>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/0101-human-avatar/0101-human-avatar.html" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Digital Human - Avatar</div>
      </a>
    </td>
  </tr>
</table>