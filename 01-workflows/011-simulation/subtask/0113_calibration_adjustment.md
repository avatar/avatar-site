---
layout: default
title: A2.4 - Calibration and Adjustment
grand_parent: XR Development Workflows
parent: A2 - Simulation
nav_order: 1
---
# A2.4 - Calibration and Adjustment

To calibrate and adjust the simulation, a practical approach is to set identical tasks in the real system and in the simulation. Considering robots as an example, it is possible to trace a common trajectory from point A to point B that both the real and the simulated robot should calculate and follow.

Once this trajectory is determined, record the same values to be studied and compared, such as the elapsed time, the current position, and the velocity of the robot's joints during the execution of the trajectory.


Then, it is crucial to compare these data between simulation and reality to ensure that there are no significant discrepancies. If any discrepancies are discovered, adjustments must be implemented to align the simulation more closely with the actual behavior of the robot. Parameters such as stiffness, friction, servo motor control, or even the creation of a multi-system model of the robot could be taken into consideration. 

It is important to note that this process is performed by external software, which introduces an additional component of communication between the two systems. In this communication, data must be transferred, which involves a certain transit time.

This time must be taken into account, as it may introduce a delay or latency in the communication. It is necessary to determine if this time is negligible, or if it is affecting the accuracy and effectiveness of the simulation.

### Input

Motion path data from both the digital twin and the physical twin, recording parameters such as elapsed time, which provides information on the robot's speed and efficiency; current position, which ensures that the robot is following the correct path and signals any deviations; and joint velocity, which gives a measure of the speed and smoothness of the robot's joint movement.

### Output

Results of the data comparison between the digital twin and the physical twin, by identifying discrepancies between the recorded parameters.  Adjustment of the simulation parameters to approximate the real robot behavior.

### Control

- Accuracy, deviation and tolerance of parameters 
- Desired accuracy between the digital twin and the physical twin
- External software for calibration and adjustment

### Resources

- Digital Twin
- Physical Twin
- Data collection tools for recording

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/011-simulation/subtask/0112_3d_behavior_embedding.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A2.3 - 3D Behavior Embedding</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/012-interaction/012-Interaction.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A3 - Interaction Workflow</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>