---
layout: default
title: A2 - Simulation
parent: XR Development Workflows
nav_order: 1
has_children: true
---

# A2 - Simulation

Creating an effective simulation for integration into a digital twin is a multidisciplinary process that integrates real-world data and advanced simulation modeles. This process serves to create a digital shadow of a physical system, providing information for predictive analysis, system optimization and potential failure detection.

By defining and developing accurate system behavior based on actual real-world data, simulation captures the essence of the real system. This accuracy ensures that the digital shadow can faithfully reproduce the system's responses under various conditions, and the integration of the behavior into a 3D structure brings a visual dimension to the simulation allowing a more intuitive understanding of the system behavior. Finally, calibration and adjustment closely align the digital shadow with the real-world system. The result is a powerful digital tool that mirrors the behaviors of the real-world system.

---
## Structure of the workflow

[comment_text]: # stable

<table>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>CONTROLS:</b><br> Technical specification - Mathematical validation - Third-party software simulation - Data integration protocol </td>
    <td></td>
  </tr>
  <tr>
    <td><b>INPUTS:</b><br> Real system and its 3D representation</td>
    <td style="width: 65%; text-align: center;"><img src="assets/011_workflow00.png" alt="scenecreation" style="display: block; margin: 0 auto; max-width: 100%; height: auto;"></td>
    <td><b>OUTPUTS:</b><br>Digital Shadow</td>
  </tr>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>RESOURCES:</b><br> Simulation and visualization software - Historical records and real-time metrics</td>
    <td></td>
  </tr>
</table>

[comment_text]: # ftable

[comment_text]: # snt

*Simulation workflow*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

---

## Workflow Building-Blocks

|  Activities |   Guidelines      |
|:---|:------------------|
| [**A2.1** <br> System Behavior<br>Definition](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/011-simulation/subtask/0110_system_behavior_definition.html)| • **Description:** To define the behavior of a system, it is necessary to carry out an exhaustive study of the system and its environment. For this, it is crucial to identify the main internal and external variables that influence and impact the behavior to be simulated. These variables may include factors such as speed, temperature, pressure among others. In addition, it is critical to collect all the necessary data, including historical records and real-time metrics, to facilitate the simulation process. Sometimes it may be necessary to implement specific hardware to collect data from the system.  <br> • **Input:** Real System <br> **• Output:** Specific variables that impact system behavior, historical records and real-time metrics. <br> • **Control:** The technical specifications or parameters that are used to guide and regulate the simulation process. <br> • **Resource:** Technical data sheet and instrument of analysis and measurement. <br> |
| [**A2.2** <br> Behavior<br>Development](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/011-simulation/subtask/0111_behavior_development.html)| • **Description:** Accuracy in the behavior of the system during simulation is vital. This accuracy is necessary because, when the virtual model interacts with the real system -either sending or receiving information-, the response is expected to be identical on both virtual-real sides. There are two possible alternatives for developing the system behavior: <br> a) Development of a particular custom behavior: this can be achieved by defining own behavioral equations from the parameters and data collected. An example would be the algorithms for calculating the inverse kinematics of a robot. It should be noted that this is a task that requires solid mathematical knowledge. <br>b) Delegate the behavior to a third party: A specialized software can take control of the behavior to be simulated by entering the parameters or information that has been collected, such as a robotic simulation software like RoboDK. However, this involves creating a connection between the rendering software and the simulation software, which could cause latency problems. <br> Both cases are valid and depend only on the purpose of the simulation. <br> • **Input:** Specific variables that impact system behavior and historical records and real-time metrics. <br> **• Output:** Custom behavioral model or third-party software integration <br> • **Control:** Mathematical validation, third-party software validation, expert and peer review. <br> • **Resource:** Technical specifications of the system, scientific literature, software documentation.  <br> |
| [**A2.3** <br> 3D Simulation<br>Integration](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/011-simulation/subtask/0112_3d_behavior_embedding.html) | • **Description:** Once the system behavior has been developed, the simulation data must be integrated into the virtual representation of the system, i.e. the 3D models. For this, the following two tasks must be performed:<br> a) 3D Structuring: This process consists of taking the visual representation of the system and recognizing its main components, which may include axes, joints, links, etc. These components should be in a hierarchical structure based on how they are linked to each other and how they move relative to each other. The positional relationships and movement constraints between each component must be clearly defined. In addition, it is necessary to eliminate any superfluous or redundant elements, such as bolts and nuts, that do not directly contribute to the simulation. <br>  b) Data Integration: This stage involves the development of components or the writing of code that will be responsible for incorporating the simulation data (dynamic, kinematic, thermal, electrical, and others) into the visual elements of the system. This process must be carried out in a way that accurately reflects the limitations and constraints of each system element. <br> These tasks are essential to ensure that the 3D models that are part of the system accurately and effectively reflect the simulation results that will in turn reflect the behavior of the real system.<br> • **Input:** 3D model of the system and simulated behavioral data. <br> **• Output:** 3D visualization of the result obtained from simulated behavior. <br>  • **Control:** Data integration protocol, design guidelines and software parameters.  <br> • **Resource:** Software tools for 3D modeling and simulation. <br> |
| [**A2.4** <br> Calibration<br>and Adjustment](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/011-simulation/subtask/0113_calibration_adjustment.html) | • **Description:** After the behavior has been simulated and integrated into the 3D model, it's necessary to calibrate and adjust the system to align with the real-world system as closely as possible. Calibration consists of aligning the behavior and response of the simulated system with the real system. Adjustment may include modifying parameters, altering certain behaviors, or modifying the 3D structure to ensure that the simulation accurately represents the real-world system. It is also possible to compare the output data from the simulation and the output data from the real system. This activity is an iterative process, in order to match as closely as possible the behavior of the real system.  <br> • **Input:** 3D visualization of the result obtained from simulated behavior and Real world data. <br> **• Output:** 3D visualization of the calibrated and adjusted simulation that accurately represents the real-world system. <br>  • **Control:** Real world system data and performance benchmarks. <br> • **Resource:** Real world performance data, experts for verification and adjustment, simulation software for model <br> |

---

## Application With Students

[comment_text]: # snt
<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/038/038_IA_for_inverse_kinematics.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Using Reinforcement Learning to simulate robot Inverse Kinematics in Unity</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/033/033_kinematics_modeling.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Technological Cell - Kinematic modeling</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0311/0311_machine_animation.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Animation of Machine Center in Virtual Space</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0312/0312_gcode.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Gcode Interpreter and Toolpath Generator</div>
      </a>
    </td>
  </tr>
</table>

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/010-scene-creation/subtasks/0104_xr_environment_definition.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A1.5 - XR Environment Definition</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/011-simulation/subtask/0110_system_behavior_definition.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A2.1 - System Behavior Definition</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt