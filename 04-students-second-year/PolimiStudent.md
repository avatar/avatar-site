<!-- ---
layout: default
title: PoliMi - Project Report
parent: 2nd Year Student Work
nav_order: 0
--- -->

# AVATAR Project Report - PoliMi students

## Table of Contents

0. Abstract
1. Brief Introduction about Industry 4.0
2. Learning Phase University of Belgrade
    - 2.1 CAD modelling 
    - 2.2 Introduction to Unity
    - 2.3 RoboDK & Robot kinematics 
    - 2.4 Unity and Solidworks integration 
    - 2.5 VR environment
    - 2.6 Digital shadow  
    - 2.7 Communication protocol between ROAC and unity 
    - 2.8 Haptic Control 
    - 2.9 Physical Robot Programming 
    - 2.10 Feedback & potential improvements for the JLL in Belgrade
3. Project scope description 
    * 3.1 Introduction to education 4.0
    * 3.2 Introduction to VEB.js
    * 3.3 Reference learning frameworks
        - Challenge-based learning
        - David Kolb's experiential learning method
        - Game-based learning (soft skill, basis of a serious game)
    * 3.4 Use-case and project work goals
4. Project workflow description
    * 4.1 CAD acquisition/generation and modeling
    * 4.2 Manipulation of GLB/GLTF files
    * 4.3 Generation of the scene
    * 4.4 ROBODK, ROBODK API and Asset Info Panel functionality
    * 4.5 Simulation of robot behavior (generation of animations)
5. Project learning related outcomes
    * 5.1 Methods for advanced learning outcomes
    * 5.2 Method for simple learning outcomes
6. Final considerations and conclusions
7. Future developments
8. References
    


## Abstract
This report is the culmination of the work conducted during the course Smart Manufacturing Lab at Politecnico di Milano, and its content is founded on a joint learning lab in Belgrade directed by the ERASMUS+ collaboration project AVATAR. This report firstly aims to replicate the digital twin generation of a robotic work-cell of University of Belgrade, using different technological frameworks. In this project, indeed, the digital twin was genetated on VEB.js, while in the joint learning lab it was established in Unity. Another important aspect of this report is to show how education 4.0 can be integrated into a university setting, using different conceptual frameworks applied to our robotic work-cell digital twin. These frameworks include one based on Challenge-based learning, one on David Kolb's experiential learning method and one on serious game. The ultimate goal of this project is to expand an existing education 4.0 platform that is represented by VEB.js with a new digital twin (the robotic cell). This will promote learning through increased insight of how to integrate industry 4.0 technologies in education and manufacturing settings. 

---

### 1. Brief Introduction about Industry 4.0

Industry 4.0 refers to the fourth industrial revolution which was initially caused by the first industrial revolution which came with the advent of mechanization, steam power and water power.
This was followed by the second industrial revolution, which revolved around mass production and assembly lines using electricity.
The third industrial revolution came with electronics, I.T. systems and automation, which led to the fourth industrial revolution that is associated with cyber physical systems. Generally-speaking, Industry 4.0 describes the growing trend towards automation and data exchange in technology and processes within the manufacturing industry, including:
- The internet of things (IoT)
- The industrial internet of things (IIoT)
- Cyber-physical systems (CPS)
- Smart manufacturing
- Smart factories
- Cloud computing
- Cognitive computing
- Artificial intelligence

This automation creates a manufacturing system whereby machines in factories are augmented with wireless connectivity and sensors to monitor and visualize an entire production process and make autonomous decisions.
Wireless connectivity and the augmentation of machines will be greatly advanced with the full roll out of 5G. This will provide faster response times, allowing for near real time communication between systems.
This automation includes interconnectivity between processes, information transparency and technical assistance for decentralized decisions.
In short, this should allow for digital transformation. This will allow for automated and autonomous manufacturing with joined-up systems that can cooperate with each other. The technology will help solve problems and track processes, while also increasing productivity. Universal Robots chief technology officer and co-founder, explained, "Industry 5.0 will make the factory a place where creative people can come and work, to create a more personalized and human experience for workers and their customers.

----

### 2. Learning Phase University of Belgrade


As already stated, this year the joint learning Lab took place in the University of Belgrade. It was planned to be four full learning Days and one last day for the final presentation. The main objective of the Lab is to introduce students to advanced and fundamental engineering tools to deal with digital twin technology in the future. 
The Lab was structured in activities and milestones for each day. By the end of the lab period students should acquire knowledge about the tools used by the university of Belgrade to create the digital twin. To summarize the activity done in the lab we are going to divide the lab into modules, these modules actually represent the experienced workflow during the experience. The modules can be summarized in this way:
- CAD modelling 
- Introduction to Unity
- RoboDK & Robot kinematics 
- Unity and Solidworks integration 
- VR environment
- Digital shadow  
- Communication protocol between ROAC and unity 
- Haptic Control 
- Physical Robot Programming 

![image1.png](assets/image1.png)

*Figure 1: robotic cell of the Univerisity of Belgrade*

### 2.1 CAD modelling 
The first session covered basics CAD modeling skills using SOLIDWORKS. The key concept was to know which modifications had to be implemented on the existing CAD models. This was done in order to be able to open it in a virtual scene. Firstly, we had to check if the measurements of the CAD model were coherent with the ones of the physical robot. Secondly, we had to apply for each link, a coordinate system. This had to be done in order to make sure that future implementation in unity would not have errors. 
The model we worked on was the MOTOMAN SIA 10F, and we were able to download the entire rigid body from the YASKAWA website.
The next step was to export the model in a .gltf format. To do so we had to firstly open the model in Solidworks’s Visualize Professional, after adding the Visualize Professional Add-In in the Solidworks app. We then exported the project into the gltf format.
Now we had to prepare Unity: by going to Package Manager and adding a package via Git URL, we added the UNIGLTF package. The robot was ready to be imported into Unity: after allocating the .gltf file into the assets folder of the project, with a simple drag and drop in the scene, we had the robot ready to work with.

### 2.2	Introduction to Unity
What is unity? Unity allows you to import and assemble assets, write code to interact with your objects, create or import animations for use with an advanced animation system.
Once we imported our system into Unity, we noticed that our coordinate system wasn’t working properly. The coordinate base orientation was different in Unity and in our CAD model. This is because our robotic arm’s links rotate relatively to the parent’s coordinate system. To make sure this problem wouldn’t drag on we had to change them. We added a new coordinate system identical to the one in Unity on every joint by assigning a point on every link.

### 2.3 RoboDK& Robot kinematics 
Direct kinematics refers to the use of the kinematic equations of a robot to compute the position of the end effector from specified values for the joint parameters. Inverse kinematics refers to the reverse process that computes the joint parameters that achieve a specified position of the end effector.
Knowing this, we proceeded in visualizing the robot in RoboDK. This software can implement the inverse kinematics, thus enabling us to plan and execute trajectories. During the movements we could see the various angle changes. Being a 7 degree of freedom robot, we had the possibility to choose different configurations, since the 7th degree is redundant, giving us multiple solutions.
We then went to physically see the robot and manually teach it the points where to move. To teach it the positions in which to move we have to be in teach mode with the server on. We then move the robot whilst pressing down on the dead man’s button. Once in the desired position we must save its coordinates by clicking on the insert button and then the enter button. Before playing the movement, we should check it by scrolling through the saved positions.
After setting those points, we can start our program. It will go from saved point to save point with the type of movement we set for each of them. (We must switch to play mode and be sure that the server is on).

![image4.png](assets/image4.png) <br>
*Figure 2: robot kinematics schema*
### 2.4 Unity and solidworks integration 
*Connection between Unity and SolidWorks:*
The link between SolidWorks and Unity was established using an API which is open source. The connection used was UDP. A while loop is running. inside the while loop an If conditioned is used to check if there is any data transmitted (angle for the joints etc..), if there is no data waiting in the queue the program runs the else condition, if the software has a change in angeles superior to 0.01 the UDP protocol for sending starts. 
User datagram protocol (UDP) operates on top of the Internet Protocol (IP) to transmit datagrams over a network. UDP does not require the source and destination to establish a three-way handshake before transmission takes place. Additionally, there is no need for an end-to-end connection.
Since UDP avoids the overhead associated with connections, error checks and the retransmission of missing data, it’s suitable for real-time or high-performance applications that don’t require data verification or correction. If verification is needed, it can be performed at the application layer.
 
We also discussed the connection between Unity and RoboDK. In Unity we can move the robot’s TCP (tool center point). For every frame, the TCP’s position and rotation is sent to RoboDk by using APIs. Once the TCP’s orientation and position are obtained, RoboDK calculates the inverse kinematics, which implicitly means its joint angles. After this we use APIs once more to send back to Unity the robot configuration. Depending on the frame rate, the robot position refreshes to the exact configuration.

### 2.5 VR environment
Once we had the scene, we started to visualize it in a VR environment. Virtual reality immersed us in a digital environment, with which we could also interact thanks to the use of special joysticks. The oculus used is composed of one screen that transmits two images that are slightly shifted. Thanks to the different orientation of the lenses the two images overlap giving the sensation of depth. The device needs a camera to work, and it has connection points in order to control the tracking. Thanks to this technology we could see the representation of the robot, interact with it and see its parts.

This part of the code represents the algorithm used for communication between SolidWorks and Unity:
```csharp
    while(true)
    if (udp client.canRecieve() > 0)
    {angles = parserUDP.udpRead();
    Setangles (angles);
    Array.Copy(angles, 0, old angles, 0, angles.Length);
    else
    {
        angles = Get angles();
        {
            if (!ArraySequence_Offset (angles, old_ angles, 0.01))
            {
                Array.Copy(angles, 0, old_ angles, 0, angles.Length);
                udp_client.udpSend(angles);
            }
        }
        XRCAD();
    }
```
The while loop is executing until we close the program.
First, we are asking the UDP client if he received some information from unity, if this is true, we are reading the information that is sent to us and we ask SolidWorks to update angles, after changing we store them in the variable old_angles so we can compare those angles with new ones when we need to.
If we didn’t change the angles in Unity, we are entering the else part and there we check if the difference between new angles and angles in SolidWorks is more than 0.01. If this is true the new angles are becoming old angles and we sent that information to unity using UDP.

### 2.6 Digital shadow
we applied an interactive connection between the physical world and the abstract one. At the end of the session, we were able to read joint coordinates in real time thanks to OAC (Open Architecture Control). 

![image5.png](assets/image5.png)

*Figure 3: communication schema to instantiate the digital shadow of the real robot*

In a digital shadow, you have a one-way communication between your digital robot and the real one. This basically means that the digital robot copies the movement of the real one. We were then taught what functional blocks were needed for a digital shadow. The physical robot in this type of configuration is a “Publisher” which publishes the states (the angles positions) of every joint of the robot. Our digital interface, or Unity, is our “Subscriber” which receives all the data sent by the publisher. To make these two talks we must establish a UDP type of connection. To make this “topic” travel we use APIs and the connection is physical between the two parts through an ethernet connection thanks to the switch.

We use Robot Open Architecture Controller to gather all the information we need to create the digital shadow: the angles of the joints. It must be noted that the 7th joint in the info package physically corresponds to the 3rd joint on the robot (this is decided by the manufacturer).
We had a first approach with the digital shadow. One student was controlling the robot with the controller with the objective of touching different targets (tennis balls in our case) while another student was looking at the 3D model in Unity. The 3D model would follow the movements of the physical robot.

The tennis balls were also modeled in Unity and would turn red once the robot touched them, this was made possible thanks to collision detection. When a ball was touched the next one appeared. After touching the last ball, all four balls appeared on display. We then could reset the balls and repeat the task. 
 
### 2.7 Communication protocol between ROAC and unity
All the code was written in unity scripts using C#. For communication between ROAC and unity, the UDP protocol was used. The robot control unit was connected to the PC using the switch. The same communication with a different class in C# was used for linking unity and RoboDK.
At any time, we could activate the functions of the digital shadow in unity, so our robot in VR can copy the movement of our physical robot.
As part of a demonstration the green virtual balls were created. When touched, they become red, and after a few seconds, they disappear. The physical representation of those balls were tennis balls, so we could really feel the contact and at the same time see the virtual contact between our robot and balls.
The robot can share a lot of data on different parameters but, for our purposes we only need the angles. We split the data and used the angles to change the position of the robot in unity or our physical model, depending on the data’s transfer direction.

### 2.8 Haptic Control
We were briefly taught how the haptic controller worked: they were strain gauges applied in a way to only be sensible to a directional force. They were used to be able to sense forces along the x, y and z axis separately. After converting the difference in voltage of the strain gauge to its corresponding difference in force, these data are filtered and clamped using a capacitor and a resistor. Since the signal coming from the sensor is about a few millivolts the device amplifies the current intensity and filters the signal cutting the non-important frequencies. This way the signal can go to the motherboard in order to be processed.
This is done using the following component.

![image7.png](assets/image7.png)

*Figure 4: circuit that transmits and elaborates sensors outputs*

We then translate this signal to a difference in position and send this filtered data to the physical machine, which then applies the relative movement. We must note that the sensors of the different axes have different payloads. In our case, the z axis had a higher payload and was much harder to move than the other.

We can see an additional use of Matlab, which allows us to optimize our movements and trajectories.

![image9.png](assets/image9.png)

*Figure 5: communication schema to process sensor inputs*

### 2.9 Physical Robot Programming  
For this session we interacted with both the real robot and the 3D model using 3 different ways to make them move: 
- Mounted joysticks, 
- Remote joysticks 
- VR controller

We also had 2 ways to visualize the robot’s moves: 
- 2D monitor screen 
- VR

Every time the task was to press tennis balls (real or virtual ones) as quickly and precisely as possible. 
First, we tried to make the 3D model move while wearing an HMD and by using VR controllers. We were able to get fast and precise movements but occasionally the 3D model motion was erratic. Still, we were able to reliably touch the tennis balls. 
Then, we tried to make the robot move with the mounted joysticks. The mounted joysticks were designed and manufactured in the lab.

![image10.png](assets/image10.png)

*Figure 6: Robotic cell cobot with special end-effector equipped with joysticks*

*Joystick design:*
In the joystick, it’s important to decouple the forces applied on the robot to avoid any problems. To do that, they implemented three strain gauges. One on the left side and two on the right side. The left joystick is dedicated to Z axis movements (up and down). The right one is dedicated to X axis and Y axis movements (forward/backwards and left/right respectively). Two buttons are located on the back of the joysticks. When pressed simultaneously the robot is activated and is able to move. The system stops if you release at least one of them. Two other buttons are located on the front of each joystick. The ones on the left are used to record target points, the ones on the right can record trajectories. They can also be programmed to do whatever is needed. Finally, there are 2 side buttons that can be programmed if needed.
To use the joystick controllers, you first need to press the two back buttons. Then you need to apply a slight force in the desired direction without showing resistance to the robot's movements. The left joystick is less sensitive because its strain gauge requires twice the force to achieve a similar deformation (10 kg vs 5 kg).
As intended, the 3D model would follow every move of the real one.
Following this, we saw a demonstration of the remote joysticks and although they are a bit impractical, paired with cameras they could allow someone to do remote work with ease.

![image11.png](assets/image11.png)

*Figure 7: Welding Yaskawa robot teaching pendant*

*VR Kit*
Finally, we had a glimpse of the future of this industry: The digital twin. Using the VR headset and the VR controllers, by controlling the 3D model in unity, we were able to make the real robot move and accomplish a task. This process can be seen as a physical shadow of the digital robot. The task was executed by one of the researchers, expert in doing this. However, a physical shadow can be very dangerous. Indeed, generating inputs for the movement of the robot from the digital environment can seriously damage the asset and any bug, misuse or accidental movement can create serious conseguences. 

![image12.png](assets/image12.png)

*Figure 8: Virtual reality headset and joysticks*


### 2.10 Feedback & potential improvements for the JLL in Belgrade
Some improvement potential of the JLL in Belgrade was also identified, which was shared by most of the participants. The main improvements areas identified are the following: 

**Repetition:** For some students that had extensive knowledge about CAD-modeling and/or digital twin creation, some of the lectures and practical work regarding it was perceived as a somewhat waste of time. For others this was completely new territory, and mentioned it would have been beneficial with more background knowledge before starting the JLL. In other words, the possibility of more individual learning should be explored for the next JLL. 

**Report writing:** After the lectures and work regarding the CAD-modeling and digital twin creation was finished, the groups were supposed to write a summarized report of the JLL when not in the lab with the robot. This work was by the majority of the students seen as not being very efficient, nor particularly rewarding from a learning perspective since the report writing merely was to summarize. In order to make the report writing more engaging, increase the learning output of this task and thus optimize this part of the JLL, it should be investigated how the students can have more influence over this phase.

**Presentation:** This part of the JLL is closely connected to the report writing phase. The biggest issue was that since every group presented more or less the same thing, the presentations became quite monotonous since they merely repeated what we had done/learned during the JLL. If the groups had more influence to create reports with different topics, consequently with different results, watching other groups' reports would also probably be experienced as being more interesting and rewarding from a learning perspective.
We have tried to address some of these aspects, by proposing a different approach to structure the JLL through the Challenge Based Learning method. This can be seen in chapter 3.3. 

## 3. Project scope description 

---

The scope of our project is strictly linked with the Joint Learning Lab experience. Indeed, what experienced in Belgrade resulted in an acquisition of knowledge related to Digital Twin generation workflow, to specific technologies involved in the process and to how these technologies can be used to enhance didactic activities and student learning outcomes. 

Taking what was acquired as a starting point, the scope of this project was identified in the generation of a contribution to the learning experience of the students, in the context of digital twin technological framework. More specifically, the aim of this work is to replicate some of the steps experienced during the JLL using different model driven technologies, in order to generate a digital twin and to produce contributions and possible way of use of this digital twin for didactical purposes. The final goal is therefore to enrich and expand the already existing education 4.0 platform VEB.js (chapter 3.1 and chapter 3.2) using the JLL knowledge base as a starting point. 

The expected outcomes of the project, therefore, are related to two different areas: one more technical and related to different technologies involved in digital twin generation and one that requires both theory and experience as students, related to the didactic area, combining different frameworks of the learning related literature. 

### 3.1 Introduction to education 4.0:
Today there is no unilaterally accepted definition of what Education 4.0 is, but most people would agree that it refers to the shifts in education to respond to the rapidly evolving industry 4.0 and future of work. At this point in human history the digital transformation is rapidly changing our everyday lives and the way we work, through increasingly higher degrees of automation and rapidly emerging new technological frameworks. These rapid changes also alter the requirements of industry, which have started to push for a more fit-for-purpose style of education. The covid-19 pandemic has accelerated the digital transformation both in industry and education, making implementation of education 4.0 principles more relevant than ever. But what does this implementation actually imply for the students, educators and educational institutions? The short answer is that the learning and teaching methods will increasingly be more aligned with the skills needed in the future, but how this should be done is not so obvious. There are however some important characteristics that must be considered when trying to make and implement an education 4.0 platform:
- Fuse available technologies into the learning process.
- Develop the students’ hard skills in such a way that they can actually make use of the skills and know-how learned in the workplace.
- The importance of soft skills must not be underestimated. Skills like creativity, collaboration, communication, complex problem solving, and critical thinking are all skills that can never be automated nor replaced. The WEF goes as far as saying these skills will be indispensable in the future.

In the context of this new way of defining education, the Joint Learning Lab can also be identified, as attendant we experienced a very new way of teaching, more interactive and based upon innovative technologies. This experience let us understand what can be considered an education 4.0 platform and its real academic value

### 3.2 Introduction to VEB.js:
VEB.js (Virtual Environment based on Babylon.js) is a reconfigurable model-driven virtual environment application based on [Babylon.js](https://www.babylonjs.com), a complete JavaScript framework and graphics engine for building 3D applications with HTML5 and [WebGL](https://www.khronos.org/webgl/) (Web Graphics Library). Babylon.js enables to load and draw 3D objects, manage these 3D objects, create and manage special effects, play and manage spatialized sounds, create gameplays and more. Babylon.js has been exploited to develop VEB.js as a reconfigurable model-driven virtual environment application. Summing up, VEB.js can be integrated with different technologies that makes this environment suitable and flexible for the generation of digital twins. For example, one of the digital twin present in VEB.js is represented by an automated assembly line to produce furniture’s hinges, that is currently used as an academic teaching tool in various context at Politecnico di Milano. To get a full picture of what this application is and what other kind of technologies are related to it refer to the [gitbook](https://virtualfactory.gitbook.io/vlft/). From our perspective as students, this digital environment can surely be considered an education 4.0 platform, even if its value and purpose is not only related to the didactical one and its scopes are beyond the pure learning. The model-driven reconfigurability of VEB.js is mainly related to the nature in which it generates virtual environments. As it will be deepened in next sections of the work, environments are indeed generated by using as main inputs json files (Java Script Object Notation), this particular data structure are human readable and a very spread data interchange method. By giving as input to VEB.js this json files and another derived format from CAD models, it is basically possible to generate almost any kind of virtual environments coming from a very wide range of industrial and non-industrial use-cases. Therefore, this application is not tied to a particular model or context but it’s extremely versatile and flexible and can be used for multiple purposes. Furthermore, it is accessible via browser and this makes it an easy accessible platform that is immediately ready to be used for the final user that needs just to search for its URL in order to use it. All these characteristics and its direct link with Politecnico di Milano, makes it a perfect technology upon which develop our project work, which is completely built upon VEB.js. 

### 3.3 Reference learning frameworks 
To generate a valuable work from the didactical perspective, we combined the experience gathered during the lab with other existing innovative learning frameworks. In this chapter, three different learning method frameworks will be discussed. The goal is to showcase how education 4.0 principles can be applied to a learning setting using different methods.

##### Challenge-based learning:
Challenge-based learning is what's called a learner-driven method. This means that it is the learner that takes full ownership of the challenge, defines the problem they work on, and acquires the necessary knowledge needed in order to solve the problem. CBL can be seen as a further development of Problem-based learning (PBL) because it aims to incorporate more of the soft and hard skills needed in the 21st century. It is therefore a very useful model to use in education 4.0. One can say that the method aims to bring both students, teachers and researchers together in order to solve real-life challenges in collaboration with business and society. Since the method is mostly project based, it is also a great starting point if one wants the students to learn and develop several hard and soft skills. The European Consortium of Innovative Universities (ECIU), which is co-funded by the EUs Erasmus+ program, often arranges CBL workshops. For this project, we find it useful to refer to the ECIU’s approach when talking about CBL. They divide the CBL-process into the three following steps:
- Engagement: is based upon the “the big idea” the group will be working on, and is deemed to be the basis for motivation. The theme can be anything within the 3Ps (people, planet and profit), but finally the participants, students, teachers and external partners must all agree on the final challenge that is going to be pursued.
- Investigation: is meant to ensure that all of the group-members actually understand the challenge and can contribute to solving it with their knowledge and skills. If needed, the group can take micro-modules to fill the knowledge gaps or gain new skills to help them solve the challenge. In these kinds of projects, having multidisciplinary-team is often seen as an advantage.
- Action: all of the partners use their shared knowledge to design, evaluate and potentially prototype new solutions. It is also possible to take it further than prototype, which can include functioning services, products, start-ups and spin-offs.

![image13.png](assets/image13.png)

*Figure 9: challenge based learning process steps*

##### David Kolb's experiential learning method:
Experiential learning, also commonly referred to as learning-by-doing, is a methodology that revolves around immersing the learner in an experience that encourages reflection in order to develop new skills (especially hard skills) and ways of thinking. In other words, one can say that the experiences of actions is what’s driving experiential learning. An example of the relative importance of experience can be seen in figure 10, which is an illustration of Edgar Dale’s learning pyramid. It states that while we commonly remember around 10 % of what we read, we can remember up to 90% of what we do.

![image14.png](assets/image14.png)
*Figure 10: Edgar Dale’s learning pyramid*

For this project, we will focus more specifically on David Kolb's experiential learning model. The model revolves around a four-phase learning cycle that, according to David Kolb, converts experience to knowledge. We find this model suitable as part of a course, structured as a lab. The cycle is comprised of the following phases:

*Phase 1: Concrete experience*
This is the stage where the cycle usually starts, and is where the learners first get exposed for an experience. The experiences are most often personal, meaning that the individual is “hands on” with the experience. 

*Phase 2: Reflective observation*
This step is all about taking a step back after the task or activity is concluded, to reflect on the experience. Asking questions and discussing with others that also engaged in the task/activity is in this stage highly encouraged, because communication helps the reflection process by introducing different points of views. In the reflection process, it is also common to compare the new experience with previous experiences to look for patterns and differences. 

*Phase 3: Abstract conceptualization*
This stage comes as a direct consequence of reflective observation, because based on their reflections the learner forms new ideas and concepts of the experience that culminates in conclusions.

*Phase 4: Active experimentation*
This is where the new ideas and concepts imagined by the learner are applied to their surrounding environment. The learner will then be able to assess if there are any changes when the next occurrence of the experience happens again. The new experiences will then restart the cycle because as life itself, everything can be simplified to interlinked experiences. 

![image15.png](assets/image15.png)
*Figure 11: David Kolb's four phase learning cycle*

##### Game-based learning (soft skill, basis of a serious game):
Game-based learning (GBL) happens when the game itself is teaching the students, and it has become an important tool for training students and employees in soft skills particularly. GBL must however not be confused with gamification. 
Gamification revolves around applying gaming mechanics, such as score cards and points, to promote competition and motivation to do something. It is in itself not a game, but adds gaming elements to environments that usually aren’t game related. A good example of a real-life application of gamification is when you for instance get flight points for flying with a certain airline. For something to be called a game, it usually has to have rules, a definite end (win, loose, stalemate) and a clear objective/objectives. Game-based learning is therefore a game where the objective/objectives are learning based, but it can also incorporate gamification. We also want to separate games for entertainment from educational games, and educational games are often referred to as serious games.

To develop a serious game for a GBL purpose, one can apply the framework (see figure 12) developed by professor Marcello Urgo, Walter Terkaj, Marta Mondellini and Giorgio Colombo for the article: “Design of serious games in engineering education: An application to the configuration and analysis of manufacturing systems”. It gives a solid foundation for developing a serious game in a manufacturing setting. A serious game could be a valuable addition to standard lectures, and as formerly mentioned regarding the Edgar Dale’s pyramid in figure 10, the experience of serious game could increase the output from 10% to 90%. The results of “Design of serious games in engineering education: An application to the configuration and analysis of manufacturing systems”, also clearly indicate that the students prefer a serious game to traditional lectures.

![image16.png](assets/image16.png)
*Figure 12: serious game schema*

### 3.4 Use-case and project work goals: 
As already stated, the starting point of our project is represented by the JLL experience, this laboratory was completely focused on a real robotic welding cell. Despite the robotic cell was composed by two different robots, the complete digital twin workflow and almost all the experience, was focused only on the cobot MOTOMAN SIA10F. As already described in the previous section, this 7-joints robot and its open-source robot controller architecture where the main founding technological blocks behind the workflow experienced. Also, the software for inverse kinematics ROBODK played an important role during the lab, indeed, its presence allowed the possibility to generate a digital twin for the robotic cell without needing the two real robots. Our project work main idea was therefore developed in this context, combining the experienced elements and technological frameworks that we considered more suitable for our purpose. 

The main object of interest was identified in the robotic cell with just the cobot. Since such robot is in Belgrade, we identified as another fundamental technological block the inverse kinematics software ROBODK. During the experience we were also able to acquire the CAD models of our use case and, as already described, to understand how it can be used for learning purposes. Combining all these elements resulted in the idea of generating a digital twin of this robotic cell on VEB.js, leveraging on ROBODK to simulate robot movements and program. In order to get to the expected outcome, several steps and intermediate goals must be completed. Even if the complete and detailed activities for each process will be described in next sections of the report, here we present a brief and general description of the workflow.

First, starting from the CAD models it is necessary to generate a scene of the robotic cell in the virtual reality environment, this is not simply a matter of uploading files, indeed starting from the CAD it is necessary to export a different file format called GLB/GLTF that can be visualize by the virtual reality graphical engine. To generate something that can simulate the behavior of a real industrial element and define a digital twin, the internal structure of elements of these files is very relevant and cannot be randomly defined, therefore manipulation and tuning are required both on the original CAD models and on derived GLB/GLTF file. Core activities in this process are mainly related to tune the hierarchy of the different assemblies and to the placement of origins of meshes and nodes. Once completed this step, it is necessary to generate a data structure that can make the virtual environment understand which are all the elements that have to be visualized in a scene, their layout, their relationships and their positions and relative positions. In VEB.js this is done through a very common and already introduced kind of file, which is called json, and that must be defined according to certain rules. Once defined a scene, and therefore generated the json file of the scene, it is also good to set up an environment, in order to define virtual reality elements such as lights and cameras, that play an important role for the user experience. This activity is also done via json file. Once generated a scene and an environment it is time to start to simulate the real object of study behavior, in case of a robot it is necessary to make it moves in a consistent way with its kinematics. It is therefore necessary to animate the scene, the animation in VEB.js are again generated via json file. Animating a robot by scratch can be quite complex, indeed, trying to generate an animation through direct kinematics is almost impossible, for this reason it is necessary to have an inverse kinematics software, which is able to provide an output of some kind, that can be manipulated and used to generate a json file to animate the robot. In this process ROBODK and a ROBODK python API, which is able to export a csv file format with all angles value of a trajectory in time, play a fundamental role, allowing the generation of an animation json file. Once reached this milestone, it is time to formalize how to use a virtual reality environment for didactical purposes, in this context our experience as students and our creativity must be combined with literature learning frameworks, in order to generate a significant and meaningful output. 

All the previous generally described processes, represents the workflow of our project work, which finally allowed us to enrich an already existing education 4.0 platform (VEB.js), with a new virtual environment and possibly, with new learning outcomes for students. Besides this direct output, we also understand the value of producing knowledge about this new recent and emerging technologies, which are not always trivial and easy to use. For this reason, another indirect scope and goal of this project, is to document and improve the knowledge about digital-twin generation workflow. This will allow us to contribute to the learning material available for students, that will be used in the future, for projects related with this technologies. Finally, here are summarized in a schematic way our project-work goals:
- Proper CAD model generation (Tuning structure of assemblies and reference systems)
- Proper GLB/GLTF files generation
- Generation of the JSON file of the scene
- Generation of the JSON file of the environment
- Generation of a csv trajectory file from ROBODK (using ROBODK python API)
- Generation of a python code able to manipulate the csv file and transalting its information content in JSON format
- Generation of the JSON files of the animations (simulation of robot behavior in the virtual environment)
- Creating way to use virtual reality environment for didactical purposes
- Generating knowledge about the studied technological frameworks for future students


## 4. Project workflow description
### 4.1 CAD acquisition/generation and modeling
In this part of the report, we are going to explain and clarify each step to import and manipulate a CAD model, in order to generate a suitable GLB/GLTF file, that can be loaded and visualized in a scene of VEB.js environment and most of all, that can be animated in a consistent way with the real asset. In order to achieve this scope, it is fundamental to guarantee that the asset CAD model respects some properties that are achieved through manipulation on a CAD software. The procedure has been done on SolidWorks on the CAD model of our use-case, the cobot of the Belgrade university lab. 

During this phase the objectives are:
- Create a CAD model consistent with the real asset
- Make sure the origin for each part is placed in the right place 
- Create a proper hierarchy of the assembly
- Make sure to have consistent reference systems for each part 
- Set up a proper global reference system for the whole assembly 
- Generate GBL/GLTF file preserving the original CAD hierarchy and the origins of meshes 

Here the steps are analyzed one by one:

**1. Create a CAD model for the robot:**
The Robot CAD used was MOTOMAN SIA10F. The CAD model can be downloaded from the website of the manufacturer Yaskawa as STP format and then imported to Solidworks.
After importing the robot we need to disassemble it, saving each part alone independently

**2. Origin and reference system adjustments:**
Now we have seven joints saved as SLDPRT format, we will inspect each joint reference system and adjust it to be on its rotational center and oriented in the right way.
The reason behind this process is related to the simulation step. Indeed after having generated a scene in VEB.js we are going to generate rotations and movements for the robot, therefore is fundamental that parts’ origins and reference systems must be placed on a consistent way respect to the kinematic chain of the robot. This has to be done starting from the CAD model, which is the first input in the digital twin creation process. As shown in the Image the center of the joint is shown on the center of the internal rotational surface with a proper orientation for the joint axis. Each joint reference frame must be oriented in the same way.

**3. Creation of hierarchy of the assembly:**
In this part we are going to summarize how and why we created a certain hierarchy structure for the assembly. We have recorded the steps in a tutorial with explicit explanation. The hierarchy is needed since there are different parts that will be subjected to different rotational inputs. Furthermore, nested parent-son relationships are required in the hierarchy, to model the robot kinematic chain where the position of a part depends on the position of parts before it in the chain. The placement of a joint must be relative to its parent.  

![image19.png](assets/image19.png)

*Figure 13: example of assemblies hierarchy*

By following this logic, we have assembled the whole Robot. We started assembling parts from the top to the bottom, in other words from the most inner part in the nested hierarchy to most outer part. For example, in our case we started from the last joint of the robot (Joint 7) until we reached the base. Following this order in the process is necessary to achieve the desired hierarchy structure. First, we created an SLDASM and we imported the last joint which from the previous step was saved as SLDPRT. Then we saved the assembly and recreated a new blank assembly where we placed the first assembly which now, we'll call Joint 7, and we assemble it to Joint 6. We repeat these steps until we reach robot base, generating a main assembly that contains the joints as subassemblies, where each joint is parent of its successor in the kinematic chain and son of its predecessor. The Whole Assembly must have the reference system placed in the center of the bottom surface of the base part and the orientation should be kept consistent with other reference frames in the model.

**4. Overview on the complete cell model:**
In order to build a model that represented the robotic cell of Belgrade we need to design some parts such as the foam base adding to it four the tennis balls. As already described, these balls were used as target points for the robot during experiments conducted in the lab, since we want to simulate such robot movements, we add them to the model. Moreover, we import the steel structure assembly(predesigned by university of Belgrade) to complete the Belgrade cell. As shown below this is the final output for the assembly work carried on the CAD software. Before getting into the exportation of GLB/GLTF files, it must be clarified that we didn’t export the whole robotic cell assembly, instead we kept the three main elements separated (the cobot, the foam and the base assembly) and ‘assemble’ them via JSON file of the scene in a process that will be described in next sections. This was done in order to not export a uselessly complex, heavy and hard to manipulate GLB/GLTF file of the whole assembly.

![image17.png](assets/image17.png)
*Figure 14: CAD model of the robotic cell*

**5. Generate GBL/GLTF file preserving the original CAD hierarchy and the origins of meshes:**
What is a GLB/GLTF file and why did we use it?
GLB is the binary file format representation of 3D models saved in the GL Transmission Format (glTF). Information about 3D models such as node hierarchy, cameras, materials, animations and meshes in binary format. This binary format stores the glTF asset (JSON, .bin and images) in a binary blob. It also avoids the issue of increase in file size which happens in case of glTF. GLB file format results in compact file sizes, fast loading, complete 3D scene representation, and extensibility for further development. The format uses model/gltf-binary as MIME type. GLB/GLTF files can be loaded and visualized by VEB.js, therefore they are the 3D model main input to generate a scene in this virtual environment.

To export the GLB file using Solidworks we need to use an add-in which is called Solidworks Visualizer.
In order to simplify the importing process to VEB.js we separated the CAD files, one dedicated assembly containing all the hierarchy and references for the robot assembly, another for the frame base and the last one for the foam base with the tennis balls. 

![image18.png](assets/image18.png)

*Figure 15: Solidworks Visualizer import settings* 

We Imported the Robot Assembly adjusting the import setting -> part grouping ->Group/Appearance 
This is a very crucial step because this maintains in the GLB file the hierarchy and the origins.
The last step is to export from the visualizer choosing the GLB format.

### 4.2  Manipulation of GLB/GLTF files:
After having generated a GLB/GLTF file from SolidWorks visualize, it is important to check if the internal structure and the needed properties of the model have been preserved in the new format. This is done through a software able to visualize and load GLB/GLTF format like Gestaltor and Blender. Anyway, Gestaltor is a less powerful solution, according to our experience it can be used to check the goodness of a 3D model, but, if something is not properly defined and manipulation is required, this software can be a little tricky and complex to use. For this reason, all the GLB/GLTF manipulation steps have been done in Blender. However, if the CAD model is properly defined why is it necessary to do this step?

This is related with GLB/GLTF files exporter, a lot of CAD softwares can export this format files, but the technology that guarantee the transformation is not yet fully mature and the complexity of the process, most of the time, generates problem and inconsistencies between exported and original files. For this reason, the manipulation on Blender is required, here in this section we present the main steps followed and the solution approach to faced problems. In addition to this schematic description, we have prepared a video tutorial that can be found in project related material.
1. **Removing useless nodes added by SolidWorks:** This first step is very simple and consist in deleting nodes that are linked to solidworks cameras and lights and that are useless for our purpose, since these elements are set-up in VEB.js through the JSON file of the environment.

2. **Changing not preserved nodes’ origins position to the desired one:** This step is related with issues happening during the export of the GLB/GLTF file. We have experienced indeed, that the origins of the nodes that contain the meshes of the parts are preserved, while their parent nodes, the ones that correspond to subassemblies in the whole assembly, are not preserved and they all coincide with the whole assembly origin (see figure 16). This is a problem, as already explained in the CAD modelling section the origins of these nodes must be in the right place. To change them we have to use the cursor functionality of Blender, by selecting in the hierarchy a node with the origin placed well (such as the parent node of a mesh which preserved its original origin), we can see an orange point corresponding to its origin, by clicking Shift+S we access to a shortcut, therefore we select the command cursor to selection. We will see our cursor moving to the same orange point corresponding to the desired origin. Now before changing the position of the origin of the parent node with the well placed one of its son, it is necessary to go on the bar menu transformation on the right and to flag just ‘affects only parents’. Now we select the node of which we want to change the origin from the hierarchy, we use again the shortcut Shift+S but this time we select the command ‘selected to cursor’, we will see the origin of the node changing and moving to the desired place (remember that the cursor during the transformation was in the position in which we moved it in the first step). It is necessary to repeat this procedure for all the nodes which didn’t preserve their origin.

![image22.png](assets/image22.png)
![image23.png](assets/image23.png)
*Figure 16: R02_J4 is the parent node of R02_LINK_04-1 which is the parent node of the mesh of the joint 4. It is possible to observe that the two origins are not coincident, R02_J4 origin was not preserved and put equal to the whole assembly origin.*

3. **Defining new positions and changing origin of meshes:** During the project we needed to change the origin of meshes and/or to define new position for origins. To change the origin of a mesh to a position already defined in the 3D model the procedure is the same of the step 2, with the exception that it is necessary to flag only ‘affects only origins’ in the transformation bar menu. To define completely new position instead, it is necessary to move the cursor to these desired points, how to do that? The section of Blender called geometry nodes helps a lot in this, it allows the user to place the cursor with a much higher precision than the layout section, of course it’s limited and not precise as a CAD software but still good to define origins of parts that are not expected to be animated. After placing the cursor in the right point, come back to section layout and follow step 2 or 3 depending on the needs. In our case this functionality helped us a lot to fix origins of tennis balls and balls meshes and for the balls’ base.

![image24.png](assets/image24.png)
*Figure 17: R02_J4 origin has been fixed*

4. **Removing useless assembly nodes and file dimension optimization:** This procedure was necessary to achieve a simple and not uselessly complex structure of the GLB/GLTF file and to low the file dimension to not incur in problems during the animation. For this reason, all empty nodes, like the root node and some intermediate nodes have been deleted, this was of course done after changing parent relationships of the file, through the section where relations are defined. The file dimension optimization was done through the GLB/GLTF optimizer that can be downloaded and used from the terminal.

### 4.3 Generation of the scene:
Once having defined proper GLB/GLTF files, it is time to generate the JSON file of the scene. A scene indeed can be defined according to a specific schema that consists of three root properties:
- “context”: definition of context setup.
- “scene”: definition of the 3D scene.
- “assets”: detailed definition of assets that are included or not in the scene. Assets not included in the scene are models/templates that are referenced or could be later instantiated in the scene.

The JSON file can be automatically generated from an xlsx spreadsheet, for more details about this refer to the [gitbook](https://virtualfactory.gitbook.io/vlft/kb/instantiation/assets/json). In the spreadsheet (an example is available on the gitbook), it is necessary to clearly define assets in terms of different properties, it is important to specify their position and their relations with other assets, properties like ‘parentObject’ must be consistent with the hierarchy structure of the GLB/GLTF file. Another important property that must be considered is represented by ‘placementRelTo’, if not empty, this property states that the position defined for the asset is not absolute (relative to the world reference frame in VEB.js), but relative to another asset. This property is helpful when, as in our case, there are different GLB/GLTF files that represents asset that we want to be in contact. For example, the cobot leans on a specific area of the base assembly structure and also the foam base balls is attached to it. To get to the desired layout, it is necessary to identify the contact points of assets and define them as ‘empty nodes’ in the JSON file (they are not nodes present in GLB/GLTF files and obviously are not parent of any mesh). These empty nodes can be considered just points, it is smart to define them as sons of the asset to which we want to attach other assets, finally the asset to be attached will be placed relative to these nodes in the JSON. In our scene JSON file ‘base_assembly.point_1’ and ‘base_assembly.cobot_plate’ are example of application of the described methodology.

The JSON of the environment is defined according to a specific schema (refer to <https://virtualfactory.gitbook.io/vlft/tools/vebjs/input-output>), for the scope of this project the most important elements is represented by cameras. Defining a proper position for the main camera, helps indeed to get the user in a position where it is possible to have the most complete view of relevant assets in the scene at first sight, without the need of moving and look for them in the environment.

![image20.png](assets/image20.png)

*Figure 18: the scene of the robotic cell loaded in VEB.js*

### 4.4 ROBODK, ROBODK API and Asset Info Panel functionality: 
ROBODK is an offline programming and simulation software for industrial robots, which represents a fundamental tool in our project work. Indeed, without such instrument it would be impossible for us to replicate and simulate the behavior of a robot. In ROBODK it is possible to import robot models, define a robot program, for example by teaching the robot poses and target positions and run it, the models can be downloaded from an [online library](https://robodk.com/it/library). In order to simplify the description of the tool, a video tutorial has been made by us for students, its aim is to provide an explanation of how to run simple programs with robot and replicate the steps we have done.

Another important characteristic of this software is that it has an API that supports python and other languages, it can be used not only for robot programming, but also for a lot of different purposes. Moreover, on ROBODK website there is a rich library with a lot of codes ready to be used (<https://robodk.com/doc/en/PythonAPI/index.html>). For our project work indeed, we have used an already present code, which is called Monitor Joints and it’s written in python language. When this code is run on ROBODK API it registers angles value of each joint during robot motion, both generated by a robot program running or by user inputs. When stopped, it returns a csv file with all the joints angles values expressed in degrees and an additional column with the time stamp corresponding to that joint configuration. The rotations of joints are expressed in degrees and are not absolutes, each joint rotation is expressed as relative to its joint predecessor in the kinematic chain. The rotation of the first joint is relative to the robot base reference system, which has to be consistent with the reference system defined in the scene in VEB.js, most of the time therefore, it is necessary to change the default reference system of a robot ROBODK model in order to make it consistent both in terms of orientation and position. By doing this, the output csv file contains proper values of the joints rotation and can be elaborated to generate an animation JSON file. In our use case we decided to generate a simple robot program, where the cobot of the robotic cell touches the four tennis balls, the first time with joints trajectory and the second time with linear trajectory and then go back to its resting position. In the video tutorial of ROBODK it is shown how to generate this specific kind of program.

To derive the target positions for this robot program, which are represented by the four tennis balls relative positions respect to the cobot base, a new functionality has been developed in VEB.js by Walter Terkaj, our project tutor and VEB.js developer. This functionality is called Asset Info Panel, it allows the user to select an asset, or a node (a part) of an asset as reference frame, then it is possible to select all the desired assets or assets nodes and exporting a JSON file. In this file the relative positions of the selected items respect to the selected reference frame are stored. This functionality allowed us to easily generate robot programs without the needs of deriving relative positions by computation. Moreover, it has an important value from the education 4.0 platform perspective, this concept will be deepened in next sections of the report.

### 4.5 Simulation of robot behavior (generation of animations):
The animation of assets in VEB.js can be defined according to a specific schema composed by four root properties:
- “context”: set of properties of initial setup for the whole animation
- “nodes”: an array defining what happens during the animation
- “sequences”: an array defining animation sequences
- “bookmarks”: an array defining bookmarks

To get more detailed information about the schema refer to the [gitbook](https://virtualfactory.gitbook.io/vlft/kb/instantiation/animations). For the scope of our project the most important elements are represented by animation events of type ‘show’ and ‘animation’, that play a role in the “nodes” root property and by animation sequences, defined in the “sequences” root property. The show event is used to make the robot assume the initial pose of the robot program, indeed by generating a show event and defining position and rotation properties, it is possible to impose a desired pose to the robot. The animation event ‘animationAdditive’ can be used to execute animation sequences, in the data property of this event it’s possible to put the ID of the animation sequence that we want to be executed when this animation event happens. To generate an animation that resembles a robot program it is necessary to exploit animation sequences. Sequences can be defined according to a specific schema that contains the following properties:
- "id": unique identifier of the animation sequence
- "FPS": integer defining the FPS (frame per second).
- "pos": array of positions in the 3D space for each frame
- "rot": array of rotations (as Euler angles YXZ in radians or alternatively as quaternion for "animation" and axis-angle for "animationAdditive") in the 3D space for each frame

Since our input file to generate the animation is a csv file, it is necessary to translate its content into the described JSON schema. After defining for each joint, a show event, aimed at making the robot assume the initial desired configuration, an animation event must be defined for all the joints. These animation events must be defined with the same timestamp value and with a data property equal to the ID of animation sequence. A sequence for each joint must be defined, since what generates robot motion is the rotation of joints and not their translational movement in the 3D space, the “pos” property can be defined as a three-dimensional array containing for each frame of the animation always the same position null vector. These positions are equal to zero because we are using the 'animationAdditive' animation kind event and therefore positions are incremental. The property “rot”, instead, is fundamental to generate robot motion, and must be defined in a proper way. We decided to use YXZ Euler angles in radians to fill this property, we will have therefore a three dimensional array containing incremental rotations in radians around X,Y,Z axis. Since in the csv file the rotations are expressed in degrees, we need to transform them in radians, then it is important to identify around which axis each joint rotates. For this reason, the orientation of reference system of each joint was defined in a consistent way starting from the CAD model. Sequences “rot” properties are therefore filled basing on the joints’ rotational axes. For example, if a joint N has its rotational center around the Y axis, its sequence “rot” property will be filled with zero radians for X and Z fields and with the transformed into radians incremental angles values coming from the csv file column joint N in the field Y. The generation of the JSON of the animation has been done using python language, using dictionaries and lists to build the data structure and through the library JSON. The commented python code is available in the project work material.



## 5. Project learning related outcomes
This section is meant to show how education 4.0 can be integrated and linked with the workflow of our project and its final outputs. We have combined the three reference learning frameworks (CBL, Kolb’s method and serious game) in order to generate possible exploitation of our work to provide learning outcomes for the students. The proposed methods can be divided into two categories: 
- Methods for advanced learning outcomes: mainly based on CBL and Kolb’s experiential learning method and related with laboratory and project works
- Methods for simple learning outcomes: mainly based on serious game and Kolb’s experiential learning method and related with the direct use of our digital twin combined with VEB.js functionalities

The first category of methods is focused on the digital twin generation workflow, rather than on the education 4.0 platform exploitation. Therefore, the main base behind these proposed methods is the explicitly codified knowledge acquired through our project work in this report, video tutorials and related materials. While the second category, is focused on the direct exploitation of the digital environment in VEB.js. Advanced learning outcomes can be achieved by students that replicate a similar workflow, while simpler ones can be provided by using and interacting with the digital environment. 

### 5.1 Methods for advanced learning outcomes:
**An example of Challenge Based Learning:** 
For this project we want to show how one can use CBL as a conceptual framework, with our robotic work-cell digital twin workflow as the foundation. This CBL approach can be used both in a lab activity or in a project work. This is based both on our experience from the JLL in Belgrade and from the acquired knowledge in the digital twin generation. We believe that it is a good solution for improving both the students' hard and soft skills. Hard skills for this CBL method can include CAD-modeling, programming and scene generation, while soft skills can include collaboration, creativity, communication, complex problem solving and critical thinking. 

*Phase 1: Selecting the big idea:* Considering the context and the scope of this work, the main topic must belong to production environment digital twin. It is important that the big idea is of a wide character, so that many areas can be pursued, and thereby maximizing the learning output. Also, the big idea should be directed towards a real-life challenge facing for instance the manufacturing industry. For our concept, we have chosen the following “big idea”:
> “Use of digital twins as a competitive advantage in manufacturing”

The idea is aimed at engaging the students to identify and promote various possible applications for digital twins in a manufacturing setting. The manufacturing industry is rapidly evolving, and for many companies (particularly in high-cost countries in Europe and North America), it is essential to continuously discover new ways to develop a competitive advantage.

*Phase 2: Identify essential questions:* by decomposing the “big idea” into several essential questions, the students will then as a group have a good starting point for charting their own knowledge developing through identifying and selecting their own challenge. 

*Phase 3: Agree upon a challenge:* Together with the help of professors and hopefully some external partnering companies and institutions, each group will have a distinct challenge to work on. The overall goal of this challenge and the subsequent solution/solutions is that the students, professors and external actors will increase their insight into the possibilities of the “big idea”.

*Phase 4: Micro-modules:* The aim of the micro-modules is to fill in the knowledge gaps through mostly individualized learning, and the core idea is that you only take a certain module if you feel you need to. For some modules, like for instance work-shops, it could be beneficial to make it obligatory. What we want to achieve with this, is that everyone in the group fully understands the challenge. The micro-modules can be organized in various ways, such as small lectures before or during the lab activity or material that can be accessed individually by students, micromodules can be both obligatory or not. Micro-modules can also be organized as small work-shops, where the students can test and observe various equipment, such as AR and VR headsets and robotic work-cells. For this framework concept, our workflow will serve as the foundation of most of these micro-modules. The modules seen in figure 20  are the ones that we consider to be most relevant. 

*Phase 5: Solution development:* the students will use their multidisciplinary knowledge, and explore how the challenge can be solved. It is important to have an open mind, be creative, explore different possibilities and evaluate the various possibilities identified. Through self-assessment and testing as an iterative process, one or a few solutions should be selected.

*Phase 6: Solution assessment:* both the final solution/solutions and the CBL-process that resulted in it should be assessed, evaluated and presented by the students. The group's findings should be presented to all the stakeholders that took part in the project/lab to maximize the learning output. The professors responsible should also conduct their own assessment of the CBL-process, and the degree of which the ILOs were reached. 

![image25.png](assets/image25.png)
*Figure 20: CBL process schema*


**An example of David Kolb's experiential learning method:**
We believe that using David Kolb’s experiental learning method as a foundation to learn students specific hard skills, can be a useful addition to the traditional lectures to increase the learning outcome. Also, it can be useful to use as part of a micro-module in the CBL proposed methodology. In table xx, we have prepared an example of this methodology regarding a digital twin scene generation.

| | Virtual laboratory exercise to develop student hard skills within scene generation using digital robot work-cell in VEB.js|
|---|---|
| Preparation | Before the activity starts, an introduction should be held to address the topic, and explain how it will be conducted. Source CAD models of the robotic cell should be provided and an introduction on CAD modeling, GLB/GLTF manipulation and on how to generate a scene in VEB.js should be helded. |
| Phase 1: Concrete experience | Let the students experiment with the steps involved in the process of a scene generation using the digital robotic work-cell, either individually or in groups. Professors and/or tutors should walk around aiding students that need it.|
|Phase 2: Reflective observation | Let the students discuss with each other and/or in plural with the professors. Example of questions that could be of interest to discuss and reflect around can for instance include: What was the biggest obstacle of creating a scene, and how should it be solved? What can be the practical applications of generating scenes of manufacturing settings? |
| Phase 3: Abstract conceptualization | Based on the reflective observation session, the students should now either individually or in groups use their gained knowledge to come up with new ideas. For this experiment, this could include: Creating a scene for a specific application and simplifying and optimizing the scene generation process into a standardized process.
| Phase 4: Active experimentation | From the previous phases, the student/learner has increased knowledge and experience from experimentation of scenes generation in a virtual environment and reflections on for instance its possible applications. This can then be applied later to their surroundings in various scenarios. If one chooses to run the experiment again, letting the student apply their abstract conceptualization, the circle (kolb’s experimenting learner model) will restart.|

### 5.2 Method for simple learning outcomes:
**An example of using the robotic cell digital twin in the learning scope:**
The following learning approach is a combination of elements belonging to the serious game learning framework and of the David Kolb’s experiential learning method. It is aimed at providing simple learning outcomes about robotics and robot programming to the students, following a procedure on a virtual reality environment that can hopefully capture the curiosity of the students and stimulate their engagement. 
The steps of this method can be summarized in the following schema: 
- Accessing VEB.js and loading the robotic cell digital twin
- Explore the virtual environment and analyze the simulation of robot behavior
- Ask students simple questions about robot trajectories to make them reflect
- Ask students to derive target points of the robot program
- Ask the students to generate a robot program with same target points on ROBODK and export csv file through ROBODK API
- Ask the students to generate via available python code the animation JSON file 
- Ask the students to load and validate the goodness of the animation in VEB.js

*Description of the proposed method:*
VEB.js can be accessed by the students via browser to access to our digital twin. After a brief introduction on VEB.js functionalities and on how to use ROBODK, students can start explore the virtual environment, run the animation and understand its characteristic. In the available animation indeed, the robot touches the four balls the first time by using joints movement, while the second time it uses linear movements. Understanding this first difference can make the students learn different way of motion that a robot can perform and start to reflect about their pros and cons. In this step the student could make or be asked to made consideration about which is the fastest and/or more efficient way for the robot to move. After this step, students can try to experiment with the new VEB.js functionality called Asset Info Panel. Through this functionality, it is asked to the students to derive the positions of the four balls as target points for a robot program. In order to achieve this goal, the students have to explore and experiment with VEB.js functionality. Moreover, it is needed to reflect and understand different topics such as reference systems, orientation of reference systems and input data necessary to generate a robot program. After this intermediate step, students are asked to generate on ROBODK a robot program in which the cobot moves along a trajectory that approach the four balls. This can make the students experiment and understand the logics of ROBODK and simple robot programming. After this step it is asked to the students to launch the generated robot program by using ROBODK API to generate the output csv file. The csv file can be quickly converted in an animation JSON file by using our code and finally directly evaluating in VEB.js the goodness of this program. This direct feedback can make the students understand if some mistakes has been done, and the goal of producing such robot program can make the students experiment and go deep in the learning. 

The proposed method combines elements  can provide students simple learning outcomes related with robotic related topics, in particular students can learn: 
- What is a robot kinematic chain and what are its relevant characteristics
- Concepts related to robot kinematics
- The role of reference systems and their orientation
- What are the relevant elements of a robot program
- How to create and run a simple robot program on an inverse kinematics software

Using serious game elements, it is particularly useful also to develop students’ soft skills like for instance complex problem solving. Our robotic work-cell does however have a somewhat limited use by itself, and the range of tasks connected to it is quite limited but can provide some relevant learning outputs. In addition to this, having developed this methodology on a virtual reality environment can stimulate students' engagement and curiosity. An approach like this could be a valuable addition to standard lectures, and as formerly mentioned regarding the Edgar Dale’s pyramid in figure xx, the experience of serious game could increase the learning output from 10% to potentially 90 %. The results of “Design of serious games in engineering education: An application to the configuration and analysis of manufacturing systems”, also clearly indicate that the students prefer a serious game to traditional lectures. Moreover, being VEB.js capable of supporting VR, this learning approach could also be experienced in a more immersive way by students, using for example VR headset. It would, of course, probably be more beneficial to implement the robotic work-cell, in a larger virtual environment to create a more immersive experience for the students and a wider range of possible tasks. Our digital twin of the robotic cell and the simulated tennis balls program is availabe upon request.

## 6. Final considerations and conclusions
The outputs of this project can be summarized in this schematic way:
- Generation of a robotic cell digital twin
- Development of robot motion simulation approach 
- CAD modeling manipulation video tutorial 
- Blender GLB/GLTF file manipulation video tutorial
- ROBODK robot programs generation video tutorial
- Production of relevant knowledge in digital twin generation workflow
- Development of learning approaches related with digital twin generation workflow and digital twin 

Despite not achieving some of the initial expected goals, we believe that the outcomes of the project have an important value in the academic context. Indeed, they can be used effectively to provide both advanced and simpler learning outcomes to the students. In addition to this, we have expanded the already existing education 4.0 platform VEB.js, by adding the new digital environment of the robotic cell. For this reason, we consider having generated an output of discrete value. Since our backgrounds were not completely aligned with the competences required by the workflow, we need to put ourselves in the learning of several new arguments and technological frameworks. Working with such innovative approaches, very close to research areas trigger our interest and curiosity and was an additional stimulus to put effort in the work. We consider this project very challenging but at the same time we perceive its great value in terms of hard and soft skills contribution and the enrichment it provides to our professional competences.

### 7. Future developments
This section is dedicated to discuss the limitations of some project results and to present possible future improvements that can be made on project work outputs.  

The first area that can be improved is related to the robot simulation approach, this is currently done by an intermediate python code that elaborates the csv file, however the ROBODK API Monitor Joints could be tuned in order to directly outputs the JSON animation file. This could make the simulation approach leaner and faster. This area could be further improved by establish a communication protocol between VEB.js and ROBODK. This would require the generation of a tailored ROBODK API able to send messages to the browser application, achieving unidirectional communication. The possibility of establishing also bi-directional communication, making VEB.js able to send messages to ROBODK should also be explored. The more suitable communication protocol to achieve this scope is probably represented by UDP, since it’s necessary to rapidly exchange data in order to have a smooth robot simulation.  

Another improvement that can be done on the simulation approach, regards the experimented workflow on real-industrial use cases. As already described, our approach may be not suitable to generate exact trajectories, in case of inconsistencies between the original cad model and the ROBODK model. This issue must be investigated in order to develop a scene generation method that can be animated using ROBODK outputs.  

Another area of improvement is represented by a more expert use of ROBODK, this tool is very powerful and can be exploited more to develop a better simulation approach. Indeed, in ROBODK it is possible to import stl files, the procedure of setting up an environment consistent with the one of the digital twin is quite complex, but can be achieved. This would allow to model parts that are not present in ROBODK such as special end-effectors and objects, with which the robot needs to or must not interact (better handling process simulation and avoiding of collisions). 

All these possible developments will generate a contribution in the virtual simulation of industrial processes in which complex robots are involved. This would generate benefits both from the industrial use perspective and from the academical one.



## 8. References
- <https://1drv.ms/v/s!Aic7GLlSn-birRYQz-EhIWp5YSxk?e=JkFUbN>
- <https://1drv.ms/v/s!Aic7GLlSn-birSp0mTXsbEaNT7uN?e=SRACHf>
- <https://www.eciu.org/for-learners/about>
- <https://challenges.eciu.org/for_learners/>
- <https://www.yaskawa.eu.com/products/robots/handling-mounting/productdetail/product/sia10f_710>
- <https://docs.fileformat.com/3d/glb/>
- <https://www.turbosquid.com/3d-models/industrial-robot-motoman-sia-3d-model/972809>
- <https://www.growthengineering.co.uk/kolb-experiential-learning-theory/>
- <https://virtualfactory.gitbook.io/vlft/>


















