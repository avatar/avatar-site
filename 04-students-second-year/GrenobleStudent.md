<!-- ---
layout: default
title: Towards digital twin creation
parent: 2nd Year Student Work
nav_order: 3
--- -->
## AVATAR - Digital Twins

![00](assetsgrenoble/image00.png){:height="600px" width="600px"}
## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Acknowledgements 

We had the opportunity to work on the project AVATAR Digital Twin for more than 8 months and this would not have been possible without the support of our professors and collaborators.  

We would like to thank our tutors, Frédéric Vignat and El-Haddi Mechekour for their advice and their continuous support. They were able to guide us on the right path when we were  having  difficulties.  They  were  also  honest  and  accurate  in  their  judgment  which prompted us to do some intROSpection.  

We would like to thank  Frédéric Noël who had the difficult task of being our teacher and  our  client.  He  provided  us  with  extensive  knowledge  for  the  project  without compromising his other role.  

We want to give special thanks to the teachers and students from the university of Belgrade and from Politecnico di Milano. They shared with us their culture , values, their skills and their vision on this project and we had a great time working with them in the joint learning lab.  

Finally we want to express our utmost gratitude to Camilo Medina who was a pillar in this project and without whom none of this would have been possible. It was a pleasure to work with him.

## Introduction 

During the first months of this project, we studied the different steps of creating a digital  twin.  We  familiarised  ourselves  with  virtual  reality  technologies  and  all  the  tools necessary to create a digital twin. By correctly dividing the different steps, we took in hand the Unity 3D software as well as all the assets allowing us to use it as a virtual reality engine. We also set up a connection between Unity and ROS in order to control the real system from the virtual system and vice versa. In addition, we took in hand the OptiTrack tracking system, as well as its integration in the virtual scene created in Unity. We also designed a digital double of our real robot that we implemented in a scene and made moves according to a pick and place scheme. 

All these steps of documentation and training were necessary so that we were all comfortable, on the one hand with the theory of the Digital Twin, and on the other hand with all the tools and assets that we would need during its development. 

We have recently started a new phase in our project, in which we are going to use all the knowledge acquired previously as well as the work done (setting up a ROS  - Unity connection, importing the robot using URDF files, etc...) in order to contextualise and give meaning to this digital twin. 

Indeed, we have thought about several possible scenarios of use of the Digital Twin of the 7 axis robotic arm (use case). These scenarios must describe how this digital twin will be used, what will be its added value. For each scenario, depending on the use that will be made of the digital twin, it is therefore necessary to establish the needs. Material needs within the virtual scene in which the robot and the user will be integrated, needs in terms of interaction (what will be the possibilities given to the user, what does he need to be able to do so that the scenario unfolds as desired), needs in terms of simulation as well (what events must happen in the virtual scene). 

It is from these needs that we started to establish several virtual scenes (one per scenario), in which we integrate the material as well as the metaphors of interactions and the elements wanted for the scenario to unfold as planned. 

This step of contextualising the digital twin in its environment is necessary to give meaning to the project, and constitutes a solid base from which we can create coherent and relevant virtual scenes. Later on, we will use the knowledge acquired earlier in the project to set up everything we will need (virtual robot linked to the real robot, tracking of the user with optitrack, ...)

## Context of the study 

The project "digital twin" that we have to realise this year is part of a larger project, the AVATAR project. This AVATAR project aims to facilitate the integration of virtual reality and augmented reality technologies in the engineering curriculum.   

This project has a European scope and is done in collaboration with three universities and a research center: The Polytechnic Institute of Grenoble (France), Polytechnic Milan (Italy), the University of Belgrade (Serbia), and finally the National Research Council STIIMA (Italy). 

![01](assetsgrenoble/img01.png)

Today, virtual reality and augmented reality technologies are in full evolution and their potential is great. They present a multitude of advantages and can be useful in many ways to engineers, students and teachers. They allow a better approach to engineering problems because they offer visualisation possibilities and a very realistic three-dimensional experience. In addition, learning via virtual reality modules can provide a deeper and more robust understanding of the problems being studied. Finally, both AR and VR can provide interesting and innovative tools for the evaluation of engineering student’s skills (practical evaluation on a complex system for example). 

Within the AVATAR project, each partner develops its own know-how, which will be freely available for the other partners or any other external contributor, with the aim of standardising the knowledge and the different approaches. In addition, the AVATAR project also aims to promote collaboration between young European engineers. 

In this framework, collaborative projects will be organised during which students will be trained in virtual reality and augmented reality technologies and will develop technological bricks to demonstrate the applicability of VR and AR in an industrial context. 

The goal of the AVATAR project is therefore on the one hand to train the students who take part in it on different areas of virtual / augmented reality, and also to propose a solid documentation base provided by the work of the students / professors / engineers who are part of the project. This documentation should help newcomers to the project as well as help external students or engineers to train in the field of virtual reality. 

Our Digital Twin project is also part of a parallel process: the design of a Digital Twin for a robotic arm. 

Digital Twins are virtual replicas of real objects or systems. They are at the heart of Industry 4.0 and are becoming more and more widespread and used. 

![02](assetsgrenoble/img02.png){:height="700px" width="700px"}

*Fig.1: First Idea on the project*  

Indeed, depending on the object of study (it can be a robot, a production line or even an entire company), and the phase in which it is used, it offers new and very advantageous possibilities. The use of a Digital Twin during the design phase of a product can, for example, allow problems to be identified quickly and limit prototyping costs. It can also enable predictive maintenance by anticipating future failures, or reinforce product safety. 

Digital Twins therefore offer many advantages and are also very well integrated into the training curriculum of engineering students. Indeed, the use of Digital Twin by students allows them to familiarise themselves with the world of virtual reality, which is still very little known and taught in schools, but also to be trained on machines in complete safety or 

to provide realistic remote training. This is all the more interesting in the current context of the health crisis, which has greatly increased the number of courses that must be taught at a distance. In this context, the use and manipulation of complex systems can be difficult or impossible without the presence of a Digital Twin. 

## Final requirement negotiated with the client

The need expressed by our customer was the following: 

To realise the functional Digital Twin of the 7 axis robotic arm present at the Industrial Engineering school. This Digital Twin had to allow a user to manipulate the robot in a virtual scene created by us, with the help of interaction metaphors that we had to define, in order to respond to a scenario that we had established.

A connection should be established between the virtual robot and the real robot, while another should exist between the real robot and the virtual robot (two-way connection). The main goal is to be able to use the digital twin for various applications, such as security training or simply training with virtual reality tools. In addition to the creation of this digital twin, our client wanted the entire work to be documented through a series of 6 workflows. A workflow had to be established for each step of the digital twin creation, and it had to be broken down into two parts. 

A generic part, based on a template established beforehand, explaining the needs to realize this step, the main lines, the tools etc. The purpose of this first part is to explain the procedure to follow to carry out this step of creation of a digital twin without entering in a choice of technical solution or system of study in particular, and this with the aim that external people, not working with the same tools or on the same robot, can all the same use the contents of the workflows. The second part of the workflows had to be a "usecase" part, in which we explain in detail the approach followed, the technical choices (software, robots, virtual reality hardware, interaction metaphors). 

This part had to be documented with photos, videos, appendices, to allow users to follow step by step the progress of our digital twin. The two distinct parts of these workflows should then allow readers to understand the main steps and the procedure to follow to realize a digital twin, while having an example of the technical choices that could be made. 

## Specification

### Background and problem definition 

Creation of a digital twin and documentation of the process through workflows.  

### Context 
The "Digital Twin" project is part of a larger project, the AVATAR project. The aim of the avatar project is to train engineering students in virtual reality technologies. Our project will focus on the creation of the digital twin of a 7-axis robotic arm as well as the writing of workflows allowing students to understand and ultimately reproduce our method.  

### Objective
Our long-term objective will be to create a simulation capable of emulating a pick & place that is faithful to the robot's behaviour and to document the process through 6 workflows, the content of which will be discussed below. 

### Project needs and constraints

### Functional requirements 

We need a product that allows students to have a first contact with virtual reality technologies and the concept of the digital twin. Our method must be able to be used remotely and extrapolated to other types of robots. 

### Constraints
We are constrained by time, as the project must be completed by May. We are also "restricted" to the equipment available in the G-Scop laboratory where we work. 

### Expected outcome at the end of the project 

Our work will be divided into two parts. 

A program part that can be tested by a user, as detailed and illustrated below.  

The user will be immersed in a virtual scene of the hangar type, in front of him is a table on which the Robot franka is placed. On the table there is an area labelled pick and an area labelled place. In the pick area there are small objects (a cube of 8 cm side and a sphere of similar size for example). The user clicks on the cube, then the user selects a tile from the grid in the same way, and finally presses a button which triggers the pick & place. The robot will then pick up the object and place it in the selected square. The user will also have access to instructions that will float beside the table. 

Another part in the form of a file dedicated to the 6 workflows that detail the method we used to develop our program. 

- Concept art 

![03](assetsgrenoble/img03.png){:height="500px" width="500px"}

*fig.2 : Concept art of the final product*

- Octopus Diagram 

![04](assetsgrenoble/img04.PNG){:height="600px" width="600px"}

*fig.3 : Octopus diagram*

To remain relevant in the functions of our table, we had to start from our objective: the digital twin, and then work our way back to its essential elements, which appear in the octopus diagram above. 

### Functional table

In our functional table we will use four levels of flexibility:  

- **Must have:** Essential and non-negotiable function of the product. 

- **Should have:** Function to be implemented as far as possible. 

- **Could have:** Function contributing to user satisfaction but not essential. 

- **Won't have:** Function that is beyond the scope of the project but could be integrated later. 


|**Nom** |**Service function** |**Criterium** |**Flexibility** |
| - | - | - | - |
|**FP1** |**Realisation of a pick & place scenario** |The pick & place takes place |**Must** |
|**FS 1**  |**Creating a virtual environment**  |||
|**FS 1.1** |Creation of a hangar |The hangar exists |**Must** |
|**FS 1.1.1** |Furnished stage for better immersion |Number of props n |**Should** 10<n<20 |
|**FS 1.1.2** |Spacious stage to allow for movement |Surface S |**Must** S>50m² |
|**FS 1.2** |The stage must contain a table for supporting the cobot |Presenting table |**Must** |
| - | :- | - | - |
|**FS 1.2.1** |The tables are of suitable size |Table surface S |**Must** S>=4m² |
|**FS 1.2.2.** |The table contains two gridded areas: "pick" and "place". |The zones exist |**Must** |
|**FS 1.2.3.** |On the table is the Franka robot and a cube and ball  |existence |**Must** |
|**FS 2** |**The user must be able to interact with his environment** |-Interactions with objects in the scene -User movements in the scene |**Must** |
|**FS 2.1** |Instructions displayed to the user |existence |**Must** |
|**FS 2.2** |The user must be able to interact with the scene (manipulate the props and implement pick & place) |Yes/No |**Must** |
|**FS 2.2.1** |The user must be able to interact with the scene using the Raycast |Yes/No |**Must** |
|**FS 2.2.2**  |The user should be able to interact with the scene with their hands |Yes/No |**Should** |
|**FS 2.2.3** |The user must be able to teleport anywhere in the scene** |Yes/No |**Must** |
|**FS 2.3.1** |The user must be able to see his hands |Yes/No |**Must** |
|**FS 2.3.2** |The user must be able to see the whole body |Yes/No |**Could** |
|**FS 3** |**The programme must link the software used** |Yes/No |**Must** |
|**FS 3.1** |Unity and ROS software communicate |Yes/No |**Must** |
|**FS 3.2** |Unity and Motive software communicate |Yes/No |**Should** |
|**FS 3.3** |The real robot and ROS communicate |Yes/No |**Could** |
|**FS4** |Pick & place |-Speed -Acceleration -Trajectory -Range of action -Travel time -Accuracy |**Must** |
|**FS4.1** |The maximum arm speed must be controlled |-speed cm/s |**Must** 20<v<40 |
|**FS4.2** |The maximum acceleration of the arm must be controlled |-acceleration cm/s² |**Should** 30<a<50 |
|**FS4.2** |The range of action of the robot must allow a wide range of movements   |-vertical and horizontal angle of action |**Must 80°< a°v <90°** 160°< a°h <190°**|
|**FS 4.3** |The movement of the cube must be brief |-travel time |**Should** t<3s |
|**FS 4.4** |The movement of the simulation must be faithful to the calculated trajectory.  |Deviation from the actual trajectory   |**Should** <5% |
|**FS 5** |Optitrack tracking works |Yes/No |**Must** |
|**FS 6** |Drafting of an optitrack leaflet |Existence |**Should** |
|**FP 2**  |**Drafting of workflows for each step** |Number of workflow n |**Must  n = 6** |
|**FS 6** |Workflows contain a "general" part |Existence  |**Must** |
|**FS 6.1** |The general part uses the established template (annex) |Yes/No |**Must** |
|**FC 7** |Workflows contain a "use case" part |Existence |**Must** |
|**FS 7.2** |The "use case" part contains photos / captures  |Yes/No |**Should** |

## Product presentation  

The product that we were able to provide at the end of this project is not completely in accordance with what had been previously established. Indeed, despite the work done, we were not able to finish the complete realisation of the digital twin of the 7 axis robotic arm. 

This can be explained by the fact that a long period at the beginning of this project was used to train on the tools that we were then asked to use. Indeed, the nature of the project and the skills required to realise a functional digital twin were somewhat out of our field of expertise and even knowledge. With the help of engineers already in place on the project (notably Mr. Sergio Camillo), we had to take in hand the various tools necessary to the realisation of the digital twin, such as Unity, ROS, the C## programming language, or all the virtual reality hardware (Optitrack, HTC vive, Oculus Rift). On top of that, a certain understanding of the "digital twin" concept was required to correctly understand the project's stakes in its entirety, and by the same token, to set up a fair and relevant project management. Because of this "start-up" phase, we didn't start the concrete work for the implementation of the digital twin until late in the project. Moreover, despite the acquired 

skills and a more accurate and precise understanding of the objectives and stakes of the project, many technical aspects of the digital twin still remain outside our field of competence. 

The product that we were able to deliver is the following: We set up a functional simulation of the 7 axis robotic arm. Within a virtual scene that we created, the user can interact with an exact replica of the Franka robot, and thus perform the pick and place task that was requested. A connection has also been created between the real robot and the virtual robot (ROS - Unity connection), but it does not allow at the moment to synchronise the movement of the real robot and its virtual twin. 

In parallel, we have established the required workflows, documenting all the steps we have gone through. These workflows are, as expected, divided in two parts: A part based on the template we have established, allowing the reader to know the main steps, the INs and the OUTs, and a second use case part in which we detail our approach, the chosen software, the choices made, etc. 

What is missing from the initial request of our customer is the presence of a real digital twin, that is to say the combination of a digital shadow (movements of the real robot transcribed on the virtual robot) and a physical shadow (movements of the virtual robot transcribed on the real robot). 

However, we had the opportunity at the end of this project, during the week of May 9 to 14, to go to Belgrade in Serbia, in order to work with Serbian and Italian engineers on the creation of a complete and functional digital twin, with as object of study a robot arm. During this week, we were able to learn a lot about the steps we were missing, in addition to discovering new working methods and new approaches. With the help of the information gathered and the knowledge acquired during this trip, we were able to get closer to our goal, namely a functional digital twin. 

## Technical and scientific work  

The creation of a digital twin goes through several major steps. The term digital twin defines a mutual connection between the real robot and the virtual robot, present in the XR environment. 

A simple connection between the real robot and the virtual robot is called a "digital shadow", while a connection between the virtual robot and the real system is called a "physical shadow". It is the combination of these two connections that forms what is called a digital twin: an exact replica of a system, integrated in an adapted virtual environment, which interacts continuously with the real system. Thus, the movements of one of the two systems (virtual or real) is immediately applied and retranscribed on the other system.     

![05](assetsgrenoble/img05.png){:height="600px" width="600px"}

*fig.4 : Difference between Digital Shadow & Digital Twin* 

The creation of this digital twin goes through several steps, all essential to its proper functioning. 

First of all, the creation of a virtual scene: the place in which the virtual replica of the studied system will be placed. This scene must be created according to the established needs, themselves defined by the use that will be made of the digital twin once it is completed. For that, we define scenarios of use of the digital twin, in which we list all the needs which allow its good progress. A virtual scene can be very simplistic if the scenario is simple. We will try to make a purified scene, in which each element has a utility, and we will avoid making a scene unnecessarily complex, not to disturb the user, and not to increase the workload unnecessarily. 

Once the virtual scene has been created, it is necessary to implement the interactions that will be necessary for the scenario to run smoothly. By interaction, we mean everything that it will be possible to do inside the scene for the user who will be immersed in it. Moving, grabbing an object, controlling the robot. In the same way as for the creation of the scene, we define the interactions to be implemented according to the scenario envisaged, in order to avoid setting up too many interactions which would then make the user's experience unnecessarily complex. 

In parallel to the development of the virtual scene and the interactions within it, it is necessary to implement a connection between Unity (or any other software used to create the virtual scene) and ROS (or any other trajectory calculation software). The ROS software allows to perform trajectory calculations, based on reverse kinematics, as well as on obstacle avoidance. The connection between ROS and UNITY allows, when the robot's gripper (or any other part on a different study system) is moved on Unity, to calculate the rotation that each joint of the robot must make to reach this position, and then to send these angles and rotation back to Unity, which applies them to the robot. It is thanks to this that when the user indicates "target coordinates" to the robot's gripper, the robot can get there by moving each joint in a certain way. The link between these two softwares is done through computer programs, generally in C## language.   

![06](assetsgrenoble/img06.png){:height="400px" width="400px"}

*fig.5 : Stages of the creation of a digital Twin*        

In parallel to these different stages, it is necessary to set up a system of tracking of the user, in order to retranscribe his movements in the virtual scene. This can be done in several ways, for example with a complete tracking system such as optitrack, which uses different cameras and combinations with multiple sensors. It is also necessary to create a human avatar, and to integrate this one in the virtual scene, in order to represent the user within this one. 

- ### Virtual scene  

The virtual scene we created was made with the Unity software. It is a relatively simple software to use (compared to some virtual reality or modelling tools which can quickly become very difficult to handle) and which offers all the necessary options to set up a simple virtual scene adapted to our needs. 

We created the scene based on the scenario of use that we had established for the digital twin, namely the realisation by the user of a simple pick and place. From this scenario, we established the material needs of the scene, which are relatively simple. 

![07](assetsgrenoble/img07.PNG){:height="222px" width="222px"}   ![08](assetsgrenoble/img08.PNG){:height="400px" width="400px"}

*fig.6: Virtual Scene*                    *fig. 7: Model 3D of the robot* 

Within the scene, it was necessary to import the 3D model of the robot arm. For that, it was necessary to export this CAD model in URDF format, a file that allows a model to keep the kinematic information and thus allows the robot, once integrated into the scene, to be able to move and function in the same way as the real robot. Then, to ensure the "Pick and Place" task, two tables were imported to the scene from the Sketchfab.com website, and the robot will be placed on another table between the two to adjust the height of the two tables so that the pick and place task is.  With the aim of increasing the precision of the pick and place, we defined places (blue square) in the table which is going to receive the object, these places are the places where it is possible to put an object inside, by respecting the constraint of the length of the arm. 

![09](assetsgrenoble/img09.png){:height="222px" width="222px"} 

*Fig.8: Grid for positions* 

Indeed, the robot does not manage to transport the objects to the farthest squares of the list, but we can solve this problem by decreasing the size of the tables and also the dimensions of the cubes to place. 

![09](assetsgrenoble/img10.png){:height="600px" width="600px"} 

*fig. 9: Final virtual Scene* 

- ### Interactions  

To be able to follow the scenario of use of the digital twin established beforehand, it is necessary to set up a certain number of interactions, in particular so that the user can move and interact with its environment. 

The first interactions to set up within the virtual scenes are the possibilities of displacement given to the user. Indeed, the user must be able to move easily within the set up scene, and these metaphors of displacement must be simple and intuitive. In addition to the simple movement  caused  by  the  user's  movements  in  space,  we  decided  to  implement  a teleportation system. With the help of a laser cursor integrated in the user's joysticks, the user can access any place in the scene without moving physically (some users may have a physical movement space in which movements are limited). Obviously the zones in which the user  can  teleport  are  defined  beforehand  to  avoid  that  the  user  leaves  the  scene.  This teleportation is done via an add in integrated on unity and a C## code. In addition to that, we have implemented a continuous movement system, which is more realistic. With the help of joysticks, the user can move his avatar in the virtual scene, and this by controlling the speed of movement and of course the orientation. This movement metaphor is also implemented through a C## code. (You will find the codes in the appendix) 

The next step was the implementation of an object input system for the user. Indeed, the user must be able to move some objects in the scene (objects that must be defined in advance). For example, the objects to be moved by the robot during the pick and place operation must be able to be moved simply by the user if he wishes to perform another pick and place from one or several different positions. Therefore, we have implemented a simple pick and place system: With the same laser system integrated in the user's joystick, the user can pick up objects from a distance (when they are in the laser field) and move them away / towards him in the direction of the laser, using the joystick. With this simple interaction system, the user can simply grab and move what they want (without necessarily being next to them) within the scene. However, we only made some objects movable to avoid the user moving objects that should remain fixed. 

Finally, we have implemented a control system for the robot to make it perform the desired pick and place. Interaction metaphors can be multiple and more or less complex, depending on the desired task. Here, it did not seem useful to implement a real control system of the robot (using joysticks and/or more or less complex metaphors). The user must simply target (still using the laser pointer) the starting object to move. Once the robot has grabbed the object, the user must then target an arrival location, where the robot will then drop the object (as long as this location is within the robot's range). This robot control system is extremely easy to use and allows you to realize the desired scenario. 

At the same time, we added a code to prevent the robot from hitting other objects that could potentially be placed around the object to be targeted by the user. This code tells the robot by which movement to retrieve the object in order to avoid a collision with the accessible and potentially occupied areas surrounding the target object. We will talk later about the system we have implemented via ROS to avoid potential collisions while the robot is moving.  

- ### Virtual reality equipment 

The virtual reality material we use is central because it is thanks to it that our project makes all its sense. We mainly worked with the HTC Vive HMD (Head mounted display) which allows us to use them wirelessly which contributes to the immersion. 

The virtual reality helmets work by using the principle of stereoscopy which consists in displaying two images slightly shifted to each eye to simulate an impression of relief.  To correctly follow the movements of the helmet there are several solutions. The first is to cover the helmet and the controllers with infrared emitters that will be detected by receiver bases that will be able to calculate the position in space of the hmd. 

![11](assetsgrenoble/img12.png){:height="300px" width="300px"} 

*fig.10: Head-mounted display one*  

The other solution is to use cameras to locate the environment and the controllers. Finally, for helmets dedicated to professionals, you can combine the two methods for maximum precision. 


![12](assetsgrenoble/img11.png){:height="300px" width="300px"} 

*fig.11: Head-mounted display two*  

The controllers we use work with the same principle and are equipped with several push or touch buttons that allow them to interact with the 3D scene. 

- ### Robot Operating System (ROS) 

During this project we had to use the ROS software, which gathers a set of tools allowing us to design specific applications for robotics. ROS (Robot Operating System) is essential to be able to control both the real and the virtual robot. To do so, we had to communicate ROS with the Unity software, used to visualize our digital twin in its virtual environment. In order to use the different ROS tools and compose our programs controlling our virtual and real robots, we need to create nodes. Nodes are unique executable files that communicate and/or receive data and process it. They are made up of C++ or Java code directly by the ROS software when the application we have created is called. 

```js
#include "ros/ros.h"
#include "std_msgs/String.h"
/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg→data.c_str());
}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "listener");
  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;
  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * The second parameter to the subscribe() function is the size of the message
   * queue.  If messages are arriving faster than they are being processed, this
   * is the number of messages that will be buffered up before beginning to throw
   * away the oldest ones.
   */
  ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);
  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */
  ros::spin();

  return 0;
}
```
*fig.12 : Empty "subscribed" and documented node provided by the ROS documentation* 

A ROS application is therefore composed of a series of nodes that process and share data with each other. There are different types of communication between nodes in ROS. It is divided into 3 main types of communication: 

1) **Subscriber Editor Topic:** 

In this configuration, one node (the publisher) publishes data (topic) on a continuous or non-continuous basis and different nodes (subscribers) retrieve the information. However, this configuration is only unidirectional. It is ideal for processing sensor data, for example. This is unidirectional UDP communication, which is advantageous because of its great speed and the few steps it contains. It happens in two steps: the publisher sends the information then the subscriber receives the information. 

2) **Service server service client :**

This  configuration  is  different  from  the  previous  one.  In  this  configuration,  the initiative is taken by the node that receives the data (service client). The service client sends a request to the node that holds the data. The latter then processes the data and sends it (the service).  This  method  of  communication  is  almost  immediate  and  involves  only  one repetition. It is therefore ideal when using requests that are specific to a button or that only need to be triggered a few times. Nevertheless, this method is slower due to more steps to be completed before the client receives the information. Nevertheless, the discussion is bidirectional, we speak of TCP communication. It takes place in 4 steps, the client sends a request for information, the server receives it then processes it. It then sends information that the client then receives and processes. 

![13](assetsgrenoble/img13.png){:height="500px" width="500px"} 

*fig.13 : Exemple d’un cas service server appliqué à notre cas d'étude* 

3) **Server action client action:** 

This configuration is close to the previous one. The client action makes a request and the server action processes it, but unlike the previous configuration, the server action returns to the client action until it satisfies the request. This case is used in a specific context that we unfortunately do not encounter.

The link between Unity and ROS is done by creating nodes. For the system, we have created a total of 7 nodes.  Our first node reacts to the button press (server service) and "wakes up" the program on the ROS side (client service). A node on the ROS side (server service) makes an information request. With the help of a package added to unity a node (editor and client service) retrieves the information from our virtual system (positions of the different joints of the virtual robot, mass, position of the target, button pressed...). This same node  sends  this  information  and  a  trajectory  request  to  different  nodes.  Another  node (subscriber) retrieves them, processes these data and calculates the trajectory that the robot must follow. A third node (action server) receives this trajectory and communicates with the unit (action client) so that our robot can move. In total, 7 nodes are thus connected to allow the movement of our robot. 

![14](assetsgrenoble/img14.png){:height="600px" width="600px"} 

*fig.14: Our virtual robot performing a pick and place* 

The next step in order to carry out the pick and place simulation of our robot was to set  up  the  collisions.  Fortunately  for  us,  the  program  we  use  to  simulate  the  robot's movement takes collisions into account as long as our different objects have been declared in ROS. So we added to ROS 2 new Nodes allowing us to declare the existence of an object that can collide with the robot. 

Although the pick and place of our digital robot has been implemented, the relationship between the digital robot and the real robot has not yet been implemented. Nodes collecting and processing information that can be collected from the real robot need to be added as well as nodes communicating the trajectories we would like to achieve. 

We have already tried to implement a node collecting information from our real machine with success. The next step will be to turn this information collection into a real discussion between the two platforms. 

- ### Simulation  

The simulation was done with the help of the ROS software. It is possible to carry out trajectory calculations directly on unity and thus to proceed directly to the simulation of movement on unity. Nevertheless the use of Unity and VR without trajectory calculation are already trying for our machines. Adding a trajectory calculation could add long latency times or even slow down the computers we use. 

We also found in the literature other projects using the robot we use. They had satisfactory results when using unity and ROS together for motion simulation. So we decided to do the same and to use ROS for the trajectory calculation. 

For the trajectory calculation a series of publishers and subscribers are used and will be explained in the part specific to ROS. Nevertheless, to understand how the trajectory calculation of our robot works, we must be aware of the existence of several reference points in the architecture of our robot. Our robot is a 7 axis robot. It has 7 independent parts (the famous  axes)  all  linked  by  pivots.  These  joints  are  set  in  motion  by  actuators  (motors controlled in position). 

![15](assetsgrenoble/img15.png){:height="600px" width="600px"} 

*fig.15: Ilustation Joints of The robot*

Now we want to move the TCP (tool center point) of our robot, that is the end of the gripper. In order to establish the movement of this TCP, in a Cartesian reference frame, we need to establish the relationship between the rotation of each of these pivots and the final position of this TCP. We must therefore proceed to the inverse kinematics. This is done in our case by the ROS software. 

When a new trajectory is requested, ROS retrieves the position information in our global Cartesian frame, and determines, in an optimal way, what angles each of the robot's joints must have in order to reach this position. Unity then proceeds to the motion simulation. We get the values of the different angles and we make our actuators move to reach them. So we have a communication of angles at regular intervals informing the robot of the next move to follow. This arrangement remains interesting from our point of view. The choice of speed of the different robot elements or the physical elements of the different axes are controlled by unity. So we can interact directly with our robot with a single software if ROS can run in the background. 

![16](assetsgrenoble/img16.png){:height="600px" width="600px"} 

*fig.16: First Simulation of pick and place* 

Nevertheless, our solution remains one of many. For example, during our training in Belgrad,  we  had  the  chance  to discover  a new  way  of  doing  things.  Indeed,  during  the elaboration of their project, the Serbians admitted to us that they do not use ROS in the creation of their Digital Twin. They have a different solution, they use RobotDk, another calculation  software  for  robots  which  works  differently  but  which  also  allows  them  to simulate the trajectory of the robot. 

- ### Optitrack  

From the beginning of the project, we understood the potential of the Optitrack motion capture system. Having the ability to track the user's movements made sense because the cobot is designed to work in tandem with a human in a shared environment. In fact, for the experience proposed by our product to be relevant, it was important that we could demonstrate the feasibility of the human avatar. Thus, we first set ourselves the goal of tracking the user's movement and then allowing him to see himself in the virtual environment with the HMD. This requires a connection between the Motive software (proprietary software of optitrack which allows to capture and record complex motion tracking sequences) and unity. 

Concerning the hardware, we have at our disposal 6 optitrack flex 13 cameras which are professional mid-range cameras and which allow to track the movements of the user in a scene the size of a room.  

![17](assetsgrenoble/img17.png){:height="300px" width="300px"} 

*Fig.17: Optitrack flex 13 cameras* 

This system is particularly interesting because of its precision and low latency but it forced us to learn the use of new software and motion capture equipment, subjects that are very unfamiliar to us and far from the curriculum offered by GI. 

We have recently acquired a complete motion tracking suit that allows us to follow the slightest movements of the user. It consists of a cap, a long-sleeved shirt, pants, shoes and gloves. The fabric allows to hang trackers which are white reflective balls that will be detected by the cameras. In total we have 53 sensors that allow us to have a follow-up of all the members as well as the fingers. 

![18](assetsgrenoble/img18.png){:height="300px" width="300px"} 

*Figure 18 : Motion capture suit* 

We thought about how to make this system useful to our digital twin. 

This system being new, we also had the mission to write a user manual which will allow those who read our workflow and would like to follow them to install the optitrack system without any worries. It can be found in the appendix. 

## Continuation of the project 

With the current progress of the project it is possible to observe in VR the behaviour of our cobot during simulations of trajectory operated by Unity and ROS. Nevertheless, due to lack of time, we have not yet reached the final goal of the project, namely the creation of a digital twin. 

If colleagues, future engineers of GI would like to proceed to the continuation of this project, they will have to take care of the link between digital robot and real robot. The two versions of our cobot being currently existing and functional, the creation of a bidirectional link between the 2 is still to be implemented. To do so, we advise them to try to control their robot using ROS and encourage them to take inspiration from the various bibliographic links that we provide at the end of this report as a starting point. These links present projects similar to ours and can be used as a starting point for a state of the art of the continuity of this project. 

## Workflows

As previously explained, in addition to the implementation of the digital twin of the robotic arm, we have during this project created a series of workflows explaining each step of the creation process. We had to create 6 workflows, corresponding to the following 6 steps: XR scene, interactions, ROS - Unity, simulation, optitrack, and finally … 

However, due to the delay we encountered and are facing at the end of this project, it was impossible for us to establish the workflows concerning the steps we did not validate. Therefore we were able to deliver the following workflows: Virtual scene, interactions, optitrack and ROS connection - Unity 

The workflows were realized in the following way: 

A first general part, based on a common template. This part must allow any reader to understand the stakes of this step, the main lines that constitute it, as well as to know the INs (what is necessary to start this step, data, 3D models), the OUTs (what is obtained once this step is correctly done), and the TOOLs (tools necessary to realize this step, software, hardware). We voluntarily constituted these "general" parts without basing ourselves on a precise study system (7 axis robotic arm), nor without going into details of the hardware or software used. 

Following that, we also set up a second part "usecase", in which we take again the great stages quoted in the general part, and develop, for each stage, the system of study which was ours, the software used, but also the technical choices which we made. These use case parts are accompanied for the most part by photos, screenshots and diagrams, and are written in a more narrative way. The objective of these parts is to allow the reader to retrace our path in the implementation of a digital twin, and to have a concrete example of application of the "general" part of the workflow, and of choices and possible solutions. 

The 4 workflows we have written are available in the appendix. 

### Workflow 1 : Creating the XR environnement for robot

|Acitivities       | Overview         | 
|:-------------|:------------------|
| A1 <br> Scénario  | -	Description: Identification of the use scenario (tasks to be performed by the user) and choice of technologies to be used accordingly (some technologies and devices allow more detailed or more precise movements) <br> -	Input: Studied system, action to be performed <br> -	Output: Device selection, Usecase scenario <br> -	Tool:  | 
| A2 <br> Needs | -	Description: Identification of material needs within the virtual scene (movable objects, fixed objects). <br> - Input: Usecase scenario, studied system. <br> - Output: Requirements for the virtual scene: physical objects to be integrated. <br> - Tool: | 
| A3 <br> XR scene | -	Description: Creation of a virtual environment in adequacy with the usecase scenario, from the needs identified during the previous phases. <br> - Input: scenario, physical needs, studied system. <br> - Output: XR scene <br> - Tool: 3D engine (Unity, …) | 
| A4 <br> 3D model | -	Description: import the 3D model of the studied system into the virtual scene and ensure its proper physical functioning <br> - Input: XR Device (output A3) and expected perception. <br> - Output: [Bowman] Selection, Manipulation, Navigation - <br> - Tool: |


- **Scénario**

In our case, the study system is a 7 axis robotic arm, present in our industrial engineering school. We have to set up a functional digital twin of this robotic arm. The scenario of use on which we base ourselves is simple: the robot will have to carry out a simple action of pick and place. The user will have to be able to move in the virtual scene in which he will be immersed, and to interact with the robot as well as with the objects present in the scene as well as with the robot to make it carry out a pick and place (seize one of the objects present in the scene and bring it to a position which will be requested by the user). 

- **Needs**  

From the scenario of use of the robot that we have established, we have been able to make a list of the objects needed in the scene. An important point is to correctly analyse the scenario in order to be able to propose a relevant scene, in which the available objects allow the good development of the scenario, without making the user's experience unnecessarily complex. To perform the pick and place action, we decided to integrate in our scene 3 tables, whose 3D models would be retrieved from the internet. One of these three tables will support our robotic arm, while the other two will serve as supports for the objects that will be moved by the robot. The scene will take place in a warehouse, and will be limited to one room. We will also add different objects, all relatively simple (cubes, spheres), which will then be placed on the tables, and which the user will be able to use to perform the pick and place. 

- **Scene creation**  

From the needs established according to the scenario of use of our robotic arm, we were able to set up a relevant virtual scene, allowing us to perform the desired pick and place. The scene was created on the Unity software. We downloaded a 3D model of a warehouse from the sketchfab website. In the same way, we downloaded a table model which seemed to suit us. The layout of the tables must be such that the robot, placed on the middle table, has access (from its range of action) to the entirety of the two other tables. We also positioned the user in the middle of the room when he arrived in the virtual scene, and prevented him from accessing other rooms. The imported objects (tables, warehouses) all have similar physical properties: immobile and rigid. Finally, we added several small objects, positioned on one of the three tables. They will be moved by the robot during the pick and place process. 

![19](assetsgrenoble/img19.png){:height="300px" width="300px"}

*Figure 19 : Scene in Unity* 

- **3D Model** 

Finally, we had to add the robotic arm to our scene. To do this, we retrieved the CAD model of the robotic arm (assembly). We had to export this CAD model in a URDF file, a format that allows an assembly to keep its kinematic properties when reused. After taking care of some versioning problems, thanks to this, the robot imported on Unity works in the same way as the real robot. The connections between each joint of the robot are consistent and realistic. This 3D model of the robot was then placed and fixed on the central table of our scene. 

### Workflow 2 : Connection ROS - Unity 

|Acitivities       | Overview         | 
|:-------------|:------------------|
|A1 <br> ROS Preparation  | -	Description: Prepare a virtual machine in Linux, to be connected to a real scene with the ROS tool, following the instructions described below. <br> - Input:  Linux Machine <br> - Output: Linux Machine with ROS installed <br> - Tools: Linux, ROS … | 
|A2 <br> Unity Preparation  | -	Description: Try to install the necessary packages to ensure the connection with the already prepared virtual machine. <br> - Input: Virtual Scene in Unity <br> - Output: Unity ready to connect <br> - Tools: Unity| 
| A3 <br> Connection ROS - Unity | -	Description: Launch the different steps to have a subscriber ROS that can receive answers from the publisher created in unity, through services prepared in unity. <br> - Input:  ros and unity prepared <br> - Output: Connection establish. <br> - Tools: ROS, Unity | 
| A4 <br> Implementation | -	Description: Import components to a virtual scene, with a robot ready to be used, as well as launching the systems that will manipulate this robot. <br> - Input: URDF model of a robot <br> - Output: <br> - Tools: CAO, Unity |
| A5 <br> Check Implementation | -	Description:  Test the connection between the two scenes, by means of basic pick and place tests, or even have a position in the virtual scene and try to clone it in Unity within a precise time margin. <br> - Input: Two connected scenes<br> - Output: Criteria met <br> - Tools: Unity, ROS |

**- ROS Préparation:**

ROS (Robot Operating System) is a software that gathers a set of tools to design robotics applications. In order to control both our real robot and our virtual model. ROS uses several tools to ensure a communication with the client, we define then :  

- Publisher & Subscriber ; 
- Request (works only one time client ask for something and then get response from the serve) 
- Response (feedback until the action is done)  

The different steps to follow to prepare ROS start with :  

- Install ROS (Follow the tutorial on how to install ROS on a virtual machine); 
- Create a workspace & Define a Service; 
- Run the service and build a request  
- Integrate ROS TCP connector in the ROS workspace. 

**- Unity Preparation:**

In order to manage to connect Unity and ROS, we need the tool that captures the connection established with ROS :  

- Add the ROS TCP connector package (Click On Windows  →  Package Manager  →  click on +  →  add package from git URL\*) 
- Add the ROS IP address of the virtual machine (Robotics  →  ROS Settings  →  Add your ROS IP) 

**- Connection ROS - Unity:**

To establish the ROS - Unity connection, we start by: 

- Adding ROSConnectionPrefab to the current virtual scene ; 
- Build the ROS service and message in Unity; 
- Run the ROS Connector Launcher with the command "ROSlaunch ROS\_tcp\_endpoint.launch".  
- Launch the scene on Unity  

In order to understand how the link between Unity and ROS works, we have put together this diagram which details the different output and input of the two tools. 

![20](assetsgrenoble/img20.png){:height="550px" width="550px"}

*Figure 20 : sctructure connexion ROS/Unity* 

**- Implementation:**

Let's start by explaining the format of the cao file, it is the urdf (Unified Robot Description Format), an XML format allowing to describe in a standardized file a complete robot. The described robot can be static or dynamic and physical and collision properties can be added. To perform this task, we need to import on unity an Urdf import package ( Click On Windows  →  Package Manager  →  click on +  →  add package from git URL), then import the robot from an already downloaded urdf file. 

To distinguish the movement of the robot, Unity needs to capture its different parts, for this, we will assign each component to its corresponding part in the scene.  

A controller is pre-built in the Unity URDF Importer to help showcase the movement of the robot. The Controller script is added to the imported URDF by default. This will add robot and Joint Control components at runtime. 

**- Check Implementation:**

To test the connection between the two virtual and real scenes, we can make the robot placed in the real scene move from a fictitious tool identified in the virtual machine, this starts by :  

- Installing the Moveit tool in the virtual machine, using the command sudo apt install ROS-melodic-moveit  
- Clone the robot configuration to share access to the features with moveit. 

If everything is done correctly, we can open the moveit on ROS by the command ROSlaunch panda\_moveit\_config demo.launch, a window will appear with the same robot model: 

![21](assetsgrenoble/img21.png){:height="600px" width="600px"}

*Figure 21 : Vizualisation Franka Emika in Rviz* 

At this stage, to have the same position of ROS in unity, we have to create a service in Unity which will ask ROS for the position of each joint of the robot, (Follow the tutorial of creation of a service). To check if the movement is consistent between the two scenes, we just have to execute.   

### Workflow 3 : Optitrack


The OPTITRACK workflow defines the steps students need to carry out to create a tracking area, track the movements of the user in order to animate a 3D avatar that serves as their digital shadow in Unity. 

The focus of this workflow is not to create a polished, fluid and operational avatar but to provide a visual representation that : 

- Allows the client to imagine the final product 
- offers the user a way to see himself while wearing the HMD 
- Allows the user to interact with the environment, with his body rather than with the controllers  

|**Activities** |**Overview**|
| - | - |
|A1 <br> Installation |- Description: If the lab or school isn’t equipped with a dedicated motion tracking area, it is essential to set up one. The goal will be to explain the different steps needed to efficiently set-up the Area. It basically boils down to 4 steps : <br> 1) Installing the different cameras  <br> 2) Installing the software <br> 3) Connecting the cameras to the software <br> 4) Calibrating the whole system <br> - Input : Empty room, required hardware <br> - Output : Motion Tracking ready area. <br> - Control : Tracking feasibility, calibration tests.  <br> - Resource: AVATAR’s Optitrack installation guide|
|A2 <br> Motion tracking |- Description : To properly track the movements of the user, a motion capture suit is required. Depending on its specifications it can track more or less refined movement (for example the movements of the fingers) <br>  - Input : The user movement. <br> - Output : A user setup adapted to motion tracking. <br>  - Control : Suit wearing instructions, instructor’s supervision. <br> - Resource : Motion tracking capable cameras (Optitrack Cameras) , Motion capture suit (optitrack suit) , Motion tracking software (motive). |
|A3 <br> Skeleton animation/Motive |- Description : Now that the area and the user are good to go, we need to track the user’s movements in motive. The user will have to follow a couple steps for that : <br>  1) create the skeleton with the differents markers <br> 2) Start the recording <br> 3) Do the desired actions <br> 4) Stop the recording the user can then use either the pre-recorded movements or have his movements streamed live. <br> - Input : Input from A1 <br> and A2 <br> <br> - Output : Animated skeleton<br>  - Control : Frame rate <br> - Resource : Motion capture software (Motive)|
| A4 <br> Data transfer |- Description : The motive software is heavy to run, to avoid latency we use a second computer for unity. Thus we need to have a fast connection between the two computers. Also we need to use the Optitrack to Unity plugin. <br> - Input : Input from A3 <br> <br> - Output : Animated skeleton in Unity <br> - Control : Skeleton’s fluidity and fidelity. <br> - Resource : Data transfer protocol, wifi routers/ethernet cables, two computers, Optitrack for Unity plugin |
|A5 <br> Avatar animation/ Properties  |- Description: Now that the Avatar is moving, we can add some properties to him like collisions or gravity so that it can interact with its environment <br> - Input : Input from A4 <br> <br> - Output : Interactive digital shadow of the user. <br> - Control : Users ability to touch or move assets in the scene. <br> - Resources : 3D Engine (Unity)  |

### Workflow 4 : Interaction 

|**Activities** |**Overview**|
|A1<br>Création of an avatar for the user in the virtual scène<br>|- Description :<br>Creating an avatar for our user in the virtual scene. For doing so we have to. <br>Create à game object that will set the position of our user in our scene<br>1) Set a camera that will allow our user to see with his casque the virtuelle scene<br>2) Putting both of them in a Xr Rig that will connect those to our VR casque. <br>  <br>- Input:  Users position (opti-track système for exemple), controllers, Vr casque <br>- Output: A representation of our user in the virtual scene being able to interact with its environment <br>- Tools: Interaction manager, Input action manager<br>|
|A2<br>Defining User’s metaphor of  interactions <br>|- Description :<br>Defining the différent type of interaction we allow the user to have with his environment <br>1) How will he move himself within the virtual scene? We can for example allow him to teleport or configure a continuous movement both controlled by his controller.<br>2) How will he interact with the different game objects present in the scene? Will he be able to grab them or to push them? If he is able to grab objects will he be able to do it from afar (using a ray) or will he have to touch them to grab them ?<br><br>After defining them we have to set them. We can do so by setting an interaction manager and an input action manager and adding them to our Xr Rig <br>- Input:  Users position(opti-track système for exemple), controllers, Vr casque <br>- Output: Description of the metaphors of interactions <br>- Tools: None<br>|
|A3<br>Setting User’s metaphor of interactions<br>|- Description : After defining the different types of interaction of our user with his environment we have to set them. We can do so by setting an interaction manager and an input action manager and adding them to our Xr Rig<br>- Input : Description of the metaphors of interactions<br>- Output : A representation of our user in the virtual scene being able to interact with its environment <br>- Tools : Interaction manager and an input action manager<br>|
|A3.5<br>(If we chose the teleportation as a metaphor of interaction) <br>Define and install an area of teleportation<br>|- Description : If using teleportation as an interaction metaphor of deplacement for our user, we have to define an area in which our user can freely teleport. For doing so we can either add to our scene a teleport area or a teleport anchor. They both let us freely teleport in a limited area of the virtual scene. But the teleport anchor always teleports us in a fixed position in our area while the teleport area lets us freely navigate in the area defined by the teleportation area.<br>- Input : <br>- Output : <br>- Tools : teleportation area / teleportation anchor<br>|
|A4<br>Adding the ground<br>|- Description : We have to add a plane for our scene objects to rest on  to be sure that our game objects don’t fall under the ground<br>- Input : a plane<br>- Output : A ground for our scene<br>- Tools :  Game object <br>|
|A5<br>Interactable Grab <br>|- Description: To interact with an object in our virtual scene we have to first add some component to this object. We have to add to it a rigid body, a box collider and a XR Grab Interractable<br>- Input: Gameobjects<br>- Output: interactable objects<br>- Tools: Rigid bodies, box collider, XR Grab Interactable<br>|
|A6<br>Socket<br>|- Description: Let us place an object to a precise position when entering a collider. For doing so you have to create a game object, add it a collider and a socket interactable referencing another position. It than automatically teleport any object to the position referenced whenever it enters your collider<br>- Input : Collider, a position<br>- Output : an area in which your object is directly teleported to the referenced position <br>- Tools: collider, socket collider<br>|
|A7 <br>Clickable buttons<br>|- Description:  To create buttons you have to first add and set a canvas and an event systeme. To this event systeme you have to add a XR UI input module to your event systeme and both ui reticle  and tracked device graphic  to be able to interact with it in vr. Then add buttons to your canvas. For those buttons a C# script can be added to add functions to those buttons<br>- Input: Buttons, canvas <br>- Output: Clickable buttons than have referenced actions<br>- Tools: Canvas, buttons <br>|

## Appendices 

**Optitrack Highlights**

The Optitrack motion capture system is a set of tools that allows you to track and record your movements. With its different hardware solutions and an easy to use software, optitrack is the way to go for any project involving motion capture. 

Our main goal is being able to use the optitrack motion capture system in order to track the movements of our user. The movements will be used to animate the 3D representation of the user inside our simulation.  

To get started, we will have to follow a couple of steps.  

Here we will provide a quick summary of the official Quick Start Guide you can access[here](https://v30.wiki.optitrack.com/index.php?title=Quick_Start_Guide:_Getting_Started##Hardware_Setup)

**Hardware setup** 

**Capture Area**  

You will first have to prepare the capture area.  

It’s recommended you minimize any ambient light, especially sunlight. 

The capture area shall be void of any unnecessary obstacle especially if they are reflective.  Every reflective surface in the capture area shall be covered or taped to reduce reflections. 

**Cabling and Load Balancing** 

Follow the instructions provided in the Quick Start Guide for cabling. *Placing and Aiming Cameras.*  

To compute the 3D coordinates of our target, the tracking system uses multiple 2D images. For best results, it is recommended that every camera is disposed in such a way that they all capture a unique vantage of the capture area.  

Place the cameras all around the capture area and aim them so that every marker is visible by at least 2 cameras. The cameras must be secure, any movement after system calibration may require recalibration.  

For more details see also :[ Camera Placement ](https://v30.wiki.optitrack.com/index.php?title=Camera_Placement)and[ Camera Mount Structures.](https://v30.wiki.optitrack.com/index.php?title=Camera_Mount_Structures) 

**Lens Focus** 

To obtain accurate tracking data, it is crucial that all the cameras are properly focused to the target volume. For our purposes, focus-to-infinity is fine. However it’s important to confirm that every camera  is properly focused. To adjust or to check camera focus, place some markers on the target tracking area. Then, set the camera to raw grayscale mode, increase the exposure and LED settings, and then Zoom onto one of the retroreflective markers in the capture volume and check the clarity of the image. If the image is blurry, adjust the camera focus and find the point where the marker is best resolved. 

For more details see also :[ Aiming and Focusing ](https://v30.wiki.optitrack.com/index.php?title=Aiming_and_Focusing)

**Software setup** 

Please follow the installation instructions.System Calibration  

Please follow the calibration instructions Recording Data

To record your data select the live mode and push the red button to record. Once you’re satisfied with your movements press the red button again.  

To recover the recording simply go to your files and go to recent recordings. 

**Skeleton Creation and Markers placement** 

You can place markers on various objects or on someone thanks to the motion tracking suit. 

The markers must be securely attached.  

To create a skeleton go to the builder pan, go to skeleton creation options, and choose the full body and hands marker set (54 markers). It’s important to know that there are two types of markers. They appear differently on the skeleton preview.  

**Optitrack plugin** 

Download the Motive to Unity plugin from this[ link.](https://optitrack.com/support/downloads/plugins.html##unity-plugin) 

Once downloaded, open your unity project then start the plugin. Unity will ask you to import the plugin assets, select import all.  

Once the plugin is installed, you will be able to use the capabilities of OptiTrack in Unity. 

Go to Assets → OptiTrack→Scenes and open “Opti Track Scene” 

Go to Edit → Render Pipeline → Universal Render Pipeline → Upgrade Project Materials to UniversalRP Materials and render the scene. 

In the scene’s Hierarchy you will find a camera, lighting as well as what’s called Origin. 

In Origin, you will find a default skeleton and a rigid body. Click on Origin in the hierarchy. In the inspector you will find the Optitrack streaming client. The client allows you to connect to motive through your wifi connection. See more down below in Data Streaming. 

Now click on the skeleton. In the inspector you will find the Optitrack skeleton animator. You MUST write the same name in Skeleton Asset name in Unity and in Motive (Case-sensitive).   

**Data Streaming** 

To stream data between two computers, you must open Motive on one computer and Unity on another.  

In Motive, open the streaming settings and set the transmission type to unicast. Set the  

In Unity, in the optitrack streaming service, set the Server address to the IP address of the computer running Motive (In our case 192.168.137.1) and set the local address to the IP address of the computer running unity (in our case 192.168.137.239) 

## Bibliography

1. [“Chapter 04 Important Concepts of ROS - YouTube.” https://www.youtube.com/watch?v=HuKgOs59mbk&ab_channel=ROBOTISOpenSourceTeam (accessed May 18, 2022). ](https://www.zotero.org/google-docs/?jpcH4b)
1. [“FRANKA EMIKA  Support.” https://support.franka.de/ (accessed May 18, 2022). ](https://www.zotero.org/google-docs/?jpcH4b)
1. [“Introducing: Unity Robotics Visualisations Package,” *Unity Blog*. https://blog.unity.com/manufacturing/introducing-unity-robotics-visualizations-package (accessed May 18, 2022). ](https://www.zotero.org/google-docs/?jpcH4b)
1. [“OptiTrack Documentation Wiki - NaturalPoint Product Documentation Ver 3.0.” https://v30.wiki.optitrack.com/index.php?title=OptiTrack_Documentation_Wiki (accessed May 18, 2022). ](https://www.zotero.org/google-docs/?jpcH4b)
1. [“Pick and Place Tutorial — moveit_tutorials Kinetic documentation.” http://docs.ROS.org/en/melodic/api/moveit_tutorials/html/doc/pick_place/pick_place_tutorial.h tml?fbclid=IwAR3TQgzpOITMteEsagpoR73KLzYVIsHXSpDNUKv-Eq1efcUTnutX2v4i7SA (accessed May 18, 2022). ](https://www.zotero.org/google-docs/?jpcH4b)
1. [“Robotics simulation in Unity is as easy as 1, 2, 3!,” *Unity Blog*. https://blog.unity.com/technology/robotics-simulation-in-unity-is-as-easy-as-1-2-3 (accessed May 18, 2022). ](https://www.zotero.org/google-docs/?jpcH4b)
1. [“ROS/Tutorials - ROS Wiki.” http://wiki.ROS.org/ROS/Tutorials?fbclid=IwAR3TQgzpOITMteEsagpoR73KLzYVIsHXSpDNUKv- Eq1efcUTnutX2v4i7SA (accessed May 18, 2022). ](https://www.zotero.org/google-docs/?jpcH4b)
1. [“ROS Unity Integration,” *XR Resources*. https://vision-r.gricad-pages.univ-grenoble- alpes.fr/xrresources/RobotOperatingSystem/ROSUnity/ROSUnityIntegrations.html (accessed May 18, 2022). ](https://www.zotero.org/google-docs/?jpcH4b)
1. [U. Technologies, “Unity - Manual: Scripting.” https://docs.unity3d.com/Manual/ScriptingSection.html?fbclid=IwAR1Jee1Yzzo3O_ReWIlu9vW nf3SeWbYzEVVHT6DAgZH7lees1HBy_GkmgKc (accessed May 18, 2022). ](https://www.zotero.org/google-docs/?jpcH4b)

---

## Project Management Report

### Distribution of tasks

During this project, we used a division of tasks that was based on the main steps of the digital twin creation that we had to realize. In the same way, the different documents and project management tools were distributed among the different members of the group. It was indeed possible for us to work almost until the end on our respective subjects without sharing information. This organization by poles was beneficial for us because we did not depend on the others to progress and the "learning" part was done quite simply. 

Concerning the creation of the digital twin, at the beginning of the year, we all followed a similar training with Camillo, the engineer who was in charge of guiding us in this project. At the end of this common work, aiming at integrating the sufficient knowledge to work on the manufacturing of the digital twin, we established the different steps that we would have to realize. The steps are the following: 

Virtual scene and environment - Interaction - Simulation - Tracking and human avatar - Connection ROS - UNITY 

We assigned each task to a group member (or two for the most complex tasks). It is worth noting that some of these tasks run in parallel, while others cannot start if the previous ones are not completed. If one of the members of the group finished one of the parts, he could be asked to help the other members on their respective tasks if it was possible. In addition to that, each member was in charge of writing the workflows corresponding to his part. 

For the organization of the writing of the deliverables, we each had a project management element (management, risk analysis, cost and state of the art) and we were in charge of writing the parts we were working on.

| Activity      | Responsible       |
|:-------------|:------------------|
| Gantt      | Mohamed |
| State of art | Florian   |
| Risk Analysis | Anass|
| Requirement | Ryan|
| XR Scene| Florian|
| Interactions| Anass|
| ROS - Unity Integration| Mohamed|
| Simulation | All |
| Optitrack| Ryan|
| Testing | All |


### Appointment and communication with the client

As our client had a busy schedule, it was not possible for us to set up weekly meetings with him to discuss the project. However, we were in the presence of Mr. Sergio Camillo every week during the sessions, who was very often in contact with our client, and therefore had the possibility to inform us of potential changes. We also decided to arrange our schedule so that it would be coordinated with Mr. Camillo's availability. In fact, he was the one who guided and assisted us in the work we had to do. And sometimes we organized meetings outside the different sessions of the project. 

During the last month of the project we regularly organized a second or third working session with Mr. Medina. 

### Shared documents

In order to properly process the data we were to use in this project, we made several choices regarding storage platforms and sharing modalities. 

Technical documents, project management documents, or simply the reports and workflows that we establish, were stored in a google drive, shared with all the members of the group. The choice of this platform to store "light" documents is explained by its ease of use. Indeed, it allowed us to store in a simple and judicious way all the documents that we used during this project. And it allowed us to easily participate to the writing of the same document. 

For the documents made in the framework of our project, the heavier documents such as our VR scene or the 3D model of our robot we decided to use a different sharing space. It is a place where we put our VR scene, our different programs and applications necessary to the use of our project. This choice is justified by the important size of the documents to transport but also by the already existing compatibility between unity and git simplifying our backups and recovery of our project. 

### Task planning - Gantt analysis 

The weekly update of the glove was one of the most difficult tasks of our project. Indeed, due to the great complexity of our project and the lack of initial skills, we had only vague estimates of the tasks to be done, often wrong. We thought of redoing the Gantt chart, since now we are aware of the duration that each task can take, the final result is filed in the appendix.  

The last tasks were difficult to realise, because we need a lot of experience in the physical interaction with the real robot. Indeed, we were not able to realise a digital shadow (real robot  →  Unity), because, technically, the connection between the real robot in Gi's lab, and Unity, requires a frequency of 50Hz, so we didn't have enough time to make a command for a computer that can respond to this condition. We also noticed that the realisation of a digital twin is not simple, since the movement (Input) will be sent from Unity to the real robot, and there is no guarantee that it will not be dangerous on the group members. So we decided to prepare a perfect "Pick and Place" simulation and to leave the realisation of the connection between Unity and real robots to the students who will be able to approach this subject in the next few years. 

### Risk Analysis  

From the beginning of the project, several problems were identified by the members of the group, which can be divided into internal problems (between the members of the group), and external problems (in relation to the project's environment). On the one hand, we tried to minimise the effects of the foreseeable risks, proposing solutions appropriate to the seriousness of the risk, on the other hand, the unforeseeable risks were hard to prevent and anticipate, but each time, we posed plan B as a second solution, so as not to disrupt the progress of the Project. Let's take the example of the following risk: "Unavailability of the customer", we proposed that we keep in touch with our customer Mr. Noel by email, especially since his needs were not clear at the beginning, to the point of not having weekly meetings. The purpose of risk analysis is to mitigate the impact of risks on operations 

At this point of the project planning, we can well name the most dangerous risks on the final result of our project, indeed the major obstacle is that of the specifications, it is obvious that the specifications were requested at the beginning of the project, but the need of our customer was not clear enough for us, because its expectations were a digital twin that will meet an industrial need (Pick & Place) and thus the drafting of workflows explaining the process of creating this twin, in order that any stakeholder can understand how it works and the steps of creation. The need of the writing was recovered without taking into consideration the creation of the digital twin. The validation of the cdcf was very important to us, so we exchanged many versions of the cdcf with our client, in order to clarify his expectations and also to build a clear and precise picture of his needs. The last version of the cdcf is very detailed compared to the previous versions.  

We can say that the most dangerous risk was the one of planning, because we found a lot of problems to manage the time factor well, even with the total respect of the Gantt chart. In fact, we misattributed the duration to the selected tasks, because this field of Digital Twin is totally new for us, we didn't have much knowledge and we knew exactly how much time the tasks set can take. 

The following table shows the risk analysis made during the whole project: 


|ID|Risks|Proba|Level|Criticality|Risk Mitigation|Work around|
| - | - | - | - | - | - | - |
|1 |The absence of the fifth member of the group|3|2|6|Sending reminder emails, Holding Zoom meetings|share the tasks of the absent member among the other members|
|2 |The end of face-to-face courses, and the switch to Zoom|1|3|3|Respecting barrier gestures and setting up a remote work strategy|Meet at sb's house|
|3 |The absence of a strong machine that meets the IT needs ( ROS, Unity .... )|1|1|1|Inform the school of the possibility of needing a computer|Borrow a Gi computer|
|4 |Delay in delivery of the real cobot to GI|2|3|6|Insist on fast delivery of the cobot, send emails to the manager|Wait for delivery and operate the 5-axis machine|
|5 |The unmet duration of each task|2|1|2|Be punctual and check the status of the tasks every weekend|work during the vacations|
|6 |Not finding the CAD model of our cobot available|1|3|3|Ask the cobot supplier for the CAD model|Search for other suppliers of the model, to find the model|
|7 |The need for materials not existing at the school|1|2|2|Stay away from rare materials,|Trying solutions where we will not need these materials|
|8 |Exceeding the proposed budget for this project|2|3|6|Work with the resources available at the school||
|9 |Stock shortage of any hardware we might need to buy|1|2|2|Take into consideration the safety stock of materials|Trying other solutions|
|10 |Departure of a member of the group on exchange abroad|2|2|4||Share the remaining tasks with the rest of the members|
|11 |No longer having access to the VR room|1|3|3|Be responsible when using the VR room|Install the necessary tools in another room|
|12 |The disappearance of the 5 axis machine|1|2|2|Ask the other group to keep it still and be responsible when using it|if the cobot is present, we use it, if not, we choose another system to use|
|13 |Having a very busy customer|2|1|2|Send check-in emails and request more meetings|Meet with the customer once every two weeks|
|14 |Having exams and deliverables at the same time|2|2|4|anticipate the tasks we have during exams|Work in addition on weekends and at any time off|
|15 |Loss of communication between group members|1|3|3|Be respectful and share tasks according to the desire of each member|Have personal meetings / Be understandable|
|16 |Having the characteristics of the virtual model not identical to the real one|1|2|2|Make accurate measurements and double- check calculations|Modify the characteristics of the virtual robot|
|17 |Need to have a very advanced level in CAD|1|3|3|Review CAD design courses|Divide the tasks on the members that are adapted to the CAD|
|18 |Lack of accurate information on the creation of the digital twin|1|2|2|Summarise theses/ review last year's report|Self-informing about the features needed for our tasks|
|19 |To be outside the expectations of the customer|2|3|6|Meet with our client on an ongoing basis|Literally follow the specifications|
|20 |Obligation of an interesting computer level|3|2|6|Training in important languages such as Python C##.|Ask for some help from programmers|
|21 |Building workflows that are difficult to understand by anyone|1|2|2|Detail complicated scientific topics to the maximum|Modify the entries of the workflows|
|22 |Not finding an interesting goal for our digital twin|1|3|3|Think more about the needs of the school/ Be creative|Follow the objective suggested by our tutors|
|23 |Client's absence |1 |2 |2 |Schedule meetings and send emails ||
|24 |Specifications not validated |3 |3 |9 |Ask for the customer's agreement for each modification |Try to detail as much as possible the client's needs |
|25 |Departure abroad |1 |2 |2 ||Keep in touch by video |
|26 |Have other projects in parallel |2 |2 |4 |Anticipate work to minimise losses  |Complete the tasks to be done during free time |
|27 |Not meeting the customer's requirements |2 |3 |6 ||Ask the customer for the criteria for each requirement  |
|28 |Lack of communication between members |1 |1 |1 |Try to complement each other  |Share each other's views and failures |
|29 |Lose commitment to the project |1 |3 |3 |Helping group members at every opportunity  |Recall the commitment signed at the beginning |
|30 |Falling behind the Gantt |3 |2 |6 |Try to recover the delay  ||
|31 |Missing workflows |2 |2 |4|||
|32 |Delay of the delivery of the optitrack equipment |2 |2 |4 |||

### Cost analysis  

The cost analysis purpose is to estimate if a project is worth doing and if it can generate profit or at least be within the allocated budget if not for sale. Our project is obviously closer to research than to the creation of a fungible product so it would not make sense to try to estimate the cost of “one digital twin”. However it is still interesting to evaluate how much this whole project cost AVATAR which has a limited budget. 

First we need to take into account the cost of our labour. Supposing we all are working in a R&D lab and are all qualified engineers, we can estimate that we earn 57 euROS an hour and that on the 30 weeks span we got, M.Medina worked 20h a week on the project (it is indeed his job) and that each of us worked 6 hours a week. This all amounts to 75240 euROS.  

Then we took into account the cost of equipment (hardware and software)  

As we can see, our project is one of the more balanced ones concerning the costs of 

equipment vs labour. Indeed, our project is hardware heavy, we work with bleeding edge equipment and it all requires a good computational power. Moreover, bar Motive which is a professional grade tracking software, every software we use is free of charge as long as we don’t plan on making money with our work. This allows us to save a lot on software however this would not apply to a company willing to implement a digital twin in their workflows. In the end we expect our project to cost 75k€ for labour and 45k€ for equipement, respectively 62% and 38% of the total cost.  


![22](assetsgrenoble/img220.PNG){:height="800px" width="800px"}

*Figure 22 : Cost Analysis* 

### Experience feedback

This Digital Twin project has been very formative and enriching for all the members of our team, for several reasons. 

First of all, a project of this type, taking place over a whole year, simulating the work of engineers at the service of a client to respond to his request, strongly helps to have a more global and accurate vision of the major steps to be taken and validated in this type of work. We will have learned to set up all the important elements of project management, such as a GANTT, a risk analysis, a cost analysis, or a functional and realistic specification. Realising all these elements according to a project in which we are concretely engaged for a long period of time allowed us to apply the knowledge we had acquired during the previous years in a concrete way and close to a professional framework. This was not without effort, as we had a lot of difficulty in proposing functional specifications that suited our client. This is mainly due to the complexity of the project, which was, for the most part, outside our field of knowledge and expertise. 

This is what also made the project so rewarding for us. Where some project topics deal with areas in which we are already trained and competent (mechanics, prototyping, manufacturing techniques, etc.), our project topic, the "digital twin", dealt with several areas in which we were, at the beginning of the year, only beginners. In addition to the initial dimension of the project board, which is meant to be professionalising, realistic, we also had the opportunity to learn a lot about the world of virtual reality, its use and contextualization in the world of engineering, as well as about digital twins, which are a very powerful tool and which will be, in a few years, at the heart of industry 4.0. We are very grateful to have been able to work in the heart of this field, and to learn so much about it, as it seems to be a great asset for our professional future. Even if we have not perfectly integrated all the knowledge necessary to establish a fully functional digital twin (because it represents a lot of knowledge and skills), we are now perfectly able to understand the stakes of the implementation of a digital twin, and to situate each step of this implementation. 

Finally, we had the chance to go to Belgrade for a week, during the week of May 9 to 15, in order to work with Serbian and Italian students on the implementation of a complete and functional digital twin. This experience was very enriching for all of us. First of all for the exchange and the pluriculturality that marked this week. We worked for a week with foreign engineering students, discovered new approaches, potentially new "culture" of work, in addition to obviously perfecting our level of English by putting us in a total immersion situation. Moreover, we were able to use and take advantage of all the knowledge and skills we acquired during the project, in addition to being able to compare them with those of the other students. Indeed, we all worked together on the implementation of the digital twin of a robotic arm similar to ours, and we could see that where the main steps were similar to our work, some approaches and / or solutions implemented were different from those we had used. This allowed us to have a more peripheral and open vision of the process of creating a digital twin. 

Finally, from a purely personal point of view, this project also taught us a lot about teamwork and the division of tasks, in its good and bad aspects. Working together on such a complex project, in which we had for a long time difficulty to situate ourselves and to approach with hindsight, was very formative. All in all, we made a lot of missteps, whether on the project management or coordination side, but in our opinion, this is what made this project so special and so instructive. It is certain that if today we had to start this project from the beginning, we would approach it in a much more organized and meticulous way, keeping in mind the mistakes made during this year. And this is surely one of the objectives of the project board: to make mistakes in a project of this magnitude, to understand them and assimilate them in order not to make them again in a future project. 

### Appendices : Gantt chart

![23](assetsgrenoble/img23.png){:height="800px" width="800px"}

*Figure 23 : Final Result* 