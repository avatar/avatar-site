---
title: Home
layout: default
nav_order: 1

---



![Avatar logo](Assets/0-home/00-avatarlogolarge.png)

# Advanced Virtual and Augmented Reality Toolkit for Learning

## 2020-1-FR01-KA203-080184

[![License](https://img.shields.io/badge/License-CC%20BY--NC%204.0-lightgrey.svg)](LICENSE.md)

---

This book is relative to the AVATAR ERASMUS+ project. The book reports the AVATAR activities through the description of workflows which helps a new developer in XR to build a coherent project but it also provides guidelines for trainers who want to teach XR development. Both are illustrated by results of student projects developed during the AVATAR project.

## Objectives

The main objective of the AVATAR Erasmus+ project is to support and facilitate the adoption of VR/AR technologies in engineering curricula. Within the set of advanced digital technologies in fact, AR and VR have the following characteristics and advantages:

- AR is able to provide a more understandable perspective on engineering problems, using a three-dimensional experience (VR) to leverage on visuo-spatial ability of the students.
- AR is the right tool to deliver meaningful set of information to specific geometrical locations, providing the students a deeper understanding.
- VR/AR-enabled telepresence enables high-quality distance and self-learning, being able to provide the students a rich experience even if they cannot access laboratorial facilities and/or get in touch with real components.

To pursue this objective a set of workflows will be developed, describing and formalizing how to implement learning activities in engineering taking advantage of VR/AR tools. The workflows will be organized in three clusters:

- Learning approaches for experiencing/developing VR/AR technologies to support the use and implementation of AR/VR tools (e.g., how to build a 3D scene for VR, how to prepare and organize contents for VR/AR, how to address interactivity in VR/AR).
- Learning approaches using AR/VR within integrated engineering approaches to provide a better and deeper understanding of engineering problems (e.g., in the design of a manufacturing system and/or in the definition of detailed trajectories in robotized assembly processes).
- AR/VR as an innovative environment for learning assessment addressing address how to implement assessment tools (exercises, quizzes, polls, etc.) within VR/AR environments.

Within a very evolving technological context (i.e., VR/AR/Robotics), every XR specialist has developed his own know-how that will be made available within a common standard environment to support integration and interoperability of different approaches. The four AVATAR partners have technological facilities for VR, AR, manufacturing and robotics usable for training, developing and testing the AVATAR workflows.

The AVATAR partners engaged themselves to share the methods and results in open access for any potential user to contribute in this direction for non-profit purposes. This book is thus provided in open-access mode for the interested communities under the license.

[![License](https://img.shields.io/badge/License-CC%20BY--NC%204.0-lightgrey.svg)](LICENSE.md)

Beside this, AVATAR also aims to build a collaboration culture between young engineers in Europe working together in a multidisciplinary and international environment. Collaborative projects were organized where students were trained to Virtual Reality and Augmented reality technologies by developing some technological bricks to demonstrate the applicability of VR and AR within an industrial context.

---

## Partnerships

This training program is taking benefit of the research effort of the four partners in the last decades to transform it as efficient curricula content. 

![Logo of the 4 partner institutions](Assets/0-home/01-universities-logo.png)

[comment_text]: # snt
*Logo of the 4 partner institutions*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt

---

## Book Layout

---

[comment_text]: # snt

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/00-main/01-workflows.html"  class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next →</div>
        <div style="color: blue;">Workflows</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}
</style>

[comment_text]: # fnt
