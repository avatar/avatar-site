I"�1<h1 id="a5---system-integration--operation">A5 - System Integration &amp; Operation</h1>

<p>The SYSTEM INTEGRATION &amp; OPERATION workflow defines a set of activities that students need to carry out to make developed Manufacturing DT System fully integrated, ready to be deployed and operated. Therefore, the goal of this activity is threefold:</p>

<ol>
  <li>integration of HW and SW components of all Building Bricks (A1 to A4), with a physical manufacturing system (such as a CNC machine tool, robot or similar) and thus creating a ready to deploy Manufacturing DT System with built-in HMI function based on XR</li>
  <li>deployment of the integrated system - bringing the integrated physical and virtual system to a fully operational state and fine tuning, and</li>
  <li>demonstration of its function and performance, which includes:
    <ul>
      <li>basic operations of the deployed system and testing its functional performances,</li>
      <li>execution of assigned operational tasks (like robot programming by XR demonstration, for instance) and, through all of the above,</li>
      <li>gaining basic practical experience and skills in using manufacturing DT technology with built-in XR HMI function.</li>
    </ul>
  </li>
</ol>

<p>These goals are intertwined, because each step of system-integration is always accompanied by performing partial operational activities to check the correctness of installed HW and SW components and to check the performance of the achieved functions. Accordingly, each step requires the following sequence to be performed:</p>

<ol>
  <li>installation of mechanical and software modules, i.e. their integration into the system created in the preceding steps, and</li>
  <li>
    <p>revival of the achieved stage of the system integration process (bringing it to a functional state, partially or in whole), that requires the following iterative activities to be executed:</p>

    <ul>
      <li>activation of integrated modules (HW and SW),</li>
      <li>quality assessment - testing of added sub-functionalities, and then,</li>
      <li>if quality is NOT GOOD, performing needed adjustments and eventual correction of the physical hardware and/or related software, until the desired performances of the added sub-functionalities are reached,</li>
      <li>if quality is GOOD, then go to the next step of system integration.</li>
    </ul>
  </li>
</ol>

<p>Obviously, this is an iterative and optimization procedure, typical of any engineering work. This means that only after the successful completion of the above-mentioned sequence, the next step can be carried out and thus gradually progress towards the full physical integration of the designed Manufacturing DT System. In some cases, since system integration is not necessarily a linear (uncoupled) process, students should take a few steps back to perform necessary corrective actions that were omitted because they were not previously recognized as potentially important.</p>

<p>When all the integration activities are completed and all subsystems are brought to the desired operational state, the integrated manufacturing system will be deployed, and after that, ready for the final demonstration and experimentation.</p>

<hr />
<h2 id="workflow-structure">Workflow Structure</h2>

<table>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>CONTROLS:</b><br /> Building Bricks specs - Manufacturing Equipment specs - Required performances - Safety rules and measures.  </td>
    <td></td>
  </tr>
  <tr>
    <td><b>INPUTS:</b><br /> Manufacturing system HW and SW - Building Bricks A1 to A4 - technical specs.</td>
    <td style="width: 65%; text-align: center;"><img src="assets/014_workflow0.png" alt="scenecreation" style="display: block; margin: 0 auto; max-width: 100%; height: auto;" /></td>
    <td><b>OUTPUTS:</b><br /> XR based manufacturing DT System tested </td>
  </tr>
  <tr>
    <td></td>
    <td style="text-align: center;"><b>RESOURCES:</b><br /> XR development SW - Manufacturing system specs - Technical docs - SW development tools</td>
    <td></td>
  </tr>
</table>

<p><em class="fs-3 text-grey-dk-000">System Integration &amp; Operation workflow</em></p>

<hr />
<h2 id="workflow-building-blocks">Workflow Building-Blocks</h2>

<table>
  <thead>
    <tr>
      <th style="text-align: left">Activities</th>
      <th style="text-align: left">Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left"><a href=""><strong>A5.1</strong> <br /> XR Platform <br /> Integration</a></td>
      <td style="text-align: left">• <strong>Description:</strong> Integration of the XR Platform hardware and software. XR system configuration, anticipating integration of basic Building Bricks for developed Manufacturing DT System.  <br /> • <strong>Input:</strong> Computer hardware and software, XR hardware and software, technical specifications and docs <br /> • <strong>Output:</strong>  XR Platform ready for basic Building Bricks integration (A1 to A4, with a content that depends of the selected manufacturing use case). <br />  • <strong>Control:</strong> Integrated system assessment - checking performances, settings and adjustments. <br /> • <strong>Resource :</strong> Computer hardware and software, XR hardware and software, technical specifications and docs, Guidance provided by Lab technical staff <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><a href=""><strong>A5.2</strong> <br /> Building Bricks <br /> Integration</a></td>
      <td style="text-align: left">• <strong>Description:</strong> Integration of previously created Building Bricks A1 to A4 into the Basic XR Platform of the designed Manufacturing DT System, Building Bricks interlinking and integration, setting up and preliminary testing of the integrated XR system behaviour and performances, adjustments and fixing errors, tuning up and similar. <br /> • <strong>Input:</strong> Executable Building Bricks A1 to A4. <br /> • <strong>Output:</strong> Fully functional XR System with integrated Building Bricks A1 to A4 <br /> • <strong>Control:</strong> Compatibility of bricks, requested specifications / performances of XR environment <br /> • <strong>Resource:</strong> Building Bricks specific resources, SW development tools for Bricks installation, integration and interlinking</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href=""><strong>A5.3</strong> <br /> Digital Twin <br /> Integration</a></td>
      <td style="text-align: left">• <strong>Description:</strong> The activity which should provide real-time interactivity of the XR System with  a counterpart physical system of the chosen manufacturing use-case. This includes integration of the HW and SW components for data acquisition and exchange (comm interfaces installation and configuration) – Digital Shadow creation, installation of simulation applications and digital control equipment programming. <br /> • <strong>Input:</strong> Fully operational XR System, specifications for manufacturing DT building, tech docs, and similar for manufacturing HW and SW <br /> • <strong>Output:</strong> Manufacturing DT system (including Digital Shadow) fully operational and ready for deployment, including dedicated SW for real-time data acquisition for system/operator-behaviour recording.  <br /> • <strong>Control:</strong> Initial specifications of requested performances, compatibility of XR system and manufacturing system control HW / SW <br /> • <strong>Resource :</strong> Open architecture control system of manufacturing hardware, network and multiprocess communication tools, dedicated tools for programming and setting up of manufacturing equipment control systems and cell controller, dedicated simulation SW.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href=""><strong>A5.4</strong> <br /> DT System <br /> Deployment</a></td>
      <td style="text-align: left">• <strong>Description:</strong> First run of the integrated manufacturing DT system, performing basic functions, checking performances, adjustments and corrective activities at the system level, fine tuning-up. <br /> • <strong>Input:</strong> Fully integrated and deployment ready Manufacturing DT System for the selected manufacturing use-case with built-in XR HMI function <br /> • <strong>Output:</strong> Deployed Manufacturing DT System with built-in XR HMI function, ready for use. <br /> • <strong>Control:</strong> Testing primitive functions related to the Building Bricks functions and for the bidirectional interaction of the XR-based DT and the physical manufacturing system counterpart, according to the selected use case and its usage scenarios <br /> • <strong>Resource:</strong> The same as for A5.3</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href=""><strong>A5</strong> <br /> DT Demonstration <br /> and Operation</a></td>
      <td style="text-align: left">• <strong>Description:</strong> Training students to perform basic operations on the developed Manufacturing DT System and conducting planned experiments. Recording the behaviour of students using the developed Manufacturing DT System for future technical and pedagogical quantitative evaluation. <br /> • <strong>Input:</strong> Deployed manufacturing DT system (cnc machine tool, robotic system, assembly line or the like) ready for use  <br /> • <strong>Output:</strong> Recorded data for future evaluation  <br /> • <strong>Control:</strong> Safety measures <br /> • <strong>Resource:</strong> Technical support of laboratory staff.</td>
    </tr>
  </tbody>
</table>

<hr />

<h2 id="application-with-students">Application With Students</h2>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/034/034_PT_DT.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Technological Cell - PT-DT Interaction</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/037/037_physical_digital_twin.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Physical and Digital Twin</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/039/039_controling_vizualizing%20_CNC_machine_using_VR.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">Controling and vizualizing a CNC machine using Virtual Reality</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0315/0315.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore &rarr;</div>
        <div style="color: blue;">COBOZ - Cobot Training from VR</div>
      </a>
    </td>
  </tr>
</table>

<hr />

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0134_assesment.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A4.5 - Assessment</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/014-operation-integration/subtask/0140_xr_platform_Integration%20copy%202.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">A5.1 XR Platform Integration</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

:ET