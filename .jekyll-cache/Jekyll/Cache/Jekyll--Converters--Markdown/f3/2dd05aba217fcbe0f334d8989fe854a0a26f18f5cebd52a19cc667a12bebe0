I"]r<h1 id="coboz---cobot-training-from-vr">COBOZ - Cobot Training from VR</h1>

<p>This project aims to leverage the potential of Virtual Reality (VR) and Digital Twin (DT) technologies to enhance academic or industrial training experiences. The immersive features of VR, coupled with the advanced capabilities of DT technologies, empower the creation of a 3D virtual environment that simulates potentially dangerous or remote environments. Taking a Cobot as a case study, our project wants to ensure that users understand the basic principles of how cobots operate in a safe virtual environment. This encompasses critical areas such as safety, accuracy, ergonomics and seamless human-robot collaboration, the project has two main objectives:</p>

<ul>
  <li>VR training for Cobot operation:</li>
</ul>

<p>Using VR for training allows new Cobot operators to gain experience in a risk-free environment, ensuring effective real-world collaboration.</p>

<ul>
  <li>Standardized framework for developing VR training:</li>
</ul>

<p>This will provide a basis for creating VR training modules for other machinery, such as 3D printers, improving safety and efficiency across multiple industrial tools.</p>

<h2 id="vr-training-for-cobot-operation">VR training for Cobot operation</h2>

<p>The first part of this project is to implement a training program in the VR environment to educate potential users of a Cobot. These potential users could be a worker in industry or a student in school or university. This virtual reality environment should include all the elements present in the physical environment in order to guarantee a better immersion and an experience as close as possible to reality. In order to meet this requirement. Additionally, <strong>the user should be able to execute a ‘Pick and Place’ operation with the robot, mirroring the experience of interacting with the actual machine</strong>.</p>

<p>The scene is composed by:</p>

<ul>
  <li>Cobot UR16e</li>
  <li>Control unit</li>
  <li>Control Tablet</li>
  <li>Table for the Cobor</li>
  <li>The virtual interface</li>
  <li>Pick and place components</li>
</ul>

<p><img src="assets/img00_cobot.PNG" alt="Virtual enviroment of the cobor cobot" /></p>

<p><em class="fs-3 text-grey-dk-000">Virtual Environment Scene</em></p>

<p>After having modeled the environment, the environment had to be interactive. It must give information and indications to the user for each element in the environment. To do this, the environment communicates with the user by means of pop-ups that display information and directions to the user, as well as arrows and highlights. For a complete immersion the user can not only read the information but also listen to it.</p>

<p><img src="assets/img12.png" alt="Information about the highlighted joint" height="350px" width="350px" /></p>

<p><em class="fs-3 text-grey-dk-000">Information about the highlighted joint</em></p>

<p><img src="assets/img13.png" alt="Pop-up with an arrow showing the cobot" height="350px" width="350px" /></p>

<p><em class="fs-3 text-grey-dk-000">Pop-up with an arrow showing the cobot</em></p>

<p>After having received all the indications and information concerning the cobot. The user must be able to move the cobot in the environment, to pick up objects and also to move them. To do this, the user has a tablet in the virtual environment that is identical to the real tablet.<br />
With this tablet the user can not only move the cobot tool which results in the relative movement of the joints (inverse kinematics) but also move each joint at his convenience.</p>

<p><img src="assets/img14.png" alt="Tablet interface" height="350px" width="350px" /></p>

<p><em class="fs-3 text-grey-dk-000">Tablet interface</em></p>

<p><img src="assets/img15.png" alt="Moving the cobot using the tablet" height="350px" width="350px" /></p>

<p><em class="fs-3 text-grey-dk-000">Moving the cobot using the tablet</em></p>

<h2 id="vr-expirience">VR Expirience</h2>

<p><img src="assets/img16.jpg" alt="user using the virtual tablet" height="250px" width="250px" /></p>

<p><em class="fs-3 text-grey-dk-000">user using the virtual tablet</em></p>

<ul>
  <li>Discovering the enviroment</li>
</ul>

<p><img src="assets/00_VR.PNG" alt="user using the virtual tablet" height="450px" width="450px" />
<img src="assets/04_VR.PNG" alt="user using the virtual tablet" height="450px" width="450px" /></p>

<ul>
  <li>Interactions</li>
</ul>

<p><img src="assets/03_VR.PNG" alt="user using the virtual tablet" height="450px" width="450px" />
<img src="assets/02_VR.PNG" alt="user using the virtual tablet" height="450px" width="450px" /></p>

<h2 id="standardized-framework-for-developing-vr-training">Standardized framework for developing VR training</h2>

<p>The making of a Training application on VR follows many steps, this elaboration could be complex and tricky, this workflow aims to somehow standardize and lead to a logical plan to implement such an application, steps are defined according to a methodological pattern represented with the diagram below:</p>

<p><img src="assets/img00.png" alt="Main workflow diagram, how to do a training session on VR" /></p>

<p><em class="fs-3 text-grey-dk-000">Main workflow diagram, how to do a training session on VR</em></p>

<p>This process diagram describe the sequencing steps followed to implement such an application, each step is described as follows :</p>

<p><img src="assets/img01.png" alt="Main workflow and the activities" /></p>

<p><em class="fs-3 text-grey-dk-000">Main workflow and the activities</em></p>

<h2 id="workflow-description">Workflow Description</h2>

<table>
  <thead>
    <tr>
      <th style="text-align: left">Acitivities</th>
      <th style="text-align: left">Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left"><strong>A1</strong> <br /> Scope and Audience   <br /></td>
      <td style="text-align: left">• <strong>Description:</strong>  First, determine learning goals. Defining expectations from VR program. What will this training teach the user to accomplish? This phase is about needs and requirement. Also, the intended users of VR application should be identified. Because different users will need different approaches, designs, and indicators. This stage permits planning material and user experience. <br /> •  <strong>Input:</strong> Preliminary idea or concept of the VR program and any pre-existing training or educational materials that can serve as a basis for the VR program. <br /> • <strong>Output:</strong> Clearly defined learning objectives and expectations for the VR program through identification of expected user groups and their specific needs. <br /> • <strong>Control:</strong> Periodic meetings for review and validation with stakeholders to ensure that the learning objectives are in line with their expectations. <br /> • <strong>Resource:</strong> Periodic meetings for review and validation with stakeholders to ensure that the learning objectives are in line with their expectations.  <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A2</strong> <br />  Design Learning content <br /></td>
      <td style="text-align: left">• <strong>Description:</strong>  Create educational tools and resources for virtual reality that offer a richer experience than traditional media via interactive scenarios, dynamic simulations and immersive exercises. <br />  •  <strong>Input:</strong> Feedback and opinions from users and stakeholders on desired VR experiences. Technical specifications and capabilities of the intended VR platform.  <br /> • <strong>Output:</strong> Enhancing and richer learning experience thanks to interactive scenarios, dynamic simulations and immersive exercises adapted to the virtual reality platform. <br />  • <strong>Control:</strong> Iterative design process with regular user testing and feedback. Quality checks to ensure content meets VR best practices and educational standards. <br /> • <strong>Resource:</strong> VR content creators and designers familiar with designing educational tools, as well as technical team to develop and implement interactive scenarios and simulations<br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A3</strong> <br /> Scenario and Storyboard <br /></td>
      <td style="text-align: left">• <strong>Description:</strong> Design a detailed, step-by-step storyboard that describes and shows the user experience, the interactions that will occur in the VR environment and the metaphors that will be used. <br /> •  <strong>Input:</strong> Objectives of the VR experience defined in the previous activities. <br /> • <strong>Output:</strong> Detailed, visually represented storyboard outlining user interactions within the VR environment and identification of metaphors to be used in the VR environment. <br />  • <strong>Control:</strong> Validation against expectations of the VR experience and its scope.  <br /> • <strong>Resource :</strong> Collaborative tools and software for designing, annotating, and sharing storyboards. <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A4</strong> <br /> VR Environment  <br /></td>
      <td style="text-align: left">• <strong>Description:</strong> Use 3D and VR development software to design and create the VR environment based on the storyboard. By developing and integrating necessary assets (3D/2D models, audio video, etc.) to translate the scenario described by the storyboard into an immersive experience. <br /> •  <strong>Input:</strong> Detailed storyboard and scenario specifications.  List of resources derived from the storyboard (e.g., specific models, audio files, video clips).  Technical requirements of the VR platform on which the environment will be deployed.<br /> • <strong>Output:</strong> Fully developed VR environment that reflects the design and storyboard intent. <br />  • <strong>Control:</strong> Periodic testing of the VR environment to ensure conformance with the storyboard and user experience objectives. Quality checks to identify and rectify any bugs, errors or misalignments in the VR environment. <br /> • <strong>Resource :</strong> VR development software and tools tailored to the design of immersive environments. <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A5</strong> <br /> User Interactions <br /></td>
      <td style="text-align: left">• <strong>Description:</strong>   Develop by scripting o triggers the interactions, commands and manipulations that will be executed on the VR app <br /> •  <strong>Input:</strong> Fully functional interactions within the VR application.  <br /> • <strong>Output:</strong> Interactions and manipulations defined in the scenario and storyboard phase. Technical specifications of the VR platform, including its supported interaction methods. <br />  • <strong>Control:</strong> Periodic test sessions to ensure that the scheduled interactions conform to the intended user experiences and are error-free. <br /> • <strong>Resource:</strong>  VR developers skilled in scripting and programming VR interactions. Tools and software to enable programming of interactions. <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A6</strong> <br /> Test, Launch and Maintain  <br /></td>
      <td style="text-align: left">• <strong>Description:</strong>   Execute testing and measurements of users and provide feedback/evaluation in order to enhance the user experience and meet the learning goals. Launch the VR training application once the previous phase is finished, then maintain it by fixing any bugs or problems that occur and adding new material as necessary. A continuous maintenance system should be following the app. <br /> •  <strong>Input:</strong> VR training application fully developed. User groups for testing, feedback and evaluation.  <br /> • <strong>Output:</strong> A VR training application refined from test results. Detailed reports on user experience, bugs and areas for improvement. <br />  • <strong>Control:</strong> Scheduled test phases, including alpha and beta testing, to gather user feedback. Periodic review and update sessions to ensure that the VR application remains relevant and effective. <br /> • <strong>Resource:</strong>  Test teams familiar with VR applications to identify technical and user experience issues. <br /></td>
    </tr>
  </tbody>
</table>

<h3 id="sub-activities">Sub Activities</h3>

<h3 id="a1---scope-and-audience">A1 - Scope and Audience</h3>

<table>
  <tbody>
    <tr>
      <td><img src="assets/img023.png" alt="Workflow Scope and Audience" /></td>
    </tr>
  </tbody>
</table>

<p><em class="fs-3 text-grey-dk-000">Workflow Scope and Audience</em></p>

<ul>
  <li><strong>A1.1</strong> - Identify the skills, information and knowledge that need to be taught : x, y and z tasks on a M machine.</li>
  <li><strong>A1.2</strong> - Identify the expected outcomes and results of the training : a user able to do x, y and z tasks.</li>
  <li><strong>A1.3</strong> - Define in which way the success of the training will be evaluated : a rate for the ability of the new user to execute x, y and z without failure</li>
  <li><strong>A1.4</strong> - Express precisely who the training is for: the potential users category, industrial or academic purpose…</li>
  <li><strong>A1.5</strong> - Identify the audience’s existing knowledge and experience : possible related background, prerequisites, fields..</li>
  <li><strong>A1.6</strong> - Determine how the audience will use the training : the frame in which the usage of the training would be required ( TP, apprenticing…)</li>
</ul>

<h3 id="a2---design-learning-content">A2 - Design Learning Content</h3>

<table>
  <tbody>
    <tr>
      <td><img src="assets/img03.png" alt="Workflow Design Learning Conten" /></td>
    </tr>
  </tbody>
</table>

<p><em class="fs-3 text-grey-dk-000">Workflow Design Learning Content</em></p>

<ul>
  <li><strong>A2.1</strong> -	Write a lesson/Course plan that outlines the sequence of learning process :  Titled chapters and topicswith descriptions and explanations related to the aim of the training such as guided works, practical works, courses…</li>
  <li><strong>A2.2</strong> - Identify the type of content that will be used :  Texts, Audios, images, videos, simulations…</li>
  <li><strong>A2.3</strong> -	Determine the interactivity level : clearly express what this learning requires as assimilation, going from the simple theoretical information to the handy/physical manipulation</li>
</ul>

<h3 id="a3---create-a-storyboard">A3 - Create a Storyboard</h3>

<table>
  <tbody>
    <tr>
      <td><img src="assets/img05.png" alt="Guidelines" /></td>
    </tr>
  </tbody>
</table>

<p><em class="fs-3 text-grey-dk-000">Guidelines for Storyboard</em></p>

<table>
  <tbody>
    <tr>
      <td><img src="assets/img03.png" alt="Workflow Scope and Audience" /></td>
    </tr>
  </tbody>
</table>

<p><em class="fs-3 text-grey-dk-000">Workflow Create a Storyboard</em></p>

<ul>
  <li><strong>A3.1</strong> - Design a visual representation of the training process, how you imagine the look of what the user will experience, from the introduction until the end : Start with introductive information followed by tasks to execute and end with an evaluation and a certificate provided.</li>
  <li><strong>A3.2</strong> - Define the interactions taking place within the VR environment: express the gestures, behaviors and all sequenced possibilities happening during the training process.</li>
  <li><strong>A3.4</strong> - Define how the content will be presented : shapes, panels, metaphors used to illustrate and support the content.</li>
</ul>

<h3 id="a4---develop-vr-environment">A4 - Develop VR Environment</h3>

<table>
  <tbody>
    <tr>
      <td><img src="assets/img06.png" alt="Workflow Develop VR Environment" /></td>
    </tr>
  </tbody>
</table>

<p><em class="fs-3 text-grey-dk-000">Workflow Develop VR Environment</em></p>

<ul>
  <li>
    <p><strong>A4.1</strong> Determine the level of detail required for the environment according to the main functions :</p>

    <ul>
      <li>Illustrate : how the user will visualize the VR environment.</li>
      <li>Navigate : How the user will circulate and move in the VR environment</li>
      <li>Select : How the user will choose and pick options and functions.</li>
      <li>Manipulate : How the user will interact with objects.</li>
    </ul>
  </li>
  <li><strong>A4.2</strong> - Collect a dataset of files and  information that you will use in your VR environment</li>
  <li><strong>A4.3</strong> - Select and Create the assets required for the environment, this list below gives some suggestions.</li>
  <li><strong>A4.4</strong> - Develop  the first scene zones lightings, cameras and textures for the VR environment</li>
</ul>

<h3 id="a5---implement-user-interactions">A5 - Implement User Interactions</h3>

<table>
  <tbody>
    <tr>
      <td><img src="assets/img07.png" alt="Workflow Implement User Interactions " /></td>
    </tr>
  </tbody>
</table>

<p><em class="fs-3 text-grey-dk-000">Workflow Implement User Interactions</em></p>

<ul>
  <li><strong>A5.1</strong> - Develop the metaphors that will be used within the VR environment in the form of Assets.</li>
  <li><strong>A5.2</strong> - Determine how the user will interact with the environment (address a strategy )</li>
  <li><strong>A5.3</strong> - Strategy exemple :
    <ul>
      <li>give clear instructions</li>
      <li>fllow a linear progression of executions (freezing/unfreezing interactions in a sequential way )</li>
      <li>time limits of executions (freezing some interactions after a certain time )</li>
      <li>limited options ( freezing the access of a part of the interactions )</li>
    </ul>
  </li>
  <li><strong>A5.4</strong> - Identify any additional functionality that is required.</li>
</ul>

<h3 id="a6---test-launch-and-maintain">A6 - Test, Launch and Maintain</h3>

<table>
  <tbody>
    <tr>
      <td><img src="assets/img08.png" alt="Workflow Implement User Interactions " /></td>
    </tr>
  </tbody>
</table>

<p><em class="fs-3 text-grey-dk-000">Workflow Implement User Interactions</em></p>

<ul>
  <li><strong>A6.1</strong> - Collect feedback from users during the testing phase.</li>
  <li><strong>A6.2</strong> - Measurement with KPIS</li>
  <li><strong>A6.3</strong> - Identify areas of improvement for the VR training application.</li>
  <li><strong>A6.4</strong> - Iterate on the design based on feedback received.</li>
  <li><strong>A6.5</strong> - Launch the VR training application.</li>
  <li><strong>A6.6</strong> - Address any bugs or issues that arise.</li>
  <li><strong>A6.7</strong> - Update the content as needed.</li>
</ul>

<p>Depending on the size and complexity of the requirements for the VR training program as well as the particular needs of the project, different tools may be needed.</p>

<h3 id="application">Application</h3>

<h3 id="a1---defining-the-learning-objective-and-target-audience">A1 - DEFINING THE LEARNING OBJECTIVE AND TARGET AUDIENCE:</h3>
<p>The learning objective of this proof of concept is to be an introduction for engineering students of a 5 axis 3D printer and especially the range of movements that can be done by this type of machine.</p>

<h3 id="a2---design-the-learning-content">A2 - DESIGN THE LEARNING CONTENT:</h3>

<p>Like the rest of our project the goal is to make this learning and discovery possible and viable through the Unity software and with the use of VR controls.</p>

<h3 id="a3---create-a-scenario-storyboard">A3 - CREATE A SCENARIO STORYBOARD:</h3>

<p>The storyboard in mind for this use case is quite simple. A student would go into the environment and be able to freely manipulate each axis on the machine within its limitations while having a screen near the machine to provide insight about each part and maybe add some questions to keep a more pedagogic aspect.</p>

<h3 id="a4---develop-vr-environment-1">A4 - DEVELOP VR ENVIRONMENT:</h3>

<p>The first step is then to import the CNC machine into the unity environment, this importation is made in a FBX format. The FBX format can be obtained with the Blender software by converting the cad model in it.
Then in the proof of concept scene the first thing to do is to drag and drop the FBX file into the hierarchy area of Unity.</p>

<p><img src="assets/img17.jpg" alt="CNC machine scene with colored parts" /></p>

<p><em class="fs-3 text-grey-dk-000">CNC machine scene with colored parts</em></p>

<p>To keep the parts recognizable between them and be seen correctly through the environment, a material Object from unity has to be created in the adequate folder of your choice in the middle part of the folder explorer (bottom left of the screen). With this material object you can decide which texture and color a object can have and so to directly drag and drop into the part that needs to change color and texture.</p>

<p>The first metaphors to add for a better understanding of the system is to add a highlight system that will show which part is selected in order to manipulate its range of movement.</p>

<p><img src="assets/img18.jpg" alt="Part highlight and its axis description arrow" /></p>

<p><em class="fs-3 text-grey-dk-000">Part highlight and its axis description arrow</em></p>

<p>The highlighted part is done by dragging and dropping into an array of parts a copy of the part that has been colored with a bright texture. In the screenshot below the number of parts can be changed on the top left to adapt to all types of multipart assemblies.</p>

<p><img src="assets/img19.jpg" alt="Highlighted part array" /></p>

<p><em class="fs-3 text-grey-dk-000">Highlighted part array</em></p>

<p>Moreover another metaphor to better comprehend in which axis the part highlighted is moving is the use of an arrow that appears during the selection.
These arrows are managed by an Object called “managerArrows” that only contains a script that will accordingly activate the arrow that must be placed beforehand by the teacher in the scene manually, the 3d model of the arrow has to be also put in the array made by the script. These arrow models will have to be deactivated in order for the script to work properly. To deactivate any object on unity and just toggle off the box on the top left of the object’s inspector area.</p>

<p><img src="assets/img20.jpg" alt="ManagerArrows GameObject (all the arrow object are deactivated)" /></p>

<p><em class="fs-3 text-grey-dk-000">ManagerArrows GameObject (all the arrow object are deactivated)</em></p>

<p><img src="assets/img21.jpg" alt="ManagerArrows GameObject (all the arrow object are deactivated)" /></p>

<p><em class="fs-3 text-grey-dk-000">Shown in the inspector (right screen) the array has to be completed with the arrow GameObject</em></p>

<p>One more pedagogic aspect that we will lean on is the parts description through a basic user interface that will show some text to describe the machine that is presented. As such in this proof of concept the choice has been made to write a script that allows a text entry for each part, in our case there are 5 moving parts so there will be 5 texts that can be shown.</p>

<p><img src="assets/img22.PNG" alt="Texttest script for the part description in the UI" /></p>

<p><em class="fs-3 text-grey-dk-000">Texttest script for the part description in the UI</em></p>

<p>In the figure above, in order for the script to work you have to link the manager (1) that we will see just after and the text part of the UI (2) that has to be created and placed by the user in the scene.
The UI is a game object that can be created and is linked to a Text entry that can be modified, here the script takes charge of the modification of the text.</p>

<p><img src="assets/img23.jpg" alt="Selected part (element 0) and the corresponding text written on the UI" /></p>

<p><em class="fs-3 text-grey-dk-000">Selected part (element 0) and the corresponding text written on the UI</em></p>

<p>This topic is more oriented about the overhaul configuration of the system that needs to be studied, as mentioned before the manager will take the main role in that. As such as the highlighting process, another array is available to fill. In this part some data has to be given such as the Gameobject Transform (which is the combination of the 3D model and a set of xyz axis), its movement limit, if it is a rotating part or a translating one and its starting position. In our case each part is linked one to the other as a parent/child hierarchy so the starting position relative to the other piece is (0,0,0). Also the limit is expressed in meters for the translation and in degrees for the rotation.</p>

<p><img src="assets/img24.jpg" alt="Part description configuration and Machine FBX hierarchy" /></p>

<p><em class="fs-3 text-grey-dk-000">Part description configuration and Machine FBX hierarchy</em></p>

<h3 id="a5---implement-user-interactions-1">A5 - IMPLEMENT USER INTERACTIONS:</h3>

<p>The most important part, especially for a learning purpose, is to give access for the students to manipulate the machine that they are studying through the VR environment.
With the help of the part description script another script has the task to configure the movement of each part by assigning a speed either for rotation and translation to each pieces.
as an example the keyboard has been used for this proof of concept due to the availability of functioning VR headset during its development.</p>

<p><img src="assets/img25.jpg" alt="Control setup on the Machine" />
<img src="assets/img26.jpg" alt="Control setup on the Machine" />
<a href="# fnt">comment_text</a>: # snt</p>

<p><em class="fs-3 text-grey-dk-000">Control setup on the Machine</em></p>

<p>From the image above , two inputs are configured, that is : SelectPart done with the “up” and “down” arrows and “MovePart” done with the left and right arrows.
This reference has to be configured through the script which is the trickiest part of the process. But the packages installed for VR control are also already installed so with external help the part can be adapted for VR controls as seen in the first part of the product.</p>

<h3 id="a6---test-launch-and-maintain-1">A6 - TEST, LAUNCH AND MAINTAIN:</h3>

<p>The best way to reshape this proof of concept would be to configure the VR movements in the environment and then test the product with teachers and students to refine what can be improved upon, first in the configuration area where someone not versed in the Unity software language could have difficulties and secondly has what feature students liked and didn’t like.</p>

<p>By following the workflow we can then have a good basis on how to make a study of mechanical objects through VR more accessible for teachers and students.</p>

<h2 id="conclusion">Conclusion</h2>

<p>During this project we experienced challenging situations, especially technical issues that we didn’t have any previous experience of facing. Some of these challenges are software version problems of unity, complexity and incompatibility of usage of VR material with some computers, synchronization problems during the push and pull of GitHub, breakdown of the university IT systems at the beginning of the second semester. Another challenge was lack of time and manpower due to absence of one member of the group which led to the alteration of the second objective of the project.</p>

<p>For a better continuance of this project in the future, one suggestion could be to start the Unity or other required software training in the first semester. In this case, students will have more time to tackle technical problems and implement more complex projects in a VR environment.</p>

<p>At the end of this project, the team managed to deliver the requirements specified by the client and this has been done by carrying out continuous meetings with our client and updating the requirements expected by the client.</p>

<hr />

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0313/0313_3d_modelling_vr.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">3D Modelling in Virtual Reality</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0315/0315.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Framework for training using VR</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

:ET