I"�(<h1 id="physical-and-digital-twin">Physical and Digital Twin</h1>

<p>In this use case, the objective was to make a physical prototype of the robot arm piloted by its twin in virtual reality. The main goal is to control the robot arm in different ways as manual or virtual ways.</p>

<hr />
<h2 id="poster">Poster</h2>
<p><img src="assets/037_poster.png" alt="Poster" /></p>

<hr />
<h2 id="workflow-development">Workflow development</h2>

<table>
  <tbody>
    <tr>
      <td> </td>
      <td><strong><center>CONTROL:</center></strong><center> Physical part assembly - Accuracy and responsiveness of the comunication - effectiveness of robot arm control via Simulink - Precision of manual control - Accuracy of angle instructions from VR </center></td>
      <td> </td>
    </tr>
    <tr>
      <td><strong>INPUT:</strong> <br /> CAD Models robot arm - servomotors</td>
      <td><img src="assets/037_workflow.png" alt="scenecreation" width="2000" height="2000" style="vertical-align:middle" /></td>
      <td><strong>OUTPUT:</strong> <br /> A robot arm controllable through the Unity interface in VR <br /></td>
    </tr>
    <tr>
      <td> </td>
      <td><strong><center>RESOURCE:</center></strong> <center>  SolidWorks - 3D printer - Servomotors - MATLAB Simulink - Arduino - Unity 3D </center></td>
      <td> </td>
    </tr>
  </tbody>
</table>

<h2 id="workflow-description">Workflow Description</h2>

<table>
  <thead>
    <tr>
      <th style="text-align: left">Acitivities</th>
      <th style="text-align: left">Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left"><strong>A1</strong> <br /> Redesign and production  <br /></td>
      <td style="text-align: left">• <strong>Description:</strong> The redesign and production of the real prototype of the robot arm have commenced using CAD models on Solidworks. Modifications to the CAD models are required to properly integrate the servomotors into the arm’s links. In total, there are six servomotors needed in this prototype. Then, thanks to the use of a 3D printer, each part of the robot arm is efficiently and cost-effectively produced for testing purposes.<br /> •  <strong>Input:</strong>  CAD models, servomotor specifications (dimentions) <br /> • <strong>Output:</strong>  Adjusted CAD models, 3D printed for robot arm prototype assembly  <br />  • <strong>Control:</strong>  Part assembly and fitting of servomotorss <br /> • <strong>Resource:</strong> SolidWorks, 3D printer, servomotors  <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A2</strong> <br />  Physical-Digital connection <br /></td>
      <td style="text-align: left">• <strong>Description:</strong>  A critical point is the connection between the physical and digital prototype. A connection was designed between MATLAB Simulink and Arduino, which represents an interface card for the communication between the robot and the Simulink project. With this software, the robot is intended to be correctly controlled in both ways thanks to the communication between Simulink and Unity through the local server.  <br /> •  <strong>Input:</strong> The physical robot prototype, digital twin in Simulink   <br /> • <strong>Output:</strong> A bidirectional communication link between the physical and digital robot arm   <br />  • <strong>Control:</strong> Accuracy and responsiveness of the comunication <br /> • <strong>Resource:</strong> MATLAB Simulink, Arduino, Robot arm prototipe <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A3</strong> <br /> Multiphysics model <br /></td>
      <td style="text-align: left">• <strong>Description:</strong> It is necessary to build the multiphysics model to respect the requirements. The work focused on the connection of the angular instructions with the virtual model and then to establish the link between Arduino and Simulink, to prepare the manual and virtual control of the robot. The advantage offered by Simulink is to propose the entire Arduino library in download. Thus, it is possible to use the Arduino IDE functions in MALTLAB Simulink if necessary. Simulink can recognize these functions <code class="language-plaintext highlighter-rouge">servoRead</code> and <code class="language-plaintext highlighter-rouge">servoWrite</code> when they are used. The goal is to replace the virtual robot in Simulink with the real one: when we integrate real parts or motors into a Simulink project, it is called Hardware-in-the-loop. The servo motors are linked to the Arduino board on different pins. Simulink recognizes the Arduino board thanks to the compilation of a .PDE code on the board, which allows to use the Arduino library in Simulink. <br /> • <strong>Input:</strong>  Arduino functions and library, robot arm prototipe, MATLAB Simulink project <br /> • <strong>Output:</strong> Arduino-integrated Simulink project that can interact with the robot arm prototipe <br />  • <strong>Control:</strong>  Accuracy of Arduino-Simulink integration, effectiveness of robot arm control via Simulink  <br /> • <strong>Resource:</strong> MATLAB Simulink, Arduino library <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A4</strong> <br /> Manual control <br /></td>
      <td style="text-align: left">• <strong>Description:</strong> By implementing the Arduino library and connecting angular instructions to the servo motor pins, it is possible to manually control the robotic arm using a block parameter interface. <br /> •  <strong>Input:</strong> Servomotor pins, angle instructions, block parameters interface  <br /> • <strong>Output:</strong> A manually controllable robot arm  <br />  • <strong>Control:</strong> Precision of manual control  <br /> • <strong>Resource:</strong> MATLAB Simulink, Arduino library  <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A5</strong> <br /> Virtual control  <br /></td>
      <td style="text-align: left">• <strong>Description:</strong>  The major point of data communication in the project is the two computers that each host a project, one the Simulink project and the other the Unity project. It should be noted that the ports for sending and receiving data must be changed depending on the IP address of the two computers. The objective now is to adapt the manual control to the virtual control. It is necessary to modify the input data interface for the real prototype. The Operator parameter block needs to be converted into a virtual interface in Unity. The angular instructions will come directly from Unity. It is possible to get the data sent by Unity for the angle instructions, after having informed what angle in unity is required. Thanks to 6 different levers, it is possible to choose the angle to be sent to each servomotor and then send the values to the Simulink Project, which will send the converted values to the Arduino and move the servomotor. Now the virtual robot in Unity reproduces the behavior of the real robot with the instructions of the angles and not the real angles with errors certainly.  <br /> •  <strong>Input:</strong>  Unity project, VR headset, control interface, command for angles <br /> • <strong>Output:</strong> A robot arm controllable through the Unity interface in VR <br />  • <strong>Control:</strong> Accuracy of angle instructions from VR, communication latency <br /> • <strong>Resource:</strong>  MATLAB Simulink, Unity 3D, Arduino  <br /></td>
    </tr>
  </tbody>
</table>

<hr />
<h2 id="results">Results</h2>

<p>We can control the real robot thanks to the virtual interface in Unity easily. Two improvements can be made on the robot : to make a more intuitive interface (with joysticks) to facilitate handling of the robot in virtual reality and to establish automation of the data transmission because it could be a requirement of the industrial specifications ; to use a system of recovery of the current position (angle) of the servomotors. Rotary encoders allows to give a feedback of the position of a servomotor by positioning then along the axis of the servomotor concerned. Thanks to Arduino, we can collect this data and send it to Simulink which establish the connection with Unity. Then, Simulink send the date to the input of the virtual servomotor and by this way, the virtual robot in Unity will reproduces the behavior of the real prototype with potential errors of angle (exchange of information).</p>

<h2 id="conclusion">Conclusion</h2>

<p>We can show several uses as manipulation of a robot without any risk for the user for instance because it is remote handling of a robot. It opens to other possibilities as automation and programming series of movements for a use in industrial production line. It can become a low cost remote simulator.</p>

<hr />

<h3 id="avatar-for-me">Avatar for me</h3>

<p>Avatar project was a great experience for me, over all thanks to the collaboration between nations and projects. I havelearnt about XR technologies and their applications in several fields of the engineering or the industry. I will reuse this experience in my professional career because I think it was a relevant opportunity in my student life.</p>

<hr />

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/036/036_from_cad_to_simulation.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Exporting from CAD an URDF package usable for simulation and VR</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/037/037_physical_digital_twin.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Physical and Digital Twin</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

:ET