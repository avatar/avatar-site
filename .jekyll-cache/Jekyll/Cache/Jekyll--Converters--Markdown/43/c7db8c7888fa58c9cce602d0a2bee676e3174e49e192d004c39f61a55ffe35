I"nS<h1 id="jll-2022---team-1">JLL 2022 - Team 1</h1>

<p><img src="assets/img11.jpg" alt="09" height="600px" width="600px" /></p>

<hr />

<h2 id="day-1">DAY 1</h2>

<p><strong>Objective: The single objective for JLL2022 is to INTEGRATE software and hardware components and DEMONSTRATE operation of the MANUFACTURING DIGITAL TWIN system, e.g., AVATAR ROBOTIC WORK-CELL DIGITAL TWIN.</strong></p>

<p>The first day of AVATAR week started with a presentation of the University and their relative course, projects, and opportunities that it offers. Moreover, an introduction about the topics that we will do were given during the sessions, starting from the creation of the model to the digital twin implementation. We discovered all the features that the project exploited and the main advantages that will bring us in terms of knowledge and work opportunities for future works. This project will give us a powerful tool to improve the manufacturing lines in the context of Industry 4.0, exploiting the virtual reality for evaluating spaces and operating conditions.</p>

<p>In the afternoon session of the first day, we started with the first working session, based on the CAD modeling of the real physical system to be implemented in the virtual reality simulation later.
The first step was to check the measurement of the real system to be sure that the CAD model will be coherent with the digital one. And for that reason, according to the measurement references of the robot “SIA 10F”, we went on solidworks and we changed all the measurements of the CAD imported from the official site “Yaskawa”, brand owner of the robot.
In that moment another important step was necessary, the reference coordinate system must be set up on each link in the right way in order to be sure that the future simulation implemented in Unity will work.
Since the objective of the first working session was to create the scene in a Virtual context, we exported the cad model in the GLTF format, so that was possible, through an unity importing package, to bring the CAD model in the house of the simulation.
At the end of the day, after generating the robot and the simulation scene, we went on with the creation of some scripts to start controlling the robot. Very basic scripts in C# were implemented to start moving simple objects.</p>

<h2 id="day-2">DAY 2</h2>

<h3 id="morning-session">Morning session</h3>

<p><strong>Objective: XRSCENE ANIMATION - RWCKinematics Modeling</strong></p>

<p>During this session, we will be introducing RoboDK, a software that helps us solve direct and inverse kinematic problems for robots and be able to exploit it inside unity.</p>

<p>Important steps for being able to repeat the protocol:</p>

<ol>
  <li>Introduction to RoboDK : Software that helps us to solve direct and inverse kinematic problems ,</li>
  <li>Importing the “Motoman CSDA10F” Robot Model into RoboDK,</li>
  <li>Modify the reference systems on RoboDK, to make them consistent with unity references,</li>
  <li>Add a new reference on the TCP, to add 20 mm that were missing in RoboDK model compared to real robot in the lab,</li>
  <li>Create a code that will help us generate the movement,</li>
  <li>Define some points to be our targets (target points),</li>
  <li>Define also the type of the movement that the robot will make between targets, movJ= Joints movements, movC = Circular movement, movL = Linear movement.</li>
  <li>Experiment with direct and inverse kinematics, with Run and Loop command.</li>
  <li>On the Unity side, we learn the logic behind the parent-child relation.</li>
</ol>

<p><strong>Lab visit</strong></p>

<p>We went to the lab and we experimented with robot programming by teaching, in particular we got used to the logic behind the teach pendant. We were able to move the robot following the logics of both direct and inverse kinematics. We get knowledge about safety issues and the risk of programming and interacting with a real robot.</p>

<p><img src="assets/img00.jpg" alt="00" height="300px" width="300px" />      <img src="assets/img01.jpg" alt="01" height="300px" width="300px" /></p>

<h3 id="afternoon-session">Afternoon session</h3>

<p><strong>Objective: XRSCENE ANIMATION - RWCKinematics Modeling</strong></p>

<p>The main objective of this program is to analyze the API of RoboDK, Unity and SolidWorks and understand how to implement the communication between the three softwares using the User Datagram Protocol (UDP).
<img src="assets/img02.png" alt="03" height="700px" width="700px" /><br />
The main characteristics of the communication layer are the following:</p>
<ul>
  <li>A main code with an algorithm for obtaining new data (angles) which is triggered only when a new message is sent into the buffer, but also sending data (angles) when changes are made in one of the softwares (Unity or SW)</li>
  <li>Another code which is called by the main code, with the purpose of converting angles, because of the problem of having different coordinate systems among the softwares.</li>
  <li>Another two codes which are called by the main codes, which have the aim to convert the message to UDP protocol convention.
As an example, here is the code from Unity that is used for communication with SolidWorks. Below is the main algorithm, which is called in every frame when Unity is started, because it is in the void Update() method. The same code with little addings is used in Visual Studio for SolidWorks communication with Unity.</li>
</ul>

<div class="language-csharp highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">private</span> <span class="k">void</span> <span class="nf">SolidWorks_DT</span><span class="p">()</span>
<span class="p">{</span>
    <span class="k">if</span> <span class="p">(</span><span class="n">UDP_client_SW</span><span class="p">.</span><span class="nf">canRecieve</span><span class="p">()</span> <span class="p">&gt;</span> <span class="m">0</span><span class="p">)</span>
    <span class="p">{</span>
        <span class="k">try</span>
        <span class="p">{</span>
            <span class="n">uglovi</span> <span class="p">=</span> <span class="n">UDP_parser_SW</span><span class="p">.</span><span class="nf">parsiraj</span><span class="p">(</span><span class="n">UDP_client_SW</span><span class="p">.</span><span class="nf">udpRead</span><span class="p">());</span> 
        <span class="p">}</span>
        <span class="k">catch</span>
        <span class="p">{</span>
            <span class="n">Array</span><span class="p">.</span><span class="nf">Copy</span><span class="p">(</span><span class="n">startuglovi</span><span class="p">,</span> <span class="m">0</span><span class="p">,</span> <span class="n">uglovi</span><span class="p">,</span> <span class="m">0</span><span class="p">,</span> <span class="n">uglovi</span><span class="p">.</span><span class="n">Length</span><span class="p">);</span>
        <span class="p">}</span> 
        <span class="nf">Set_parametcrs</span><span class="p">(</span><span class="n">uglovi</span><span class="p">);</span>
        <span class="nf">Get_parameters</span><span class="p">();</span>
        <span class="n">Array</span><span class="p">.</span><span class="nf">Copy</span><span class="p">(</span><span class="n">uglovi</span><span class="p">,</span> <span class="m">0</span><span class="p">,</span> <span class="n">stari_uglovi</span><span class="p">,</span> <span class="m">0</span><span class="p">,</span> <span class="n">uglovi</span><span class="p">.</span><span class="n">Length</span><span class="p">);</span> 
    <span class="p">}</span>
    <span class="k">else</span>
    <span class="p">{</span>
        <span class="nf">Get_parameters</span><span class="p">();</span>
        <span class="k">if</span> <span class="p">(!</span><span class="nf">Array_Sequence_Offset</span><span class="p">(</span><span class="n">uglovi</span><span class="p">,</span> <span class="n">stari_uglovi</span><span class="p">,</span> <span class="n">UDPAngleOffset</span><span class="p">))</span>
        <span class="p">{</span>
            <span class="n">Array</span><span class="p">.</span><span class="nf">Copy</span><span class="p">(</span><span class="n">uglovi</span><span class="p">,</span> <span class="m">0</span><span class="p">,</span> <span class="n">stari_uglovi</span><span class="p">,</span> <span class="m">0</span><span class="p">,</span> <span class="n">uglovi</span><span class="p">.</span><span class="n">Length</span><span class="p">);</span>
            <span class="n">UDP_client_SW</span><span class="p">.</span><span class="nf">UDP_send_string</span><span class="p">(</span><span class="s">"ugaol_x:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">0</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugaol_y:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">1</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugaol_z:"</span> <span class="p">+</span> <span class="n">uglovi</span>
            <span class="p">[</span><span class="m">2</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao2_x:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">3</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugaol_y: "</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="n">U</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao2_z:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">5</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> 
            <span class="s">"ugao3_x:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">6</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao3_y:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">7</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao3_z:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">8</span><span class="p">]</span> <span class="p">+</span><span class="s">";"</span> <span class="p">+</span> <span class="s">"ugaoll_x:"</span> <span class="p">+</span> <span class="n">uglovi</span>
            <span class="p">[</span><span class="m">9</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugaol_y:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">10</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugaoU_z:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">11</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao5_x:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">12</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> 
            <span class="s">"ugaol_y:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">13</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao5_z:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">14</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao6_x:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">15</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao6_y:"</span> <span class="p">+</span> 
            <span class="n">uglovi</span><span class="p">[</span><span class="m">16</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao6_z:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">17</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao7_x:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">18</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"ugao7_y:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">19</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> 
            <span class="p">+</span> <span class="s">"ugao7_z:"</span> <span class="p">+</span> <span class="n">uglovi</span><span class="p">[</span><span class="m">20</span><span class="p">]</span> <span class="p">+</span> <span class="s">";"</span> <span class="p">+</span> <span class="s">"\n"</span><span class="p">);</span>
            <span class="n">MWF</span> <span class="p">=</span> <span class="k">true</span><span class="p">;</span> 
        <span class="p">}</span>
    <span class="p">}</span>
<span class="p">}</span> 
</code></pre></div></div>
<p>We can split the algorithm into two main parts, if part and else part. In if part Unity is taking data (joint angles) from SolidWorks when there is something in the Buffer (sent from SW) and in else part Unity is sending data (joint angles) to SolidWorks when there is a change in robot configuration. Thus, the exchange is event driven, as Unity is sending data only when there is the difference in angle configuration in Unity scene. Same applies to SW.</p>

<p><strong>Lab visit:</strong></p>

<p>During the second visit we had the opportunity to try the VR optical device and to experiment a virtual walk in a static scene. The scene contained the robotic welding scene and we had the chance to look directly and in a very immersive way at its characteristics.</p>

<p><img src="assets/img03.jpg" alt="03" height="300px" width="300px" />  <img src="assets/img04.jpg" alt="04" height="300px" width="300px" /></p>

<h2 id="day-3">DAY 3</h2>

<h3 id="morning-session-1">Morning session</h3>

<p><strong>Objective: RWC DIGITAL SHADOW Integration</strong></p>

<p>During the morning session, we began by gaining an understanding of what digital twins and digital shadows are. From there, we delved into the connection between real robots and Unity. We analyzed an UDP code to better grasp this connection</p>

<ol>
  <li>Introduction to what is a digital twin and also a digital shadow</li>
  <li>We start talking about connection between real robot and unity</li>
  <li>In order to do that we analyze an UDP code</li>
  <li>
    <p>We study what is an OAC (Open Architecture Controller) which is the necessary kind of controller to enable the communication.  <br /> <img src="assets/img05.png" alt="05" height="700px" width="700px" /></p>
  </li>
  <li>So we tried what we studied in particular we used Oculus to see the digital shadow working while another team member was moving the physical  robot with the teach pendant</li>
  <li>we came back to study the code to understand how the data (angles) are send from Robot Controller to Unity, and replicated in real time in the software (DigitalShadow)</li>
  <li>These angles are also saved by Unity in a file so they can be played later both in software and back to the physical robot (PhysicalShadow)</li>
</ol>

<p><strong>Lab visit</strong></p>

<p><img src="assets/img06.png" alt="06" height="300px" width="300px" /></p>

<h3 id="afternoon-session-1">Afternoon session</h3>

<p><strong>Objective: Manipulating the robot Manually</strong></p>

<ul>
  <li>First explanation of the manual robot controller, acquiring technical information about the sensors used to transfer human input forces to the controller of the robot.</li>
  <li>Analysis of load cells and the wheatstone bridge mechanism upon which they can work.</li>
  <li>Introduction to the mechanical structure of the haptic device, in particular an analysis of the spring system together with the safety switch and with all the 3d printed components.</li>
  <li>Moving the two virtual robots with OCULUS joysticks in VR.</li>
  <li>Have the opportunity to see the robot manual controller in action, maneuvered in order to generate a trajectory and a job for the robot.</li>
  <li>Visualize the trajectory generated on matlab, filter it and visualize its 3d representation.</li>
  <li>Observing an expert maneuvering the physical robot through virtual reality by using OCULUS system, and that was the first experience with a digital twin.</li>
</ul>

<p><img src="assets/img07.jpg" alt="07" height="600px" width="600px" /></p>

<p><img src="assets/img08.jpg" alt="08" height="600px" width="600px" /></p>

<h2 id="day-4">DAY 4</h2>

<h3 id="morning-session-2">Morning session</h3>

<p><strong>Objective: Operation of the integrated RWC Digital Twin and GAME Based LEARNING</strong></p>

<ol>
  <li>XRProgramming by Demonstration</li>
  <li>Each team member programmed the robotic arm to complete the touch and jump task</li>
  <li>In order to do the second point we use different kind of command:
a)	using KINESTHETIC haptic controller 
b)	using KINESTHETIC haptic controller and XRHMD visual feedback
c)	using TELEOPERATION haptic controller and XRHMD visual feedback</li>
  <li>The learned robot trajectory will be recorded, from the first movement of the robot until the last point of the task and then reproduced by the virtual robot in its virtual task space and by the physical robot in its physical task space.</li>
</ol>

<p><strong>Lab Visit</strong></p>

<p>We went to the lab and we start to program a physical robot to
complete the Touch and Jump task. In order to do that, first we used a KINESTHETIC haptic controller without VR and after that we used it. We did that to program the robot by Demonstration (PbD).</p>

<h3 id="afternoon-session-2">Afternoon session</h3>

<p><strong>Objective: Operation of the integrated RWC Digital Twin and GAME Based LEARNING</strong></p>

<p>In the afternoon session we focused on the new lab visit and in the making of the final presentation.</p>

<p>Please summarize the important steps for being able to repeat the protocol :</p>

<p><strong>Lab Visits</strong></p>

<p>In the second lab visit we had the same task to do but this time we used the TELEOPERATION haptic controller, for the first time we used it without VR and the second time with VR.</p>

<p><img src="assets/img09.jpg" alt="09" height="600px" width="600px" /></p>

<h2 id="day-5">DAY 5</h2>

<p><strong>Objective: Operation of the integrated RWC Digital Twin and GAME Based LEARNING</strong></p>

<p>In the morning session we did our last lab visit and finished our final presentation. Inbetween, we filled in the necessary questionnaires made by Marta (psychologist) and gave an interview.</p>

<p><strong>Lab Visit:</strong></p>

<p>In our last lab visit we had similar task to do but this time we used only VR technology. So, for robot movement we used Oculus Rift s controllers, by holding an appropriate button we were able to control TCP of the robot. It was not connected to the physical system, thus the speed of the robot was not limited by its physicality and we were able to complete the task swiftly.</p>

<p><img src="assets/img10.png" alt="09" height="600px" width="600px" /></p>

<hr />

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0316/0316_HMI_Robot.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">AR for operator training</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0318/0318_JLLG1.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">JLL 2022 - Team 1</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

:ET