I"b<h1 id="a45---assessment">A4.5 - Assessment</h1>

<p>A user interface is a system and any designed system a development process needs to converge from specification to a product then to progressively check its behavior. We can reference a V-Cycle or any model and we will face :</p>

<ul>
  <li>
    <p>unit tests : at a very low level checking that every basic function is working. It is especially necessary here to check</p>

    <ul>
      <li>
        <p>the importers functions</p>
      </li>
      <li>
        <p>the metaphor rendering</p>
      </li>
      <li>
        <p>the metaphor behaviors</p>
      </li>
      <li>
        <p>the low level selection functions</p>
      </li>
      <li>
        <p>the low level navigation functions</p>
      </li>
      <li>
        <p>the low level manipulation functions</p>
      </li>
    </ul>
  </li>
  <li>
    <p>once unit tests are validated some technical integration tests will support to check that every upper functions are properly accessible. The use case diagram here allow to plan integration test starting from low level function and following backwards the extend and include links. When integration tests are validated we may say that the product is functional but it is not yet acceptable.</p>
  </li>
  <li>
    <p>the end user perception tests are a complementary assessment to ensure that the system is :</p>

    <ul>
      <li>
        <p>efficient : based on basic scenarios we can measure the performance and quality of task performed :</p>

        <ul>
          <li>
            <p>performance may be measured as a task duration or a quantity of sub-tasks performed in a given delay</p>
          </li>
          <li>
            <p>the quality will measure the quality of the task result which depends on the task itself: Artifacts properly positioned, completeness of the activity, etc</p>
          </li>
        </ul>
      </li>
      <li>
        <p>the use perception may be assessed through</p>

        <ul>
          <li>
            <p>bio-sensors : to measure strengths, brain load, stress etc</p>
          </li>
          <li>
            <p>when bio-sensors are not available or when no competencies about these sensors, some questionnaires provides good views</p>

            <ul>
              <li>SUS system</li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
  </li>
</ul>

<h3 id="input">Input</h3>

<p>The developped user interface</p>

<h3 id="output">Output</h3>

<p>A validation of the user interface or a set of expected corrections</p>

<h3 id="control">Control</h3>

<p>The use case is a good tool to plan the unit and integratiion test</p>

<h3 id="resources">Resources</h3>

<p>Sensors and questionnaires. Hereafter a set of available and referenced questionnaires</p>

<ol>
  <li>
    <p><strong>System Usability Scale (SUS)</strong><br />
<img src="assets/sus.png" alt="System Usability Scale Image" height="300px" width="300px" /></p>

    <p><em class="fs-3 text-grey-dk-000">System Usability Scale Image</em></p>

  </li>
  <li>
    <p><strong>Usefulness questionnaire (USE)</strong><br />
<img src="assets/img6.png" alt="USE Image" height="300px" width="300px" /></p>

    <p><em class="fs-3 text-grey-dk-000">Usefulness questionnaire Image</em></p>

  </li>
  <li>
    <p><strong>Attractivity (AttrackDif2)</strong><br />
<img src="assets/AttrackDIf2.png" alt="AttrackDif2 Image" height="300px" width="300px" /></p>

    <p><em class="fs-3 text-grey-dk-000">AttrackDif2 Image</em></p>

  </li>
  <li>
    <p><strong>Task load index (Nasa TLX)</strong><br />
<img src="assets/NASATLX.png" alt="Nasa TLX Image" height="300px" width="300px" /></p>
  </li>
</ol>

<p><em class="fs-3 text-grey-dk-000">Nasa TLX Image</em></p>

<hr />

<table style="width:100%; width: 50%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/013-UI/subtasks/0133_implementation.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">A4.5 - Implementation</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/014-operation-integration/014-operation-integration.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;"> A5 - System Integration &amp; Operation Workflow</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

:ET