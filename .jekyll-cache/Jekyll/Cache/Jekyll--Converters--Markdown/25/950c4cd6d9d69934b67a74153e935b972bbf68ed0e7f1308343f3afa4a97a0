I"�"<h1 id="animation-of-machine-center-in-virtual-space">Animation of Machine Center in Virtual Space</h1>

<p>The main purpose of this document is to shed light and provide information on how to define motion of some of the dynamic parts of the <strong>Mikron HPM450</strong> machine integrated with the <strong>Pallet Assembly</strong>. A defined process that will be elaborated further in this document is followed to achieve the output. The simulation achieved is performed easily in a completely web-based setup</p>

<hr />
<h2 id="poster">Poster</h2>

<p><img src="assets/0311_poster.PNG" alt="Poster" /></p>

<hr />

<h2 id="workflow-development">Workflow development</h2>

<table>
  <tbody>
    <tr>
      <td> </td>
      <td><strong><center>CONTROL:</center></strong><center>  JSON schema - System Motion </center></td>
      <td> </td>
    </tr>
    <tr>
      <td><strong>INPUT:</strong> <br /> 3D model placed in VEB.js</td>
      <td><img src="assets/00.Workflow.png" alt="scenecreation" width="2000" height="2000" style="vertical-align:middle" /></td>
      <td><strong>OUTPUT:</strong> <br /> JSON schema of the animation sequence to import in VEB.js</td>
    </tr>
    <tr>
      <td> </td>
      <td><strong><center>RESOURCE:</center></strong> <center>  VEB.js - Excel  </center></td>
      <td> </td>
    </tr>
  </tbody>
</table>

<h2 id="workflow-building-blocks">Workflow building-blocks</h2>

<table>
  <thead>
    <tr>
      <th style="text-align: left">Acitivities</th>
      <th style="text-align: left">Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left"><strong>A1</strong> <br />  JSON Schema into VEB.js <br /></td>
      <td style="text-align: left">• <strong>Description:</strong>  It is the JSON schema of the machine center integrated with the pallet assembly. This 3D model can be placed in the virtual space by importing this JSON schema in <a href="http://mi-eva-d001.stiima.cnr.it/vebjs/?inputscene=">VEB.js</a> using <strong>import scene</strong> option. <br /> <img src="assets/01.JSON_Schema_of_the_3D_model.png" alt="JSON schema of the 3D model" /><img src="assets/02.Preview_of_the_3D_model.png" alt="Preview of the 3D model" /> <br /> • <strong>Input:</strong>  JSON schema <br /> • <strong>Output:</strong>  A 3D model placed in VEB.js <br /> • <strong>Control:</strong>  Import scene option in VEB.js to handle the importation of the JSON schema <br /> • <strong>Resource:</strong>   VEB.js and the JSON schema <br /></td>
    </tr>
    <tr>
      <td style="text-align: left"><strong>A2</strong> JSON Schema of the Animation Sequence</td>
      <td style="text-align: left">• <strong>Description:</strong><br /> It is the JSON schema of the animation which requires some prerequisite steps. First we have to define the motion of the dynamic parts of the 3D model. This can be done by identifying the type of motion each dynamic part performs. This motion can be defined in a <strong>.txt</strong> file format. An excel spreadsheet has been developed in which only the frames, frames-per-second, intial position of the part and the final position of the part have to be provided as inputs. This spreadsheet will provide you with the ouput animation sequence. <img src="assets/03.Excel_input_values.png" alt="Excel_input" /> <img src="assets/04.Excel_ouput_values.png" alt="Excel_output" /> The .txt file with the defined motion sequence is the input for the JSON schema for animation. The motion is defined for three dynamic parts in this case, with two parts rotating and the third part performing a linear motion. Here it can be observed that the values are all same, this is in fact due to the type animation that has been used. It is an additive animation sequence, in which the animation is incremental with respect to the current position, or in other words each for frame the value is added to the value of the previous frame. In the image below, it can be observed that the first dynamic part is rotating along the Y-axis and the degree of motion is <strong>0 - negative 90 degrees</strong>. The second dynamic part has the range of motion defined from <strong>0 - 180 degrees</strong> along the Y-axis. The third dynamic part performs a linear motion and moves <strong>360mm</strong> along the Y-axis in negative direction or towards the pallet asembly. <img src="assets/05.Defined_motion.png" alt="Defined Motion" /> After defining the motion sequence, JSON schema of this sequence has to be developed. Fundamental infromation regarding the animation can be found <a href="https://virtualfactory.gitbook.io/virtual-learning-factory-toolkit/knowldege-base/instantiation-workflow/animations">here</a>. Image below shows a part of the JSON schema. <img src="assets/06.JSON_Schema_of_Animation.png" alt="JSON Schema" /> The second input is ready to be imported in the virtual space. Once 3D model has been imported, the JSON schema of the animation can be uploaded using the <strong>import animation</strong> option. Once both the schemas have been uploaded just play the animation. Examples of assets and animations can be found <a href="https://virtualfactory.gitbook.io/virtual-learning-factory-toolkit/use-cases/assets-and-animations">here.</a> <br /> • <strong>Input:</strong> .txt file with the defined motion sequence for the dynamic parts of the 3D model <br /> • <strong>Output:</strong> JSON schema of the animation sequence to import in VEB.js <br />  • <strong>Control:</strong> The motion sequence of the dynamic parts in the 3D model <br /> • <strong>Resource:</strong>  Excel for creating the .txt file with the motion sequenc <br /></td>
    </tr>
  </tbody>
</table>

<hr />

<h2 id="results">Results</h2>

<p>The resultant video shows us exactly how the machine center, by being in the virtual space, will perform in real world. A video of the above mentioned motions has been provided. Although this is not the final task, the ouput obtained here can be used as input for further tasks.</p>

<h2 id="conclusion">Conclusion</h2>

<p>The input of this task was actually the ouput of previous task of generating the 3D model in GLTF format. And the excel spreadsheet develped has been quite handy as instead of defining the motion for each frame, this spreadsheet just takes the input position and the output position and provides us with the motion sequence ready to be save in .txt format. The JSON schema provides the freedom to define the animation accordingly. The virtual reality is a graphic interface that has broader uses. This workflow can be taken to next step by coonecting a VR headset and interacting with the setup directly as the user himself will experince the virtual space first hand and he/she will be able to interact with the setup and extract the data.</p>

<hr />

<h2 id="avatar-for-me">Avatar for me</h2>

<p>Was a journey filled with knowledge and experience. The knowledge of XR technologies and how they are building the future of human kind. The experience of being a part of such a diverse group and able to connect with colleagues and professors with vast expertise. I was more involved in the machining use case and worked from the start, developing the 3D models so that they are compatible with the future tasks such as animating the model. During this process, I was exposed to various tools and softwares, some I had forehand knowledge about, while others I had to learn from the scratch. The lectures and the tutorials have helped a lot especially while working with theses completely new tools. And with the tutorials of the tasks performed by current students I hope the future students will find this project equally enjoyable and knowledgeable.</p>

<hr />

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0310/0310_3d_cad_web-based_vr.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Manufacturing Environment for a Machining Center and Pallet Assembly</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0311/0311_machine_animation.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Animation of Machine Center in Virtual Space</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

:ET