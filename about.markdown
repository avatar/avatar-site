---
layout: page
title: About
permalink: /about/
---
# About this book

## Aknowledgement

This    project was funded by the ERASMUS+ program under the grant N°2020-1-FR01-KA203-080184.

## Authors

| Name          | Firstname     | Institute    |
|:-------------:|:-------------:| ------------ |
| Noël          | Frédéric      | Grenoble-INP |
| Medina Galvis | Sergio Camilo | Grenoble-INP |
| Pinquié       | Romain        | Grenoble-INP |
| Urgo          | Marcello      | Polimi       |
| Mondellini    | Marta         | CNR          |
| Petrovic      | Petar         | UBelgrade  |

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0320/0320_JLLG3.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">JLL 2022 - Team 3</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Home</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

---

[![License](https://img.shields.io/badge/License-CC%20BY--NC%204.0-lightgrey.svg)](LICENSE.md)