---
layout: default
title: AR for operator training
parent: Application with Students
nav_order: 17
---
# Creation of an application in an XR environment for operator training

![02](assets/Img003.PNG){:height="500px" width="500px"} 

---

## Table of content

1. Abstract
2. Introduction
3. Literature review
4. Methodology
5. Results analysis
6. Conclusion
7. Bibliography

---
### 1. Abstract

This work is part of a study and research work carried out by a student of the school Grenoble INP/UGA-Industrial Engineering. This document details the methodology followed to develop an application on the Hololens 2 augmented reality headset for the training of operators for the startup and manipulation of robots on a production line.

*Key words : Augmented Reality, User Interface, Training, Supply Chain*


### 2. Introduction

This work is part of the AVATAR project, a European Erasmus+ project between researchers and students from the universities of Milan (Politecnico Milano), Belgrade (University of Belgrade) and Grenoble (Grenoble INP). The main ob-jective of the AVATAR project is to support and facilitate the introduction of AR/VR technology in the engineering environment. I got to know the project when I accompanied the visit of the recently graduated engineers of my school. I was able to participate in this project as part of the teaching study and research work proposed by my school. Thus, as part of this teaching, Frédéric Noël proposed me to work on this project by leaving me the freedom to find a use case in connection with my field of study, the supply chain.

Personally, I am interested in the future of the industry, what is called industry 4.0 and recently industry 5.0. This industry is an industry that would like to be more human, more sustainable and more resilient while continuing the achievements of the 4.0, that is to say, the integration of high technology in the production chains. In the school, we have a platform (Operation Management) which is the reproduction of a production line in real size, on this platform, there are many technologies, cobot, robot complex in their use (mobile conveyor, inventory drones, cobots 7 and 5 axes ...). In fact, I wondered if the use of augmented reality could be useful to facilitate the human within complex production lines like the platform we have at school. Thus, my work focused on the creation of an application in an augmented reality headset to train operators in the use of complex technologies.

To realize this application, I used the Unity software with the MRTK extension developed by Microsoft, it provides a very useful set of components and features for the development of cross platform mixed reality applications in Unity.

For the augmented reality environment, I developed on Microsoft’s headset Hololens 2 (figure 1), which is the successor of the Hololens and embeds a lot of new features like eyes tracking and is much more comfortable to wear.

![00](assets/Img000.jpg){:height="300px" width="300px"} 

*Figure 1: Hololens 2*

As this is a first prototyping of the application, I focused on a single cobot for the operator training, the articulated robot Panda (figure 2), commercialized by Franka Emika, it is a collaborative and articulated robot with 7 axes, used in many industries.

![00](assets/Img001.jpg){:height="600px" width="600px"} 

*Figure 2: Cobot Franka*

Finally, the objective of this application is that a normal operator can train himself to use the Franka cobot in an interactive and ergonomic way.

This report has for objective to present my work and to analyze the proposed solution, it is composed of the part on a review of the literature concerning my subject of work then I will present you the methodology which I set up to develop my application. Then I will analyze it by putting forward its strong points, its weak points and thus what can be improved, finally I will conclude on my study and my experience.

### 3. Literature review

**A bit of context**

During the last two decades and with the rise of the Internet, the industry has experienced great technological changes and an intensification of production due to an ever increasing consumption. While the fourth revolution is already well underway, the industry is seeing the emergence of a fifth revolution, allowing the human and the environment to be put back at the center of industrial concerns that had been somewhat pushed aside by the technological growth.[1] [2]

If we focus our vision on the supply chain, we realize that today these new technologies have made them evolve under different aspects, new structures, more and more complex economic models, mountains of data and especially humans. Where man has not been replaced by the machine, he must cohabit with it. To promote this cohabitation, the industry of the future is interested in promoting communication between human and machine, through technology such as XR for example.[2] [3]

**AR, VR, XR, what is it?** 

First of all, it is important to know that these technologies are not new, indeed, the first 3D helmet appeared in 1850. Since the 50’s, and the growth of digital technology, this technology has not stopped growing. Since 10 years, with a first application in the video games sector, this technology is more and more frequent in the supply chain, allowing to combine human capacity and virtual reality. [3] [4]

- Augmented reality (AR): it is a physical extension of reality through the addition of information
such as text, graphics, videos ... tothe real environment. [4]

- Mixed reality (MR): it is almost the same principle as for AR with the difference that physical objects can coexist with holograms.[4]

- Virtual reality (VR) : it is a totally artificial environment with 3D images generated by computer, it is the most immersive of the three. [4]

I want to underline that in this report, I use a lot the word AR to describe what I do, but it’s by abuse of language, it’s more the word MR that corresponds since the operator is in contact with the Cobot, a physical object.

**Application of AR in the supply chain**

Augmented reality has a large number of use cases at any level of the supply chain, as shown in Figure 3. The area that interests me in my research is that of production, more precisely the use of a machine by an operator in a production line.[5]

![02](assets/Img002.PNG){:height="400px" width="400px"} 

*Figure 3 : AR use cases in supply chain*

Studies have already been conducted on this subject, whether it is learning or safety, and they have proven to be fruitful, showing in several different situations that learning with a virtual reality headset is 25% faster than with a more standard format such as a manual or a video. [5] [6] [7]

**User Interface**

To finish, it seems to me essential to reference methods on how to develop human-machine interface in augmented reality, many tools exist like persona (to define different types of users), taking into account the outputs and others3. [8]

### 4. Methodology

**Handling of the software environment**

The first step was to adapt to the working environment. Indeed, I had never used Unity and even less coded in C#, a language necessary for some interactions in Unity. So with the help of Camilo Medina, I started to discover the working environment with simple interactions.

**Consideration of the hardware environment**

In a second time, I started to consider the devices with which I was going to work, first of all the Hololens 2 helmet and what it is possible to do with it, it is necessary to know that in addition to being able to detect the movement of the arms and a displacement in space, it is equipped with sound sensors in order to be able to detect verbal actions
and it is also equipped with tracker which makes it possible to know where the user looks. Without going into details, these features are very useful when the user has his hands gripped and wants to perform an action, counting on the fact that this action has been programmed to be triggered by the way or the look. 

This first handling allowed me to apprehend what are the outputs that the helmet provides to the user, a very important point when one makes human machine interface. The other device
that I was able to handle, is the Franka cobot, indeed to develop an application allowing the training of operator on this robot, they needed that I was myself able to understand how they work, so with the help of the already existing manual, I was
able to understand how the cobot work is what it was possible to do with and which is relevant for an activity on a supply chain.

**Upstream reflection on the introduction of RA**

Indeed, before anything else, it is important to think upstream, how an operator with a reality helmet is in line with the reality of the cobot. For this, it is necessary to define a common reverential, to do this, I used a QR Code, defining different distances useful for the experience of the operator in his training (the cobot, the emergency stop button, the on/off button ...). On the other hand during the development of my application, it was useful to have the right information to place the signals or information at a comfortable distance from the operator.

A second point, equally important, is to think about the input that the operator brings to the application but also the output that the application sends back to inform him. For example, if the operator clicks on a button and this button is not necessarily linked to the triggering of an action, the operator must be aware that his action has been taken into account. For my application, I considered two types of output, the first one is that during the action the button seems to be pressed,
and once it is pressed a little box is checked to let the operator know that the action has been performed and that he can go to the next one.

**Development on Unity**

Here we are finally at the technical part, to realize this part, I proceeded with many phases of test, as soon as a part of my application seemed to me well I was going to test it. For the interface, I opted for a basic theme with a menu where the operator can see a video of the cobot in action then a button that when pressed launches the cobot startup tutorial in
5 steps.

Once the tutorial is launched, the steps are as follows :

- Step 1 : Pre-start check
  - Make sure the emergency stop button is pushed in (The switch is indicated by an arrow)
  - Connect the robot to a power source
- Step 2 : Starting the cobot
  - Connect the Ethernet cable to Cobot’s computer
  - Turn on the main unit and wait about 1 minute for the lights to stabilise (The on/off button is indicated by an arrow
  - Turn on the Cobot’s computer
- Step 3 : Connection to the programming interface
  - Connect the Cobot to your computer via the internet cable
  - On your computer : Open Google Chrome and go to http://robot/franka.de/
  - Enter your login and password
  - Before going further, read the user manuel in the user menu (top right tab)
- Step 4 : First use
  - Unlock the robot axes : Click on the cadena in the lower right part of the application
  - Handling of the Cobot : Press the two grey buttons on either side of the end of the cobot and move it slowly
- Step 5 : Manipulation of the cobot
  - Create a new task via the corresponding tab (task)
  - To add a brick to "Unnamed-1"; drag and drop an application. We start with "Joint Motion"
  - Gently move the robot to the desired position by pressing the grey buttons and then click on "Add"
  - Once the movement is added, it is necessary to adjust the speed of the movement (it is advised not to exceed 70%)
  - Once all the commands have been added, test your program, pressing the black button and then click on "play", always keep your hand on the emergency stop button

Once the operator has completed an action, he ticks it and moves on to the next one, once all the actions of the same step are completed, the next step is launched. In order to visualize the application, there are in figure 4 and 5 two screens from the operator’s point of view when he uses the AR helmet and the application.

![03](assets/Img003.PNG){:height="500px" width="500px"} 

*Figure 4: Application menu*

![04](assets/Img004.PNG){:height="500px" width="500px"} 

*Figure 5: Some actions in the application*

## 5. Results analysis

In order to analyze my solution, I decided to adopt the SWOT5 method by detailing to analyze my solution under four aspects. These analyses are based on my personal observations as well as the feedback I got from people who were able to test my application (about 3-4 people with different knowledge of Cobot).

**Strengths and benefits**

- The simplicity of use and the clarity of the instructions: indeed, the technology proposed by Microsoft makes it possible to make clear and educational graphic interfaces. So I used it to design a sober and uncluttered application to facilitate understanding

- Interactivity: This is the advantage of augmented reality, this technology allows a very strong interaction with the operator.

- Finally, my application meets my first objective, that of allowing the use and understanding of the Cobot, no matter the level of knowledge of the person on the machine.

**Weaknesses and areas for improvement**

- The ergonomics of the application is not optimal, I only used the possibility to click on buttons, and for a user who is not used to the sensitivity of the gesture can be a problem.
- The Hololens 2, despite these improvements over the first version, remains quite uncomfortable when worn for a long time.
- My application lacks a bit of output, for now, they are purely visual, an operator who does not look in the right place could be confused.

**Opportunities and voices for improvement**

- To counter some weaknesses, we could consider using other inputs than touch, like the way or the look for example. Moreover, we could add some outputs (sounds for example) as soon as an action is done.
- On the other hand, the application could be extended to training for other machines present on the platform.
- Finally, it would be interesting to add modes, if a person wants to go further in the use of a machine

**Threats and risks**

- The first and main threat comes from the hardware, indeed as it is quite uncomfortable if worn for too long, an operator might want to use a user manual instead.
- The technology used (AR) is a very innovative technology and few people have already handled it, as it requires a time of adaptation, some could be discouraged at the beginning and quickly leave it aside.

## 6. Conclusion

To conclude on my work and my research experience, I would like to thank F. Noël, C. Medina and the students and teachers of the AVATAR project for their knowledge sharing throughout the project. I am quite satisfied with the application that I managed to prototype knowing that I had never worn an augmented reality headset. This experience allowed me to convince myself a little more that these technologies are part of the industry of the future and that they can considerably increase
the experience and comfort of use on the production lines.

However, it is necessary to take a step back on these technologies and ask yourself before starting a project, what is the potential added value of integrating these technologies and especially if they are in the direction of the industry of the future which places the human being and the transition at the center of its concerns.

## 7. Bibliography

- [1] Hermann Mario, Pentek Tobias, Otto BorisDesign, Principles for Industrie 4.0 Scenarios: A literature review, 2015
- [2] Uthayan Elangovan Industry 5.0 : The Future of the Industrial Economy, Dec 2021
- [3] Massimo Merlino, Ilze Sproge The Augmented Supply Chain, 2016
- [4] Deloitte, Utilizing virtual reality to drive supply chain innovation, 2018
- [5] Naveen Joshi, 4 ways the supply chain industry will use Augmented Reality, 2018
- [6] Melanie Senum, VR Training for Logistics Companies
- [7] Camille Mulquin, Upskilling supply chain with virtual reality : 4 use cases, 2020
- [8] Regina Koreng, Heidi Krömker User Pattern for AR in Industrial Applications, 2021


---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0316/0316_HMI_Robot.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">HMI in a collaborative human-robot workplace</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0318/0318_JLLG1.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">JLL 2022 - Team 1</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>