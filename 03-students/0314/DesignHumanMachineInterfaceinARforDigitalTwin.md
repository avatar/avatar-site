<!-- ---
layout: default
title: Design a Human Machine Interface in AR for Digital Twin
parent: 1st Year Student Work
nav_order: 15
--- -->

# Design a Human Machine Interface in AR for Digital Twin
## Hybrid Manufacturing Environment for a Machining Center
- - -
## Introduction
The purpose of the study is to design a Human Machine Interface (HMI) in AR for digital twin. Allowing the visualisation and interaction with the indicators that are part of the Supply Chain management (Production, Logistics, Maintenance, etc) thanks to the HoloLens 1. The indicators must be linked to a 5 axis CNC machine. The interface also aims at the possibility to control the machine and to simulate certain scenarios.

![HoloLens1](assets/HoloLens1.png)

## Workflow Description
As the machine was not in operation, I imagined the possible indicators. It would be necessary to make a more in-depth study by observing an operator at work, identifying the needs more precisely, knowing what kind of information the digital 5-axis machine gives and how to improve the display with augmented reality. 

Then, I used these indicators to imagine the interface and the possible kinds of interactions such as gesture, speech and gaze recognition but also the kinds of feedback given by the device. 
![Interface](assets/Interface.png)

Next, what I had to do was to learn how to superpose the digital machine on the real one, the easiest way for the operator and then how to create an interface and interact with it. Thus, I learned how to used Unity for AR and more especially I discovered the MRTK possibilities chart. 

![workflow](assets/workflow.png)

Afterwards, I had to anchor the digital machine on the real machine. 
The programming language looks like Java, so it wasn’t so hard to learn it for me. But, I had to understand the logic of Unity and AR. 
As it was hard to directly move the machine because it is quite big, I created a small cube in order to use it as a button to move the virtual machine on the real one.
As it wasn’t efficient, we cannot ask an operator to do that before starting its task ! That’s why the next step was to place the virtual machine automatically and with great precision on the real machine thanks to QR code recognition.
![Anchoring](assets/anchoring.png)

Then, I created the interface thanks to the MRTK TOOLKIT. I had to create the menu and the interaction. First it is needed to understand how to make the menu hologram with all the different "visions" of the menu possible. Next, it is needed to position (and make visible or not) the different pages of the menu. It is possible to create a page or a menu that follows you when you move so that you always see it. The difficulty is therefore to follow the user's gaze. In general, there is an acceptable latency but not perfect fluidity.
[Menu and Security Zone](assets/menu.png)

For instance, concerning the security zone which is a cube, I created a security button in the menu, and when I clicked on this button, the security zone appeared thanks to a simple code : security On Off.


## Results
We obtain an accurate enough anchoring and an interactive interface that is useful for an operator. The design of the interface has to be improved.  The disadvantage is that with HoloLens 1, to select an object, you have to point it by moving your head. So, it can be a bit tiring and uncomfortable. The Hololens 2 may resolve this problem and give even more possibilities ! 


## Conclusion 
This is a method to anchor a digital and a real machine and to create interactive interfaces.
To achieve this, you need a back ground in programming and robotics.
After the success in anchoring the real and the virtual machine, creating the interface and the information interactions, the next step would be to be able to collect the datas and be able to interact on the real machine by  interacting on the virtual one. 

---
#### Lucie PELLISSIER
#### University of Grenoble 
#### lucie.pellissier@grenoble-inp.org
---
### Avatar for me 
When welcoming the students for the oral exam of “Génie industriel - Grenoble INP”, I showed and discovered the R room with all these fascinating technologies. When I received the email presenting the project, I took the opportunity to learn how to use these tools ! In fact, I had no skills in XR but I had a background in CAD, kinematics and code editing. Then, I was interested in working on application of VR and AR in industry within the Supply Chain (to improve the process and reduce any kind of waste for instance).Thus, I started to research virtual reality and augmented reality applications in the Supply Chain. 

As an engineering student, I was excited to develop competencies in innovative technologies for the industry 4.0 that are useful for industrial performance by taking into account social and human factors. I have made a lot of progress and still have so much to learn about these incredible tools. I really would like to improve myself and learn others steps in order to find good uses of AR within the Supply Chain and more specially to improve the environmental performance.
 