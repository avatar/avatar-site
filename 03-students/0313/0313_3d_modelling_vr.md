---
layout: default
title: 3D Modelling in VR
nav_order: 14
parent: Application with Students
---
# 3D Modelling in Virtual Reality
Computer Aided Design (CAD) is widely used by engineers, architects, and construction managers. They used it as an alternative to drawing by hand. There is absolutely no doubt about the fact that the use of CAD tools brings some advantages such as improve creativity as well as innovative, thanks to CAD, designers can make more accurate representations and easily modify them to improve the quality of the design. 
Virtual reality is getting more and more interest for supporting engineering activities in industry 4.0, especially for reviewing candidate design solutions. Nevertheless, the modelling of the geometry is still based on traditional CAD software before being transformed into polyhedral objects that can be visualized with a virtual reality device. However, in the future, we may expect engineers to directly sculpt 3D shapes in virtual reality.
The objective of this project is define and prototype an immersive virtual environment allowing to model the structure of a system. 

- Model 3D geometric entities (point, plane, cylinder, sphere, cube, cone, prism, block, pyramid)
- Model linear and circular repeat operations
- Model plane symmetry operations
- Model 3D positioning constraints: dimensional and geometric
- Export the geometry in an exchange format (preferably STEP)

---
## Poster
![Poster](assets/0313_poster.PNG)

---

## Workflow Description
![Work_Flow](assets/WorkFlow.PNG)

Our solution is to use CAD B-Rep kernels and VR game engine. The main benefit from combining these two applications is that the CAD models made in VR could be viewed, edited, and evaluated in the traditional CAD after the user completed the modeling. 
The effectiveness of using the game engine is the user has an immediate and natural sense of depth, scale, and proportion. They can look at the model in real 3D, rather than a 2D projection onto a computer screen within the VR environment.

 1. FreeCAD
FreeCAD is an open-source parametric 3D modeler made primarily to design real-life objects of any size [1]. FreeCAD allows to import and export models and many other kinds of data from dozens of different file formats such as  STEP, IGES, OBJ, STL, DWG, DXF, SVG, SHP, STL, DAE, IFC or OFF, NASTRAN, VRML, OpenSCAD, CSG... 
The communication between the FreeCAD core and the user interface is coded in Python, a flexible, user-friendly, easy to learn programming language

 2. Unity 3D
Unity is a cross-platform game engine developed by Unity Technologies [2]. Unity has an excellent Integrated Level editor with supporting JavaScript and C# for scripting. The main benefit of Unity is support for EditorXR's extensible framework, we can create content directly in extended reality. EditorXR [3] allows viewing things from the user's perspective while still accessing the full capabilities of the Unity Editor.

 3. The connection between Unity and FreeCAD
As mentioned above FreeCAD works based on python and Unity supports Java and C# so we need to find the connection between C# and python as well as Unity and FreeCAD. In 30 Mars 2021, Unity release a new version of the Python for Unity package [4], this time Unity support python 3.7.  Thanks to this we can use FreeCAD base on python 3.7 with Unity.

---

## Results
- Create shape canonical: cube, cylinder, sphere
- Translation and rotation object.
- Boolean operation: subtraction, union, interaction
- Change dimension by push and pull surface
[Demonstration](https://www.youtube.com/watch?v=JS6ZBkyxQG8)

## Conclusion 
This environment will better support the direct modelling on canonical shapes, for example by using face-dragging with VR-controllers instead of dimensional properties edited with a keyboard and a mouse. Another improvement will concentrate on the VR-metaphors for 2D sketching, like the fitting of planar curves with VR controller trajectories, and the fitting of sketch constraints with VR controller gestures

---
### Avatar for me 
Avatar is an international project where our targets are very diverse with different profiles of young people have their own specific types of desires. Therefore the goal we are trying to reach and the leading process will take a different shape and a different length of time for each individual. Thanks to this project, I have learned a lot of think about Virtual Reality and Augmented Reality. Not only did I learn interesting knowledge about science, but there I also met great professors, they guided us with passion. I had the opportunity to meet new teammates, we learned how to work in a team and how to collaborate between oversea universities. Finally, I would like to thank the Avatar  for giving me the opportunity to study and work in a dynamic and friendly environment.

### References
[1 "FreeCAD."](https://www.freecadweb.org/)

[2 "Unity 3D"](https://unity.com/)

[3 "EditorXR"](https://github.com/Unity-Technologies/EditorXR) 

[4 "Python for unity"](https://forum.unity.com/threads/python-for-unity-release-announcements.1084688/) 


---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0312/0312_gcode.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Gcode Interpreter and Toolpath Generator</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/0314/0314_Desing_HMI_for_AR.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Design a Human Machine Interface for AR</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>