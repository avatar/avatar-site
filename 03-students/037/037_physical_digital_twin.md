---
layout: default
title: Physical and Digital Twin
parent: Application with Students
nav_order: 8
---
# Physical and Digital Twin

In this use case, the objective was to make a physical prototype of the robot arm piloted by its twin in virtual reality. The main goal is to control the robot arm in different ways as manual or virtual ways.

---
## Poster
![Poster](assets/037_poster.png)

---
## Workflow development

| |  **<center>CONTROL:</center>**<center> Physical part assembly - Accuracy and responsiveness of the comunication - effectiveness of robot arm control via Simulink - Precision of manual control - Accuracy of angle instructions from VR </center>|  |
| **INPUT:** <br> CAD Models robot arm - servomotors | <img src="assets/037_workflow.png" alt="scenecreation" width="2000" height="2000" style="vertical-align:middle">| **OUTPUT:** <br> A robot arm controllable through the Unity interface in VR <br>  |
| | **<center>RESOURCE:</center>** <center>  SolidWorks - 3D printer - Servomotors - MATLAB Simulink - Arduino - Unity 3D </center>|   |

## Workflow Description

|  Acitivities |   Overview      |
|:---|:------------------|
| **A1** <br> Redesign and production  <br> | • **Description:** The redesign and production of the real prototype of the robot arm have commenced using CAD models on Solidworks. Modifications to the CAD models are required to properly integrate the servomotors into the arm's links. In total, there are six servomotors needed in this prototype. Then, thanks to the use of a 3D printer, each part of the robot arm is efficiently and cost-effectively produced for testing purposes.<br> •  **Input:**  CAD models, servomotor specifications (dimentions) <br> • **Output:**  Adjusted CAD models, 3D printed for robot arm prototype assembly  <br>  • **Control:**  Part assembly and fitting of servomotorss <br> • **Resource:** SolidWorks, 3D printer, servomotors  <br> |
| **A2** <br>  Physical-Digital connection <br> | • **Description:**  A critical point is the connection between the physical and digital prototype. A connection was designed between MATLAB Simulink and Arduino, which represents an interface card for the communication between the robot and the Simulink project. With this software, the robot is intended to be correctly controlled in both ways thanks to the communication between Simulink and Unity through the local server.  <br> •  **Input:** The physical robot prototype, digital twin in Simulink   <br> • **Output:** A bidirectional communication link between the physical and digital robot arm   <br>  • **Control:** Accuracy and responsiveness of the comunication <br> • **Resource:** MATLAB Simulink, Arduino, Robot arm prototipe <br> |
| **A3** <br> Multiphysics model <br> | • **Description:** It is necessary to build the multiphysics model to respect the requirements. The work focused on the connection of the angular instructions with the virtual model and then to establish the link between Arduino and Simulink, to prepare the manual and virtual control of the robot. The advantage offered by Simulink is to propose the entire Arduino library in download. Thus, it is possible to use the Arduino IDE functions in MALTLAB Simulink if necessary. Simulink can recognize these functions `servoRead` and `servoWrite` when they are used. The goal is to replace the virtual robot in Simulink with the real one: when we integrate real parts or motors into a Simulink project, it is called Hardware-in-the-loop. The servo motors are linked to the Arduino board on different pins. Simulink recognizes the Arduino board thanks to the compilation of a .PDE code on the board, which allows to use the Arduino library in Simulink. <br> • **Input:**  Arduino functions and library, robot arm prototipe, MATLAB Simulink project <br> • **Output:** Arduino-integrated Simulink project that can interact with the robot arm prototipe <br>  • **Control:**  Accuracy of Arduino-Simulink integration, effectiveness of robot arm control via Simulink  <br> • **Resource:** MATLAB Simulink, Arduino library <br> |
| **A4** <br> Manual control <br> | • **Description:** By implementing the Arduino library and connecting angular instructions to the servo motor pins, it is possible to manually control the robotic arm using a block parameter interface. <br> •  **Input:** Servomotor pins, angle instructions, block parameters interface  <br> • **Output:** A manually controllable robot arm  <br>  • **Control:** Precision of manual control  <br> • **Resource:** MATLAB Simulink, Arduino library  <br> |
| **A5** <br> Virtual control  <br> | • **Description:**  The major point of data communication in the project is the two computers that each host a project, one the Simulink project and the other the Unity project. It should be noted that the ports for sending and receiving data must be changed depending on the IP address of the two computers. The objective now is to adapt the manual control to the virtual control. It is necessary to modify the input data interface for the real prototype. The Operator parameter block needs to be converted into a virtual interface in Unity. The angular instructions will come directly from Unity. It is possible to get the data sent by Unity for the angle instructions, after having informed what angle in unity is required. Thanks to 6 different levers, it is possible to choose the angle to be sent to each servomotor and then send the values to the Simulink Project, which will send the converted values to the Arduino and move the servomotor. Now the virtual robot in Unity reproduces the behavior of the real robot with the instructions of the angles and not the real angles with errors certainly.  <br> •  **Input:**  Unity project, VR headset, control interface, command for angles <br> • **Output:** A robot arm controllable through the Unity interface in VR <br>  • **Control:** Accuracy of angle instructions from VR, communication latency <br> • **Resource:**  MATLAB Simulink, Unity 3D, Arduino  <br> |

---
## Results

We can control the real robot thanks to the virtual interface in Unity easily. Two improvements can be made on the robot : to make a more intuitive interface (with joysticks) to facilitate handling of the robot in virtual reality and to establish automation of the data transmission because it could be a requirement of the industrial specifications ; to use a system of recovery of the current position (angle) of the servomotors. Rotary encoders allows to give a feedback of the position of a servomotor by positioning then along the axis of the servomotor concerned. Thanks to Arduino, we can collect this data and send it to Simulink which establish the connection with Unity. Then, Simulink send the date to the input of the virtual servomotor and by this way, the virtual robot in Unity will reproduces the behavior of the real prototype with potential errors of angle (exchange of information).

## Conclusion

We can show several uses as manipulation of a robot without any risk for the user for instance because it is remote handling of a robot. It opens to other possibilities as automation and programming series of movements for a use in industrial production line. It can become a low cost remote simulator.

---

### Avatar for me

Avatar project was a great experience for me, over all thanks to the collaboration between nations and projects. I havelearnt about XR technologies and their applications in several fields of the engineering or the industry. I will reuse this experience in my professional career because I think it was a relevant opportunity in my student life.

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/036/036_from_cad_to_simulation.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Exporting from CAD an URDF package usable for simulation and VR</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/038/038_IA_for_inverse_kinematics.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Using Reinforcement Learning to simulate robot Inverse Kinematics in Unity</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

