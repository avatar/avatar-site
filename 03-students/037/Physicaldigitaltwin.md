<!-- ---
layout: default
title: Physical and Digital Twin
parent: 1st Year Student Work
nav_order: 8
--- -->

# Physical and Digital Twin
### The robot arm

- - -

## Introduction

In this use case, the objective was to make a physical prototype of the robot arm piloted by its twin in virtual reality.

## Workflow Description



![Workflow](assets/00.Workflow.png)

The main goal is to control the robot arm in different ways as manual or virtual ways. I have started with the redesigning and production of the real prototype of the robot arm, from the CAD models on Solidworks. A modification of the CAD models is necessary to insert correctly the servomotors to the links of the arm, there are 6servomotors needed in this prototype. Then, thanks to the use of a 3D printer, we obtain each part of the robot quickly and at low cost to test. The most important point in this use case is the connection between the physical prototype and the digital one thanks to MATLAB Simulink and Arduino, which represents an interface card to communicate between the robot and the Simulink Project. With this software, we can hope to control the robot correctly in the two ways. Thus, it is possible to connect Simulink with Unity through the local server at this time. For this part of the project, there is no code.

We have to elaborate a multiphysical model to respect our expectations. The work was focused on the connection of the angle instructions with the virtual model and then to establish the link between Arduino and Simulink, in order to prepare the manual and virtual control of the robot.

![Workflow](assets/01.Workflow.png)

The advantage that Simulink offers is to propose all the Arduino library in downloading. So, we can use the functions of the Arduino IDE in MALTLAB Simulink if necessary. Simulink can recognize these functions (servoRead, servoWrite) when they are used. The goal is to replace the virtual robot in Simulink by the real one : when we integrate real parts or motors in a Simulink project, it calls Hardware-in-the-loop. The servomotors are linked with the Arduino Card on different pins. Simulink recognize the Arduino card thanks to compiling a .PDE code on the card, that allows to use the Arduino library in Simulink.

![Workflow](assets/02.Workflow.png)

Thanks to the implement of the Arduino Library and the connection of angle instructions to the servomotors pins, we can control manually the robot thanks to an interface of block parameters.

![Workflow](assets/03.Workflow.png)

The most important point in the data communication in the project is the two computers which hosts each a project, one the Simulink Project and the other they project. We modified the ports of sending and receiving data based on the IP address of the two computers. The goal now is to adapt the manual control to the virtual control. The modification of the interface of the input data for the real prototype is necessary. The block parameters Operator needs to become a virtual interface in Unity. The angle instructions will come from Unity directly. We can collect data sent by Unity so the angle instructions, after having informed which angle under Unity we need.

![Workflow](assets/04.Workflow.png)

Thanks to 6 different levers, we can choose the angle we want for each servomotor and then send the values to Simulink Project which send the converted values to the Arduino. We can have access to the instruction sent by Unity on the SImulink project. Now the virtual robot on Unity reproduces the behavior of the real robot with the angle instructions and not the real angles with certainly errors.

![Workflow](assets/05.Workflow.png)


## Results

We can control the real robot thanks to the virtual interface in Unity easily. Two improvements can be made on the robot : to make a more intuitive interface (with joysticks) to facilitate handling of the robot in virtual reality and to establish automation of the data transmission because it could be a requirement of the industrial specifications ; to use a system of recovery of the current position (angle) of the servomotors. Rotary encoders allows to give a feedback of the position of a servomotor by positioning then along the axis of the servomotor concerned. Thanks to Arduino, we can collect this data and send it to Simulink which establish the connection with Unity. Then, Simulink send the date to the input of the virtual servomotor and by this way, the virtual robot in Unity will reproduces the behavior of the real prototype with potential errors of angle (exchange of information).

## Conclusion

We can show several uses as manipulation of a robot without any risk for the user for instance because it is remote handling of a robot. It opens to other possibilities as automation and programming series of movements for a use in industrial production line. It can become a low cost remote simulator.

---

#### Francesca Luciani

#### Grenoble INP

#### Francesca.luciani@grenoble-inp.org

---

### Avatar for me

Avatar project was a great experience for me, over all thanks to the collaboration between nations and projects. Iâ€™ve learnt about XR technologies and their applications in several fields of the engineering or the industry. I will reuse this experience in my professional career because I think it was a relevant opportunity in my student life.

---

