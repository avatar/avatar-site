<!-- ---
layout: default
title: Inverse Kinematics Using Reinforcement Learning
parent: 1st Year Student Work
nav_order: 9
--- -->

# Inverse Kinematics Using Reinforcement Learning in Unity
## Kinematic Modelling 
- - -
## Introduction
The package ML-Agents offers to implement machine learning in Unity projects. The project was to explore the implementation of reinforcement learning to create a Inverse kinematic model of the digital twin in a given scenario.
## Workflow Description
![Workflow](assets/Workflow.png)
## Results
A pick and place scenario was developed. Several brains were trained, having effective behaviours relatively to the given requirements. However the movements of the digital twin may lack of precision and efficiency to be used as they are.
![Video1](assets/Video_1.mov)
![Video2](assets/Video_2.mov)

## Conclusion
The models developed are flexible and can be quickly adapted to minor changes in the scenario. Improvements can be made in the training and treatment of output datas to get more accurate behaviours. 

---
#### Hugues MICHEL
#### Grenobl-INP
#### hugues.michel@grenoble-inp.org
---
### Avatar for me
This project is an amazing way to explore a subject and have the possibility to exchange with international students about the details of our works. Furthermore, I love the idea of developping knowledges that can be useful to other students in the future.
---
