<!-- ---
layout: default
title: CAD of a Cobot to a VR Scene
parent: 1st year Student Work
nav_order: 7
--- -->

title: CAD of a Cobot to a VR Scene

# CAD of a Cobot to a VR Scene
### Hybrid Human-Robot Processes
- - -
## Introduction
The main objective of this part was to unerstand how to create a scene on Unity from a complete CAD model (Solidworks). 
Especially the role of the different paramaters in the CAD model and the impact of them on a virtual scene.
## Workflow Description
![Workflow_Descritpion](assets/Workflow.png)
Inputs are some information about the cobot Franka Emika and an incomplete Solidworks model. Then the output is a scene in VR which introduce the digital twin notion.
## Results
The results will be presented in several points following the chronology that I could follow during the project.

##### 01. CAD model on Solidworks and parameters
The first point of my project was to exploit the incomplet CAD model. It was incomplet because this model doesn't have the link and the correct definition
of the parts. 
So, i renamed the differents parts in order to have the same parts name as the official robot description on the website of Franka Emika.
After that, i make a correct assembly with the correct mates for having the real movement and constraints of the cobot.
![Cobot on Solidworks](assets/Cobot_SW.png)
I've got some limits on my model because some parts aren't closes so they're not volumes and create problems for determinate inertia on URDF file.


##### 02. URDF 
This part was the main subject of my project. The creation of an URDF file is essential for having a good simulation of the movement on VR. The URDF needs some parameters which are specific
to the cobot.
All URDF files are composed of this information : 
- Definition of the different axis for the rotation
- Inertia of every part
- Dynamic aspect with the velocity and friction for instance
- Limits of rotation or translation in order to define collision area

To create the URDF, I used a plugin on solidworks and it looks likes the two pictures below.
![Plugin URDF on Solidworks](assets/URDF_1.png) | ![Plugin URDF on Solidworks](assets/URDF_2.png)

We can look an extract from an URDF file which is official and distribute by the constructor of the cobot.
This extract is the description of "Link_0". That's the foot of the cobot.
![URDF of Link_0](assets/URDF_example.png)

It was divided in three parts:
- Properties of inertia
- Details about visual that correspond to meshes editing by Solidworks
- Details about collision: type of area, size

This work about URDF was necessary for understanding the importance of Solidworks model and the different parameters during assembly
and the definition of the mates between parts.

##### 03. Unity, ROS and Visualization

Once the URDF file is created, we can now use it on Unity and ROS to visualize our cobot in virtual reality.
After creating a scene on Unity, we were able to import the URDF file and the Meshes files so that Unity 
could generate the cobot with all the parameters presented in the paragraph above.

![VR result](assets/VR_result.png)
That what we see on the VR headset after transferring the URDF file and the meshes on a Unity scene.

To go further, we simulated with Camilo a pickandplace system. 
We used ROS and Unity to achieve this simulation. 
The video below (VR_result) shows this operation from the point of view of the VR headset.
![Video of VR result](assets/VR Result.mkv)

I make a small flowchart in order to illustrate the device set up for this simulation.
![Flowchart devices](assets/Flowchart.png)

## Conclusion 
To conclude, it was very important to have a complete and precise CAD model in order to facilitate the exportation of the URDF file
and had a virtual model as real as possible.

---
#### DUBOIS Alexandre
#### Grenoble INP Génie Industriel
#### alexandre.dubois1@grenoble-inp.org
---
Avatar for me has represented a chance to discover new technologies which are the key to the industry 4.0 . I was thrilled to be able to participate in this project and work with international students.

---
