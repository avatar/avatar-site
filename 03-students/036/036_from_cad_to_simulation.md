---
layout: default
title: From CAD to VR
parent: Application with Students
nav_order: 7
---
# Exporting from CAD an URDF package usable for simulation and VR

Process to extract from a series of static robot parts in SolidWorks a package to be used in simulations or virtual environments. SolidWorks plays a critical role in defining parameters that will directly impact the simulation. It starts with the import and simplification of the model parts, followed by the definition of the system dynamics through mates and constraints. Then the prepararion of the assembly and finally the export of the package ready for simulation.

![ML unity](assets/img0.png)

---
## Poster
![Poster](assets/036_poster.PNG)

---

## Workflow development

| |  **<center>CONTROL:</center>**<center> Robot data sheets -  Solidworks - Interference Detection - Assembly Verification    </center>|  |
| **INPUT:** <br>  Robot CAD parts .step | <img src="assets/036_workflow.png" alt="scenecreation" width="2000" height="2000" style="vertical-align:middle">| **OUTPUT:** <br> URDF Package for simulation |
| | **<center>RESOURCE:</center>** <center> Solidworks - ROS </center>|   |

## Workflow building-blocks

|  Acitivities |   Overview      |
|:---|:------------------|
| **A1** <br> Geometric definition <br> | • **Description:**  Import all robot parts properly named using the convention (Link1, Link2... Linkn). These parts must be simplified, i.e. it is necessary to remove components that are not necessary for the overall model, such as bolts, nuts, washers and other minor accessories that do not affect the overall functionality of the robot. This process is vital to optimize the model once it is used in a virtual environment or in a simualation sofware. <br> •  **Input:**  CAD parts .step  <br> • **Output:** Detached CAD parts in a SolidWorks assembly <br>  • **Control:** Robot data sheets <br> • **Resource:** Solidworks and Robot manufacturer or supplier <br> |
| **A2** <br> Kinematic assembly definition <br> | • **Description:** Define the mates (associations) and constraints (such as surface to surface, concentric, distance, parallel, etc.) between the robot links. These mates and constraints allow to create a relative motion between them, simulating the real operation of the robot.  <br> • **Input:**  CAD Assembly <br> • **Output:** Kinematic Assembly <br>  • **Control:** Verification and validation of the kinematic assembly, Solidworks - Interference Detection - Assembly Verification <br> • **Resource:** Solidworks <br> |
| **A3** <br> [URDF Exporter](http://wiki.ros.org/sw_urdf_exporter) <br> | • **Description:** <br>  Add the URDF Exported (Unified Robot Description Format) plugin in SolidWorks. This Plugin is used to create a ROS (Robot Operating System) package that includes a directory for meshes (geometric representations), textures (surface materials) and robots (urdf files). URDF files are essential in ROS as they describe the physical characteristics of a robot, such as its size, shape, color, and the relationships between the various parts. For assemblies, the URDF exporter will build the links and create a tree based on the SolidWorks assembly hierarchy.  <br> • **Input:** Kinematic assembly and URDF Exporter <br> • **Output:** Solidworks capability to export a URDF package <br>  • **Control:**  ROS Documentation <br> • **Resource:** Solidworks and ROS <br> |
| **A4** <br> URDF Package for simulation and VR<br> | • **Description:** To export a URDF file from SolidWorks, first make sure that all components of the model are correctly named and organized. During this process, it is crucial to correctly define the links and joints, including their orientation, limits of motion and other physical properties. Once the export is complete, a package including the URDF file and a mesh of the robot geometry is created, which can be imported into ROS for simulations and other software such as Unity 3D.   <br> • **Input:**  Robot assembly and URDF Exporter <br> • **Output:**  URDF package of the robot <br>  • **Control:** Import the URDF package into ROS and verify the Robot behavior <br> • **Resource:** Solidwork, URDF Exporter and ROS <br> |

---
## Conclusion 
To conclude, it was very important to have a complete and precise CAD model in order to facilitate the exportation of the URDF file and had a virtual model as real as possible.

---

## Avatar for me
Avatar for me has represented a chance to discover new technologies which are the key to the industry 4.0 . I was thrilled to be able to participate in this project and work with international students.


---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/035/035_DT_cobot.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Digital Twin of a Cobot</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/03-students/037/037_physical_digital_twin.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Physical and Digital Twin</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>