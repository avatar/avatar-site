---
layout: default
title: Joint learning lab
parent: Trainer Teacher Guidelines
nav_order: 4
has_children: true
---

# Joint Learning Laboratory - JLL

The joint learning laboratory is an event organised once a year hosted by one of the three partners. It answers to three main goals:

- To consolidate what was made in every countries by renewing activities on a new use case. The students were trained in their original country to specific use cases. By travelling to a new university, they discover a new use case and re-apply what they learned in this new use case with new technologies new libraries and application interface.
- To reinforce sharing a common knowledge and know-how about VR. Even if they follow a few lessons about the main XR concepts the remote activity does not ensure to share a real common knowledge. During the joint learning lab, they belongs to international teams that expect cognitive and practice alignment. The hosts students must be able to explain, to share and train their colleagues to the local practice while the invited students must adapt themselves and why not promote other solutions.
- To reinforce networking between students. During the first period students are connecting together through the various existing remote connection solutions (video-conferencing, chat, email). But obviously to meet physically is a better opportunity for networking. We assume that the joint learning lab is a major step to reinforce networking between students at the European level. It directly participates to the European policy supporter through the Erasmus+ projects

## Hosting team

The host is in charge to prepare the overall week. The task is technically complex to ensure operational labworks during the joint learning lab.


<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/024-joint-learning-lab/0240-jll-2021/0240-jll-2021.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore→</div>
        <div style="color: blue;">Joint Learning Lab 2021</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/024-joint-learning-lab/0241-jll-2022/0241-jll-2022.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore→</div>
        <div style="color: blue;">Joint Learning Lab 2022</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/024-joint-learning-lab/0242-jll-2023/0242-jll-2023.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore→</div>
        <div style="color: blue;">Joint Learning Lab 2023</div>
      </a>
    </td>
  </tr>
</table>

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/023-development-curricula/development-curricula.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Development of the Curricula</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/024-joint-learning-lab/0240-jll-2021/0240-jll-2021.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Joint Learning Lab 2021 - JLL 2021</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>


<!-- #### Visit of local labs
{: .fs-4} 
- International teams : 3 or 4
- Applying their new workflow competencies on a new use-case

#### Final presentation
{: .fs-4}  -->



