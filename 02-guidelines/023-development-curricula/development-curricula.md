---
title: Development of the Curricula
layout: default
parent: Trainer Teacher Guidelines
nav_order: 3
---

# Development of the Curricula

This section provides a complete outline of the AVATAR Curricula designed to facilitate the understanding and application of Extended Reality (XR) technologies, focused on a manufacturing environment and the creation of a Digital Twin. The curriculum begins with a Welcome Meeting, followed by four lessons that address the focus of the program and ends with collaborative work in the Joint Learning Laboratory. 

---
## Welcome Meeting

### AVATAR Project 

- Overall objectives
- Team presentation 
- Student self presentation 

### The two themes 

Use Case 1: Hybrid Manufacturing Environment for a Machining Center
- Scenarii illustrations
- Engineering process 

Use Case 2: Hybrid Human-Robot Processes  
- Scenarii illustrations
- Engineering process 

### The Overall agenda 

- Date of lessons
- Date of Joint Learning Laboratory

### Lessons presentations 

Quick overview of the lessons
  - XR Basics Awareness
  - Create Interactive Scenes
  - XR For Manufacturing
  - Human Perception Impact

### Sharing contacts  

Software and tools for communication and sharing

---
## Lesson 1: XR Basics Awareness

### Discovering Extended Reality (XR) technologies

- Definition of Virtual Reality (VR) vs Augmented Reality (AR) vs Mixed Reality (MR)
- History of VR and AR
  - Timeline of events and tech development
- Classification of technologies
- Cybersickness issues and other limitations Perception distorsion, Uncanny valley
- Main stream to prepare a XR environment
  - The main steps when developping a project
  - 3D modelling, Translation to VR, etc, device connexion, sensors integration, inter process communication, systemic approch, cyberphyscal systems

|[🔗 Download full Lesson - PDF version](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-1/AVATAR_LESSON_1%20-%20XR_BASICS.pdf?inline=false)|

### Human Perception

- General notes on SYSTEMIC context – MULTIMODAL Human – Manufacturing System Interaction
  INTERFACING of BIOLOGICAL (human) and ENGINEERED (technical) spaces within the FACTORY ENVIRONMENT
- Visual Perception System
- Auditory Perception System
- Smell Perception
- Tactile Perception System
- AVATAR Multimodal XRTechnology for Experiential Learning

|[🔗 Download full Lesson - PDF version Part 1](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-1/AVATAR_LESSON_1%20-%20HUMAN_PERCEPTION.pdf?inline=false)|

|[🔗 Download full Lesson - PDF version Part 2](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-1/AVATAR_LESSON_1%20-%20HUMAN_PERCEPTION_SMELL.pdf?inline=false)|


---
## Lesson 2: Create Interactive Scenes

### SCENE Creation, ANIMATION and XR INTERACTIONS

- General considerations about CADtools, basic and advanced use
- Conceptual framework of the CAD-based XRInteraction Modeling & Control – AVATAR Approach
- AVATAR & SolidWorks XRInteraction Platform
  - Project #1: KINEMATIC PAIR
  - Project #2: MINIMAL ROBOT ARM – SCARA
  - Project #3: DATA STREAMING
  - Project #4: COLLISION CONTROL

|[🔗 Download full Lesson - PDF version](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-2/AVATAR_LESSON_2%20-%20SCENE_Creation_ANIMATION_XR_INTERACTIONS.pdf?inline=false)|

### Digital Model for XR

- Exchange processes (Data formalizationand exchange)
- Various type of XR scenes behaviors 
- OWL ontology-FactoryData Model
- Rendering parameters 

|[🔗 Download full Lesson - PDF version](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-2/AVATAR_LESSON_2%20-%20Digital_Model_for_XR.pdf?inline=false)|

### Implementation within usual game engines

- Unity for Virtual Reality Augmented Reality and Robotics
- WebGL-based application using babylonjs

|[🔗 Download full Lesson - PDF version Part 1](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-2/AVATAR_LESSON_2%20-%20Unity_for_Virtual_Reality_Augmented_Reality_and_Robotics.pdf?inline=false)|

|[🔗 Download full Lesson - PDF version Part 2](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-2/AVATAR_LESSON_2%20-%20WebGL-based_application_using_babylonjs.pdf?inline=false)|


---
## Lesson 3 : XR For Manufacturing

### Digital Twin and Applications

- Digital Twin, Digital Master and Digital Shadow
- Digital Twin classification scheme
- Manufacturing system design
- A Digital Twin for Process Reconfiguration

|[🔗 Download full Lesson - PDF version](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-3/AVATAR_LESSON_3%20-%20Digital_Twin_and_Applications.pdf?inline=false)|

### A complete digital chain to enable the Digital Twin of a shop floor

- Conditions to get a digital twin
- Shopfloor digital twin
- Digital continuity
- 3D digital twin
- Human collaboration

|[🔗 Download full Lesson - PDF version](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-3/AVATAR_LESSON_3%20-%20Digital_Twin_Shop_Floor.pdf?inline=false)|

### ROBOTIC DIGITAL TWIN - Basic interactions and data information flow

- General observations specific to Robotic Digital Twin
- PT → DT data flow–Robotic Digital Shadow
- DT→PT data & information flow –Digital Twin for XRPbD& LfDApproaches

|[🔗 Download full Lesson - PDF version](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-3/AVATAR_LESSON_3%20-%20Digital_Twin_XR_Robotic.pdf?inline=false)|

---

## Lesson 4 : Human Perception Impact

### Human perception effects in VR/AR

- Main issues
- Discrepancy between vision and inner ear perception
- Parameters impacting discrepancy
- Simulation in VR
  - Effects of latency
  - Simulation complexity
- Telepresence and immersion

|[🔗 Download full Lesson - PDF version](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-4/AVATAR_LESSON_4%20-%20Human%20Perception%20Impacts.pdf?inline=false)|

### XR Sensors
- Location tracking in XR environment
- Inertial tracking
- Optical tracking


|[🔗 Download full Lesson - PDF version](https://gricad-gitlab.univ-grenoble-alpes.fr/avatar/avatar-site/-/raw/master/Downloads/Lessons/Lesson-4/AVATAR_LESSON_4%20-%20XR_SENSORS.pdf?inline=false)|

---

## Joint learning Lab

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/024-joint-learning-lab/0240-jll-2021/0240-jll-2021.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore→</div>
        <div style="color: blue;">Joint Learning Lab 2021</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/024-joint-learning-lab/0241-jll-2022/0241-jll-2022.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore→</div>
        <div style="color: blue;">Joint Learning Lab 2022</div>
      </a>
    </td>
  </tr>
</table>

<table style="width:100%;">
  <tr>
    <td style="text-align:left;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/024-joint-learning-lab/0242-jll-2023/0242-jll-2023.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Explore→</div>
        <div style="color: blue;">Joint Learning Lab 2023</div>
      </a>
    </td>
  </tr>
</table>

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/022-teaching-workflows/022-teaching-workflow.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Teaching Workflows</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/024-joint-learning-lab/024-joint-learning-lab.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Joint Learning Laboratory - JLL</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>


<!-- #### Visit of local labs
{: .fs-4} 
- International teams : 3 or 4
- Applying their new workflow competencies on a new use-case

#### Final presentation
{: .fs-4}  -->


