---
layout: default
title: Organisation
parent: Trainer Teacher Guidelines
nav_order: 0
has_children: false
---

# Organisation

The AVATAR project selects about 15 students per year from three universities, the constraints are very different, so each partner is responsible for recruitment. The program is organized around a continuous work cycle during the spring semester, which culminates in reporting, activity assessment, and a final jury evaluation. Successful students will receive a certificate acknowledging their participation in the program. 

### Navigation Structure
{: .no_toc }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>


---
## Selection process 

### Heterogeneous constraints

The selection process refers to the selection of students. About 15 students per years are targeted, around 5 from each university partner. Excellence in selection is expected, especially in disciplinary knowledge and proficiency in English, which should be fluent. The constraints from the 3 universties are fully different and cannot be easily unified: 
1. The unversity curricula and semester schedules are not the same
  - The spring semester at the University of Belgrade usually starts in the second week of February and ends after 15 weeks, at the end of May or the beginning of June, depending on the calendar year.
  - Spring semester in Polimi starts at end of february and ends at the beginning of June.
  - Spring semester in Grenoble-INP start on the third week of january and ends at the end of may.
2. The disciplinary knowledge profiles of students are deeply different
  - Students from the University of Belgrade have a deep knowldge in manufacturing technologies, robotics, factory automation and mechatronic systems design (up to the level of microcontroller programming and control/sensor hardware integration), then in product design and extensive skills in the use of various CAD tools for engineering design, as well as a strong theoretical background in control systems, with respective coding skills (from assembly level for microcontrollers programming up to a high-level computer languages, such as C#, Matlab, Python and similar).
  - Students from Politecnico di Milano are from both Industrial and Mechanical engineeing courses.
  - Students from Grenoble-INP are from industrial logisitic and product devlopment.
3. the graduation level is also very different :
  - At Belgrade University students are exclusively at master level M1
  - At Polimi students are recruited from both bachelor's and master's degree level.
  - At Grenoble-INP students are at level L3, M1 or M2

With such constraints a unique selection process does not make sense. Therefore, a flexible approach is used, with some general constraints related to required disciplinary knowledge (use of CAD tools, computer programming and manufacturing systems understanding), as well as required level of proficiency in English. Every partner is responsible of the recrutment following the processes of the following sections.

### Belgrade University selection process

Only students of the Faculty of Mechanical Engineering (FME) participate in the AVATAR project. Engineering studies at the FME are organized according to the Bologna model, format 3 + 2 (3 years B.Sci + 2 years M.Sci level, ECTS 180 + 120 = 300). Taking into account the previously acquired knowledge and skills, only M1 level master's students (first year M.Sc.) are invited to participate in the AVATAR course. The invitation is public and is primarily addressed to students of Production Engineering and students of the Control Engineering department. 

The admissions process starts at the beginning of the winter semester and continues throughout the semester in a sequence of the following five steps:

- **Public announcement and invitation**

General information about the AVATAR project is publicly announced with an invitation to interested students a the 1st year of the M.Sci. level to apply for participation in the AVATAR course; activities take place during October, beginning of the winter semester at UBelg.

- **Pre-selection** 

The student application consists of: (a) personal information about the student/applicant, (b) motivation letter – this letter should present the applicant’s motivation to enroll the AVATAR course, preferred study track and mobility, the competencies and skills he / she would like to achieve, future perspectives and aspirations after the course, etc., and (c) CV with information about relevant experience and professional training; the collected EoI forms of interested students are analyzed and based on the estimated performance of disciplinary knowledge, English language skills, as well as students' motivation to participate in the AVATAR project, a group of at least 10 students who are believed to be potentially good candidates is selected; selected students are then invited to the first consultative meeting where basic information about the AVATAR project is shared, students are interviewed and given the opportunity to ask questions; activities take place during November.

- **Selection**

The final selection is carried out using a questionnaire for self-evaluation of students; collected questionnaires are analyzed, students are ranked, and based on that, the 5 best placed students are selected (taking into account gender equality to the extent possible); all students are informed about the results of the selection process; activities take place during December.

- **Preparatory activities** 

Selected students are invited to two working meetings; at the first working meeting, they get acquainted with the working environment in which the teaching will take place (presentation of the laboratory space, key equipment and software tools) and with the key personnel (instructors and technicians); at the second working meeting, introductory information about virtual and augmented reality technology, digital twin technology, as well as key details about the organization of work during the spring semester is communicated to the students; activities take place during January.
By the end of January, the UBelg group of students is fully ready to engage in the AVATAR course.

- **Final approval** 

A group of selected students is presented to the coordinators of all partner organizations for final approval; the activity is carried out immediately before the beginning of the spring semester at a special consultative meeting.
The list of selected UBelg students who have attended the AVATAR course in three academic years, 2020/21, 2021/22 and 2022/23 is as follows:

- **Spring semester 2020/21**

| No. | Student ID | Name               | Gender | Major                |
|-----|------------|--------------------|--------|----------------------|
| 1   | 1008/20    | Veljko Vučinić     | Male   | Control Engineering  |
| 2   | 1002/20    | Jovana Kovačević   | Female | Control Engineering  |
| 3   | 1335/20    | Aleksa Žarković     | Male   | Production Engineering |
| 4   | 1006/20    | Marija Veselinović | Female | Control Engineering  |
| 5   | 1097/20    | Dušica Baralić     | Female | Production Engineering |

- **Spring semester 2021/22**

| No. | Student ID | Name                | Gender | Major                |
|-----|------------|---------------------|--------|----------------------|
| 1   | 1012/21    | Slobodan Radovanović| Male   | Production Engineering |
| 2   | 1004/21    | Nevena Nikolić      | Female | Control Engineering  |
| 3   | 1014/21    | Dušan Božić         | Male   | Control Engineering  |
| 4   | 1054/21    | Bogdan Kostić       | Male   | Control Engineering  |
| 5   | 1041/21    | Nikola Petrović     | Male   | Control Engineering  |
| 6   | 1131/21    | Valentina Obradović | Female | Production Engineering |

- **Spring semester 2022/23**

| No. | Student ID | Name               | Gender | Major                |
|-----|------------|--------------------|--------|----------------------|
| 1   | 1013/2022  | Matija Žuža        | Male   | Control Engineering  |
| 2   | 1016/2022  | Tamara Kandić      | Female | Production Engineering |
| 3   | 1014/2022  | Kristina Golo      | Female | Control Engineering  |
| 4   | 1089/2022  | Tijana Lukić       | Female | Production Engineering |
| 5   | 1054/2022  | Aleksandar Ćosić   | Male   | Control Engineering  |
| 6   | 1070/2022  | Nađa Belić         | Female | Control Engineering  |
| 7   | 1103/2022  | Pavle Mitrović     | Male   | Production Engineering |
| 8   | 1074/2022  | Marija Milićević   | Female | Production Engineering |

Total number of UBelg students who have successfully completed the AVATAR course in three academic years: 19 students, of which 10 students are female and 9 students are male, and 8 students with a specialization in production engineering and 11 students with a specialization in control engineering.

### Politecnico di Milano selection process

The admissions process starts at the beginning of the spring semester according to the following steps:

- **Public announcement and invitation**

General information about the AVATAR project is publicly announced ina set of courses within the mechanical and management engineering courses, with an invitation to submit an application.

- **Application** 

The application for students entails: (a) academic curriculum, (b) personal curriculum, (c) motivation letter. 

- **Selection**

The selection of the students is operated using a ranking indicators involving all the submimtted documents. The top ranked students are selected (taking into account gender equality to the maximum extent possible). All the students are informed about the results of the selection process.

- **Preparatory activities** 

The selected students are invited to a first introductory meeting to explain the organisational details of the project and the planned activities. Hence, the students are introduced in the official meetings of the project and start working on their own project work.

The list of selected Polimi students who have attended the AVATAR course in three academic years, 2020/21, 2021/22 and 2022/23 is as follows:

- **Spring semester 2020/21**

| No. | Name               | Gender | Major                |
|-----|--------------------|--------|----------------------|
| 1   | Aksay Kumar Asuthkar    | Male   | Industrial Engineering  |
| 2   | Alessandra Uva   | Female | Mechanical Engineering  |
| 3   | Tao Zhu     | Male   | Mechanical Engineering |
| 4   | Giovanni La Rosa | Male | Industrial Engineering  |
| 5   | Pavel Popov    | Male | Industrial Engineering |
| 6 | Juan Diego Diáz Artola | Male | Industrial Engineering

- **Spring semester 2021/22**

| No. | Name                | Gender | Major                |
|-----|---------------------|--------|----------------------|
| 1   | Pawar Aditi | Female   | Industrial Engineering |
| 2   | Alessandro Stefanone      | Male | Mechanical Engineering  |
| 3   | Matteo Schiattarella         | Male   | Mechanical Engineering  |
| 4   | Alessandro Zara       | Male   | Mechanical Engineering  |
| 5   | Gioacchino Moscato   | Male   | Mechanical Engineering  |
| 6   | Joakim Hoydal Tinholt | Male | Industrial Engineering |
| 7  | Leonardo Meacci | Male | Industrial Engineering |
| 8 | Lorenzo Curti | Male | Industrial Engineering |
| 9 | Aaron Raffa | Male | Industrial Engineering |
| 10 |  Ludovico Lingi | Male | Industrial Engineering |
| 11 |  Adel Ahmed Adel Rachid Fansa | Male | Industrial Engineering |


- **Spring semester 2022/23**

| No. |  Name               | Gender | Major                |
|-----|--------------------|--------|----------------------|
| 1   |  Alessandra Lupo        | Female   | Mechanical Engineering  |
| 2   |  Giovanni Maggiolo  | Male | Mechanical Engineering |
| 3   |  Andrea Casiraghi     | Male | Mechanical Engineering  |
| 4   |  Leonardo Lomacci      | Male | Mechanical Engineering |
| 5   |  Matteo Speranza  | Male   | Mechanical Engineering  |
| 6   | Marco Varisco        | Male | Mechanical Engineering  |
| 7   |  Saverio Rocchi    | Male   | Industrial Engineering |
| 8   |  Enver Eren  | Male | Industrial Engineering |

### Grenoble-INP selection process

The concerned students may be from various levels and departments from the School Grenoble-INP Industrial Engineering.

An announcement about the AVATAR semester is made for the Master's thesis program, which is worth 30 ECTS (M2). In this case students are fully dedicated to the master thesis during the spring semester, but need to focus on a personal method or tool development. It is also proposed for TER (Research initiation) at M1 level: it is a 5 ECTS activity. 

This announcement must be made in September. On the same date a project is also submitted for the design project. This activity at M1 starts in early September and ends at the end of May along the two semesters. During semester 1 the students are expected to create a specification document for a project submitted by a client. The spring semester is dedicated to the operational tasks to build the solution. In this case, the AVATAR program is viewed as a client and a team of 5 students are selected by the activity teachers. The overall activity leads to 10 ECTS.

All the students in these programs have already been selected based on excellence criteria. For AVATAR, student motivation is the main criterion for selection.

![](assets/img0.png)

[comment_text]: # snt
*Selection process at Grenoble-INP*{: .fs-3 .text-grey-dk-000}

[comment_text]: # fnt



The list of selected students who have attended the AVATAR course in three academic years, 2020/21, 2021/22 and 2022/23 is as follows:

- **Spring semester 2020/21**

| No. | Name                 | Gender | Major                            |
|-----|----------------------|--------|----------------------------------|
| 1   | Francesca  Luciani   | Female | Industrial Engineering           |
| 2   | Lamine Amir          | Male   | Master Degree Industrial Engineer |
| 3   | Lucie Pellissier     | Female | Industrial Engineering           |
| 4   | Hugues Michel        | Male   | Industrial Engineering           |
| 5   | Tran Thi Thanh Hoa   | Female | Industrial Engineering           |
| 6   | Dubois Alexandre     | Male   | Industrial Engineering           |
| 7   | Desgouttes Thibaut   | Male   | Industrial Engineering           |

- **Spring semester 2021/22**

| No. | Name              | Gender | Major                |
|-----|-------------------|--------|----------------------|
| 1   | Ryan Baude        | Male   | Industrial Engineering |
| 2   | Florian Piccolo   | Male   | Industrial Engineering |
| 3   | Farouk Anass      | Male   | Industrial Engineering |
| 4   | Mohamed Merrouche | Male   | Industrial Engineering |

- **Spring semester 2022/23**


| No. | Name                  | Gender | Major                                                         |
|-----|-----------------------|--------|---------------------------------------------------------------|
| 1   | Si Youcef Karima Sihem| Female | Industrial Engineering - Sustainable Industrial Engineering Master's Degree |
| 2   | Bekhrad Kaveh         | Male   | Industrial Engineering - Sustainable Industrial Engineering Master's Degree |
| 3   | Scordia Gweltaz      | Male   | Industrial Engineering                                        |
| 4   | Kan Kouakou           | Male   | Industrial Engineering                                        |

---

## Student teams

Two types of teams are created. Groups from each university are building 3 local teams. The goal is to create cross cultural exchange.  Three international teams are created with the following repartition : 

|        | Belgrade University                                               | Polimi                                                            | Grenoble-INP                                                      |
| ------ | ----------------------------------------------------------------- | ----------------------------------------------------------------- | ----------------------------------------------------------------- |
| Team 1 | ![hello](assets/img1.png) ![](assets/img1.png) | ![](assets/img1.png) ![](assets/img1.png) | ![](assets/img1.png)                                  |
| Team 2 | ![](assets/img1.png) ![](assets/img1.png) | ![](assets/img1.png)                                  | ![](assets/img1.png) ![](assets/img1.png) |
| Team 3 | ![](assets/img1.png)                                  | ![](assets/img1.png) ![](assets/img1.png) | ![](assets/img1.png) ![](assets/img1.png) |

If more than 5 students are selected from a given university, the teams are adjusted to ensure balanced teams.

The international teams are created in the first month of the spring semester. They must be operational during the Joint Learning Lab but create a first organisation to promote remote connections and networking from the three locations.



---

## Typical semester agenda

The spring semester agenda is organized according to the following GANTT chart. Before the semester starts, the main task of the teacher teams is to define and prepare the subjects. In parallel we start publicity towards the students to prepare selection following partner procedure.

At mid-February, the spring session is starting. Students are expected to fill a pre-questionnaire about their competencies on various subjects. This questionnaire is presented in the next section and is reused at the end to assess the student filling about their competency evolution.

The spring semester is organised along a quite continuous work  from the student split by four lessons organised in visio conference to share a common knowledge. 

The joint learning lab is the apogee of the project. Students are joining a common location during  one week to experience their knowledge to the technology (a machine, a cell whatever) owned by the host.

At last the project ends with reporting, assesment of the activity, the final jury to deliver a certificate to the students.

![](assets/img2.png)

---

## Student's evaluation

In a working world where digital knowledge and skills are fundamental elements for business, it therefore becomes of primary importance not only to teach but also to evaluate specific technical knowledge related to new technologies that permeate engineering after university. Luckly, researches show that participating in learning programs that use advanced techniques provides students with positive experiences and that students feel more confident in their abilities. This specific skills, namely the one that are job-related and are required to complete the job are called hard skills. Meanwhile, the world of work and especially the engineering world in recent years requires more and more parallel and non-specific skills, necessary to relate to colleagues and customers, to manage several projects in parallel, to organize one's work independently; companies prefer to hire person with positive attitude, who have effective communication and willing to work and learn These competencies are called soft-skills. The aim of the Erasmus+ AVATAR project was also to train young students to acquire both specific and parallel skills, and for this reason an assessment of these skills has been introduced in our work. 

It is necessary to propose the same standard test before and after the learning experience to assess learning effectiveness objectively. However, the project foresees the participation of students with very different backgrounds and, therefore, a highly heterogeneous group of knowledge, which would not allow the creation of a test of the same difficulty for everyone. Furthermore, one of the project's strengths is promoting collaboration and mixing knowledge. Another aspect indicative of scientific rigor is usually comparing the results of the experimental group with those of a non-experimental group; this comparison was not feasible in AVATAR. Firstly, not all the subjects dealt with in AVATAR are studied in a traditional way by all students. Secondly, during the AVATAR experience, students continue participating in other life and learning experiences, so it is difficult to isolate any change in the subject's skills solely due to the AVATAR impact. For this reason, subjective feedback from students through questionnaires and interviews is the most appropriate evaluation method.

In addition, since in some research the level of engagement towards a subject was associated with a more effective learning, some items in our questionnaire were aimed at measuring student engagement in AVATAR activities. The modified subscale of the SSSQ short stress state questionnaire (Helton, 2004) was used to assess this motivation factor. 

We proposed a questionnaire to our students; the tool was administered in an anonymous form via MS form and it was composed by different section:

- Self-definition by the student based on his/her school knowledge background
- Hard skills: self assessment of the students before and after the learning activities for hard skills, e.g., 3d modelling, etc. 
- Soft skills: self assessment of the students before and after the learning activities for soft skills, e.g., working in groups, presenting the results, etc.
- Engagement: assessment of the engagement of the students at the end of the learning activities.

The questionnaire, in its entirety, looked like this:

### Define yourself

How would you define yourself?

- Industrial engineering undergraduates with no experience in manufacturing systems
-	Mechanical engineering undergraduates with no experience in manufacturing systems but with experience in related topics, e.g., design.
- Other engineering undergraduates with no experience in manufacturing systems.
- Other engineering undergraduates with no experience in manufacturing systems but with experience in related topics, e.g., computer science.
- Other undergraduates with no experience in manufacturing systems, e.g., social sciences, design, etc.
- Industrial engineering master students who have taken the Manufacturing Engineering course (MSE) or with background in conceptual modelling of manufacturing systems and methodology for performance evaluation (analytical tools).
- Mechanical engineering master students who have taken courses in the design and analysis of manufacturing systems (Integrated Production Systems, …).
- Mechanical engineering master students who have not taken courses in manufacturing systems’ design and analysis.
- Trainees using the VR environment as a training experience.
- Engineers from industry working in the design and analysis of manufacturing systems, e.g., middle management for production, maintenance, etc.
- Users from industry evaluating the relevance of this environment for training (e.g., HR).

### Skills expertise

For each of the skills listed, indicate your expertise with a score from 1 (very poor) to 5 (totally agree)

| 1 | 2 | 3 | 4 | 5 |

1. **SIMULATION**
- Static modelling
- Mechanics, kinematics
- General systems theory & control
- Other simulation skills

2. **CAD MODELLING AND ANIMATION**
- CAD systems - part definition
- CAD assembly
- CAD kinematics
- 3D animation modelling, Blender, Maya, Sketchup
- 3D rendering (lights, texturing, etc.)

3. **MANUFACTURING PROCESS** 
- Identify and characterize the components of a manufacturing system.
- Analyze and characterize a product and its manufacturing process.
- Match the requirements of a manufacturing process with the capability of the equipment
- Identify the candidate pieces of equipment to be included in a manufacturing system.
- Identify possible alternative choices in the selection of the equipment to be included in a manufacturing system.
- Identify possible alternative choices in the matching of processes and equipment in a manufacturing system
- Select and integrate pieces of equipment, process-equipment matching and routing decisions in a manufacturing system configuration.
- Identify relevant KPIs for the analysis of a manufacturing system configuration.
- Assess the performance associated to a process and a workstation using proper tools and modelling hypothesis (e.g., level of detail).
- Analyze observations, experiments, data to derive an assessment of the performance of a process or a workstation or a manufacturing system.
- Compare alternative configuration options in a qualitative and quantitative way.
- Define proper modelling hypothesis for a manufacturing system
- Sketch a layout of a manufacturing system 
- Model a workplace configuration in terms of tasks, tools and human-related aspects (e.g., ergonomics).
- Model a manual-executed process in terms of execution rules and constraints
- Design a workplace configuration matching a set of requirements.
- Assess a workplace configuration in terms of ergonomics and comfort.
- Assess a workplace configuration in terms of performance.

4. **FUNDAMENTALS OF XR TECHNOLOGY**
- I already created 3D scene for XR
- I already developed XR applications
- I know various visualisation technologies
- I know various interaction devices

5. **LANGUAGE**
- English

6. **SOFT SKILLS**
- My overall communication skills
- My overall presentation skills
- Ability to work as a team member
- Solve conflicts
- Leadership
- Ability in comparing possible solutions of a problem and choose the most appropriate
- Ability in analyzing problems and situations
- Ability in planning my own work
- Ability in managing my time

7. **ENGAGEMENT**
- I am engaged in activities related to AVATAR
- I am concentrated during activities related to AVATAR
- I feel the control of the situation during AVATAR activities
- Activities related to AVATAR are challenging
- I am skillful at activities related to AVATAR
- AVATAR activities are important to me
- I succeed in AVATAR activities
- I am satisfied with how I am doing AVATAR activities

---

## Certificate

The AVATAR certificate is delivered to every student who completed successfully the semester. An absence to the joint learning lab is mandatory. Every partner may validate ECTS for his own university by following his internal rules, the student profile and the time spent in the project.

|![09](assets/img3.png){:height="1000px" width="1000px"}|

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/00-main/02-toolkit-guideline.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Trainer Teacher Guidelines</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/021-teaching-learning/021-teaching-learning-method.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Teaching and Learning Method</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>
