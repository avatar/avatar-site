---
layout: default
title: Teaching and Learning Method
parent: Trainer Teacher Guidelines
nav_order: 1
has_children: false

---

# Teaching and learning method

The AVATAR project is largely focused on pedagogical challenges, dominantly bringing them into the context of one of the primary goals of the project - **the development of the AVATAR course** and the extension of that task towards the possible integration of AVATAR course into the wider context of the curricula that already exist in the three partner Universities, and even more widely through the project's multiplier mechanisms.

The essence of the AVATAR course is that it is conceived as a **flexible experimental platform** that should serve to carry out research activities driven by various goals, including those that go beyond the AVATAR project. Two research objectives are obvious:

- Examination of the pedagogical dimension of XR technology learning when XR technologies, in some of their engineering and broader aspects, appear as a **subject of learning**. 

- Examination of the pedagogical dimension of XR technology learning, when XR technologies appear as a **pedagogical instrument**, a means that enables the innovation of the educational process in sense of improvement of its performances.

This kind of pedagogical dualism is a peculiarity of XR technologies in the context of education, in general.
Based on the above, the development of the AVATAR course and development of its components is structured around the following three pedagogical questions:

- Question of METHODOLOGY – a set of different pedagogical and didactic aspects of the development and implementation of the AVATAR course;
- Question of INTEGRATION – a set of different aspects of the integration of the AVATAR course in three partner universities: University Grenoble Aples - Grenoble Institute of Technology, France; Politecnico di Milano, Italy, and the University of Belgrade - Faculty of Mechanical Engineering, Serbia, taking into account the existing curricula, the model of the organization of the teaching process (going down to the level of very practical details and solving the problem of diversity of that kind), as well as wider cultural determinants; 

- Question of QUALITY ASSESSMENT – development of models and procedures for evaluating the quality of students' work on the AVATAR course during its three-year implementation period (three spring semesters of the academic school year 2020/2021, 2021/22 and 2022/23), including the process of selecting students who will be actively involved in teaching activities ( 5 students from each of the three partner universities per academic year, international cohort of at least 15 students).

These pedagogical questions are explored through a systemic framework. The question of methodology is further addressed in more detail with the aim of explaining the general theoretical guidelines that were used to design the AVATAR course.

## The question of methodology

The question of methodology considered within the AVATAR project is strongly bounded by the nature of its central topic of study - Virtual and Augmented reality technology (shortly XR technology). The phenomenological essence of XR technology is a radical advance, a kind of qualitative discontinuity in the representational capacity of the visual perception interface that exists in some context in which the immediate interaction of human with the digital world occurs. The leap from two-dimensional to three-dimensional (3D/spatial) perception leads to a fundamental change in the way the human brain perceives the digital world and strongly influences the expressiveness of the acquired experience, because the human cognitive system evolved through the presence of embodied spatial visual interaction with the environment. In addition, the extension of Virtual Reality to the so-called Concept of Mixed Reality should also be considered as an important advance. Mixed reality seamlessly merges the physical and digital worlds, affording natural and intuitive 3D interactions between people, computers (interactive digital models) and the physical environment. XR technology brings the cognitive processes of the human brain into a qualitatively new context, whose behavior, from the perspective of pedagogy and psychology of education, emerges as a new, largely unexplored topic.

The methodological basis of the AVATAR pedagogical framework is very broad, comprehensive, even holistic and organized around two key pillars: 

- AVATAR Pillar 1 – MATHEMATICS
- AVATAR Pillar 2 – DIDACTICS
- AVATAR Pillar 3 – XR to support learning vs learning XR

## AVATAR Pillar #1 – MATHETICS:

This pillar is fosucused to the research of cognitive dimension of the learning process and the methodological framework associated to this; This pillar relies of four adopted and then blended philosophical and psychological approaches in understanding the cognitive mechanisms of the human brain, and theories derived from this platform:

### Constructivism

An approach in the philosophy of mind that starts from the belief that knowledge acquired through learning is not attained but constructed! Cognitive construction, or constructivism, asserts two main principles whose application has far-reaching consequences for the study of cognitive development and learning, as well as for the practice of teaching and interpersonal management in general: (a) knowledge is not passively received but actively constructed by the learning subject, and (b) the function of cognition is adaptive and serves the organization of the experiential world rather than the discovery of ontological reality. Constructivism was pioneered by Jean Piaget who set out a theory explaining the mechanism by which the learning subject, who has access only to the sensations and operations of the mind, gradually builds a mental construction of a relatively stable experiential world. 

A particularly important extension of such an understanding of the cognitive process is found in the works of Leo Vygotsky and his concept of cognitive development, which is based on what he called the Zone of Proximal Development (ZPD), which further implies: (a) the introduction of the social context into the cognitive process, i.e. cooperative or collaborative dialogue with More Knowledgeable Others (MKO), and (b) the setting of practical rules for designing a Constructivist Learning Environment. The context of the ZPD is defined by its prefix, named Zone of Achieved Development (ZAD) and its suffix, named Zone of Distal Development (ZDD). The key pedagogical challenge is the recognition of the boundaries, personall as well for the entire students cohort,  of the ZAD - ZPD - ZDD context. 

### Embodied Cognition

An approach in the philosophy of mind that emphasizes the belief that cognition usually involves acting with the physical body on the environment in which that body is immersed, that is, that the mind is not only connected to the body but that the body influences the mind (we need a body to reason!). Embodied cognition complements the constructivist approach, shedding more light on the mechanisms by which the mind interacts with the environment. Taking into account the pedagogical peculiarities of XR technology, embodied cognition considered within the AVATAR project is bounded by the following theoretical directions: 

    (a) Enactivism
    (b) Phenomenology
    (c) Perceptual Psychology
    (d) Ecological Psychology
    (e) Affordance Theory

### Experiential Learning Theory

Kolb's experiential learning theory was chosen to be central to the AVATAR pedagogical approach. It draws heavily on fundamental approaches to the philosophy of mind, particularly on the long historical trace of inquiry into the nature of experience and the process of learning from it (emerged in the works of William James and his philosophy of radical empirism, John Dewey, Kurt Lewin, Jean Piaget and Lev Vygotsky and his theory of social constructivism, but undoubtedly, its intellectual origins can be found in the philosophy of John Locke, brilliantly summarized in his famous proposition: 'Nihil est in intellectu quod antea non fuerit in sensu' - there is nothing in the intellect that has not first passed through the senses, with which he categorically defended his empiricist view of the essence of cognitive mechanisms of the human brain, as well as no less famous (and important!) Leibniz's extension of Locke's proposition: 'nisi intellectus ipse' - meaning except the intellect itself, opening the way to the view later taken up by Kant, that 'the forms of reason form an innate structure conditioning the nature of experience itself'). The Experiential Learning Theory also offers very concrete guidelines and even practical ideas for designing curricula or courses, especially in the field of engineering education. 

As Kolb states in his book Experiential Learning - Experience as the source of learning and development, the process of experiential learning is a fourstage cycle involving four adaptive learning modes:

- Hands-on modes of learning 
    1.	Active Experimentation – doing (AE mode)
    2.	Reflective Observation – feeling (RO mode)

- Minds-on modes of learning
    3.	Concrete Experience – watching (CE mode)
    4.	Abstract Conceptualization – thinking (AC mode)

Knowledge results from the combination of two processes: (a) the process of grasping experience – refers to the cognitive process in which learners take in information, and (b) the cognitive process of transforming experience – refers to the process by which learner interpret and act on that information. 

Kolb further introduces the principle of dialectics into the experiential learning model. He interprets the process of grasping experience as a concrete/abstract dialectic, which takes place in the Concrete Experience - Abstract Conceptualization dimension, (CE - AC axis), and consists of two different and opposed processes: apprehension - grasping the immediate, tangible experience through sensory perception (feeling) and the direct experience with the world (doing), and comprehension - gathering knowledge and experience through conceptual interpretation (e.g. to break down experience into meaningful events) and symbolic representations (e.g. to place the meaningful events within a mental symbolic system). The transforming experience is interpreted through an active/reflective dialectic, which takes place in the Reflective Observation - Active Experimentation dimension, (RO - AE axis), and consists of two different and opposed processes: intention – the process which requires learner to reflect internally on its own knowledge, and extension – the process that involves active manipulation of the learner's external world. 

In addition to the above, the following is also important to note. First, the four-stage cycles are concatenated in time and thus create a continuous chain of learner's individual movement through the cognitive space. The process of experiential learning can therefore be viewed as a recursive process, a conical helix whose central axis represents both the time axis and the axis of the accumulation of what has been learned, i.e. the growth of the student's knowledge and, equivalently, the progress of the learning process. The two remaining axes are the CE - AC and RO - AE axes, described above, along which radial growth occurs in the experiential (hands-on) and mental (minds-on) subspaces. Second, and equally important, the concept of dialectics can be used to partition the learning space and, on this basis, lay the formal foundations of the concept of learning style.

Learning style can be differentiated when one or more learning modes are preferred over others to shape the experience, resulting in a narrowing and widening of the experience space around each of the four learning modes. Learning style can be paired with aspects of individuality in the learning process (individuality, the self, and learning style), as well as educational specialization (in engineering education). The same holds for matching (profiling) the learning style with technological aspects related to the XR technology and its mediation role in the experience grasping and the experience transforming.

Kolb's theory of experiential learning is recognized as the central theoretical basis of the AVATAR course, which is why its graphic representation in the form of a circle with four diametrically distributed spheres is incorporated into the logo of the AVATAR project. 

### The New Taxonomy of Educational Objectives

Provides practical guidelines for curriculum or course design, focusing on the eucational objectives and cognitive system of the learner. Unlike the traditional Bloom's taxonomy, the New Taxonomy of Educational Objectives structures the learner's thought system, into three components, further systematized in six hierarchically organized levels: 

1.	Cognitive System – responsible for the efficient processing of information that is crucial for completing a learning task, and consists of four constituents:
(a)	Retrieval (level 1)<br>
(b)	Comprehension (level 2)<br> 
(c)	Analysis (level 3) <br>
(d)	Knowledge utilization (Level 4)<br>

2.	Metacognitive System (level 5) - responsible for designing strategies for accomplishing a given goal once it has been set, and 
3.	Self-system (level 6) - a prime determiner in the motivation learner brings to a task of learning (learner's emotional response, but also hierarchy of goals that learner have built up, bit by bit over the years). 

Obviously, the Level 6 – the Self-system, is the highest level, while Level 1 is the lowest level of the hierarchy of human thought. 

Further, the New Taxonomy also organizes knowledge in three general categories:

1.	Information, 
2.	Mental procedures
3.	Psychomotor procedures

The reasoned aspect of teaching refers to the WHAT dimension of educational goals. The WHAT dimension is directly related to the three aforementioned general categories of knowledge. The intentional aspect of teaching concerns the HOW dimension of the educational process. The HOW dimension is directly related to the above-explained six levels of the student's thought system. This is how the learning environment, activities and experiences should be designed to support learner(s) achieve the intended and expected educational goals. 

The AVATAR pedagogical method relies on the New Taxonomy of Educational Objectives. The WHAT and HOW dimensions are coupled to create a two-dimensional relationship space. We called that construct the Pedagogical Matrix of Educational Objectives (PMEO). PMEO enables the process of designing the AVATAR course, or even the entire curriculum, to be realized in a systematic, formally consistent and tractable manner. It is important to emphasize that the AVATAR pedagogical method firmly relies on the complementary philosophical stratum, which is placed at its base, and consists of the triplet of aforementioned approaches to understanding the cognitive processes of the human brain and knowledge, namely: Constructivism, Embodied Cognition (with a selected set of its sub-directions) and Experiential Learning Theory.

## AVATAR Pillar #2 – DIDACTICS

This pillar is fosucused to the research of practical dimension of the teaching process and the methodological framework associated to this. For many years, this topic has been part of a wider discourse that is placed in the context of the reform of higher education, especially in the field of engineering and in general, the STEM corps, initiated by the need of the labor market and economy for a robot-proof generation of engineers who will be able to effectively exploit the latest technological achievements, first of all those related to digital technologies in all their forms. AVATAR Didactics rests on four components:

### Engineering method

The engineering method should be the starting point of any didactic approach designed for engineering education. Knowing and understanding the essence of the engineering method, its algorithmic structure as well as its psychological and motivational background, provides the best possible practical framework for creating a curriculum or courses, while keeping the focus on both educational goals and the learner's cognitive system. Engeinnering method serves as a logical basis for setting up different didactic approaches, for example problem-based learning, project-based learning, experience-based learning, inquiry-based learning, challenge-based learning and other forms of x-based learning and teaching. In this sense, project-based learning was adopted as the didactic basis of the AVATAR course. Students are presented with a problem and their task (or challenge) is to solve that problem through the synthesis of solutions that satisfy a set of given functional requirements, as well as a set of constraints, primarily related to the available laboratory equipment and the available time frame. All work activities are team-based, active/creative (not tutor-centered) and organized to take the form of a typical engineering project.

### Engineering thinking and engineering habits of mind

A complement to the engineering method, which takes into account a specific personality profile that is a natural talent for engineering and has an affinity for engineering challenges. This very interesting aspect was specifically addressed by the Royal Academy of Engineering (RAEng).

### Technological mediation

It seems that the issue of technological mediation in the cognitive process is particularly challenging and undoubtedly under-researched in the context of XR technologies. Although its scope exceeds the thematic framework of the AVATAR project, its importance is recognized and certain efforts have been made to integrate that aspect into the didactic foundations of the AVATAR course.

### CDIO Curricular Framework

The fourth didactic component of the AVATAR course is the organization of the curriculum within the framework of the four key activities of every engineering work: Conceive – Design – Implement – Operate = CDIO (web link: http://www.cdio.org/). Basically, CDIO represents the organizational complementation of the engineering method. Within the AVATAR course, a project-based learning environment is organized, from a didactic point of view, through the concept of a four-dimensional workspace equipped with appropriate equipment, tools and instructions: CONCEIVE space - enables students to envision a new system, reach understanding of user needs, and develop concepts; DESIGN space - supports the new paradigm of collaborative, digitally supported design based on XR technology and focused on the domain of XR and Digital Twins technology in the domain of manufacturing industry; IMPLEMENTATION space - enables students to physically manufacture or integrate their design, i.e. to build a physical system which includes mechanical, electronic, and software components; OPERATE space – offers opportunities for students to learn about operations, practically validate their design and gain experience by conducting their experiments. This didactically comprehensive concept of learning environment was used as a basis for setting up workflows designed to facilitate student work activities within the AVATAR student project and the Joint Learning Laboratory as its final activity. 

## AVATAR Pillar #3 – XR to support learning vs learning XR

Extended reality may impact curricula in many directions. Avatar projects enumerates the following learning directions :

### Learning XR

First XR is an innovative technology which requires many technologies and and knowledge but also which impacts our senses. Most students do not know the difference between AR and VR our Mixed Reality. A curriculum about XR  may develop this complex knowledge and know-how along the following steps.
  
### Discovering XR

The basic concepts and an initial practice are learned at this level. The perception of more or less presence in various devices (HMD, CAVES, etc) is a very first step to understand XR. It is also a major step to distinguish what is realistic today or in the next few year respect to commercial announcements.
  
  
### Developping XR 

Environment requires almost computer science capacities, sometimes it requires automation or electronics knowledge for developing new devices but the step remains the creation of 3D scenes and scripting specific behaviors. Training this step is quite simple for computer scientists but becomes very complex as soon as more senses, devices, sensors are integrated in the XR environment. It is also a kind of specialty in computer science or in another application domains as soon as students are accepting to enter coding tasks. The discovering step is mandatory before Developping XR environments.

### Spécification of XR environment

For a specific applications. Based on the "discovering XR" step, with few knowledge about "development", it is possible to train students to specify new XR environment. This specification should be used by other engineers to develop the corresponding XR environment. In this case understanding the context and constraints of the specific domain (health care, industry, museum, whatever it is !) is mandatory and must be linked to a good knowledge about XR technology capacities to build meaningful specifications.

### Using XR for expert training and usual expert work 

On another hand XR should become a tool deployed in everyday practise. Then XR may be used in many other process where the application result is the main goal and where introduction to XR technology should be minimized. XR technologies may be used to train somebody to a specific expert process:
  
### By providing a virtual space where operator can replicate physical actions

Indeed it is not so new, since every fligth simulators are the perfect illustration of this type of training. One can imagine a lot of applications in many domains. No much school or universities invested this direction, surely because of costs, but also because of efforts to create virtual environment and to maintain it along the time. Anyhow this direction remains a great opportunity where AVATAR should initiate new projects.
  
### By training students to new way of doing things through XR

XR is a source of potential developments to renew expert practice. Unfortunately XR remains quit new in its developments and there is few emerging standard applications than can be promoted. One can observe a recent evolution in the deployment of applications and this training mode will have to be adapted to new existing tools.
  
### Enactive training 

Enactive training is another opportunity opened by XR. The point is that human knowledge and intelligence is not reduced to conceptual reasoning. Perceptions, gestures repetitions or filling are strong enablers for understanding some phenomena. Enactive training is a branch of XR opportunities where we use XR technologies to provide better perceptions of concepts in a complementary mode of traditional lessons. Enactive modalities are seldom developped surely because it requires deeply specialized about a concept and simultaneously interested in developing XR.

AVATAR did not filled all the above directions but proposed a framework to start filling them in a collaborative mode at an international scale. AVATAR clearly worked about the "learning XR direction and creates some environments to train some specific processes within XR for the industry 4.0 domain.

In the subsections we first provide teacher worklflows for:

- **Discovering XR**

- **Developping XR environments**

- **Spécification of industry 4.0 environment**

- **Developping training and assesment environment for machines**

By covering this aspects we fill 4 directions over the six previously described.

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/020-organisation/020-organisation.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Organisation</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/022-teaching-workflows/022-teaching-workflow.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Teaching Workflows</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>