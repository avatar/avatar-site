---
layout: default
title: Joint Learning Lab 2023 Results
parent: Project Assesment
grand_parent: Trainer Teacher Guidelines
nav_order: 1
has_children: false
---

# Result JLL 2023

The following chapter lists the results reported by the students during the third year of AVATAR. In particular, the results relating to students' perceived hard and soft skills, engagement, and responses to interviews will be shown.

Taking awareness from the experience of the second year of AVATAR, during this year non-parametric analyzes have been performed on the results to compare the change of hard and soft skills between t0 and t1, namely before and after the students' experience of AVATAR. The choice was made as the number of participants is small. Furthermore, for each variable, in addition to the means, the modes and medians are reported.

## Hard skills

*Simulation skills*

<img src="images_jll3/simulation skill.jpg" width="800">

Regarding *simulation skills*, no difference emerged between t0 and t1. 

*CAD modeling skills*

<img src="images_jll3/CAD_1.jpg" width="600">
<img src="images_jll3/CAD_2.jpg" width="600">

With respect to CAD modeling skills, students felt they had improved especially in Systems part definition and CAD assembly skills; the difference between t0 and t1 is statistically different in these two variables.
There is a general tendency to feel improved in the other variables.

*Manufacturing skills*

<img src="images_jll3/manuf_1.jpg" width="800">
<img src="images_jll3/manuf_2.jpg" width="800">
<img src="images_jll3/manuf_3.jpg" width="800">
<img src="images_jll3/manuf_4.jpg" width="800">

We can see a perceived improvement in almost all the variables related to the "manufacturing" category. 

*XR skills*

<img src="images_jll3/xr.jpg" width="800">

Finally, all the variables relating to knowledge of the fundamentals of XR technologies increased.

## English

<img src="images_jll3/inglese.jpg" width="500">

No perceived improvement in English proficiency emerged.

## Soft skills

<img src="images_jll3/soft_1.jpg" width="800">
<img src="images_jll3/soft_2.jpg" width="800">

No differences were reported in perceived soft skills after the AVATAR project.

## Engagement

<img src="images_jll3/engagement.jpg" width="800">

High levels of engagement in activities related to AVATAR were reported by students, especially with regard to the desire to succeed in tasks, general motivation and commitment.

## Interviews
At the end of the JLL experience, the students participated in individual interviews. The questions, which students could answer freely and with the assurance that answers would be reported anonymously, explored:
- positive and negative aspects, and suggestion related to the online lectures, in particular: the moment of the day, the duration, topics, modality; furthermore, general comments related to lectures were grouped in the "other" section
- positive and negative aspects, and suggestion related to the Joint Learning Laboratory experience, in particular: laboratory visits, activities (the challenge proposed), organisational aspects and social aspects; Furthermore, general comments were grouped in one "general" section;
- the best moment experienced during the year and related to the project.

The results of the interviews are shown in the tables below:

<img src="images_jll3/INT_online_1.jpg" width="800">
<img src="images_jll3/INT_online_2.jpg" width="800">

In short words, regarding **online lectures**, students reported some connection and volume problems and the perception that the lessons were too long. The suggestion was to make lessons more frequent and shorter, or to insert a pause during the same day. Regarding the topics addressed, obviously for some students they were perceived as interesting, while for others as too specific and difficult to understand or, on the contrary, too general and not very useful in practice; the general suggestion was to "link" the lessons with knowledge needed during the JLL, namely to use the lesson hours to explain topics that are then used during the meeting week (e.g. coding, kinematics).

<img src="images_jll3/INT_JLL_1.jpg" width="800">
<img src="images_jll3/INT_JLL_2.jpg" width="800">

Concerning the **JLL** experience, the visits to the laboratories were highly appreciated activities. The proposed challenge created conflicting considerations. On the positive side, students reported a feeling of usefulness, of having learned useful soft skills. There was also a personal satisfaction in succeeding in something difficult. On the negative side, some students have expressed frustration, stress, the feeling of having lost too much time compared to the result obtained. One suggestion is to prepare the students by warning them about what they will have to face, so as to allow at least a mental preparation for the event. Furthermore, a general introduction of useful knowledge could be given to at least understand what is being talked about during the activities. With respect to the organizational aspects of the JLL, the days were perceived as full but enjoyable.

<img src="images_jll3/INT_JLL_3.jpg" width="800">
<img src="images_jll3/INT_JLL_4.jpg" width="800">

The social aspects of JLL were greatly appreciated, especially working together and communicating in English; the general perception was that interacting with others in a foreign language was helpful. The need to better balance the groups emerged so that there were at least two students of the same native language in the same group and it was suggested to insert an initial moment to introduce students in the whole group. Furthermore, the general feeling is that of having learned something useful for life, not just for the career.

<img src="images_jll3/INT_BEST.jpg" width="800">

The **best moments** reported by the students are mainly divided into general appreciation of the experience, and visits to the laboratories.


---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/025-project-assesment/0250_results/0250_jj2_results.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Result JLL 2023</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/00-main/application_students.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Application With Students</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>