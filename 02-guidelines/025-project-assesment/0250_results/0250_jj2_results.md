---
layout: default
title: Joint Learning Lab 2022 Results
parent: Project Assesment
grand_parent: Trainer Teacher Guidelines
nav_order: 0
has_children: false
---

# Result JLL 2022

The next graphics show the results of the sample for hard skills at time 0 (namely before participating in the AVATAR project) and time 1 (after the project), divided by sections. Tables show the means of each variable aggregated in simulation skills, CAD skills, skills related to manufacturing processes, and fundamentals of XR technology.

*Simulation skills*

<img src="images_jll2/SIM.jpg" width="600">

*CAD modeling skills*

<img src="images_jll2/CAD.jpg" width="600">

*Manufacturing skills*

<img src="images_jll2/MANUF_1.jpg" width="600">
<img src="images_jll2/MANUF_2.jpg" width="600">


*XR skills*

<img src="images_jll2/XR.jpg" width="600">

In summary, students reported that they had improved all simulation and CAD knowledge aspects. Compared to manufacturing processes, the improvements are less but still present, except for two specific capabilities: "analyze and characterize a product and its manufacturing process," the result of which remains unchanged, and "identify and characterize the components of a manufacturing system," the result of which is greater at t0. The greatest perceived improvements are found in the skills related to XR technologies. 

## English

Results show that perceived proficiency in the English language has increased little. 

<img src="images_jll2/ENGL.jpg" width="600">

## Soft skills 

<img src="images_jll2/soft.jpg" width="600">

The table below shows the students’ results for soft skills at time 0 and time 1. As can be seen, problem-solving (the ability to analyze problems + the ability to compare solutions) has increased; leadership and communication skills (presentation and oral communication) also scored higher in t1. On the other hand, some skills were rated lower in t1, for example, the ability to solve conflicts and the ability to team working. 

It is possible that the experience did not worsen these skills but made students aware of their limitations and the need to improve in some aspects.

## Engagement

Regarding engagement during JLL, students reported elevated scores, both in single items and in total scores. The item rated lower is the first, namely "I feel alert"; this could also be since the sentence was read with a negative meaning (“in warning”). 

<img src="images_jll2/engagement.jpg" width="600">

## Students' expectations on JLL and feedback 

**online questionnaires**

As already mentioned, students' feedback on JLL was collected with online questionnaires and individual open interviews. The following tables report the students' expectation before and during the JLL 

<img src="images_jll2/EXPT_1.jpg" width="600">
<img src="images_jll2/EXPT_2.jpg" width="600">

These tables shows that most of the students' expectations were related to the social and educational importance of the experience. Many students reported the learning issues and the socio-cultural context in which this would take place as positive aspects. In general, the positive aspects were reported to a greater extent than the negative ones, almost all related to the fear of encountering difficulties. The students also expected to acquire practical robotics and programming skills and increase teamwork skills. The most common adjectives to describe the expected experience were "interesting/engaging" and "innovative/different."

<img src="images_jll2/EXPT_3.jpg" width="600">

Students' expectations were mostly met. They rated the experience helpful and challenging, highlighting the aspects of usefulness for both soft and hard skills, pleasantness of the experience, engagement, and social and cultural aspects. These results are confirmed in the individual interviews.

**Individual interviews**

The results of the interviews have been categorized in *expectation*, and are the result of a more in-depth investigation into what students expected from JLL and how it matched them or not. Other categories reported are the *difficulties encountered*, *the suggestions*, *the positive aspects*, the *soft and hard skills* increased thanks to the JLL experience. Finally, students were asked to suggest *modifications* to the AVATAR project and to give feedback about the *overall experience*. 

<img src="images_jll2/EXPT_4.jpg" width="600">

Many students reported that the expectations they had were confirmed and a general satisfaction was reported. Some students thought that during the JLL there would be some tasks useful for the group project; furthermore, students expected to do more practical things, despite realizing that time was short and the topics very difficult to learn in a short time. They reported that complex topics were explained in a simple and intuitive way.

<img src="images_jll2/DIFFICULTIES.jpg" width="600">

Students reported that the topics covered were difficult and that it was necessary to have a technical background to understand. Furthermore, it emerged that some students thought they were using a software they already knew and used in a group project, which instead did not happen. Difficulties related to personal characteristics were then encountered (for example: being shy).

<img src="images_jll2/suggestions.jpg" width="600">

Among the suggestions to improve the JLL experience, the students proposed having two moments of discussion between students to discuss the issues they are working on with each other. Furthermore, in this way, it would be possible to exchange ideas on the same theme to be addressed in a different way. They also suggest proposing tutorials/videos on the theoretical topics of JLL, thus to be able to practice more during the week.
Furthermore, the students suggest organizing the times in a different way in order to have fewer empty and boring moments.
Another suggestion was to have a task to solve during the week, composed of sub-tasks that are solved daily.

<img src="images_jll2/positive_asp.jpg" width="600">

The positive aspects of JLL that emerged from the interview were the practical side of the experience and the possibility of making experiments. The students really appreciated having received clear explanations of what was being taught. The social relationships established and the fact of having to speak English are another theme that emerged in many feedbacks.

<img src="images_jll2/int_soft_skills.jpg" width="600">
 
Regarding to soft skills, students reported increased collaboration, communication and teamwork skills. Other skills that emerged were those of leadership and general social skills, such as knowing how to behave in a group.

<img src="images_jll2/int_hard_skills.jpg" width="600">

With respect to hard skills, students reported that they have generally improved their understanding of VR and robotics topics, that they have had insights and that they have improved their knowledge of unity and ROS.

<img src="images_jll2/modifications.jpg" width="600">

Compared to the AVATAR project in general, the students suggested including more moments of discussion during the year between the participants. Some lessons were perceived as too long or uninteresting. There hasn't been a unanimous suggestion on the theme of the lectures. In fact, some students said that the lessons were few, while others too many; one suggestion was to propose less focused lessons, but an opposite idea was to do more in-dephts moments. One possibility reported by many students is to do technical lessons on what will be done in the JLL.
There was an initial feeling that they had to do too much by themselves, and the students suggested more help from the teachers.
Finally, many students have reported that they would appreciate more communication about the JLL program.

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/025-project-assesment/025-project-assesment.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">Project Assesment</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/025-project-assesment/0250_results/0250_jj3_results.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Result JLL 2023</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>
