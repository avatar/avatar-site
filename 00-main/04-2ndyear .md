<!-- ---
title: 2nd Year Student Work
layout: default
nav_order: 5
has_children: true
--- -->
# 1st year Student work
{: .fs-6 }
- - -

The project will involve 15 students a year from the three countries to collaboratively work on an industrial problem through international teams. They will take advantage of the developed workflow to address the problem to study and wil be able to test, validate and extend them, and deliver a workflow demonstrator, a documentation of the work done (video, tutorials, etc) and contribution to the guidelines for the adoption of the workflows in engineering curricula. The teacher team is involved to promote the adoption of these workflows within their own lectures or within their institution curricula.Strategies will be investigating to enlarge the pilot phase for the AVATAR workflows to new universities.

