---

title: Trainer Teacher Guidelines
layout: default
nav_order: 4
has_children: true

---

# Trainer Teacher Guidelines

The AVATAR project was funded by the ERASMUS+ project under the grant N°2020-1-FR01-KA203-080184. It aims to build a training program for European students about XR technologies and applications to industry 4.0 issues. 

This part refers to the Trainer guidelines. It reports to the organisation of a training period where students are learning XR basics and deploy them to industry 4.0 use case. The core use case leads to control a robot, an automatic machine or a complete production cell from XR devices.

The program was tested during 3 years with one spring semester session from 2020 to 2023. With students from 3 different universities, in France, Italy and Serbia, the good organisation of the semester is a mandatory aspect. We report the organisation process in [Chapter 1 - Organisation](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/020-organisation/020-organisation.html). But this organisation must serve a pedagogical method. The AVATAR consortium are basing the curriculum on the CDIO initiative [http://www.cdio.org/](http://www.cdio.org/). The implementation of this approach in AVATAR is reported in [Chapter 2 - Teaching and learning method](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/021-teaching-learning/021-teaching-learning-method.html). Whatever the method is, a semester session is based on a workflow. In [Chapter3 - Teaching workflows](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/022-teaching-workflows/022-teaching-workflow.html), the workflow for trainers is structured to ensure its repetitive execution. This process is improved every year.

An initial bias, is to let student learn by doing in collaborative mode. First because the partners are convinced of the educational value but also to take advantage of the European project to impulse cross-cultural exchange, network building and share a common European vision. The specificity of the remote collaboration and lectures organisation is described in [Chapter4 - Development of the Curricula](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/023-development-curricula/development-curricula.html). The high point of the AVATAR semester is the Joint learning lab session. This Joint Learning Lab is organised once a year to bring students together for a week in a common location. During this week they   share a common experience to build a XR environment to control a specific machine or production cell. The joint learning lab organisation is described in Chapter [Chapter 5 - Joint Learning Laboratory - JLL](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/024-joint-learning-lab/024-joint-learning-lab.html).

A training semester requires an assessment method. The validation of the knowledge acquired by each student is obviously a key point of the training period but AVATAR is also subject to a continuous improvement process. The corresponding assessments are presented in [Chapter 6 - Project assesment](https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/025-project-assesment/025-project-assesment.html)

These guidelines are reported what was enabled during the AVATAR project. There are many good points and a few ones which can be improved. It is not a definitive method but it is reported here to support new programs that could adapt it for new experiences.

---

<table style="width:100%;">
  <tr>
    <td style="text-align:right; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/01-workflows/015-xr-based-learning/L2-workflow_assembly.html" class="hover-move-left" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">&larr; Previous</div>
        <div style="color: blue;">L2 - Human-Robot Collaborative Assembling Processes</div>
      </a>
    </td>
    <td style="text-align:left; width: 50%;">
      <a href="https://avatar.gricad-pages.univ-grenoble-alpes.fr/avatar-site/02-guidelines/020-organisation/020-organisation.html" class="hover-move-right" style="display: block; height: 100%; text-decoration: none;">
        <div style="color: gray; font-size: smaller;">Next &rarr;</div>
        <div style="color: blue;">Organisation</div>
      </a>
    </td>
  </tr>
</table>

<style>
.hover-move-right:hover {
  transform: translateX(10px);
  transition: transform 0.5s ease-in-out;
}

.hover-move-left:hover {
  transform: translateX(-10px);
  transition: transform 0.5s ease-in-out;
}
</style>

---

[![License](https://img.shields.io/badge/License-CC%20BY--NC%204.0-lightgrey.svg)](LICENSE.md)